<?php


namespace App\Http\Controllers;


class WelcomeController extends Controller
{

    public function subscribe()
    {
        return response()->json(["valid" => 1, "message" => "Success! Please check your mail."]);
    }

}
