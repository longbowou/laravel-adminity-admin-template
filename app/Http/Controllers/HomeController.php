<?php


namespace App\Http\Controllers;


use Faker\Factory;

class HomeController extends Controller
{

    public function dtServerProcessing()
    {
        $faker = Factory::create();

        $data = [];
        for ($i = 0; $i < 20; $i++) {
            $item = [];
            $item['first_name'] = $faker->firstName;
            $item['last_name'] = $faker->lastName;
            $item['position'] = $faker->jobTitle;
            $item['office'] = $faker->company;
            $item['start_date'] = $faker->date();
            $item['salary'] = $faker->randomNumber();
            $data[] = $item;
        }

        return response()->json(["data" => $data]);
    }
}
