<?php

Route::view('/', "welcome")->name("welcome");

Route::post("subscribe", "WelcomeController@subscribe")->name("subscribe");

Route::prefix("adminity/")->group(function () {

    Route::view("login", "auth.login.login")->name("login");

    Route::view("login-header-footer", "auth.login.login-header-footer")->name("login.header.footer");

    Route::view("login-social", "auth.login.login-social")->name("login.social");

    Route::view("login-social-header-footer", "auth.login.login-social-header-footer")->name("login.social.header.footer");

    Route::view("register", "auth.register.register")->name("register");

    Route::view("register-header-footer", "auth.register.register-header-footer")->name("register.header.footer");

    Route::view("register-multi-step", "auth.register.register-multi-step")->name("register.multi.step");

    Route::view("register-social", "auth.register.register-social")->name("register.social");

    Route::view("register-social-header-footer", "auth.register.register-social-header-footer")->name("register.social.header.footer");

    Route::view("lock-screen", "auth.lock-screen")->name("lock.screen");

    Route::view("password/reset", "auth.password.reset")->name("password.request");

    Route::view("dashboard", "dashboards.dashboard")->name("dashboard");

    Route::view("dashboard-crm", "dashboards.dashboard-crm")->name("dashboard.crm");

    Route::view("dashboard-analytics", "dashboards.dashboard-analytics")->name("dashboard.analytics");

    Route::view("pages-layouts-vertical-static", "pages-layouts.vertical.vertical-static")->name("layouts.vertical.static");

    Route::view("pages-layouts-vertical-header-fixed", "pages-layouts.vertical.vertical-header-fixed")->name("layouts.vertical.header.fixed");

    Route::view("pages-layouts-vertical-compact", "pages-layouts.vertical.vertical-compact")->name("layouts.vertical.compact");

    Route::view("pages-layouts-vertical-sidebar-fixed", "pages-layouts.vertical.vertical-sidebar-fixed")->name("layouts.vertical.sidebar.fixed");

    Route::view("pages-layouts-horizontal-static", "pages-layouts.horizontal.horizontal-static")->name("layouts.horizontal.static");

    Route::view("pages-layouts-horizontal-fixed", "pages-layouts.horizontal.horizontal-fixed")->name("layouts.horizontal.fixed");

    Route::view("pages-layouts-horizontal-icon", "pages-layouts.horizontal.horizontal-icon")->name("layouts.horizontal.icon");

    Route::view("pages-layouts-horizontal-icon-fixed", "pages-layouts.horizontal.horizontal-icon-fixed")->name("layouts.horizontal.icon.fixed");

    Route::view("pages-layouts-bottom", "pages-layouts.bottom")->name("layouts.bottom");

    Route::view("pages-layouts-box", "pages-layouts.box")->name("layouts.box");

    Route::view("pages-layouts-rtl", "pages-layouts.rtl")->name("layouts.rtl");

    Route::view("nav-light", "nav-light")->name("nav.light");

    Route::view("widgets-statistic", "widgets.widget-statistic")->name("widgets.statistic");

    Route::view("widgets-data", "widgets.widget-data")->name("widgets.data");

    Route::view("widgets-chart", "widgets.widget-chart")->name("widgets.chart");

    Route::view("ui-elements-basic-components-alert", "ui-elements.basic-components.alert")->name("ui-elements.basic-components.alert");

    Route::view("ui-elements-basic-components-accordion", "ui-elements.basic-components.accordion")->name("ui-elements.basic-components.accordion");

    Route::view("ui-elements-basic-components-box-shadow", "ui-elements.basic-components.box-shadow")->name("ui-elements.basic-components.box-shadow");

    Route::view("ui-elements-basic-components-breadcrumb", "ui-elements.basic-components.breadcrumb")->name("ui-elements.basic-components.breadcrumb");

    Route::view("ui-elements-basic-components-button", "ui-elements.basic-components.button")->name("ui-elements.basic-components.button");

    Route::view("ui-elements-basic-components-color", "ui-elements.basic-components.color")->name("ui-elements.basic-components.color");

    Route::view("ui-elements-basic-components-generic-class", "ui-elements.basic-components.generic-class")->name("ui-elements.basic-components.generic-class");

    Route::view("ui-elements-basic-components-preloader", "ui-elements.basic-components.preloader")->name("ui-elements.basic-components.preloader");

    Route::view("ui-elements-basic-components-label-badge", "ui-elements.basic-components.label-badge")->name("ui-elements.basic-components.label-badge");

    Route::view("ui-elements-basic-components-list", "ui-elements.basic-components.list")->name("ui-elements.basic-components.list");

    Route::view("ui-elements-basic-components-other", "ui-elements.basic-components.other")->name("ui-elements.basic-components.other");

    Route::view("ui-elements-basic-components-progress-bar", "ui-elements.basic-components.progress-bar")->name("ui-elements.basic-components.progress-bar");

    Route::view("ui-elements-basic-components-tabs", "ui-elements.basic-components.tabs")->name("ui-elements.basic-components.tabs");

    Route::view("ui-elements-basic-components-tooltip", "ui-elements.basic-components.tooltip")->name("ui-elements.basic-components.tooltip");

    Route::view("ui-elements-basic-components-typography", "ui-elements.basic-components.typography")->name("ui-elements.basic-components.typography");

    Route::view("ui-elements-advanced-components-draggable", "ui-elements.advanced-components.draggable")->name("ui-elements.advanced-components.draggable");

    Route::view("ui-elements-advanced-components-bs-grid", "ui-elements.advanced-components.bs-grid")->name("ui-elements.advanced-components.bs-grid");

    Route::view("ui-elements-advanced-components-bs-grid", "ui-elements.advanced-components.bs-grid")->name("ui-elements.advanced-components.bs-grid");

    Route::view("ui-elements-advanced-components-light-box", "ui-elements.advanced-components.light-box")->name("ui-elements.advanced-components.light-box");

    Route::view("ui-elements-advanced-components-light-box", "ui-elements.advanced-components.light-box")->name("ui-elements.advanced-components.light-box");

    Route::view("ui-elements-advanced-components-modal", "ui-elements.advanced-components.modal")->name("ui-elements.advanced-components.modal");

    Route::view("ui-elements-advanced-components-nestable", "ui-elements.advanced-components.nestable")->name("ui-elements.advanced-components.nestable");

    Route::view("ui-elements-advanced-components-notification", "ui-elements.advanced-components.notification")->name("ui-elements.advanced-components.notification");

    Route::view("ui-elements-advanced-components-notify", "ui-elements.advanced-components.notify")->name("ui-elements.advanced-components.notify");

    Route::view("ui-elements-advanced-components-rating", "ui-elements.advanced-components.rating")->name("ui-elements.advanced-components.rating");

    Route::view("ui-elements-advanced-components-range-slider", "ui-elements.advanced-components.range-slider")->name("ui-elements.advanced-components.range-slider");

    Route::view("ui-elements-advanced-components-slider", "ui-elements.advanced-components.slider")->name("ui-elements.advanced-components.slider");

    Route::view("ui-elements-advanced-components-syntax-highlighter", "ui-elements.advanced-components.syntax-highlighter")->name("ui-elements.advanced-components.syntax-highlighter");

    Route::view("ui-elements-advanced-components-toolbar", "ui-elements.advanced-components.toolbar")->name("ui-elements.advanced-components.toolbar");

    Route::view("ui-elements-advanced-components-tour", "ui-elements.advanced-components.tour")->name("ui-elements.advanced-components.tour");

    Route::view("ui-elements-advanced-components-treeview", "ui-elements.advanced-components.treeview")->name("ui-elements.advanced-components.treeview");

    Route::view("ui-elements-advanced-components-x-editable", "ui-elements.advanced-components.x-editable")->name("ui-elements.advanced-components.x-editable");

    Route::view("ui-elements-extra-components-session-timeout", "ui-elements.extra-components.session-timeout")->name("ui-elements.extra-components.session-timeout");

    Route::view("ui-elements-extra-components-session-idle-timeout", "ui-elements.extra-components.session-idle-timeout")->name("ui-elements.extra-components.session-idle-timeout");

    Route::view("ui-elements-extra-components-offline", "ui-elements.extra-components.offline")->name("ui-elements.extra-components.offline");

    Route::view("ui-elements-extra-components-offline", "ui-elements.extra-components.offline")->name("ui-elements.extra-components.offline");

    Route::view("animation", "animation")->name("animation");

    Route::view("sticky", "sticky")->name("sticky");

    Route::view("icons-icon-flags", "icons.icon-flags")->name("icons.icon-flags");

    Route::view("icons-icon-font-awesome", "icons.icon-font-awesome")->name("icons.icon-font-awesome");

    Route::view("icons-icon-ion", "icons.icon-ion")->name("icons.icon-ion");

    Route::view("icons-icon-icofonts", "icons.icon-icofonts")->name("icons.icon-icofonts");

    Route::view("icons-icon-material-design", "icons.icon-material-design")->name("icons.icon-material-design");

    Route::view("icons-icon-simple-line", "icons.icon-simple-line")->name("icons.icon-simple-line");

    Route::view("icons-icon-themify", "icons.icon-themify")->name("icons.icon-themify");

    Route::view("icons-icon-typicons", "icons.icon-typicons")->name("icons.icon-typicons");

    Route::view("icons-icon-weather", "icons.icon-weather")->name("icons.icon-weather");

    Route::view("forms-form-components-form-elements-add-on", "forms.form-components.form-elements-add-on")->name("forms.form-components.form-elements-add-on");

    Route::view("forms-form-components-form-elements-advance", "forms.form-components.form-elements-advance")->name("forms.form-components.form-elements-advance");

    Route::view("forms-form-components-form-elements-component", "forms.form-components.form-elements-component")->name("forms.form-components.form-elements-component");

    Route::view("forms-form-components-form-validation", "forms.form-components.form-validation")->name("forms.form-components.form-validation");

    Route::view("forms-form-masking", "forms.form-masking")->name("forms.form-masking");

    Route::view("forms-form-picker", "forms.form-picker")->name("forms.form-picker");

    Route::view("forms-form-select", "forms.form-select")->name("forms.form-select");

    Route::view("forms-form-wizard", "forms.form-wizard")->name("forms.form-wizard");

    Route::view("forms-ready-to-use-ready-cloned-elements-form", "forms.ready-to-use.ready-cloned-elements-form")->name("forms.ready-to-use.ready-cloned-elements-form");

    Route::view("forms-ready-to-use-ready-currency-form", "forms.ready-to-use.ready-currency-form")->name("forms.ready-to-use.ready-currency-form");

    Route::view("forms-ready-to-use-ready-form-booking", "forms.ready-to-use.ready-form-booking")->name("forms.ready-to-use.ready-form-booking");

    Route::view("forms-ready-to-use-ready-form-booking-multi-steps", "forms.ready-to-use.ready-form-booking-multi-steps")->name("forms.ready-to-use.ready-form-booking-multi-steps");

    Route::view("forms-ready-to-use-ready-form-comment", "forms.ready-to-use.ready-form-comment")->name("forms.ready-to-use.ready-form-comment");

    Route::view("forms-ready-to-use-ready-form-contact", "forms.ready-to-use.ready-form-contact")->name("forms.ready-to-use.ready-form-contact");

    Route::view("forms-ready-to-use-ready-job-application-form", "forms.ready-to-use.ready-job-application-form")->name("forms.ready-to-use.ready-job-application-form");

    Route::view("forms-ready-to-use-ready-js-addition-form", "forms.ready-to-use.ready-js-addition-form")->name("forms.ready-to-use.ready-js-addition-form");

    Route::view("forms-ready-to-use-ready-login-form", "forms.ready-to-use.ready-login-form")->name("forms.ready-to-use.ready-login-form");

    Route::view("forms-ready-to-use-ready-popup-modal-form", "forms.ready-to-use.ready-popup-modal-form")->name("forms.ready-to-use.ready-popup-modal-form");

    Route::view("forms-ready-to-use-ready-registration-form", "forms.ready-to-use.ready-registration-form")->name("forms.ready-to-use.ready-registration-form");

    Route::view("forms-ready-to-use-ready-review-form", "forms.ready-to-use.ready-review-form")->name("forms.ready-to-use.ready-review-form");

    Route::view("forms-ready-to-use-ready-subscribe-form", "forms.ready-to-use.ready-subscribe-form")->name("forms.ready-to-use.ready-subscribe-form");

    Route::view("forms-ready-to-use-ready-suggestion-form", "forms.ready-to-use.ready-suggestion-form")->name("forms.ready-to-use.ready-suggestion-form");

    Route::view("forms-ready-to-use-ready-tabs-form", "forms.ready-to-use.ready-tabs-form")->name("forms.ready-to-use.ready-tabs-form");

    Route::view("tables-bootstrap-tables-bs-basic", "tables.bootstrap-tables.bs-basic")->name("tables.bootstrap-tables.bs-basic");

    Route::view("tables-bootstrap-tables-bs-table-border", "tables.bootstrap-tables.bs-table-border")->name("tables.bootstrap-tables.bs-table-border");

    Route::view("tables-bootstrap-tables-bs-table-sizing", "tables.bootstrap-tables.bs-table-sizing")->name("tables.bootstrap-tables.bs-table-sizing");

    Route::view("tables-bootstrap-tables-bs-table-styling", "tables.bootstrap-tables.bs-table-styling")->name("tables.bootstrap-tables.bs-table-styling");

    Route::view("tables-data-table-extensions-dt-ext-autofill", "tables.data-table-extensions.dt-ext-autofill")->name("tables.data-table-extensions.dt-ext-autofill");

    Route::view("tables-data-table-extensions-dt-ext-basic-buttons", "tables.data-table-extensions.dt-ext-basic-buttons")->name("tables.data-table-extensions.dt-ext-basic-buttons");

    Route::view("tables-data-table-extensions-dt-ext-buttons-html-5-data-export", "tables.data-table-extensions.dt-ext-buttons-html-5-data-export")->name("tables.data-table-extensions.dt-ext-buttons-html-5-data-export");

    Route::view("tables-data-table-extensions-dt-ext-col-reorder", "tables.data-table-extensions.dt-ext-col-reorder")->name("tables.data-table-extensions.dt-ext-col-reorder");

    Route::view("tables-data-table-extensions-dt-ext-fixed-columns", "tables.data-table-extensions.dt-ext-fixed-columns")->name("tables.data-table-extensions.dt-ext-fixed-columns");

    Route::view("tables-data-table-extensions-dt-ext-fixed-header", "tables.data-table-extensions.dt-ext-fixed-header")->name("tables.data-table-extensions.dt-ext-fixed-header");

    Route::view("tables-data-table-extensions-dt-ext-key-table", "tables.data-table-extensions.dt-ext-key-table")->name("tables.data-table-extensions.dt-ext-key-table");

    Route::view("tables-data-table-extensions-dt-ext-responsive", "tables.data-table-extensions.dt-ext-responsive")->name("tables.data-table-extensions.dt-ext-responsive");

    Route::view("tables-data-table-extensions-dt-ext-row-reorder", "tables.data-table-extensions.dt-ext-row-reorder")->name("tables.data-table-extensions.dt-ext-row-reorder");

    Route::view("tables-data-table-extensions-dt-ext-scroller", "tables.data-table-extensions.dt-ext-scroller")->name("tables.data-table-extensions.dt-ext-scroller");

    Route::view("tables-data-table-extensions-dt-ext-select", "tables.data-table-extensions.dt-ext-select")->name("tables.data-table-extensions.dt-ext-select");

    Route::view("tables-data-tables-bs-basic", "tables.data-tables.bs-basic")->name("tables.data-tables.bs-basic");

    Route::view("tables-data-tables-dt-advance", "tables.data-tables.dt-advance")->name("tables.data-tables.dt-advance");

    Route::view("tables-data-tables-dt-ajax", "tables.data-tables.dt-ajax")->name("tables.data-tables.dt-ajax");

    Route::view("tables-data-tables-dt-api", "tables.data-tables.dt-api")->name("tables.data-tables.dt-api");

    Route::view("tables-data-tables-dt-data-sources", "tables.data-tables.dt-data-sources")->name("tables.data-tables.dt-data-sources");

    Route::view("tables-data-tables-dt-plugin", "tables.data-tables.dt-plugin")->name("tables.data-tables.dt-plugin");

    Route::view("tables-data-tables.dt-server-side", "tables.data-tables.dt-server-side")->name("tables.data-tables.dt-server-side");

    Route::view("tables-data-tables-dt-styling", "tables.data-tables.dt-styling")->name("tables.data-tables.dt-styling");

    Route::view("tables-editable-table", "tables.editable-table")->name("tables.editable-table");

    Route::view("tables-foo-table", "tables.foo-table")->name("tables.foo-table");

    Route::view("tables-handson-tables-handson-appearance", "tables.handson-tables.handson-appearance")->name("tables.handson-tables.handson-appearance");

    Route::view("tables-handson-tables-handson-cell-features", "tables.handson-tables.handson-cell-features")->name("tables.handson-tables.handson-cell-features");

    Route::view("tables-handson-tables-handson-cell-types", "tables.handson-tables.handson-cell-types")->name("tables.handson-tables.handson-cell-types");

    Route::view("tables-handson-tables-handson-columns-only", "tables.handson-tables.handson-columns-only")->name("tables.handson-tables.handson-columns-only");

    Route::view("tables-handson-tables-handson-data-operation", "tables.handson-tables.handson-data-operation")->name("tables.handson-tables.handson-data-operation");

    Route::view("tables-handson-tables-handson-integrations", "tables.handson-tables.handson-integrations")->name("tables.handson-tables.handson-integrations");

    Route::view("tables-handson-tables-handson-rows-cols", "tables.handson-tables.handson-rows-cols")->name("tables.handson-tables.handson-rows-cols");

    Route::view("tables-handson-tables-handson-rows-only", "tables.handson-tables.handson-rows-only")->name("tables.handson-tables.handson-rows-only");

    Route::view("tables-handson-tables-handson-utilities", "tables.handson-tables.handson-utilities")->name("tables.handson-tables.handson-utilities");

    Route::view("charts-chart-c3", "charts.chart-c3")->name("charts.chart-c3");

    Route::view("charts-chart-chartjs", "charts.chart-chartjs")->name("charts.chart-chartjs");

    Route::view("charts-chart-echart", "charts.chart-echart")->name("charts.chart-echart");

    Route::view("charts-chart-float", "charts.chart-float")->name("charts.chart-float");

    Route::view("charts-chart-google", "charts.chart-google")->name("charts.chart-google");

    Route::view("charts-chart-chartjs", "charts.chart-chartjs")->name("charts.chart-chartjs");

    Route::view("charts-chart-knob", "charts.chart-knob")->name("charts.chart-knob");

    Route::view("charts-chart-list", "charts.chart-list")->name("charts.chart-list");

    Route::view("charts-chart-morris", "charts.chart-morris")->name("charts.chart-morris");

    Route::view("charts-chart-nvd3", "charts.chart-nvd3")->name("charts.chart-nvd3");

    Route::view("charts-chart-peity", "charts.chart-peity")->name("charts.chart-peity");

    Route::view("charts-chart-radial", "charts.chart-radial")->name("charts.chart-radial");

    Route::view("charts-chart-rickshaw", "charts.chart-rickshaw")->name("charts.chart-rickshaw");

    Route::view("charts-chart-sparkline", "charts.chart-sparkline")->name("charts.chart-sparkline");

    Route::view("maps-location", "maps.location")->name("maps.location");

    Route::view("maps-map-google", "maps.map-google")->name("maps.map-google");

    Route::view("maps-map-api", "maps.map-api")->name("maps.map-api");

    Route::view("maps-map-vector", "maps.map-vector")->name("maps.map-vector");

    Route::view("chat", "chat")->name("chat");

    Route::view("social-fb-wall", "social.fb-wall")->name("social.fb-wall");

    Route::view("social-message", "social.message")->name("social.message");

    Route::view("tasks-issue-list", "tasks.issue-list")->name("tasks.issue-list");

    Route::view("tasks-task-board", "tasks.task-board")->name("tasks.task-board");

    Route::view("tasks-task-detail", "tasks.task-detail")->name("tasks.task-detail");

    Route::view("tasks-task-list", "tasks.task-list")->name("tasks.task-list");

    Route::view("to-do-notes", "to-do.notes")->name("to-do.notes");

    Route::view("to-do-todo", "to-do.todo")->name("to-do.todo");

    Route::view("gallery-gallery-advance", "gallery.gallery-advance")->name("gallery.gallery-advance");

    Route::view("gallery-gallery-grid", "gallery.gallery-grid")->name("gallery.gallery-grid");

    Route::view("gallery-gallery-advance", "gallery.gallery-advance")->name("gallery.gallery-advance");

    Route::view("gallery-gallery-masonry", "gallery.gallery-masonry")->name("gallery.gallery-masonry");

    Route::view("search-search-result", "search.search-result")->name("search.search-result");

    Route::view("search-search-result2", "search.search-result2")->name("search.search-result2");

    Route::view("job-search-job-card-view", "job-search.job-card-view")->name("job-search.job-card-view");

    Route::view("job-search-job-find", "job-search.job-find")->name("job-search.job-find");

    Route::view("job-search-job-details", "job-search.job-details")->name("job-search.job-details");

    Route::view("job-search-job-panel-view", "job-search.job-panel-view")->name("job-search.job-panel-view");

    Route::view("editor-ace-editor", "editor.ace-editor")->name("editor.ace-editor");

    Route::view("editor-ck-editor", "editor.ck-editor")->name("editor.ck-editor");

    Route::view("editor-long-press-editor", "editor.long-press-editor")->name("editor.long-press-editor");

    Route::view("editor-wysiwyg-editor", "editor.wysiwyg-editor")->name("editor.wysiwyg-editor");

    Route::view("invoice-invoice", "invoice.invoice")->name("invoice.invoice");

    Route::view("invoice-invoice-list", "invoice.invoice-list")->name("invoice.invoice-list");

    Route::view("invoice-invoice-summary", "invoice.invoice-summary")->name("invoice.invoice-summary");

    Route::view("event-calendar.event-clndr", "event-calendar.event-clndr")->name("event-calendar.event-clndr");

    Route::view("event-calendar.event-full-calendar", "event-calendar.event-full-calendar")->name("event-calendar.event-full-calendar");

    Route::view("file-upload", "file-upload")->name("file-upload");

    Route::view("image-crop", "image-crop")->name("image-crop");

    Route::view("change-loges", "change-loges")->name("change-loges");

    Route::view("sample-page", "sample-page")->name("sample-page");

    Route::view("maintenance-comming-soon", "maintenance.comming-soon")->name("maintenance.comming-soon");

    Route::view("maintenance-error", "maintenance.error")->name("maintenance.error");

    Route::view("maintenance-offline-ui", "maintenance.offline-ui")->name("maintenance.offline-ui");

    Route::view("user-profiles-timeline", "user-profiles.timeline")->name("user-profiles.timeline");

    Route::view("user-profiles-timeline-social", "user-profiles.timeline-social")->name("user-profiles.timeline-social");

    Route::view("user-profiles-user-card", "user-profiles.user-card")->name("user-profiles.user-card");

    Route::view("user-profiles-user-profile", "user-profiles.user-profile")->name("user-profiles.user-profile");

    Route::view("e-commerce-product", "e-commerce.product")->name("e-commerce.product");

    Route::view("e-commerce-product-cart", "e-commerce.product-cart")->name("e-commerce.product-cart");

    Route::view("e-commerce-product-detail", "e-commerce.product-detail")->name("e-commerce.product-detail");

    Route::view("e-commerce-product-edit", "e-commerce.product-edit")->name("e-commerce.product-edit");

    Route::view("e-commerce-product-list", "e-commerce.product-list")->name("e-commerce.product-list");

    Route::view("e-commerce-product-payment", "e-commerce.product-payment")->name("e-commerce.product-payment");

    Route::view("email-email-inbox", "email.email-inbox")->name("email.email-inbox");

    Route::view("email-email-compose", "email.email-compose")->name("email.email-compose");

    Route::view("email-email-read", "email.email-read")->name("email.email-read");

    Route::any("server-processing", "HomeController@dtServerProcessing");
});

