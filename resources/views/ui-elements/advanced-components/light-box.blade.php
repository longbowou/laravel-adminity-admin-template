@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/css/ekko-lightbox.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/lightbox2/css/lightbox.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Light Box</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Light Box</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Single Images</h5>
                    </div>
                    <div class="card-block">
                        <p> Use this code <code>data-toggle="lightbox"</code> to see image in lightbox popup. </p>
                        <div class="row">
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l1.jpg") }}" data-toggle="lightbox"
                                   data-title="A random title" data-footer="A custom footer text">
                                    <img src="{{ asset("adminity/images/light-box/sl1.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l2.jpg") }}" data-toggle="lightbox"
                                   data-title="A random title" data-footer="A custom footer text">
                                    <img src="{{ asset("adminity/images/light-box/sl2.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l3.jpg") }}" data-toggle="lightbox"
                                   data-title="A random title" data-footer="A custom footer text">
                                    <img src="{{ asset("adminity/images/light-box/sl3.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l4.jpg") }}" data-toggle="lightbox"
                                   data-title="A random title" data-footer="A custom footer text">
                                    <img src="{{ asset("adminity/images/light-box/sl4.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l5.jpg") }}" data-toggle="lightbox"
                                   data-title="A random title" data-footer="A custom footer text">
                                    <img src="{{ asset("adminity/images/light-box/sl5.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l6.jpg") }}" data-toggle="lightbox"
                                   data-title="A random title" data-footer="A custom footer text">
                                    <img src="{{ asset("adminity/images/light-box/sl6.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Image Gallery</h5>
                    </div>
                    <div class="card-block">
                        <p> Add this code <code>data-gallery="example-gallery"</code> to see image gallery in lightbox
                            popup. </p>
                        <div class="row">
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l1.jpg") }}" data-toggle="lightbox"
                                   data-gallery="example-gallery">
                                    <img src="{{ asset("adminity/images/light-box/sl1.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l2.jpg") }}" data-toggle="lightbox"
                                   data-gallery="example-gallery">
                                    <img src="{{ asset("adminity/images/light-box/sl2.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l3.jpg") }}" data-toggle="lightbox"
                                   data-gallery="example-gallery">
                                    <img src="{{ asset("adminity/images/light-box/sl3.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l4.jpg") }}" data-toggle="lightbox"
                                   data-gallery="example-gallery">
                                    <img src="{{ asset("adminity/images/light-box/sl4.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l5.jpg") }}" data-toggle="lightbox"
                                   data-gallery="example-gallery">
                                    <img src="{{ asset("adminity/images/light-box/sl5.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l6.jpg") }}" data-toggle="lightbox"
                                   data-gallery="example-gallery">
                                    <img src="{{ asset("adminity/images/light-box/sl6.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Video Gallery</h5>
                    </div>
                    <div class="card-block">
                        <p> Add this code <code>data-gallery="youtubevideos"</code> to see videos in lightbox popup.
                        </p>
                        <div class="row">
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="https://www.youtube.com/watch?v=k6mFF3VmVAs" data-toggle="lightbox"
                                   data-gallery="youtubevideos">
                                    <img src="{{ asset("adminity/images/light-box/v1.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="https://youtu.be/iQ4D273C7Ac" data-toggle="lightbox"
                                   data-gallery="youtubevideos">
                                    <img src="{{ asset("adminity/images/light-box/v2.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="http://www.youtube.com/embed/b0jqPvpn3sY" data-toggle="lightbox"
                                   data-gallery="youtubevideos">
                                    <img src="{{ asset("adminity/images/light-box/v3.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Mixin Gallery</h5>
                    </div>
                    <div class="card-block">
                        <p> Add this code <code>data-gallery="mixedgallery"</code> to see mixed gallery in lightbox
                            popup. to use vimeo add code <code>data-remote="vimeo url"</code></p>
                        <div class="row">
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="https://www.youtube.com/watch?v=k6mFF3VmVAs" data-toggle="lightbox"
                                   data-gallery="mixedgallery">
                                    <img src="{{ asset("adminity/images/light-box/v1.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/mixin.jpg") }}" data-toggle="lightbox"
                                   data-gallery="mixedgallery">
                                    <img src="{{ asset("adminity/images/light-box/smixin.jpg") }}"
                                         class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="https://vimeo.com/80629469"
                                   data-remote="http://player.vimeo.com/video/80629469" data-toggle="lightbox"
                                   data-gallery="mixedgallery">
                                    <img src="{{ asset("adminity/images/light-box/v4.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Single Images(light box 2)</h5>
                    </div>
                    <div class="card-block">
                        <p> Add this code <code>data-lightbox="roadtrip"</code> to see roadtrip gallery in lightbox
                            popup.</p>
                        <div class="row">
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l3.jpg") }}" data-lightbox="roadtrip">
                                    <img src="{{ asset("adminity/images/light-box/sl3.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l4.jpg") }}" data-lightbox="roadtrip"
                                   data-title="Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing.">
                                    <img src="{{ asset("adminity/images/light-box/sl4.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Multiple Images Or Image Set (Light Box 2)</h5>
                    </div>
                    <div class="card-block">
                        <p> Add this code <code>data-lightbox="example-set"</code> to see roadtrip gallery in lightbox
                            popup.</p>
                        <div class="row">
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l1.jpg") }}" data-lightbox="example-set">
                                    <img src="{{ asset("adminity/images/light-box/sl1.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l2.jpg") }}" data-lightbox="example-set"
                                   data-title="Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing.">
                                    <img src="{{ asset("adminity/images/light-box/sl2.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l5.jpg") }}" data-lightbox="example-set"
                                   data-title="Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing.">
                                    <img src="{{ asset("adminity/images/light-box/sl5.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-3 col-xs-12">
                                <a href="{{ asset("adminity/images/light-box/l6.jpg") }}" data-lightbox="example-set"
                                   data-title="Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing.">
                                    <img src="{{ asset("adminity/images/light-box/sl6.jpg") }}" class="img-fluid m-b-10"
                                         alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/js/ekko-lightbox.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/lightbox2/js/lightbox.min.js") }}"></script>
@endsection


