@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Draggable</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Draggable</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Draggable Default</h5>
                    </div>
                    <div class="card-block">
                        <div class="row" id="draggablePanelList">
                            <div class="col-lg-12 col-xl-3">
                                <div class="card-sub">
                                    <img class="card-img-top img-fluid"
                                         src="{{ asset("adminity/images/card-block/card1.jpg") }}" alt="Card image cap">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-3">
                                <div class="card-sub">
                                    <img class="card-img-top img-fluid"
                                         src="{{ asset("adminity/images/card-block/card1.jpg") }}" alt="Card image cap">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title 1</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-3">
                                <div class="card-sub">
                                    <img class="card-img-top img-fluid"
                                         src="{{ asset("adminity/images/card-block/card13.jpg") }}"
                                         alt="Card image cap">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title 2</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-3">
                                <div class="card-sub">
                                    <img class="card-img-top img-fluid"
                                         src="{{ asset("adminity/images/card-block/card2.jpg") }}" alt="Card image cap">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title 3</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Draggable Multiple List</h5>
                    </div>
                    <div class="card-block p-b-0">
                        <div class="row">
                            <div class="col-md-12" id="draggableMultiple">
                                <div class="sortable-moves">
                                    <img class="img-fluid p-absolute" src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                         alt="">
                                    <h6>Multiple list 1</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. when
                                        an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book.</p>
                                </div>
                                <div class="sortable-moves">
                                    <img class="img-fluid p-absolute" src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                         alt="">
                                    <h6>Multiple list 1</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. when
                                        an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book.</p>
                                </div>
                                <div class="sortable-moves">
                                    <img class="img-fluid p-absolute" src="{{ asset("adminity/images/avatar-3.jpg") }}"
                                         alt="">
                                    <h6>Multiple list 2</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. when
                                        an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book.</p>
                                </div>
                                <div class="sortable-moves">
                                    <img class="img-fluid p-absolute" src="{{ asset("adminity/images/avatar-4.jpg") }}"
                                         alt="">
                                    <h6>Multiple list 3</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. when
                                        an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book.</p>
                                </div>
                                <div class="sortable-moves">
                                    <img class="img-fluid p-absolute" src="{{ asset("adminity/images/avatar-5.jpg") }}"
                                         alt="">
                                    <h6>Multiple list 4</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. when
                                        an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book.</p>
                                </div>
                                <div class="sortable-moves">
                                    <img class="img-fluid p-absolute" src="{{ asset("adminity/images/avatar-6.jpg") }}"
                                         alt="">
                                    <h6>Multiple list 5</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. when
                                        an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Draggable Without Images</h5>
                    </div>
                    <div class="card-block">
                        <div class="row" id="draggableWithoutImg">
                            <div class="col-md-6 m-b-20">
                                <div class="card-sub">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 m-b-20">
                                <div class="card-sub">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title 1</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card-sub m-b-20">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title 2</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card-sub m-b-20">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title 3</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card-sub">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title 4</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card-sub m-t-20">
                                    <div class="card-block">
                                        <h4 class="card-title">Card title 5</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                            up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/Sortable/js/Sortable.js") }}"></script>
    <script src="{{ asset("adminity/pages/sortable-custom.js") }}" type="text/javascript"></script>
@endsection

