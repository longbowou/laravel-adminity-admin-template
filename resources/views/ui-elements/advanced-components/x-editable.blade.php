@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/x-editable/css/tm_editable.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/x-editable/css/tm_validator.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/x-editable/css/dotted-line-theme.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>X-Editable</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">X-Editable</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Inline Editable</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block inline-editable">
                        <div class="form-group">
                            <label>Input Text</label>
                            <div class="tm_editable_container input-group theme1" id="text_demo"
                                 data-iplaceholder="Type Something..">
                                <input type="text" value="This Is Input Text" class="w-inherit form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Text Area</label>
                            <div class="tm_editable_container input-group" id="textarea_demo"
                                 data-iplaceholder="Type Something..">
                                <textarea class="max-textarea form-control" maxlength="150"
                                          rows="2">Detail Text Here</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Select Option</label>
                            <div class="tm_editable_container input-group" id="radio_demo">
<span class="radio_container full_row">
<span class="radio radiofill single_radio full_row">
<label>
<input type="radio" name="radio" value="Gmail" id="r_demo1"/><i class="helper"></i>
<span class="p-l-20">Gmail</span>
</label>
</span>
<span class="radio radiofill single_radio full_row">
<label>
<input type="radio" name="radio" value="Yahoo" id="r_demo2"/><i class="helper"></i>
<span class="p-l-20">Yahoo</span>
</label>
</span>
<span class="radio radiofill single_radio full_row">
<label>
<input type="radio" name="radio" value="Facebook" id="r_demo3"/><i class="helper"></i>
<span class="p-l-20">Facebook</span>
</label>
</span>
</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="tm_editable_container input-group" id="checkbox_demo">
<span class="single_checkbox checkbox-color checkbox-primary">
<input type="checkbox" class="border-checkbox" name="foo" value="bar2" id="have_gmail">
<label for="have_gmail">Do you have gmail</label>
</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Select Option</label>
                            <div class="tm_editable_container input-group" id="select_demo">
                                <select class="my_select form-control">
                                    <option value="0" selected="selected"> Option0</option>
                                    <option value="1"> Option1</option>
                                    <option value="2"> Option2</option>
                                    <option value="3"> Option3</option>
                                    <option value="4"> Option4</option>
                                    <option value="5"> Option5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/x-editable/js/tm_editable.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/x-editable/js/tm_validator.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/x-editable/js/xeditable.js") }}"></script>
@endsection


