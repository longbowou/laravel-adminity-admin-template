@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/prism/prism.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Syntax Highlighter</h4>
                        <span>Prism syntax highlighter</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Syntax Highlighter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Basic Usage</h5>
                    </div>
                    <div class="card-block">
                        <p>The recommended way to mark up a code block(both for semantics
                            and for Prism) is a <code>&lt;pre&gt;</code> element with a
                            element <code>&lt;code&gt;</code> inside, like so:</p>
                        <h6 class="m-t-20 f-w-600">Code:</h6>
                        <pre><code class="language-markup">
&lt;pre&gt;&lt;code class="language-css"&gt;

    p {
        color: #1abc9c
      }

&lt;/code&gt;
&lt;/pre&gt;
                                        </code></pre>
                        <h6 class="m-t-20 f-w-600">Output:</h6>
                        <pre><code class="language-markup">
p {
    color: #1abc9c
  }
                                        </code></pre>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>HTML Markup</h5>
                    </div>
                    <div class="card-block">
                        <p>Use the following code to use HTML syntax highlighter.</p>
                        <h6 class="m-t-20 f-w-600">Usage:</h6>
                        <pre><code class="language-markup">&lt;pre&gt;

&lt;code class="language-markup"&gt;
    HTML CODE ...
&lt;/code&gt;

&lt;/pre&gt;
</code></pre>
                        <h6 class="m-t-20 f-w-600">Example:</h6>
                        <pre><code class="language-markup">&lt;div class="card"&gt;
    &lt;div class="card-header"&gt;
        &lt;h5&gt;Hello card&lt;/h5&gt;
        &lt;span&gt; lorem ipsum dolor sit amet, consectetur adipisicing elit&lt;/span&gt;
        &lt;div class="card-header-right"&gt;
            &lt;i class="icofont icofont-rounded-down"&gt;&lt;/i&gt;
            &lt;i class="icofont icofont-refresh"&gt;&lt;/i&gt;
            &lt;i class="icofont icofont-close-circled"&gt;&lt;/i&gt;
        &lt;/div&gt;
    &lt;/div&gt;

    &lt;div class="card-block"&gt;
        &lt;p&gt;
             "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        &lt;/p&gt;
    &lt;/div&gt;
&lt;/div&gt;


</code> </pre>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>CSS Markup</h5>
                    </div>
                    <div class="card-block">
                        <p>Use the following code to use HTML syntax highlighter.</p>
                        <h6 class="m-t-20 f-w-600">Usage:</h6>
                        <pre><code class="language-markup">&lt;pre&gt;
&lt;code class="language-css"&gt;
    CSS CODE ...
&lt;/code&gt;
&lt;/pre&gt;
</code></pre>
                        <h6 class="m-t-20 f-w-600">Example:</h6>
                        <pre><code class="language-css">a:active{
    color:#1abc9c;
}

p{
    font-size:13px;
}

.btn-primary{
    color: #1abc9c;
    background-color: #fff;
}

.label-primary {
    background-color: #1abc9c;
}

.badge-primary {
    background-color: #1abc9c;
}

.bg-primary {
    background-color: #1abc9c !important;
    color: #fff;
}

.panel-primary {
    border-color: #1abc9c;
}
</code></pre>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Line Number</h5>
                    </div>
                    <div class="card-block">
                        <p>Line number at the beginning of code lines.</p>
                        <p>Obviously, this is supposed to work only for code blocks (<code>&lt;pre&gt;
                                &lt;code&gt;
                            </code>) and not for inline code. Add class line-numbers to your
                            desired <code>&lt;pre&gt;
                            </code> and line-numbers plugin will take care.</p>
                        <p>Optional: You can specify the data-start (Number) attribute on
                            the <code>&lt;pre&gt;
                            </code> element. It will shift the line counter.</p>
                        <h6 class="m-t-20 f-w-600">Usage:</h6>
                        <pre class="line-numbers language-js"><code class=" language-js">&lt;pre class="line-numbers"&gt;
&lt;code class="language-css"&gt;
p {
    color: red
}

&lt;/code&gt;
&lt;/pre&gt;
</code> </pre>
                        <h6 class="m-t-20 f-w-600">Example:</h6>
                        <pre class="line-numbers"><code class="language-js">(function() {
    if (typeof self==='undefined' || !self.Prism || !self.document) {
        return;
    }
}

());
</code> </pre>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Line Highlight</h5>
                    </div>
                    <div class="card-block">
                        <p>Highlights specific lines and/or line ranges.</p>
                        <p>
                            You specify the lines to be highlighted through the data-line
                            attribute on the <code>&lt;pre&gt;</code> element, in the
                            following simple format:
                        </p>
                        <ol>
                            <li>A single number refers to the line with that number</li>
                            <li>Ranges are denoted by two numbers, separated with a hyphen
                                (-)
                            </li>
                            <li>Multiple line numbers or ranges are separated by commas.
                            </li>
                            <li>Whitespace is allowed anywhere and will be stripped off.
                            </li>
                        </ol>
                        <h6 class="m-t-20 f-w-600">Usage:</h6>
                        <pre><code class="language-markup">&lt;pre data-line="2, 4, 8-10"&gt;
&lt;code class="language-css"&gt;
p {
    color: red
}

&lt;/code&gt;
&lt;/pre&gt;
</code> </pre>
                        <h6 class="m-t-20 f-w-600">Example:</h6>
                        <pre data-line="2,4,7-9"><code class="language-css">pre.line-numbers {
    position: relative;
    padding-left: 3.8em;
    counter-reset: linenumber;
}

pre.line-numbers > code {
    position: relative;
}

.line-numbers .line-numbers-rows {
    position: absolute;
    pointer-events: none;
    top: 0;
    font-size: 100%;
    left: -3.8em;
    width: 3em;
    /* works for line-numbers below 1000 lines */
    letter-spacing: -1px;
    border-right: 1px solid #999;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}</code></pre>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Auto Linker</h5>
                    </div>
                    <div class="card-block">
                        <p>Converts URLs and emails in code to clickable links. <a
                                href="#!">Parses Markdown</a> links in comments.</p>
                        <p>
                            URLs and emails will be linked automatically, you don’t need to
                            do anything. To link some text inside a comment to a certain
                            URL, you may use the Markdown syntax:
                        </p>
                        <h6 class="m-t-20 f-w-600">Usage:</h6>
                        <pre><code class="language-markup">&lt;pre&gt;
&lt;code class="language-css"&gt;
@font-face {
    src: url(http://lea.verou.me/logo.otf);
}

&lt;/code&gt;
&lt;/pre&gt;
</code> </pre>
                        <h6 class="m-t-20 f-w-600">Example:</h6>
                        <pre><code class="language-js">@font-face {
    src: url(http://lea.verou.me/logo.otf);
    font-family: 'LeaVerou';
}
                                </code></pre>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Show Invisibles</h5>
                    </div>
                    <div class="card-block">
                        <p>Show hidden characters such as tabs and line breaks.</p>
                        <h6 class="m-t-20 f-w-600">Usage:</h6>
                        <h6 class="m-t-20 f-w-600">Example:</h6>
                        <pre> <code class="language-js"> (function() {
    if ( typeof self !=='undefined' && !self.Prism || typeof global !=='undefined' && !global.Prism) {
        return;
    }
    Prism.hooks.add('before-highlight', function(env) {
        var tokens=env.grammar;
        tokens.tab=/\t/g;
        tokens.crlf=/\r\n/g;
        tokens.lf=/\n/g;
        tokens.cr=/\r/g;
        tokens.space=/ /g;
    }
    );
}

)();
</code> </pre>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>File Highlight</h5>
                    </div>
                    <div class="card-block">
                        <p>Fetch external files and highlight them with Prism. Used on the
                            Prism website itself.</p>
                        <p>Use the data-src attribute on empty <code>&lt;pre&gt;</code>
                            elements, like so:</p>
                        <h6 class="m-t-20 f-w-600">Usage:</h6>
                        <pre> <code class="language-markup"> &lt;pre data-src="bootstrap-growl.min.js"&gt;
&lt;/pre&gt;
</code> </pre>
                        <p>You don’t need to specify the language, it’s automatically
                            determined by the file extension. If, however, the language
                            cannot be determined from the file extension or the file
                            extension is incorrect, you may specify a language as well (with
                            the usual class name way).</p>
                        <p>Please note that the files are fetched with XMLHttpRequest. This
                            means that if the file is on a different origin, fetching it
                            will fail, unless CORS is enabled on that website.</p>
                        <h6 class="m-t-20 f-w-600">Example:</h6>
                        <pre data-src="{{ asset("adminity/js/bootstrap-growl.min.js") }}"></pre>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/prism/custom-prism.js") }}"></script>
@endsection


