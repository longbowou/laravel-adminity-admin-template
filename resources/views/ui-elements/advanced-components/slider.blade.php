@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/owl.carousel/css/owl.carousel.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/owl.carousel/css/owl.theme.default.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/swiper/css/swiper.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Slider</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Slider</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Bootstrap Slider</h5>
                    </div>
                    <div class="card-block">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="d-block img-fluid w-100"
                                         src="{{ asset("adminity/images/slider/slide1.jpg") }}"
                                         alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block img-fluid w-100"
                                         src="{{ asset("adminity/images/slider/slide2.jpg") }}"
                                         alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block img-fluid w-100"
                                         src="{{ asset("adminity/images/slider/slide3.jpg") }}"
                                         alt="Third slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block img-fluid w-100"
                                         src="{{ asset("adminity/images/slider/slide4.jpg") }}"
                                         alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Owl Carousel Style 1</h5>
                    </div>
                    <div class="card-block">
                        <div class="owl-carousel carousel-nav owl-theme">
                            <div>
                                <img class="d-block img-fluid" src="{{ asset("adminity/images/slider/slider5.jpg") }}"
                                     alt="Third slide">
                            </div>
                            <div>
                                <img class="d-block img-fluid" src="{{ asset("adminity/images/slider/slider6.jpg") }}"
                                     alt="Third slide">
                            </div>
                            <div>
                                <img class="d-block img-fluid" src="{{ asset("adminity/images/slider/slider7.jpg") }}"
                                     alt="Third slide">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Owl Carousel Style 2</h5>
                    </div>
                    <div class="card-block">
                        <div class="owl-carousel carousel-dot owl-theme">
                            <div class="item">
                                <img class="d-block img-fluid" src="{{ asset("adminity/images/slider/slider5.jpg") }}"
                                     alt="Third slide">
                            </div>
                            <div class="item">
                                <img class="d-block img-fluid" src="{{ asset("adminity/images/slider/slider5.jpg") }}"
                                     alt="Third slide">
                            </div>
                            <div class="item">
                                <img class="d-block img-fluid" src="{{ asset("adminity/images/slider/slider5.jpg") }}"
                                     alt="Third slide">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Swiper Slider</h5>
                    </div>
                    <div class="card-block">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img class="img-fluid width-100"
                                         src="{{ asset("adminity/images/task/task-u2.jpg") }}"
                                         alt="Card image cap">


                                </div>

                                <div class="swiper-slide">
                                    <img class="img-fluid width-100"
                                         src="{{ asset("adminity/images/task/task-u3.jpg") }}"
                                         alt="Card image cap">


                                </div>

                                <div class="swiper-slide">
                                    <img class="img-fluid width-100"
                                         src="{{ asset("adminity/images/task/task-u4.jpg") }}"
                                         alt="Card image cap">


                                </div>

                                <div class="swiper-slide">
                                    <img class="img-fluid width-100"
                                         src="{{ asset("adminity/images/task/task-u1.jpg") }}"
                                         alt="Card image cap">


                                </div>

                                <div class="swiper-slide">
                                    <img class="img-fluid width-100"
                                         src="{{ asset("adminity/images/task/task-u1.jpg") }}"
                                         alt="Card image cap">


                                </div>

                                <div class="swiper-slide">
                                    <img class="img-fluid width-100"
                                         src="{{ asset("adminity/images/task/task-u2.jpg") }}"
                                         alt="Card image cap">


                                </div>

                                <div class="swiper-slide">
                                    <img class="img-fluid width-100"
                                         src="{{ asset("adminity/images/task/task-u3.jpg") }}"
                                         alt="Card image cap">


                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/owl.carousel/js/owl.carousel.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/js/owl-custom.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/components/swiper/js/swiper.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/js/swiper-custom.js") }}"></script>
@endsection


