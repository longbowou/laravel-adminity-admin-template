@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/pnotify/css/pnotify.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/pnotify/css/pnotify.brighttheme.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/pnotify/css/pnotify.buttons.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/pnotify/css/pnotify.history.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/pnotify/css/pnotify.mobile.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/pnotify/notify.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Pnotify</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route('dashboard') }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">pnotify</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Basic Notifications</h5>
                    </div>
                    <div class="card-block table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Primary Notice</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-default">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-default</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Success Notice</td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-sm" id="pnotify-success">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-success</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Info Notice</td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-sm" id="pnotify-info">Click here!
                                            <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-info</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Danger Notice</td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm" id="pnotify-danger">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-danger</code> to use this style notification</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Desktop Notifications</h5>
                    </div>
                    <div class="card-block table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>success Notification</td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-sm"
                                                id="pnotify-desktop-success">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-desktop-success</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>info Notification</td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-sm" id="pnotify-desktop-info">
                                            Click here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-desktop-info</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>warning Notification</td>
                                    <td>
                                        <button type="button" class="btn btn-warning btn-sm"
                                                id="pnotify-desktop-warning">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-desktop-warning</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>danger Notification</td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm" id="pnotify-desktop-danger">
                                            Click here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-desktop-danger</code> to use this style notification</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Notification Option</h5>
                    </div>
                    <div class="card-block table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>No title</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-no-title">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-no-title</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Rich title</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-rich">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-rich</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Click to close</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-click">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-click</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Custom Button</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-buttons">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-buttons</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Callback Button</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-callbacks">
                                            Click here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-callbacks</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Progress Button</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-progress">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-progress</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Dynamic progress Button</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-dynamic">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-dynamic</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Multiline Button</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-multiline">
                                            Click here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-multiline</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>prompt Button</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-prompt">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-prompt</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Confirm Button</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-confirm">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-confirm</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Stickey button</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-sticky">Click
                                            here! <i class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-sticky</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>permenant Button</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm"
                                                id="pnotify-permanent-buttons">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-permanent-buttons</code> to use this style notification
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Notification Position</h5>
                    </div>
                    <div class="card-block table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Top left</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm"
                                                id="pnotify-stack-top-left">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-stack-top-left</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Bottom left</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm"
                                                id="pnotify-stack-bottom-left">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-stack-bottom-left</code> to use this style notification
                                    </td>
                                </tr>
                                <tr>
                                    <td>Bottom right</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm"
                                                id="pnotify-stack-bottom-right">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-stack-bottom-right</code> to use this style notification
                                    </td>
                                </tr>
                                <tr>
                                    <td>custom left</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm"
                                                id="pnotify-stack-custom-left">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-stack-custom-left</code> to use this style notification
                                    </td>
                                </tr>
                                <tr>
                                    <td>Custom right</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm"
                                                id="pnotify-stack-custom-right">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-stack-custom-right</code> to use this style notification
                                    </td>
                                </tr>
                                <tr>
                                    <td>Custom top</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm"
                                                id="pnotify-stack-custom-top">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-stack-custom-top</code> to use this style notification</td>
                                </tr>
                                <tr>
                                    <td>Custom Bottom</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm"
                                                id="pnotify-stack-custom-bottom">Click here! <i
                                                class="icofont icofont-play-alt-2"></i></button>
                                    </td>
                                    <td>Use id <code>pnotify-stack-custom-bottom</code> to use this style notification
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/components/pnotify/js/pnotify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/pnotify/js/pnotify.desktop.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/pnotify/js/pnotify.buttons.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/pnotify/js/pnotify.confirm.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/pnotify/js/pnotify.callbacks.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/pnotify/js/pnotify.animate.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/pnotify/js/pnotify.history.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/pnotify/js/pnotify.mobile.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/pnotify/js/pnotify.nonblock.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/pnotify/notify.js") }}"></script>
@endsection


