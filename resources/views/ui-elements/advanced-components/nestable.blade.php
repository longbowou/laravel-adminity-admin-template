@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/nestable/nestable.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Nestable</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Sample page</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Nestable</h5>
                    </div>
                    <div class="card-block">
                        <div id="nestable-menu" class="m-b-10">
                            <button type="button" class="btn btn-primary waves-effect waves-light m-b-10 m-r-20"
                                    data-action="expand-all">Expand All
                            </button>
                            <button type="button" class="btn btn-success waves-effect waves-light m-b-10"
                                    data-action="collapse-all">Collapse All
                            </button>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-sm-12">
                                <div class="cf nestable-lists">
                                    <div class="dd" id="nestable">
                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="1">
                                                <div class="dd-handle">Item 1</div>
                                            </li>
                                            <li class="dd-item" data-id="2">
                                                <div class="dd-handle">Item 2</div>
                                                <ol class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class="dd-handle">Item 3</div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class="dd-handle">Item 4</div>
                                                    </li>
                                                    <li class="dd-item" data-id="5">
                                                        <div class="dd-handle">Item 5</div>
                                                        <ol class="dd-list">
                                                            <li class="dd-item" data-id="6">
                                                                <div class="dd-handle">Item 6</div>
                                                            </li>
                                                            <li class="dd-item" data-id="7">
                                                                <div class="dd-handle">Item 7</div>
                                                            </li>
                                                            <li class="dd-item" data-id="8">
                                                                <div class="dd-handle">Item 8</div>
                                                            </li>
                                                        </ol>
                                                    </li>
                                                    <li class="dd-item" data-id="9">
                                                        <div class="dd-handle">Item 9</div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class="dd-handle">Item 10</div>
                                                    </li>
                                                </ol>
                                            </li>
                                            <li class="dd-item" data-id="11">
                                                <div class="dd-handle">Item 11</div>
                                            </li>
                                            <li class="dd-item" data-id="12">
                                                <div class="dd-handle">Item 12</div>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12">
                                <div class="dd" id="color-nestable">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="13">
                                            <div class="dd-handle nestable-success">Item 1</div>
                                        </li>
                                        <li class="dd-item" data-id="14">
                                            <div class="dd-handle nestable-warning">Item 2</div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="14">
                                                    <div class="dd-handle">Item 3</div>
                                                </li>
                                                <li class="dd-item" data-id="15">
                                                    <div class="dd-handle">Item 4</div>
                                                </li>
                                                <li class="dd-item" data-id="16">
                                                    <div class="dd-handle">Item 5</div>
                                                    <ol class="dd-list">
                                                        <li class="dd-item" data-id="17">
                                                            <div class="dd-handle">Item 6</div>
                                                        </li>
                                                        <li class="dd-item" data-id="18">
                                                            <div class="dd-handle">Item 7</div>
                                                        </li>
                                                        <li class="dd-item" data-id="19">
                                                            <div class="dd-handle">Item 8</div>
                                                        </li>
                                                    </ol>
                                                </li>
                                                <li class="dd-item" data-id="20">
                                                    <div class="dd-handle">Item 9</div>
                                                </li>
                                                <li class="dd-item" data-id="21">
                                                    <div class="dd-handle">Item 10</div>
                                                </li>
                                            </ol>
                                        </li>
                                        <li class="dd-item" data-id="22">
                                            <div class="dd-handle nestable-danger">Item 11</div>
                                        </li>
                                        <li class="dd-item" data-id="23">
                                            <div class="dd-handle nestable-info">Item 12</div>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12">
                                <div class="cf nestable-lists">
                                    <div class="dd" id="nestable2">
                                        <ol class="dd-list">
                                            <li class="dd-item dd3-item" data-id="13">
                                                <div class="dd-handle dd3-handle">Drag</div>
                                                <div class="dd3-content">Item 13</div>
                                            </li>
                                            <li class="dd-item dd3-item" data-id="14">
                                                <div class="dd-handle dd3-handle">Drag</div>
                                                <div class="dd3-content">Item 14</div>
                                            </li>
                                            <li class="dd-item dd3-item" data-id="15">
                                                <div class="dd-handle dd3-handle">Drag</div>
                                                <div class="dd3-content">Item 15</div>
                                                <ol class="dd-list">
                                                    <li class="dd-item dd3-item" data-id="16">
                                                        <div class="dd-handle dd3-handle">Drag</div>
                                                        <div class="dd3-content">Item 16</div>
                                                    </li>
                                                    <li class="dd-item dd3-item" data-id="17">
                                                        <div class="dd-handle dd3-handle">Drag</div>
                                                        <div class="dd3-content">Item 17</div>
                                                    </li>
                                                    <li class="dd-item dd3-item" data-id="18">
                                                        <div class="dd-handle dd3-handle">Drag</div>
                                                        <div class="dd3-content">Item 18</div>
                                                    </li>
                                                </ol>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/pages/nestable/jquery.nestable.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/nestable/custom-nestable.js") }}" type="text/javascript"></script>
@endsection


