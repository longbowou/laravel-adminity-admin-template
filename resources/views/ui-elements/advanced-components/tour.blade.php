@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/intro.js/css/introjs.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Tour</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Tour</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Section 1</h5>
                    </div>
                    <div class="card-block" data-intro="This is Card body" data-step="2"
                         data-hint="Hello step osne!">
                        <p>Use code inside div element <code
                                data-intro="This is Card Header" data-step="1"
                                data-hint="Hello step one!">data-intro="This is Card Header"
                                data-step="1"</code> to create tour.</p>
                        <p>
                            "Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s, when an unknown
                            printer took a galley of type and scrambled it to make a type
                            specimen book. It has survived not only five centuries, but also
                            the leap into electronic typesetting, remaining essentially
                            unchanged."
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Section 2</h5>
                    </div>
                    <div class="card-block" data-intro="This is Card body" data-step="3"
                         data-hint="Hello step osne!">
                        <p>
                            "Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s, when an unknown
                            printer took a galley of type and scrambled it to make a type
                            specimen book. It has survived not only five centuries, but also
                            the leap into electronic typesetting, remaining essentially
                            unchanged."
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Section 3</h5>
                    </div>
                    <div class="card-block" data-intro="This is Card body" data-step="4"
                         data-hint="Hello step osne!">
                        <p>
                            "Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s, when an unknown
                            printer took a galley of type and scrambled it to make a type
                            specimen book. It has survived not only five centuries, but also
                            the leap into electronic typesetting, remaining essentially
                            unchanged."
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

                <div class="card button-page">
                    <div class="card-header">
                        <h5>Button</h5>
                    </div>
                    <div class="card-block">
                        <ul>
                            <li>
                                <button class="btn btn-primary">Primary Button</button>
                            </li>
                            <li>
                                <button class="btn btn-success">Success Button</button>
                            </li>
                            <li>
                                <button class="btn btn-info">Info Button</button>
                            </li>
                            <li>
                                <button class="btn btn-warning">Warning Button</button>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

                <div class="card button-page">
                    <div class="card-header" data-intro="This is Card Header" data-step="6"
                         data-hint="Hello step osne!">
                        <h5>Rounded Button</h5>
                    </div>
                    <div class="card-block">
                        <ul>
                            <li>
                                <button class="btn btn-primary btn-round">Primary Button
                                </button>
                            </li>
                            <li>
                                <button class="btn btn-success btn-round">Success Button
                                </button>
                            </li>
                            <li>
                                <button class="btn btn-info btn-round">Info Button</button>
                            </li>
                            <li>
                                <button class="btn btn-warning btn-round">Warning Button
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

                <div class="card button-page">
                    <div class="card-header">
                        <h5>Square Button</h5>
                    </div>
                    <div class="card-block" data-intro="This is Card body" data-step="7"
                         data-hint="Hello step osne!">
                        <ul>
                            <li>
                                <button class="btn btn-primary btn-square">Primary Button
                                </button>
                            </li>
                            <li>
                                <button class="btn btn-success btn-square">Success Button
                                </button>
                            </li>
                            <li>
                                <button class="btn btn-info btn-square">Info Button</button>
                            </li>
                            <li>
                                <button class="btn btn-warning btn-square">Warning Button
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/components/intro.js/js/intro.js") }}"></script>
    <script type="text/javascript">
        introJs().start();
    </script>
@endsection


