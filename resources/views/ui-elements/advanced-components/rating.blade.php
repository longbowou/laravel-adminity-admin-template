@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/fontawesome-stars.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/bars-1to10.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/bars-horizontal.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/bars-movie.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/bars-pill.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/bars-reversed.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/bars-square.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/css-stars.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/fontawesome-stars-o.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Rating</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Rating</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <div class="card-header-left">
                            <h5>Rating</h5>
                            <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">1/10 Rating</h6>
                                <p>Use <code>id="example-1to10"</code> to see default rating</p>
                                <div class="box-body">
                                    <select id="example-1to10" name="rating" autocomplete="off">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7" selected="selected">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                    <span class="current-rating"></span>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Movie Rating</h6>
                                <p>Use <code>id="example-movie"</code> to see movie rating</p>
                                <div class="box-body">
                                    <select id="example-movie" name="rating" autocomplete="off">
                                        <option value="Bad">Bad</option>
                                        <option value="Mediocre">Mediocre</option>
                                        <option value="Good" selected="selected">Good</option>
                                        <option value="Awesome">Awesome</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">square rating</h6>
                                <p>Use <code>id="example-square"</code> to see square rating</p>
                                <div class="box-body">
                                    <select id="example-square" name="rating" autocomplete="off">
                                        <option value="" label="0"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Pill rating</h6>
                                <p>Use <code>id="example-pill"</code> to see pill rating</p>
                                <div class="box-body">
                                    <select id="example-pill" name="rating" autocomplete="off">
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                        <option value="E">E</option>
                                        <option value="F">F</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Reverse rating</h6>
                                <p>Use <code>id="example-reversed"</code> to see reverse rating</p>
                                <div class="box-body">
                                    <select id="example-reversed" name="rating" autocomplete="off">
                                        <option value="Strongly Agree">Strongly Agree</option>
                                        <option value="Agree">Agree</option>
                                        <option value="Neither Agree or Disagree" selected="selected">Neither Agree or
                                            Disagree
                                        </option>
                                        <option value="Disagree">Disagree</option>
                                        <option value="Strongly Disagree">Strongly Disagree</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Horizontal Rating</h6>
                                <p>Use <code>id="example-horizontal"</code> to see horizontal rating</p>
                                <div class="box-body">
                                    <select id="example-horizontal" name="rating" autocomplete="off">
                                        <option value="10">10</option>
                                        <option value="9">9</option>
                                        <option value="8">8</option>
                                        <option value="7">7</option>
                                        <option value="6">6</option>
                                        <option value="5">5</option>
                                        <option value="4">4</option>
                                        <option value="3">3</option>
                                        <option value="2">2</option>
                                        <option value="1" selected="selected">1</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Font awesome Rating</h6>
                                <p>Use <code>id="example-fontawesome"</code> to see font awesome rating</p>
                                <div class="stars stars-example-fontawesome">
                                    <select id="example-fontawesome" name="rating" autocomplete="off">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">CSS Stars Rating</h6>
                                <p>Use <code>id="example-css"</code> to see css stars rating</p>
                                <div class="stars stars-example-css">
                                    <select id="example-css" class="rating-star" name="rating" autocomplete="off">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">fractional star Rating</h6>
                                <p>Use <code>id="example-fontawesome-o"</code> to see fractional star rating</p>
                                <div class="stars stars-example-fontawesome-o">
                                    <select id="example-fontawesome-o" name="rating" data-current-rating="5.6"
                                            autocomplete="off">
                                        <option value="" label="0"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                    <span class="title current-rating">
Current rating: <span class="value"></span>
</span>
                                    <span class="title your-rating hidden">
Your rating: <span class="value"></span>&nbsp;
<a href="#" class="clear-rating"><i class="icofont icofont-close-circled"></i></a>
</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/jquery-bar-rating/js/jquery.barrating.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/rating/rating.js") }}"></script>
@endsection


