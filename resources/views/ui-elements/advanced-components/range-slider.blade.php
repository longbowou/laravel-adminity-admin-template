@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/seiyria-bootstrap-slider/css/bootstrap-slider.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Range Slider</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Range Slider</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Basic Range Slider</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Basic Range Slider</h6>
                                <p>Use <code>id="ex1"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex1" data-slider-id='ex1Slider' type="range" data-slider-min="0"
                                           data-slider-max="20" data-slider-step="1" data-slider-value="14"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Range selector Slider</h6>
                                <p>Use <code>id="ex2"</code> to see default rating</p>
                                <div class="range-slider">
                                    <b>€ 10</b>
                                    <input id="ex2" type="text" class="span2" value="" data-slider-min="10"
                                           data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/>
                                    <b>€ 1000</b>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Range handles Slider</h6>
                                <p>Use <code>data-slider-id="RC" id="R" data-slider-handle="square"</code> to color
                                    handles rating</p>
                                <div class="range-slider">
                                    <div class="">
                                        <p>
                                            <b>R</b>
                                            <input type="text" class="span2" value="" data-slider-min="0"
                                                   data-slider-max="255" data-slider-step="1" data-slider-value="128"
                                                   data-slider-id="RC" id="R" data-slider-tooltip="hide"
                                                   data-slider-handle="square"/>
                                        </p>
                                        <p>
                                            <b>G</b>
                                            <input type="text" class="span2" value="" data-slider-min="0"
                                                   data-slider-max="255" data-slider-step="1" data-slider-value="128"
                                                   data-slider-id="GC" id="G" data-slider-tooltip="hide"
                                                   data-slider-handle="round"/>
                                        </p>
                                        <p>
                                            <b>B</b>
                                            <input type="text" class="span2" value="" data-slider-min="0"
                                                   data-slider-max="255" data-slider-step="1" data-slider-value="128"
                                                   data-slider-id="BC" id="B" data-slider-tooltip="hide"
                                                   data-slider-handle="triangle"/>
                                        </p>
                                        <div id="RGB"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Destroy Instance Slider</h6>
                                <p>Use <code>id="ex5"</code> to see default rating</p>
                                <div class="range-slider">
                                    <div class="m-b-20">
                                        <input id="ex5" type="text" data-slider-min="-5" data-slider-max="20"
                                               data-slider-step="1" data-slider-value="0" class="md-form-control"/>
                                    </div>
                                    <button id="destroyEx5Slider"
                                            class='btn btn-primary waves-effect waves-light range-slider-contain p-absolute'>
                                        Click
                                    </button>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title"> Bind to 'slide' JQuery event on slider</h6>
                                <p>Use <code>id="ex6"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex6" type="text" data-slider-min="-5" data-slider-max="20"
                                           data-slider-step="1" data-slider-value="3"/>
                                    <div class="range-slider-contain p-absolute">
                                        <span id="ex6CurrentSliderValLabel"><span id="ex6SliderVal">3</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title"> Enabled and Disabled.</h6>
                                <p>Use <code>id="ex7"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex7" type="text" data-slider-min="0" data-slider-max="20"
                                           data-slider-step="1" data-slider-value="5" data-slider-enabled="false"/>
                                    <div class="range-slider-contain p-absolute">
                                        <input id="ex7-enabled" type="checkbox"/> Enabled
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title"> Static Tooltip.</h6>
                                <p>Use <code>id="ex8"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex8" data-slider-id='ex1Slider' type="text" data-slider-min="0"
                                           data-slider-max="20" data-slider-step="1" data-slider-value="14"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title"> Decimal value Slider</h6>
                                <p>Use <code>id="ex9"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex9" type="text"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title"> Setting custom Icon.</h6>
                                <p>Use <code>id="ex10"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex10" type="text" data-slider-handle="custom"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title"> Using a custom step interval.</h6>
                                <p>Use <code>id="ex11"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex11" type="text" data-slider-handle="custom"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Coloring.</h6>
                                <p>Use <code>id="ex12a", id="ex12b", id="ex12c"</code> to see default rating</p>
                                <div class="range-slider">
                                    <div>

                                        <input id="ex12a" type="text"/>
                                        <br/>

                                        <input id="ex12b" type="text"/>
                                        <br/>

                                        <input id="ex12c" type="text"/>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Using tick marks and labels.</h6>
                                <p>Use <code>id="ex13"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex13" type="text" data-slider-ticks="[0, 100, 200, 300, 400]"
                                           data-slider-ticks-snap-bounds="30"
                                           data-slider-ticks-labels='["$0", "$100", "$200", "$300", "$400"]'/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Using tick marks at specific positions.</h6>
                                <p>Use <code>id="ex14"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex14" type="text" data-slider-ticks-snap-bounds="30"
                                           data-slider-ticks-labels="['$0', '$100', '$200','$400']"
                                           data-ticks_positions="[0, 30, 60, 70, 100]"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">With a logarithmic scale.</h6>
                                <p>Use <code>id="ex15"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex15" type="text" data-slider-min="1000" data-slider-max="10000000"
                                           data-slider-step="5"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Focus the slider.</h6>
                                <p>Use <code>id="ex16a" and id="ex16b"</code> to see default rating</p>
                                <div class="range-slider">
                                    <div>

                                        <input id="ex16a" type="text"/>
                                        <br/>

                                        <input id="ex16b" type="text"/>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Unusual tooltip positions</h6>
                                <p>Use <code>id="ex17a"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex17a" type="text"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Accessibility with ARIA labels</h6>
                                <p>Use <code>id="ex18a" and id="ex18b"</code> to see default rating</p>
                                <div class="range-slider">
                                    <div>
                                        <input id="ex18a" type="text"/>
                                        <input id="ex18b" type="text"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Auto-Register</h6>
                                <p>Use <code>id="ex19"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex19" type="text" data-provide="slider" data-slider-ticks="[1, 2, 3]"
                                           data-slider-ticks-labels='["short", "medium", "long"]' data-slider-min="1"
                                           data-slider-max="3" data-slider-step="1" data-slider-value="3"
                                           data-slider-tooltip="hide"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Slider-Elements initially hidden</h6>
                                <p>Use <code>id="ex20a" and id="ex16b"</code> to see default rating</p>
                                <div class="range-slider">
                                    <a class="btn btn-primary waves-effect waves-light range-slider-contain p-absolute"
                                       href="#" id="ex20a">Show</a>
                                    <div class="show-well range-well" style="display: none">
                                        <input type="text" data-provide="slider" data-slider-ticks="[1, 2, 3]"
                                               data-slider-ticks-labels='["short", "medium", "long"]'
                                               data-slider-min="1" data-slider-max="3" data-slider-step="1"
                                               data-slider-value="3" data-slider-tooltip="hide"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">automatically turns it into a slider</h6>
                                <p>Use <code>id="ex21"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex21" type="text" data-provide="slider" data-slider-ticks="[1, 2, 3]"
                                           data-slider-ticks-labels='["short", "medium", "long"]' data-slider-min="1"
                                           data-slider-max="3" data-slider-step="1" data-slider-value="3"
                                           data-slider-tooltip="hide"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Using tick marks at specific positions..</h6>
                                <p>Use <code>id="ex23"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex23" type="text" data-slider-ticks="[0, 1, 2, 3, 4]"
                                           data-slider-step="0.01" data-slider-ticks-snap-bounds="200"
                                           data-slider-ticks-tooltip="true"
                                           data-ticks_positions="[0, 30, 60, 70, 90, 100]"/>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6">
                                <h6 class="sub-title">Vertical Slider</h6>
                                <p>Use <code>id="4"</code> to see default rating</p>
                                <div class="range-slider">
                                    <input id="ex4" type="text" data-slider-min="-5" data-slider-max="20"
                                           data-slider-step="1" data-slider-value="-3"
                                           data-slider-orientation="vertical"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/seiyria-bootstrap-slider/js/bootstrap-slider.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/range-slider.js") }}"></script>
@endsection


