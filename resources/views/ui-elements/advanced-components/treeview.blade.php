@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/jstree/css/style.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/treeview/treeview.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Tree View</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Tree View</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12 col-lg-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Basic Tree</h5>
                    </div>
                    <div class="card-block">
                        <div class="card-block tree-view">
                            <div id="basicTree">
                                <ul>
                                    <li>Admin
                                        <ul>
                                            <li data-jstree='{"opened":true}'>Assets
                                                <ul>
                                                    <li data-jstree='{"type":"file"}'>Css</li>
                                                    <li data-jstree='{"opened":true}'>Plugins
                                                        <ul>
                                                            <li data-jstree='{"selected":true,"type":"file"}'>Plugin one</li>
                                                            <li data-jstree='{"type":"file"}'>Plugin two</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{"opened":true}'>Email Template
                                                <ul>
                                                    <li data-jstree='{"type":"file"}'>Email one</li>
                                                    <li data-jstree='{"type":"file"}'>Email two</li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{"icon":"zmdi zmdi-view-dashboard"}'>Dashboard</li>
                                            <li data-jstree='{"icon":"zmdi zmdi-format-underlined"}'>Typography</li>
                                            <li data-jstree='{"opened":true}'>User Interface
                                                <ul>
                                                    <li data-jstree='{"type":"file"}'>Buttons</li>
                                                    <li data-jstree='{"type":"file"}'>Cards</li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{"icon":"zmdi zmdi-collection-text"}'>Forms</li>
                                            <li data-jstree='{"icon":"zmdi zmdi-view-list"}'>Tables</li>
                                        </ul>
                                    </li>
                                    <li data-jstree='{"type":"file"}'>Frontend</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12 col-lg-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Checkbox Tree</h5>
                    </div>
                    <div class="card-block">
                        <div class="card-block tree-view">
                            <div id="checkTree">
                                <ul>
                                    <li>Admin
                                        <ul>
                                            <li data-jstree='{"opened":true}'>Assets
                                                <ul>
                                                    <li data-jstree='{"type":"file"}'>Css</li>
                                                    <li data-jstree='{"opened":true}'>Plugins
                                                        <ul>
                                                            <li data-jstree='{"selected":true,"type":"file"}'>Plugin one</li>
                                                            <li data-jstree='{"type":"file"}'>Plugin two</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{"opened":true}'>Email Template
                                                <ul>
                                                    <li data-jstree='{"type":"file"}'>Email one</li>
                                                    <li data-jstree='{"type":"file"}'>Email two</li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{"icon":"zmdi zmdi-view-dashboard"}'>Dashboard</li>
                                            <li data-jstree='{"icon":"zmdi zmdi-format-underlined"}'>Typography</li>
                                            <li data-jstree='{"opened":true}'>User Interface
                                                <ul>
                                                    <li data-jstree='{"type":"file"}'>Buttons</li>
                                                    <li data-jstree='{"type":"file"}'>Cards</li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{"icon":"zmdi zmdi-collection-text"}'>Forms</li>
                                            <li data-jstree='{"icon":"zmdi zmdi-view-list"}'>Tables</li>
                                        </ul>
                                    </li>
                                    <li data-jstree='{"type":"file"}'>Frontend</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12 col-lg-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Drag Tree</h5>
                    </div>
                    <div class="card-block">
                        <div class="card-block tree-view">
                            <div id="dragTree">
                                <ul>
                                    <li>Admin
                                        <ul>
                                            <li data-jstree='{"opened":true}'>Assets
                                                <ul>
                                                    <li data-jstree='{"type":"file"}'>Css</li>
                                                    <li data-jstree='{"opened":true}'>Plugins
                                                        <ul>
                                                            <li data-jstree='{"selected":true,"type":"file"}'>Plugin one</li>
                                                            <li data-jstree='{"type":"file"}'>Plugin two</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{"opened":true}'>Email Template
                                                <ul>
                                                    <li data-jstree='{"type":"file"}'>Email one</li>
                                                    <li data-jstree='{"type":"file"}'>Email two</li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{"icon":"zmdi zmdi-view-dashboard"}'>Dashboard</li>
                                            <li data-jstree='{"icon":"zmdi zmdi-format-underlined"}'>Typography</li>
                                            <li data-jstree='{"opened":true}'>User Interface
                                                <ul>
                                                    <li data-jstree='{"type":"file"}'>Buttons</li>
                                                    <li data-jstree='{"type":"file"}'>Cards</li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{"icon":"zmdi zmdi-collection-text"}'>Forms</li>
                                            <li data-jstree='{"icon":"zmdi zmdi-view-list"}'>Tables</li>
                                        </ul>
                                    </li>
                                    <li data-jstree='{"type":"file"}'>Frontend</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/components/jstree/js/jstree.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/treeview/jquery.tree.js") }}"></script>
@endsection


