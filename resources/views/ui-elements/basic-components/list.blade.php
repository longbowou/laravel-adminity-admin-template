@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/css/list.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/css/stroll.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>List</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Basic Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">List</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>List View</h5>
                    </div>
                    <div class="row card-block">
                        <div class="col-md-12">
                            <ul class="list-view">
                                <li>
                                    <div class="card list-view-media">
                                        <div class="card-block">
                                            <div class="media">
                                                <a class="media-left" href="#">
                                                    <img class="media-object card-list-img"
                                                         src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                         alt="Generic placeholder image">
                                                </a>
                                                <div class="media-body">
                                                    <div class="col-xs-12">
                                                        <h6 class="d-inline-block">
                                                            Heading</h6>
                                                        <label class="label label-info">Agent</label>
                                                    </div>
                                                    <div class="f-13 text-muted m-b-15">
                                                        Software Engineer
                                                    </div>
                                                    <p>Hi, This is my short intro text.
                                                        Lorem ipsum is a dummy content sit
                                                        dollar. You can copy and paste this
                                                        dummy content from anywhere and to
                                                        anywhere. Its all free and must be a
                                                        good to folllow in prectice</p>
                                                    <div class="m-t-15">
                                                        <button type="button" data-toggle="tooltip" title="Facebook"
                                                                class="btn btn-facebook btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-social-facebook"></span>
                                                        </button>
                                                        <button type="button" data-toggle="tooltip" title="Twitter"
                                                                class="btn btn-twitter btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-social-twitter"></span>
                                                        </button>
                                                        <button type="button" data-toggle="tooltip" title="Linkedin"
                                                                class="btn btn-linkedin btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-brand-linkedin"></span>
                                                        </button>
                                                        <button type="button" data-toggle="tooltip" title="Drible"
                                                                class="btn btn-dribbble btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-social-dribble"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <div class="card list-view-media">
                                        <div class="card-block">
                                            <div class="media">
                                                <a class="media-left" href="#">
                                                    <img class="media-object card-list-img"
                                                         src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                         alt="Generic placeholder image">
                                                </a>
                                                <div class="media-body">
                                                    <div>
                                                        <h6 class="d-inline-block">
                                                            Heading</h6>
                                                        <label class="label  label-info">Accountant</label>
                                                    </div>
                                                    <div class="f-13 text-muted m-b-15">
                                                        Softwear Engineer
                                                    </div>
                                                    <p>Hi, This is my short intro text.
                                                        Lorem ipsum is a dummy content sit
                                                        dollar. You can copy and paste this
                                                        dummy content from anywhere and to
                                                        anywhere. Its all free and must be a
                                                        good to folllow in prectice</p>
                                                    <div class="m-t-15">
                                                        <button type="button" data-toggle="tooltip" title="Facebook"
                                                                class="btn btn-facebook btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-social-facebook"></span>
                                                        </button>
                                                        <button type="button" data-toggle="tooltip" title="Twitter"
                                                                class="btn btn-twitter btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-social-twitter"></span>
                                                        </button>
                                                        <button type="button" data-toggle="tooltip" title="Linkedin"
                                                                class="btn btn-linkedin btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-brand-linkedin"></span>
                                                        </button>
                                                        <button type="button" data-toggle="tooltip" title="Drible"
                                                                class="btn btn-dribbble btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-social-dribble"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <div class="card list-view-media">
                                        <div class="card-block">
                                            <div class="media">
                                                <a class="media-left" href="#">
                                                    <img class="media-object card-list-img"
                                                         src="{{ asset("adminity/images/avatar-3.jpg") }}"
                                                         alt="Generic placeholder image">
                                                </a>
                                                <div class="media-body">
                                                    <div>
                                                        <h6 class="d-inline-block">
                                                            Heading</h6>
                                                        <label class="label  label-info">Accountant</label>
                                                    </div>
                                                    <div class="f-13 text-muted m-b-15">
                                                        Softwear Engineer
                                                    </div>
                                                    <p>Hi, This is my short intro text.
                                                        Lorem ipsum is a dummy content sit
                                                        dollar. You can copy and paste this
                                                        dummy content from anywhere and to
                                                        anywhere. Its all free and must be a
                                                        good to folllow in prectice</p>
                                                    <div class="m-t-15">
                                                        <button type="button" data-toggle="tooltip" title="Facebook"
                                                                class="btn btn-facebook btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-social-facebook"></span>
                                                        </button>
                                                        <button type="button" data-toggle="tooltip" title="Twitter"
                                                                class="btn btn-twitter btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-social-twitter"></span>
                                                        </button>
                                                        <button type="button" data-toggle="tooltip" title="Linkedin"
                                                                class="btn btn-linkedin btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-brand-linkedin"></span>
                                                        </button>
                                                        <button type="button" data-toggle="tooltip" title="Drible"
                                                                class="btn btn-dribbble btn-mini waves-effect waves-light">
                                                            <span class="icofont icofont-social-dribble"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Dynamic List</h5>
                    </div>
                    <div class="row card-block">
                        <div class="col-md-6 col-lg-4">
                            <div class="dynamic-row">
                                <h6 class="sub-title">List Item Adding (Dump Computer)</h6>
                                <div class="dynamic-list-one effeckt-list-wrap">
                                    <ul class="effeckt-list  m-b-0" data-effeckt-type="pop-in">
                                        <li class="p-t-0">
                                            <h6>list item 1</h6>
                                        </li>
                                        <li>
                                            <h6>list item 2</h6>
                                        </li>
                                        <li>
                                            <h6>list item 3</h6>
                                        </li>
                                        <li>
                                            <h6>list item 4</h6>
                                        </li>
                                        <li>
                                            <h6>list item 5</h6>
                                        </li>
                                        <li>
                                            <h6>list item 6</h6>
                                        </li>
                                    </ul>
                                    <button type="submit"
                                            class="btn btn-primary waves-effect waves-light m-r-15 m-l-15 add">
                                        Add
                                    </button>
                                    <button class="btn btn-primary waves-effect waves-light remove">
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="dynamic-row">
                                <h6 class="sub-title">List Item Adding (Brain Clue
                                    Style1)</h6>
                                <div class="dynamic-list-two effeckt-list-wrap">
                                    <ul class="effeckt-list  m-b-0" data-effeckt-type="expand-in">
                                        <li class="p-t-0">
                                            <h6>list item 1</h6>
                                        </li>
                                        <li>
                                            <h6>list item 2</h6>
                                        </li>
                                        <li>
                                            <h6>list item 3</h6>
                                        </li>
                                        <li>
                                            <h6>list item 4</h6>
                                        </li>
                                        <li>
                                            <h6>list item 5</h6>
                                        </li>
                                        <li>
                                            <h6>list item 6</h6>
                                        </li>
                                    </ul>
                                    <button type="submit"
                                            class="btn btn-primary waves-effect waves-light m-r-15 m-l-15 add">
                                        Add
                                    </button>
                                    <button class="btn btn-primary waves-effect waves-light remove">
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="dynamic-row">
                                <h6 class="sub-title">List Item Adding (Brain Clue
                                    Style2)</h6>
                                <div class="dynamic-list-three effeckt-list-wrap">
                                    <ul class="effeckt-list  m-b-0" data-effeckt-type="fall-in">
                                        <li class="p-t-0">
                                            <h6>list item 1</h6>
                                        </li>
                                        <li>
                                            <h6>list item 2</h6>
                                        </li>
                                        <li>
                                            <h6>list item 3</h6>
                                        </li>
                                        <li>
                                            <h6>list item 4</h6>
                                        </li>
                                        <li>
                                            <h6>list item 5</h6>
                                        </li>
                                        <li>
                                            <h6>list item 6</h6>
                                        </li>
                                    </ul>
                                    <button type="submit"
                                            class="btn btn-primary waves-effect waves-light m-r-15 m-l-15 add">
                                        Add
                                    </button>
                                    <button class="btn btn-primary waves-effect waves-light remove">
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="dynamic-row">
                                <h6 class="sub-title">Details View(Dumb Computer)</h6>
                                <div class="dynamic-list-five">
                                    <ul class="m-b-0">
                                        <li class="p-t-0">
                                            <h6>...</h6>
                                        </li>
                                        <li>
                                            <h6>...</h6>
                                        </li>
                                        <li>
                                            <h6>...</h6>
                                        </li>
                                        <li id="dynamic-list-five-button">
                                            <h6>List Summary</h6>
                                            <div class="details">
                                                <p>Pellentesque habitant morbi tristique
                                                    senectus et netus et malesuada fames ac
                                                    turpis egestas. Vestibulum tortor quam,
                                                    feugiat vitae, ultricies eget, tempor
                                                    sit amet, ante. Donec eu libero sit amet
                                                    quam egestas semper. Aenean ultricies mi
                                                    vitae est. Mauris placerat eleifend
                                                    leo.</p>
                                            </div>
                                        </li>
                                        <li>
                                            <h6>...</h6>
                                        </li>
                                        <li class="p-b-0">
                                            <h6>...</h6>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="dynamic-row">
                                <h6 class="sub-title">Details View(Brain Clue)</h6>
                                <div class="dynamic-list-six">
                                    <ul class="m-b-0" id="dynamic-list-six-list">
                                        <li class="p-t-0">
                                            <h6>...</h6>
                                        </li>
                                        <li>
                                            <h6>...</h6>
                                        </li>
                                        <li>
                                            <h6>...</h6>
                                        </li>
                                        <li>
                                            <h6>...</h6>
                                        </li>
                                        <li id="dynamic-list-six-button">
                                            <h6>List Summary</h6>
                                            <div class="details">
                                                <p>Pellentesque habitant morbi tristique
                                                    senectus et netus et malesuada fames ac
                                                    turpis egestas. Vestibulum tortor quam,
                                                    feugiat vitae, ultricies eget, tempor
                                                    sit amet, ante. Donec eu libero sit amet
                                                    quam egestas semper. Aenean ultricies mi
                                                    vitae est. Mauris placerat eleifend
                                                    leo.</p>
                                            </div>
                                            <a href="#0" class="close-button" role="button" title="close">×</a>
                                        </li>
                                        <li class="p-b-0">
                                            <h6>...</h6>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>List Scroll</h5>
                    </div>
                    <div class="row card-block">
                        <div class="col-md-6 col-lg-4">
                            <div class="card card-block user-card">
                                <ul class="scroll-list cards">
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="card card-block user-card">
                                <ul class="scroll-list wave">
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="card card-block user-card">
                                <ul class="scroll-list flip">
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="card card-block user-card">
                                <ul class="scroll-list helix">
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="card card-block user-card">
                                <ul class="scroll-list fan">
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="card card-block user-card">
                                <ul class="scroll-list twirl">
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                    <li>
                                        <h6>Item1</h6>
                                    </li>
                                    <li>
                                        <h6>Item2</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>List Type</h5>
                    </div>
                    <div class="row card-block">
                        <div class="col-md-12 col-lg-4">
                            <h6 class="sub-title">Text list</h6>
                            <ul class="basic-list">
                                <li class="">
                                    <h6>Heading</h6>
                                    <p>Laborum nihil aliquam nulla praesentium illo libero
                                        nihil at odio maxime.</p>
                                </li>
                                <li class="">
                                    <h6>Heading</h6>
                                    <p>Laborum nihil aliquam nulla praesentium illo libero
                                        nihil at odio maxime.</p>
                                </li>
                                <li class="">
                                    <h6>Heading</h6>
                                    <p>Laborum nihil aliquam nulla praesentium illo libero
                                        nihil at odio maxime.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <h6 class="sub-title">Text with Icon Lists</h6>
                            <ul class="basic-list list-icons">
                                <li>
                                    <i class="icofont icofont-speech-comments text-primary p-absolute text-center d-block f-30"></i>
                                    <h6>Heading</h6>
                                    <p>Laborum nihil aliquam nulla praesentium illo libero
                                        nihil at odio maxime.</p>
                                </li>
                                <li>
                                    <h6>Heading</h6>
                                    <i class="icofont icofont-warning  text-primary p-absolute text-center d-block f-30"></i>
                                    <p>Laborum nihil aliquam nulla praesentium illo libero
                                        nihil at odio maxime.</p>
                                </li>
                                <li>
                                    <h6>Heading</h6>
                                    <i class="icofont icofont-bell-alt text-primary p-absolute text-center d-block f-30"></i>
                                    <p>Laborum nihil aliquam nulla praesentium illo libero
                                        nihil at odio maxime.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <h6 class="sub-title">Text with images Lists</h6>
                            <ul class="basic-list list-icons-img">
                                <li>
                                    <img src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                         class="img-fluid p-absolute d-block text-center" alt="">
                                    <h6>Heading</h6>
                                    <p>Laborum nihil aliquam nulla praesentium illo libero
                                        nihil at odio maxime.</p>
                                </li>
                                <li>
                                    <h6>Heading</h6>
                                    <img src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                         class="img-fluid p-absolute d-block text-center" alt="">
                                    <p>Laborum nihil aliquam nulla praesentium illo libero
                                        nihil at odio maxime.</p>
                                </li>
                                <li>
                                    <h6>Heading</h6>
                                    <img src="{{ asset("adminity/images/avatar-3.jpg") }}"
                                         class="img-fluid p-absolute d-block text-center" alt="">
                                    <p>Laborum nihil aliquam nulla praesentium illo libero
                                        nihil at odio maxime.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/js/stroll.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/js/list-custom.js") }}"></script>
@endsection

