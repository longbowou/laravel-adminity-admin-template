@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Alert</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Basic Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Alert</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Default Alert</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="sub-title">Default Alert</div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-xl-6">

                                <div class="alert alert-default">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Default!</strong> add Class <code>
                                        alert-default</code>
                                </div>
                                <div class="alert alert-primary">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Primary!</strong> add Class <code>
                                        alert-primary</code>
                                </div>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Success!</strong> add Class <code>
                                        alert-success</code>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Info!</strong> add Class <code>
                                        alert-info</code>
                                </div>
                                <div class="alert alert-warning">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Warning!</strong> add Class <code>
                                        alert-warning</code>
                                </div>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Danger!</strong> add Class <code>
                                        alert-danger</code>
                                </div>
                            </div>
                        </div>


                        <div class="sub-title">Border Alert</div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="alert alert-default border-default">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Default!</strong> add Class <code>
                                        border-default</code>
                                </div>
                                <div class="alert alert-primary border-primary">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Primary!</strong> add Class <code>
                                        border-primary</code>
                                </div>
                                <div class="alert alert-success border-success">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Success!</strong> add Class <code>
                                        border-success</code>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="alert alert-info border-info">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Info!</strong> add Class <code>
                                        border-info</code>
                                </div>
                                <div class="alert alert-warning border-warning">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Warning!</strong> add Class <code>
                                        border-warning</code>
                                </div>
                                <div class="alert alert-danger border-danger">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <strong>Danger!</strong> Add Class <code>
                                        border-danger</code>
                                </div>
                            </div>
                        </div>


                        <div class="sub-title">Solid Border Alert</div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="alert alert-default background-default">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <strong>Default!</strong> Add Class <code>
                                        background-default</code>
                                </div>
                                <div class="alert alert-primary background-primary">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <strong>Primary!</strong> Add Class <code>
                                        background-primary</code>
                                </div>
                                <div class="alert alert-success background-success">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <strong>Success!</strong> Add Class <code>
                                        background-success</code>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="alert alert-info background-info">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <strong>Info!</strong> Add Class <code>
                                        background-info</code>
                                </div>
                                <div class="alert alert-warning background-warning">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <strong>Warning!</strong> Add Class <code>
                                        background-warning</code>
                                </div>
                                <div class="alert alert-danger background-danger">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <strong>Danger!</strong> Add Class <code>
                                        background-danger</code>
                                </div>
                            </div>
                        </div>


                        <div class="sub-title">Icon Alert</div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="alert alert-default icons-alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <p><strong>Default!</strong> Add Class <code>alert-default
                                            icons-alert</code></p>
                                </div>
                                <div class="alert alert-primary icons-alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <p><strong>Primary!</strong> Add Class <code>alert-primary
                                            icons-alert</code></p>
                                </div>
                                <div class="alert alert-success icons-alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <p><strong>Success!</strong> Add Class <code>alert-success
                                            icons-alert</code></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="alert alert-info icons-alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <p><strong>Info!</strong> Add Class <code>alert-info
                                            icons-alert</code></p>
                                </div>
                                <div class="alert alert-warning icons-alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <p><strong>Warning!</strong> Add Class <code>alert-warning
                                            icons-alert</code></p>
                                </div>
                                <div class="alert alert-danger icons-alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <p><strong>Danger!</strong> Add Class <code>alert-danger
                                            icons-alert</code></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

