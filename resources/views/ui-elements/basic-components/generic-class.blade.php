@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Generic class</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Basic Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Generic Class</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-md-6">

                <div class="card">
                    <div class="card-header p-b-0">
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#margin" role="tab">Margin</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#padding" role="tab">Padding</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="card-block tab-content p-t-20">
                        <div class="tab-pane fade show active" id="margin" role="tabpanel">
                            <div class="generic-card-body">
                                <h6 class="sub-title">Margin </h6>
                                <code data-toggle="tooltip" data-placement="top" title="margin-0" data-original-title="margin-0">.m-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-5">.m-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-10">.m-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-15">.m-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-20">.m-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-25">.m-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-30">.m-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-40">.m-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-50">.m-50</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Margin Top </h6>
                                <code data-toggle="tooltip" data-placement="top" title="margin-top-0">.m-t-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-top-5">.m-t-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-top-10">.m-t-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-top-15">.m-t-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-top-20">.m-t-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-top-25">.m-t-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-top-30">.m-t-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-top-40">.m-t-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-top-50">.m-t-50</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Margin Bottom </h6>
                                <code data-toggle="tooltip" data-placement="top" title="margin-bottom-0">.m-b-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-bottom-5">.m-b-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-bottom-10">.m-b-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-bottom-15">.m-b-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-bottom-20">.m-b-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-bottom-25">.m-b-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-bottom-30">.m-b-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-bottom-40">.m-b-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-bottom-50">.m-b-50</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Margin Left </h6>
                                <code data-toggle="tooltip" data-placement="top" title="margin-left-0">.m-l-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-left-5">.m-l-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-left-10">.m-l-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-left-15">.m-l-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-left-20">.m-l-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-left-25">.m-l-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-left-30">.m-l-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-left-40">.m-l-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-left-50">.m-l-50</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Margin right </h6>
                                <code data-toggle="tooltip" data-placement="top" title="margin-right-0">.m-r-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-right-5">.m-r-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-right-10">.m-r-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-right-15">.m-r-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-right-20">.m-r-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-right-25">.m-r-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-right-30">.m-r-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-right-40">.m-r-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="margin-right-50">.m-r-50</code>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="padding" role="tabpanel">
                            <div class="generic-card-body">
                                <h6 class="sub-title">Padding </h6>
                                <code data-toggle="tooltip" data-placement="top" title="padding-0">.p-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-5">.p-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-10">.p-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-15">.p-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-20">.p-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-25">.p-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-30">.p-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-40">.p-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-50">.p-50</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Padding Top </h6>
                                <code data-toggle="tooltip" data-placement="top" title="padding-top-0">.p-t-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-top-5">.p-t-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-top-10">.p-t-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-top-15">.p-t-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-top-20">.p-t-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-top-25">.p-t-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-top-30">.p-t-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-top-40">.p-t-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-top-50">.p-t-50</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Padding Bottom </h6>
                                <code data-toggle="tooltip" data-placement="top" title="padding-bottom-0">.p-b-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-bottom-5">.p-b-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-bottom-10">.p-b-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-bottom-15">.p-b-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-bottom-20">.p-b-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-bottom-25">.p-b-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-bottom-30">.p-b-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-bottom-40">.p-b-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-bottom-50">.p-b-50</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Padding Left </h6>
                                <code data-toggle="tooltip" data-placement="top" title="padding-left-0">.p-l-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-left-5">.p-l-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-left-10">.p-l-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-left-15">.p-l-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-left-20">.p-l-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-left-25">.p-l-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-left-30">.p-l-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-left-40">.p-l-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-left-50">.p-l-50</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Padding right </h6>
                                <code data-toggle="tooltip" data-placement="top" title="padding-right-0">.p-r-0</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-right-5">.p-r-5</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-right-10">.p-r-10</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-right-15">.p-r-15</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-right-20">.p-r-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-right-25">.p-r-25</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-right-30">.p-r-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-right-40">.p-r-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="padding-right-50">.p-r-50</code>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">

                <div class="card">
                    <div class="card-header p-b-0">
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#font-style" role="tab">Font</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#text-style" role="tab">Alignment</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="card-block tab-content p-t-20">
                        <div class="tab-pane fade show active" id="font-style" role="tabpanel">
                            <div class="generic-card-body">
                                <h6 class="sub-title">Font-size </h6>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-12">.f-12</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-14">.f-14</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-16">.f-16</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-18">.f-18</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-20">.f-20</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-24">.f-24</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-26">.f-26</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-28">.f-28</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-30">.f-30</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-35">.f-35</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-40">.f-40</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-45">.f-45</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-52">.f-52</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-size-64">.f-64</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Font-Weight </h6>
                                <code data-toggle="tooltip" data-placement="top" title="font-weight-100">
                                    .f-w-100</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-weight-300">
                                    .f-w-300</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-weight-400">
                                    .f-w-400</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-weight-600">
                                    .f-w-600</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-weight-700">
                                    .f-w-700</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-weight-900">
                                    .f-w-900</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Font-Style </h6>
                                <code data-toggle="tooltip" data-placement="top" title="font-normal">
                                    .f-s-normal</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-italic">
                                    .f-s-italic</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-oblique">
                                    .f-s-oblique</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-intial">
                                    .f-s-intial</code>
                                <code data-toggle="tooltip" data-placement="top" title="font-inherit">
                                    .f-s-inherit</code>
                            </div>
                        </div>
                        <div class="tab-pane" id="text-style" role="tabpanel">
                            <div class="generic-card-body">
                                <h6 class="sub-title">Text-align </h6>
                                <code data-toggle="tooltip" data-placement="top" title="Center-Text">
                                    .text-center</code>
                                <code data-toggle="tooltip" data-placement="top" title="Left-Text">
                                    .text-left</code>
                                <code data-toggle="tooltip" data-placement="top" title="Right-Text">
                                    .text-right</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Text-transform </h6>
                                <code data-toggle="tooltip" data-placement="top" title="All Capital Text">
                                    .text-uppercase</code>
                                <code data-toggle="tooltip" data-placement="top" title="Lower case Text">
                                    .text-lowercase</code>
                                <code data-toggle="tooltip" data-placement="top" title="Capitalize Text">
                                    .text-capitalize</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Text-decoration </h6>
                                <code data-toggle="tooltip" data-placement="top" title="Overline">
                                    .text-overline</code>
                                <code data-toggle="tooltip" data-placement="top" title="Line-Throught Text">
                                    .text-through</code>
                                <code data-toggle="tooltip" data-placement="top" title="Underline">
                                    .text-underline</code>
                                <br>
                                <br>
                                <h6 class="sub-title">Vertical-align </h6>
                                <code data-toggle="tooltip" data-placement="top" title="All Capital Text">
                                    .baseline</code>
                                <code data-toggle="tooltip" data-placement="top" title="Lower case Text">
                                    .sub</code>
                                <code data-toggle="tooltip" data-placement="top" title="All Capital Text">
                                    .super</code>
                                <code data-toggle="tooltip" data-placement="top" title="Lower case Text">
                                    .top</code>
                                <code data-toggle="tooltip" data-placement="top" title="Capitalize Text">
                                    .text-top</code>
                                <code data-toggle="tooltip" data-placement="top" title="All Capital Text">
                                    .middle</code>
                                <code data-toggle="tooltip" data-placement="top" title="Lower case Text">
                                    .bottom</code>
                                <code data-toggle="tooltip" data-placement="top" title="Capitalize Text">
                                    .text-bottom</code>
                                <code data-toggle="tooltip" data-placement="top" title="Capitalize Text">
                                    .initial</code>
                                <code data-toggle="tooltip" data-placement="top" title="Capitalize Text">
                                    .inherit</code>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header p-b-0">
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#position" role="tab">Position</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#float" role="tab">Float</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#overflow" role="tab">Overflow</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="card-block tab-content p-t-20">
                        <div class="tab-pane fade show active" id="position" role="tabpanel">
                            <div class="generic-card-body">
                                <h6 class="sub-title">Position </h6>
                                <code data-toggle="tooltip" data-placement="top" title="Realtive">.pos-static</code>
                                <code data-toggle="tooltip" data-placement="top" title="Absolute">.pos-absolute</code>
                                <code data-toggle="tooltip" data-placement="top" title="Static">.pos-relative</code>
                                <code data-toggle="tooltip" data-placement="top" title="Fixed">.pos-fixed</code>
                                <code data-toggle="tooltip" data-placement="top" title="Fixed">.pos-intial</code>
                                <code data-toggle="tooltip" data-placement="top" title="Fixed">.pos-inherit</code>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="float" role="tabpanel">
                            <div class="generic-card-body">
                                <h6 class="sub-title">Float </h6>
                                <code data-toggle="tooltip" data-placement="top" title="FLoat-left">.f-left</code>
                                <code data-toggle="tooltip" data-placement="top" title="Float-right">.f-right</code>
                                <code data-toggle="tooltip" data-placement="top" title="Float-none">.f-none</code>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="overflow" role="tabpanel">
                            <div class="generic-card-body">
                                <h6 class="sub-title">Overflow </h6>
                                <code data-toggle="tooltip" data-placement="top" title="Overflow Hidden">.o-hidden</code>
                                <code data-toggle="tooltip" data-placement="top" title="Overflow Auto">.o-auto</code>
                                <code data-toggle="tooltip" data-placement="top" title="Overflow Visible">.o-visible</code>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header p-b-0">
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#bg-color" role="tab">BackGround-color</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#text-color" role="tab">Text-color</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="card-block tab-content p-t-20">
                        <div class="tab-pane fade show active" id="bg-color" role="tabpanel">
                            <div class="generic-card-body">
                                <h6 class="sub-title">Background color</h6>
                                <div data-toggle="tooltip" data-placement="top" title="" class="bg-color-box color-default waves-effect waves-light" data-original-title="#ff4444">
                                    <span>.color-default</span>
                                </div>
                                <div data-toggle="tooltip" data-placement="top" title="" class="bg-color-box color-primary waves-effect waves-light" data-original-title="#ff4444">
                                    <span>.color-primary</span>
                                </div>
                                <div data-toggle="tooltip" data-placement="top" title="" class="bg-color-box color-success waves-effect waves-light" data-original-title="#ff4444">
                                    <span>.color-success</span>
                                </div>
                                <div data-toggle="tooltip" data-placement="top" title="" class="bg-color-box color-info waves-effect waves-light" data-original-title="#ff4444">
                                    <span>.color-info</span>
                                </div>
                                <div data-toggle="tooltip" data-placement="top" title="" class="bg-color-box color-warning waves-effect waves-light" data-original-title="#ff4444">
                                    <span>.color-warning</span>
                                </div>
                                <div data-toggle="tooltip" data-placement="top" title="" class="bg-color-box color-danger waves-effect waves-light" data-original-title="#ff4444">
                                    <span>.color-danger</span>
                                </div>
                                <div data-toggle="tooltip" data-placement="top" title="" class="bg-color-box color-inverse waves-effect waves-light" data-original-title="#ff4444">
                                    <span>.color-inverse</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show" id="text-color" role="tabpanel">
                            <div class="generic-card-body">
                                <h6 class="sub-title">Text color</h6>
                                <code data-toggle="tooltip" data-placement="top" title="" data-original-title="#337ab7">.text-primary</code>
                                <code data-toggle="tooltip" data-placement="top" title="" data-original-title="#777">.text-muted</code>
                                <code data-toggle="tooltip" data-placement="top" title="" data-original-title="#3c763d">.text-success</code>
                                <code data-toggle="tooltip" data-placement="top" title="" data-original-title="#31708f">.text-info</code>
                                <code data-toggle="tooltip" data-placement="top" title="" data-original-title="#8a6d3b">.text-warning</code>
                                <code data-toggle="tooltip" data-placement="top" title="" data-original-title="#a94442">.text-danger</code>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#img-size" role="tab">Image size</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="card-block tab-content p-t-20">
                        <div class="tab-pane fade show active" id="img-size" role="tabpanel">
                            <div class="generic-image-body">
                                <h6 class="sub-title">Image Size </h6>
                                <ul>
                                    <li>
                                        <code data-toggle="tooltip" data-placement="top" title="Realtive">.img-20</code>
                                        <img src="{{ asset("adminity/images/user.png") }}" class="img-20" alt="user.png">
                                    </li>
                                    <li>
                                        <code data-toggle="tooltip" data-placement="top" title="Realtive">.img-30</code>
                                        <img src="{{ asset("adminity/images/user.png") }}" class="img-30" alt="user.png">
                                    </li>
                                    <li>
                                        <code data-toggle="tooltip" data-placement="top" title="Realtive">.img-40</code>
                                        <img src="{{ asset("adminity/images/user.png") }}" class="img-40" alt="user.png">
                                    </li>
                                    <li>
                                        <code data-toggle="tooltip" data-placement="top" title="Realtive">.img-50</code>
                                        <img src="{{ asset("adminity/images/user.png") }}" class="img-50" alt="user.png">
                                    </li>
                                    <li>
                                        <code data-toggle="tooltip" data-placement="top" title="Realtive">.img-60</code>
                                        <img src="{{ asset("adminity/images/user.png") }}" class="img-60" alt="user.png">
                                    </li>
                                    <li>
                                        <code data-toggle="tooltip" data-placement="top" title="Realtive">.img-70</code>
                                        <img src="{{ asset("adminity/images/user.png") }}" class="img-70" alt="user.png">
                                    </li>
                                    <li>
                                        <code data-toggle="tooltip" data-placement="top" title="Realtive">.img-80</code>
                                        <img src="{{ asset("adminity/images/user.png") }}" class="img-80" alt="user.png">
                                    </li>
                                    <li>
                                        <code data-toggle="tooltip" data-placement="top" title="Realtive">.img-90</code>
                                        <img src="{{ asset("adminity/images/user.png") }}" class="img-90" alt="user.png">
                                    </li>
                                    <li>
                                        <code data-toggle="tooltip" data-placement="top" title="Realtive">.img-100</code>
                                        <img src="{{ asset("adminity/images/user.png") }}" class="img-100" alt="user.png">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

