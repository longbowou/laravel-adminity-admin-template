@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Box Shadow</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Basic Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Box Shadow</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Top Only</h5>
            </div>
            <div class="card-block box-list">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-top-0 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-top-0">
                            <p class="text-sm-center">Use class <code>z-depth-top-0</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-top-1 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-top-1">
                            <p class="text-sm-center ">Use class <code>z-depth-top-1</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-top-2 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-top-2">
                            <p class="text-sm-center">Use class <code>z-depth-top-2</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-top-3 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-top-3">
                            <p class="text-sm-center">Use class <code>z-depth-top-3</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-top-4 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-top-4">
                            <p class="text-sm-center">Use class <code>z-depth-top-4</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-top-5 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-top-5">
                            <p class="text-sm-center">Use class <code>z-depth-top-5</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Bottom Only</h5>
            </div>
            <div class="card-block box-list">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-bottom-0 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-bottom-0">
                            <p class="text-sm-center">Use class <code>z-depth-bottom-0</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-bottom-1 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-bottom-1">
                            <p class="text-sm-center ">Use class <code>z-depth-bottom-1</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-bottom-2 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-bottom-2">
                            <p class="text-sm-center">Use class <code>z-depth-bottom-2</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-bottom-3 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-bottom-3">
                            <p class="text-sm-center">Use class <code>z-depth-bottom-3</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-bottom-4 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-bottom-4">
                            <p class="text-sm-center">Use class <code>z-depth-bottom-4</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-bottom-5 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-bottom-5">
                            <p class="text-sm-center">Use class <code>z-depth-bottom-5</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Left Only</h5>
            </div>
            <div class="card-block box-list">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-left-0 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-left-0">
                            <p class="text-sm-center">Use class <code>z-depth-left-0</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-left-1 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-left-1">
                            <p class="text-sm-center ">Use class <code>z-depth-left-1</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-left-2 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-left-2">
                            <p class="text-sm-center">Use class <code>z-depth-left-2</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-left-3 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-left-3">
                            <p class="text-sm-center">Use class <code>z-depth-left-3</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-left-4 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-left-4">
                            <p class="text-sm-center">Use class <code>z-depth-left-4</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-left-5 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-left-5">
                            <p class="text-sm-center">Use class <code>z-depth-left-5</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Right Only</h5>
            </div>
            <div class="card-block box-list">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-right-0 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-right-0">
                            <p class="text-sm-center">Use class <code>z-depth-right-0</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-right-1 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-right-1">
                            <p class="text-sm-center ">Use class <code>z-depth-right-1</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-right-2 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-right-2">
                            <p class="text-sm-center">Use class <code>z-depth-right-2</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-right-3 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-right-3">
                            <p class="text-sm-center">Use class <code>z-depth-right-3</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-right-4 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-right-4">
                            <p class="text-sm-center">Use class <code>z-depth-right-4</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-right-5 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-right-5">
                            <p class="text-sm-center">Use class <code>z-depth-right-5</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>All Side</h5>
            </div>
            <div class="card-block box-list">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-0 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-0">
                            <p class="text-sm-center">Use class <code>z-depth-0</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-1 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-1">
                            <p class="text-sm-center ">Use class <code>z-depth-1</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-2 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-2">
                            <p class="text-sm-center">Use class <code>z-depth-2</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-3 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-3">
                            <p class="text-sm-center">Use class <code>z-depth-3</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-4 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-4">
                            <p class="text-sm-center">Use class <code>z-depth-4</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="p-20 z-depth-5 waves-effect" data-toggle="tooltip" data-placement="top" title=".z-depth-5">
                            <p class="text-sm-center">Use class <code>z-depth-5</code> inside div element to add box-shadow.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

