@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Animated Tooltip</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Basic Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Animated Tooltip</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip Style 1</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-1</code> to use this
                            <span class="mytooltip tooltip-effect-1">
<span class="tooltip-item">Show Effect</span>
<span class="tooltip-content clearfix">
<img src="{{ asset("adminity/images/tooltip/Euclid.png") }}" alt="Ecluid.png">
<span class="tooltip-text">Also known as Euclid of andria, was a Greek mathematician, often referred.</span>
</span>
</span>
                            current effect.</p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip Style 2</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-2</code> to use this
                            <span class="mytooltip tooltip-effect-2">
<span class="tooltip-item">Show Effect</span>
<span class="tooltip-content clearfix">
<img src="{{ asset("adminity/images/tooltip/Euclid.png") }}" alt="Ecluid.png">
<span class="tooltip-text">Also known as Euclid of andria, was a Greek mathematician, often referred.</span>
</span>
</span>
                            current effect.</p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip Style 4</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-4</code> to use this
                            <span class="mytooltip tooltip-effect-4">
<span class="tooltip-item">Show Effect</span>
<span class="tooltip-content clearfix">
<img src="{{ asset("adminity/images/tooltip/Euclid.png") }}" alt="Ecluid.png">
<span class="tooltip-text">Also known as Euclid of andria, was a Greek mathematician, often referred.</span>
</span>
</span>
                            current effect.</p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip Style 5</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-5</code> to use this
                            <span class="mytooltip tooltip-effect-5">
<span class="tooltip-item">Show Effect</span>
<span class="tooltip-content clearfix">
<span class="tooltip-text">Also known as Euclid of andria, was a Greek mathematician, often referred.</span>
</span>
</span>
                            current effect.</p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip Style 6</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-6 tooltip-content-2 </code> to use this <a
                                class="mytooltip tooltip-effect-6" href="#">Home<span class="tooltip-content2"><i
                                        class="icofont icofont-home"></i></span></a> current effect. </p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip Style 7</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-7 tooltip-content-2</code> to use this <a
                                class="mytooltip tooltip-effect-7" href="#">About me<span class="tooltip-content2"><i
                                        class="icofont icofont-user-alt-3"></i></span></a> current effect. </p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip Style 8</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-8 tooltip-content-2</code> to use this <a
                                class="mytooltip tooltip-effect-8" href="#">Photography<span class="tooltip-content2"><i
                                        class="icofont icofont-ui-camera"></i></span></a> current effect. </p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip Style 9</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-9 tooltip-content-2</code> to use this <a
                                class="mytooltip tooltip-effect-9" href="#">Work<span class="tooltip-content2"><i
                                        class="icofont icofont-bag-alt"></i></span></a> current effect. </p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Bloated Tooltip</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-9 tooltip-content-3</code> to use this <a
                                class="mytooltip tooltip-effect-9" href="#">Car<span class="tooltip-content3">You can easily navigate the city by car.</span></a>
                            current effect. </p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Bloated Tooltip</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-9 tooltip-content-3</code> to use this <a
                                class="mytooltip tooltip-effect-9" href="#">Cycle<span class="tooltip-content3">You can easily navigate the city by car.</span></a>
                            current effect. </p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Bloated Tooltip</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-9 tooltip-content-3</code> to use this <a
                                class="mytooltip tooltip-effect-9" href="#">Car<span class="tooltip-content3">You can easily navigate the city by car.</span></a>
                            current effect. </p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Bloated Tooltip</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-9 tooltip-content-3</code> to use this <a
                                class="mytooltip tooltip-effect-9" href="#">Cycle<span class="tooltip-content3">You can easily navigate the city by car.</span></a>
                            current effect. </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Box Tooltip</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-effect-1 tooltip-content4</code> to use this
                            <span class="mytooltip tooltip-effect-1">
<span class="tooltip-item2">Euclid</span>
<span class="tooltip-content4 clearfix">
<span class="tooltip-text2">
<strong>Euclid</strong>, also known as Euclid of Alexandria, was a Greek mathematician, often referred to as the "Father of Geometry". He was active in Alexandria during the reign of Ptolemy I.
<a href="#">Wikipedia</a>
</span>
</span>
</span>
                            current effect. rogue laws of physics, star stuff harvesting star light, <span
                                class="mytooltip tooltip-effect-2"><span class="tooltip-item2">quasar</span><span
                                    class="tooltip-content4 clearfix"><span
                                        class="tooltip-text2"><strong>Quasars</strong> are believed to be powered by accretion of material into supermassive black holes in the nuclei of distant galaxies, making these luminous versions of the general... <a
                                            href="#">Wikipedia</a></span></span>
</span> encyclopaedia galactica are creatures of the cosmos the only home we've ever known ship of the imagination prime
                            number courage of our questions.
                        </p>
                        <p>Colonies. Jean-François Champollion, billions upon billions descended from astronomers the
                            sky calls to us! Made in the interiors of collapsing stars, billions upon billions radio
                            telescope paroxysm of global death not a sunrise but a galaxyrise, gathered by gravity
                            permanence of the stars?</p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card button-page">
                    <div class="card-header">
                        <h5>Tooltip On Button</h5>
                    </div>
                    <div class="card-block">
                        <ul>
                            <li>
                                <button type="button" class="btn btn-default waves-effect" data-toggle="tooltip"
                                        data-placement="top" title="tooltip on top">Top
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-primary waves-effect waves-light"
                                        data-toggle="tooltip" data-placement="left" title="tooltip on left">Left
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-success waves-effect waves-light"
                                        data-toggle="tooltip" data-placement="right" title="tooltip on right">right
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-warning waves-effect waves-light"
                                        data-toggle="tooltip" data-placement="bottom" title="tooltip on bottom">bottom
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-info waves-effect waves-light"
                                        data-toggle="tooltip" data-html="true"
                                        title="<em>Tooltip</em> <u>with</u> <b>HTML</b>">Html Tooltip
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Line Tooltip</h5>
                    </div>
                    <div class="card-block">
                        <p>Use class <code>tooltip-content4</code> to use this <a class="mytooltip"
                                                                                  href="javascript:void(0)"> Line
                                tooltip<span class="tooltip-content5"><span class="tooltip-text3"><span
                                            class="tooltip-inner2">Howdy, Ben!<br> There are 13 unread messages in your inbox.</span></span></span></a>
                            current effect. harvesting star light.Colonies. Jean-François Champollion.</p>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip With Link</h5>
                    </div>
                    <div class="card-block tooltip-link">
                        <a href="#!" data-toggle="tooltip" data-placement="top" data-trigger="hover"
                           title="top!">Top</a>
                        <a href="#!" data-toggle="tooltip" data-trigger="hover" data-placement="bottom" title="bottom!">Bottom</a>
                        <a href="#!" data-placement="left" data-trigger="hover" data-toggle="tooltip"
                           title="left!">Left</a>
                        <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="right!">Right</a>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip On Icon</h5>
                    </div>
                    <div class="card-block tooltip-icon button-list">
                        <p>use code in button for tooltip <code>data-toggle="tooltip" data-placement="left"
                                data-original-title=".icofont-home"</code></p>
                        <button type="button" class="btn btn-primary btn-icon waves-effect waves-light"
                                data-toggle="tooltip" data-placement="left" title=".icofont-home">
                            <i class="icofont icofont-home"></i>
                        </button>
                        <button type="button" class="btn btn-danger btn-icon waves-effect waves-light"
                                data-toggle="tooltip" data-placement="top" title=".icofont icofont-search-alt-2">
                            <i class="icofont icofont-search-alt-2"></i>
                        </button>
                        <button type="button" class="btn btn-warning btn-icon waves-effect waves-light"
                                data-toggle="tooltip" data-placement="right" title=".icofont-refresh">
                            <i class="icofont icofont-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-info btn-icon waves-effect waves-light"
                                data-toggle="tooltip" data-placement="top" title=".icofont-print">
                            <i class="icofont icofont-print"></i>
                        </button>
                        <button type="button" class="btn btn-success btn-icon waves-effect waves-light"
                                data-toggle="tooltip" data-placement="bottom" title=".icofont-paper-plane">
                            <i class="icofont icofont-paper-plane"></i>
                        </button>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltip On Popover</h5>
                    </div>
                    <div class="card-block tooltip-pop button-list">
                        <p>use code in button for tooltip <code>data-toggle="popover" data-placement="left"
                                data-original-title=".icofont-home"</code></p>
                        <button type="button" class="btn btn-default waves-effect" data-toggle="popover"
                                data-placement="top" title="" data-content="top by popover"
                                data-original-title="tooltip on top">Top
                        </button>
                        <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="popover"
                                data-placement="left" title="tooltip on left" data-content="left by popover">Left
                        </button>
                        <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="popover"
                                data-placement="right" title="tooltip on right" data-content="right by popover">right
                        </button>
                        <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="popover"
                                data-placement="bottom" title="tooltip on bottom" data-content="bottom by popover">
                            bottom
                        </button>
                        <button type="button" class="btn btn-info waves-effect waves-light" data-toggle="popover"
                                data-html="true" data-placement="top" title="<em>Tooltip</em> <u>with</u> <b>HTML</b>"
                                data-content="tooltip by HTML">Html Tooltip
                        </button>
                    </div>
                </div>

            </div>
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Tooltips On Textbox</h5>
                    </div>
                    <div class="card-block tooltip-icon button-list">
                        <div class="input-group">
                            <span class="input-group-addon" id="name"><i class="icofont icofont-user-alt-3"></i></span>
                            <input type="text" class="form-control" placeholder="Enter your name"
                                   title="Enter your name" data-toggle="tooltip">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon" id="email"><i class="icofont icofont-ui-email"></i></span>
                            <input type="text" class="form-control" placeholder="Enter email" title="Enter email"
                                   data-toggle="tooltip">
                        </div>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-r-20"
                                data-toggle="tooltip" data-placement="right" title="submit">Submit
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

