@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Color</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Basic Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Color</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Default Color</h5>
            </div>
            <div class="card-block">
                <div class="row text-uppercase text-center">
                    <div class="col-md-2 waves-effect waves-light p-b-10">
                        <div class="bg-default p-10">#BDC3C7</div>
                    </div>
                    <div class="col-md-2 waves-effect waves-light p-b-10">
                        <div class="bg-primary p-10 ">#1ABC9C</div>
                    </div>
                    <div class="col-md-2 waves-effect waves-light p-b-10">
                        <div class="bg-success p-10">#2ECC71</div>
                    </div>
                    <div class="col-md-2 waves-effect waves-light p-b-10">
                        <div class="bg-info p-10">#3498DB</div>
                    </div>
                    <div class="col-md-2 waves-effect waves-light p-b-10">
                        <div class="bg-warning p-10">#F1C40F</div>
                    </div>
                    <div class="col-md-2 waves-effect waves-light p-b-10">
                        <div class="bg-danger p-10">#E74C3C</div>
                    </div>
                    <div class="col-md-2 waves-effect waves-light p-b-10">
                        <div class="bg-inverse p-10">#34495E</div>
                    </div>
                </div>

            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h5>Colors</h5>
            </div>
            <div class="card-block">

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">red</h6>
                        <div class="red-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">pink</h6>
                        <div class="pink-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase"> purple</h6>
                        <div class="purple-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">deep purple</h6>
                        <div class="deep-purple-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">indigo</h6>
                        <div class="indigo-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase"> blue</h6>
                        <div class="blue-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">light blue</h6>
                        <div class="light-blue-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">cyan</h6>
                        <div class="cyan-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">teal</h6>
                        <div class="teal-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">green</h6>
                        <div class="green-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">light green</h6>
                        <div class="light-green-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">lime</h6>
                        <div class="lime-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">yellow</h6>
                        <div class="yellow-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">amber</h6>
                        <div class="amber-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">orange</h6>
                        <div class="orange-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">deep orange</h6>
                        <div class="deep-orange-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">brown</h6>
                        <div class="brown-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">grey</h6>
                        <div class="grey-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">blue grey</h6>
                        <div class="blue-grey-colors">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">Primary color</h6>
                        <div class="primary-colorr">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">Success color</h6>
                        <div class="success-colorr">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">Info color</h6>
                        <div class="info-colorr">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">Warning color</h6>
                        <div class="warning-colorr">
                            <ul class="m-b-20">
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="sub-title text-uppercase">Danger color</h6>
                        <div class="danger-colorr">
                            <ul>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                                <li>
                                    <p></p>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

