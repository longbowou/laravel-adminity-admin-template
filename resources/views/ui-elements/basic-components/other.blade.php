@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/jqpagination/jqpagination.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Other</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Basic Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Other</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Panel</h5>
                    </div>
                    <div class="card-block panels-wells">
                        <div class="row">
                            <div class="col">
                                <div class="panel panel-default">
                                    <div class="panel-heading bg-default txt-white">
                                        Default Panel
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt
                                            est vitae ultrices accumsan.</p>
                                    </div>
                                    <div class="panel-footer">
                                        Panel Footer
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                                <div class="panel panel-success">
                                    <div class="panel-heading bg-success">
                                        Success Panel
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt
                                            est vitae ultrices accumsan.</p>
                                    </div>
                                    <div class="panel-footer text-success">
                                        Panel Footer
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading bg-primary">
                                        Primary Panel
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt
                                            est vitae ultrices accumsan.</p>
                                    </div>
                                    <div class="panel-footer text-primary">
                                        Panel Footer
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                                <div class="panel panel-danger">
                                    <div class="panel-heading bg-danger">
                                        Danger Panel
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt
                                            est vitae ultrices accumsan.</p>
                                    </div>
                                    <div class="panel-footer text-danger">
                                        Panel Footer
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                                <div class="panel panel-warning">
                                    <div class="panel-heading bg-warning">
                                        Warning Panel
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt
                                            est vitae ultrices accumsan.</p>
                                    </div>
                                    <div class="panel-footer text-warning">
                                        Panel Footer
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading bg-info">
                                        Info Panel
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt
                                            est vitae ultrices accumsan.</p>
                                    </div>
                                    <div class="panel-footer text-info">
                                        Panel Footer
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Wells</h5>
                    </div>
                    <div class="card-block panels-wells">
                        <div class="row">
                            <div class="col">
                                <div class="well well-sm">
                                    Small Well
                                </div>
                                <div class="well">
                                    Normal Well
                                </div>
                                <div class="well well-lg">
                                    Large Well
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Pagination</h5>
                    </div>
                    <div class="card-block">
                        <div class="sub-title">Basic</div>
                        <div class="jqpagination m-b-10 pagination">
                            <a href="#" class="first disabled" data-action="first">«</a>
                            <a href="#" class="previous disabled" data-action="previous">‹</a>
                            <input type="text" data-max-page="40" class="m-t-5">
                            <a href="#" class="next" data-action="next">›</a>
                            <a href="#" class="last" data-action="last">»</a>
                        </div>
                        <div class="sub-title">Bootstrap</div>
                        <nav aria-label="...">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Pagination With Icon</h5>
                    </div>
                    <div class="card-block">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Pagination Large</h5>
                    </div>
                    <div class="card-block">
                        <nav aria-label="...">
                            <ul class="pagination pagination-lg">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Pagination Small</h5>
                    </div>
                    <div class="card-block">
                        <nav aria-label="...">
                            <ul class="pagination pagination-sm">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Pagination Alignment Center</h5>
                    </div>
                    <div class="card-block">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Pagination Alignment End</h5>
                    </div>
                    <div class="card-block">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

