@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Idle Timeout</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Extra Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Idle Timeout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Session Idle Timeout</h5>
                    </div>
                    <div class="card-block">
                        <p>
                            Current settings allow you to track user inactivity and launch a warning dialog in a fixed
                            amount of time after latest user activity. In this demo warning dialog appears <strong>after
                                5 seconds</strong> of latest user activity.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/pages/session-idle-Timeout/session_timeout.min.js") }}"></script>
    <script type="text/javascript"
            src="{{ asset("adminity/pages/session-idle-Timeout/session-idle-Timeout-custom.js") }}"></script>
@endsection


