@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/offline/css/offline-theme-slide.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/offline/css/offline-language-english.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Offline</h4>
                        <span>Automatically display online/offline indication</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Extra Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Offline</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Offline Indicator</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="sub-title">Default</h4>
                                <div class="browser offline-box">
                                    <iframe data-theme="default"></iframe>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4 class="sub-title">Slide</h4>
                                <div class="browser offline-box">
                                    <iframe data-theme="slide"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="sub-title">Dark</h4>
                                <div class="browser offline-box">
                                    <iframe data-theme="dark"></iframe>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4 class="sub-title">Chrome</h4>
                                <div class="browser offline-box">
                                    <iframe data-theme="chrome"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Indicator Theme</h5>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="sub-title">Default</h4>
                                <div class="browser offline-box">
                                    <iframe data-theme="default-indicator"></iframe>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4 class="sub-title">Slide</h4>
                                <div class="browser offline-box">
                                    <iframe data-theme="slide-indicator"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="sub-title">Dark</h4>
                                <div class="browser offline-box">
                                    <iframe data-theme="dark-indicator"></iframe>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4 class="sub-title">Chrome</h4>
                                <div class="browser offline-box">
                                    <iframe data-theme="chrome-indicator"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/components/offline/js/offline.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/offline/offline-custom.js") }}"></script>@endsection


