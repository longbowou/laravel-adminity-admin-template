@extends("layouts.app")

@section("style")
    <link href="{{ asset("adminity/pages/jquery.filer/css/jquery.filer.css") }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset("adminity/pages/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css") }}"
          type="text/css"
          rel="stylesheet"/>
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>File Upload</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route('dashboard') }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Widget</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>File Upload</h5>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div class="sub-title">Example 1</div>
                <input type="file" name="files[]" id="filer_input1" multiple="multiple">
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Image Upload</h5>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div class="sub-title">Example 2</div>
                <input type="file" name="files[]" id="filer_input" multiple="multiple">
            </div>
        </div>

    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/pages/jquery.filer/js/jquery.filer.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/filer/custom-filer.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/filer/jquery.fileuploads.init.js") }}" type="text/javascript"></script>
@endsection

