@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Wysiwyg Editors</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Editor</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Wysiwyg Editor</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Wysiwyg (What you see is what you get)</h5>
                        <span>This editor helps you to edit your content easily</span>
                    </div>
                    <div class="card-block">
<textarea>
                                    <p>hello..</p>
                                </textarea>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/pages/wysiwyg-editor/js/tinymce.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/wysiwyg-editor/wysiwyg-editor.js") }}" type="text/javascript"></script>
@endsection

