@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/long-press/css/main.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/long-press/css/app.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Long Press Editor</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Editor</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Long Press Editor</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Long Press Any Alphabet</h5>
                        <span>Write in the above field and <strong>hold key</strong> to access alternate characters.<br/>
Try ‘a’, ‘e’, ‘u’, ‘i’, ‘o’ for example.</span>
                    </div>
                    <div class="card-block">
                        <textarea class="long-press form-control" id="ta" placeholder="Hello"></textarea>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="e0354c4e4c9f744e79143308-text/javascript"
            src="{{ asset("adminity/components/long-press/js/jquery.mousewheel.js") }}"></script>
    <script type="e0354c4e4c9f744e79143308-text/javascript"
            src="{{ asset("adminity/components/long-press/js/plugins.js") }}"></script>
    <script type="e0354c4e4c9f744e79143308-text/javascript"
            src="{{ asset("adminity/components/long-press/js/jquery.longpress.js") }}"></script>
    <script type="e0354c4e4c9f744e79143308-text/javascript"
            src="{{ asset("adminity/components/long-press/js/app.js") }}"></script>
@endsection

