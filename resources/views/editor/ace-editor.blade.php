@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Ace-Editor</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Editor</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ace-Editor</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Ace Editor</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="controls">
                            <label for="mode">Language Mode:</label>
                            <select id="mode">
                                <option>php</option>
                                <option>javascript</option>
                                <option>json</option>
                                <option>xml</option>
                            </select>
                        </div>
                        <div id="editor"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/ace-editor/build/aui/aui.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/ace-editor/ace-editor-custom.js") }}"></script>
@endsection

