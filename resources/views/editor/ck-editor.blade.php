@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Ck-Editor</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Editor</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">CK-Editor</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Article Editor</h5>
                <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
<textarea id="editor1">
                                                    &lt;figure class="image image-illustration" style="float:left"&gt; &lt;img alt="" height="266" src="http://c.cksource.com/a/1/img/demo/brownie.jpg" width="400" /&gt; &lt;figcaption&gt;Bon App&amp;eacute;tit!&lt;/figcaption&gt; &lt;/figure&gt; &lt;h2&gt;Brownies&lt;/h2&gt; &lt;h3&gt;Ingredients:&lt;/h3&gt; &lt;ul&gt; &lt;li&gt;½ cup flour&lt;/li&gt; &lt;li&gt;1 cup sugar&lt;/li&gt; &lt;li&gt;½ cup butter, melted&lt;/li&gt; &lt;li&gt;2 eggs&lt;/li&gt; &lt;li&gt;1/3 cup cocoa powder&lt;/li&gt; &lt;/ul&gt; &lt;p&gt;Preheat the oven to &lt;strong&gt;350°F&lt;/strong&gt; and grease the baking pan. Combine the flour, sugar and cocoa powder in a medium bowl. In another small bowl, whisk together the butter and eggs. Stir the two mixtures until just combined. Bake the brownies for 25 to 35 minutes. Remove from the oven and let it cool for 5 minutes. &lt;/p&gt;
                                                </textarea>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Document Editor</h5>
                <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
<textarea id="editor2">
                                                    &lt;h1 style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;span style="color:#006699"&gt;Recognition of Achievement&lt;/span&gt;&lt;/span&gt;&lt;/h1&gt; &lt;p style="text-align:justify"&gt;&lt;span style="font-family:Georgia,serif"&gt;This letter acknowledges the invaluable input &lt;strong&gt;you&lt;/strong&gt;, as a member of our &lt;em&gt;Innovation Team&lt;/em&gt;,&amp;nbsp;have provided in the &amp;ldquo;Implement Rich Text Editor&amp;rdquo;&amp;nbsp;project. The Management would like to hereby thank you for this great accomplishment that was delivered in a timely fashion, up to the highest company standards, and with great results:&lt;/span&gt;&lt;/p&gt; &lt;table border="1" bordercolor="#ccc" cellpadding="5" cellspacing="0" style="border-collapse:collapse;width:100%" summary="Project Schedule"&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope="col" style="background-color:#cccccc"&gt;&lt;span style="font-family:Georgia,serif"&gt;Project Phase&lt;/span&gt;&lt;/th&gt; &lt;th scope="col" style="background-color:#cccccc"&gt;&lt;span style="font-family:Georgia,serif"&gt;Deadline&lt;/span&gt;&lt;/th&gt; &lt;th scope="col" style="background-color:#cccccc"&gt;&lt;span style="font-family:Georgia,serif"&gt;Status&lt;/span&gt;&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;&lt;span style="font-family:Georgia,serif"&gt;Phase 1: Market research&lt;/span&gt;&lt;/td&gt; &lt;td style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;2016-10-15&lt;/span&gt;&lt;/td&gt; &lt;td style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;span style="color:#19b159"&gt;✓&lt;/span&gt;&lt;/span&gt;&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td style="background-color:#eeeeee"&gt;&lt;span style="font-family:Georgia,serif"&gt;Phase 2: Editor implementation&lt;/span&gt;&lt;/td&gt; &lt;td style="background-color:#eeeeee; text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;2016-10-20&lt;/span&gt;&lt;/td&gt; &lt;td style="background-color:#eeeeee; text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;span style="color:#19b159"&gt;✓&lt;/span&gt;&lt;/span&gt;&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;&lt;span style="font-family:Georgia,serif"&gt;Phase 3: Rollout to Production&lt;/span&gt;&lt;/td&gt; &lt;td style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;2016-10-22&lt;/span&gt;&lt;/td&gt; &lt;td style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;span style="color:#19b159"&gt;✓&lt;/span&gt;&lt;/span&gt;&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p style="text-align:justify"&gt;&lt;span style="font-family:Georgia,serif"&gt;The project that you participated in is of utmost importance to the future success of our platform. &amp;nbsp;We are very proud to share that&amp;nbsp;the&amp;nbsp;CKEditor implementation was a huge success and brought congratulations from both the key Stakeholders and the Customers:&lt;/span&gt;&lt;/p&gt; &lt;blockquote&gt; &lt;p style="text-align:center"&gt;This new editor has totally changed our content creation experience!&lt;/p&gt; &lt;p style="text-align:center"&gt;&amp;mdash; John F. Smith, CEO, The New Web&lt;/p&gt; &lt;/blockquote&gt; &lt;p style="text-align:justify"&gt;&lt;span style="font-family:Georgia,serif"&gt;This letter recognizes that much of our success is directly attributable to your efforts.&amp;nbsp;You deserve to be proud of your achievement. May your future efforts be equally successful and rewarding.&lt;/span&gt;&lt;/p&gt; &lt;p style="text-align:justify"&gt;&lt;span style="font-family:Georgia,serif"&gt;I am sure we will be seeing and hearing a great deal more about your accomplishments in the future. Keep up the good work!&lt;/span&gt;&lt;/p&gt; &lt;p&gt;&amp;nbsp;&lt;/p&gt; &lt;p&gt;&lt;span style="font-family:Georgia,serif"&gt;Best regards,&lt;/span&gt;&lt;/p&gt; &lt;p&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;em&gt;The Management&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;
                                                </textarea>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Inline Editor</h5>
                <span>Click on text to change it</span>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div id="header">
                    <div id="headerLeft">
                        <h3 contenteditable="true">
                            Lorem ipsum dolor sit amet dolor duis blandit vestibulum faucibus a, tortor.
                        </h3>
                    </div>
                    <div id="headerRight">
                        <div contenteditable="true">
                            <p>
                                Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non
                                felis. Maecenas malesuada elit lectus felis, malesuada ultricies.
                            </p>
                            <p>
                                Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a,
                                convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla
                                imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/pages/ckeditor/ckeditor.js") }}" type="text/javascript"></script>
    <script type="text/javascript"
            src="{{ asset("adminity/pages/ckeditor/ckeditor-custom.js") }}"></script>
@endsection

