@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("lightgallery.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Fb Wall</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Social</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Fb Wall</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cover-profile">
                            <div class="profile-bg-img">
                                <img class="profile-bg-img img-fluid"
                                     src="{{ asset("adminity/images/user-profile/bg-img1.jpg") }}" alt="bg-img">
                                <div class="card-block user-info">
                                    <div class="col-md-12">
                                        <div class="media-left">
                                            <a href="#" class="profile-image">
                                                <img class="user-img img-radius"
                                                     src="{{ asset("adminity/images/user-profile/user-img.jpg") }}"
                                                     alt="user-img">
                                            </a>
                                        </div>
                                        <div class="media-body row">
                                            <div class="col-lg-12">
                                                <div class="user-title">
                                                    <h2>Josephin Villa</h2>
                                                    <span class="text-white">Web designer</span>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="pull-right cover-btn">
                                                    <button type="button" class="btn btn-sm btn-primary m-b-10 m-r-10">
                                                        <i class="icofont icofont-plus"></i> Follow
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-primary m-b-10"><i
                                                            class="icofont icofont-ui-messaging"></i> Message
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card bg-white">
                            <div class="post-new-contain row card-block">
                                <div class="col-md-1 col-xs-3 post-profile">
                                    <img src="{{ asset("adminity/images/user.png") }}" class="img-fluid" alt="">
                                </div>
                                <form class="col-md-11 col-xs-9">
                                    <div class="">
                                        <textarea id="post-message" class="form-control post-input" rows="3" cols="10"
                                                  required="" placeholder="Write something....."></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="post-new-footer b-t-muted p-15">
<span class="image-upload m-r-15" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Photos">
<label for="file-input" class="file-upload-lbl">
<i class="icofont icofont-image text-muted"></i>
</label>
<input id="file-input" type="file" accept="image/x-png,image/gif,image/jpeg">
</span>
                                <i class="icofont icofont-ui-user text-muted"></i>
                                <i class="icofont icofont-ui-map text-muted"></i>
                                <span><a href="#" id="post-new" class="btn btn-primary waves-effect waves-light f-right"
                                         style="display: none;">Post</a></span>

                            </div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
                                    <span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown"
                                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                          role="tooltip"></span>
                                    <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery" class="lightgallery-popup">
                                    <div class=""
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                                            ad minim veniam, quis nostrud exercitation ullamco
                                            laboris nisi ut aliquip ex ea </p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg">
                                    <a href="#">
                                        <i class="icofont icofont-heart-alt text-muted">
                                        </i>
                                        <span class="b-r-theme">Like (20)</span>
                                    </a>
                                    <a href="#">
                                        <i class="icofont icofont-comment text-muted">
                                        </i>
                                        <span class="b-r-theme">Comments (25)</span>
                                    </a>
                                    <a href="#">
                                        <i class="icofont icofont-share text-muted">
                                        </i>
                                        <span>Share (10)</span>
                                    </a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20">
<span class="f-14"><a href="#">Comments (110)</a>
</span>
                                        <span class="f-right">see all comments
</span>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 form-control msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
                                    <span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown"
                                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                          role="tooltip"></span>
                                    <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery1" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                            ">
                                    <a href="#">
                                        <i class="icofont icofont-heart-alt text-muted">
                                        </i>
                                        <span class="b-r-theme">Like (20)</span>
                                    </a>
                                    <a href="#">
                                        <i class="icofont icofont-comment text-muted">
                                        </i>
                                        <span class="b-r-theme">Comments (25)</span>
                                    </a>
                                    <a href="#">
                                        <i class="icofont icofont-share text-muted">
                                        </i>
                                        <span>Share (10)</span>
                                    </a>
                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20">
<span class="f-14"><a href="#">Comments (110)</a>
</span>
                                        <span class="f-right">see all comments
</span>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description
                                    ">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description
                                    ">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 form-control msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
                                    <span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown"
                                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                          role="tooltip"></span>
                                    <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery2" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="../files/assets/images/timeline/img1.jpg 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="../files/assets/images/timeline/img1.jpg"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="../files/assets/images/timeline/img1.jpg 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="../files/assets/images/timeline/img1.jpg"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                            ">
                                    <a href="#">
                                        <i class="icofont icofont-heart-alt text-muted">
                                        </i>
                                        <span class="b-r-theme">Like (20)</span>
                                    </a>
                                    <a href="#">
                                        <i class="icofont icofont-comment text-muted">
                                        </i>
                                        <span class="b-r-theme">Comments (25)</span>
                                    </a>
                                    <a href="#">
                                        <i class="icofont icofont-share text-muted">
                                        </i>
                                        <span>Share (10)</span>
                                    </a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20">
<span class="f-14"><a href="#">Comments (110)</a>
</span>
                                        <span class="f-right">see all comments
</span>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description
                                        ">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description
                                                    ">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 form-control msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
                                    <span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown"
                                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                          role="tooltip"></span>
                                    <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery3" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-12 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                        ">
                                    <a href="#">
                                        <i class="icofont icofont-heart-alt text-muted">
                                        </i>
                                        <span class="b-r-theme">Like (20)</span>
                                    </a>
                                    <a href="#">
                                        <i class="icofont icofont-comment text-muted">
                                        </i>
                                        <span class="b-r-theme">Comments (25)</span>
                                    </a>
                                    <a href="#">
                                        <i class="icofont icofont-share text-muted">
                                        </i>
                                        <span>Share (10)</span>
                                    </a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20">
<span class="f-14"><a href="#">Comments (110)</a>
</span>
                                        <span class="f-right">see all comments
</span>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description
                                        ">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description
                                            ">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 form-control msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
<span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown
                                        " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
      role="tooltip">
</span>
                                    <div class="dropdown-menu dropdown-menu-right b-none elipsis-box">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery4" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block c-both">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                    ">
                                    <a href="#"> <i class="icofont icofont-heart-alt text-muted"></i><span class="b-r-theme
                                    ">Like (20)</span> </a>
                                    <a href="#"> <i class="icofont icofont-comment text-muted"></i> <span class="b-r-theme
                                    ">Comments (25)</span></a>
                                    <a href="#"> <i class="icofont icofont-share text-muted"></i>
                                        <span>Share (10)</span></a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20"><span class="f-14"><a href="#">Comments (110)</a></span><span
                                            class="f-right">see all comments</span></div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
<span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown
                                    " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="tooltip">
</span>
                                    <div class="dropdown-menu dropdown-menu-right b-none elipsis-box">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery5" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block c-both">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book. </p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                    ">
                                    <a href="#"> <i class="icofont icofont-heart-alt text-muted"></i><span class="b-r-theme
                                    ">Like (20)</span> </a>
                                    <a href="#"> <i class="icofont icofont-comment text-muted"></i> <span class="b-r-theme
                                    ">Comments (25)</span></a>
                                    <a href="#"> <i class="icofont icofont-share text-muted"></i>
                                        <span>Share (10)</span></a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20"><span class="f-14"><a href="#">Comments (110)</a></span><span
                                            class="f-right">see all comments</span></div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
<span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown
                                        " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
      role="tooltip">
</span>
                                    <div class="dropdown-menu dropdown-menu-right b-none elipsis-box">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery6" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block c-both">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                        ">
                                    <a href="#"> <i class="icofont icofont-heart-alt text-muted"></i><span class="b-r-theme
                                        ">Like (20)</span> </a>
                                    <a href="#"> <i class="icofont icofont-comment text-muted"></i> <span class="b-r-theme
                                        ">Comments (25)</span></a>
                                    <a href="#"> <i class="icofont icofont-share text-muted"></i>
                                        <span>Share (10)</span></a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20"><span class="f-14"><a href="#">Comments (110)</a></span><span
                                            class="f-right">see all comments</span></div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
<span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown
                                    " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="tooltip">
</span>
                                    <div class="dropdown-menu dropdown-menu-right b-none elipsis-box">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery7" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block c-both">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                    ">
                                    <a href="#"> <i class="icofont icofont-heart-alt text-muted"></i><span class="b-r-theme
                                    ">Like (20)</span> </a>
                                    <a href="#"> <i class="icofont icofont-comment text-muted"></i> <span class="b-r-theme
                                    ">Comments (25)</span></a>
                                    <a href="#"> <i class="icofont icofont-share text-muted"></i>
                                        <span>Share (10)</span></a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20"><span class="f-14"><a href="#">Comments (110)</a></span><span
                                            class="f-right">see all comments</span></div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
<span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown
                                        " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
      role="tooltip">
</span>
                                    <div class="dropdown-menu dropdown-menu-right b-none elipsis-box">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery8" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block c-both">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                        ">
                                    <a href="#"> <i class="icofont icofont-heart-alt text-muted"></i><span class="b-r-theme
                                        ">Like (20)</span> </a>
                                    <a href="#"> <i class="icofont icofont-comment text-muted"></i> <span class="b-r-theme
                                        ">Comments (25)</span></a>
                                    <a href="#"> <i class="icofont icofont-share text-muted"></i>
                                        <span>Share (10)</span></a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20"><span class="f-14"><a href="#">Comments (110)</a></span><span
                                            class="f-right">see all comments</span></div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
<span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown
                                        " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
      role="tooltip">
</span>
                                    <div class="dropdown-menu dropdown-menu-right b-none elipsis-box">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery9" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-12 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block c-both">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                    ">
                                    <a href="#"> <i class="icofont icofont-heart-alt text-muted"></i><span class="b-r-theme
                                    ">Like (20)</span> </a>
                                    <a href="#"> <i class="icofont icofont-comment text-muted"></i> <span class="b-r-theme
                                    ">Comments (25)</span></a>
                                    <a href="#"> <i class="icofont icofont-share text-muted"></i>
                                        <span>Share (10)</span></a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20"><span class="f-14"><a href="#">Comments (110)</a></span><span
                                            class="f-right">see all comments</span></div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
<span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown
                                        " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
      role="tooltip">
</span>
                                    <div class="dropdown-menu dropdown-menu-right b-none elipsis-box">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery10" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block c-both">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                        ">
                                    <a href="#"> <i class="icofont icofont-heart-alt text-muted"></i><span class="b-r-theme
                                        ">Like (20)</span> </a>
                                    <a href="#"> <i class="icofont icofont-comment text-muted"></i> <span class="b-r-theme
                                        ">Comments (25)</span></a>
                                    <a href="#"> <i class="icofont icofont-share text-muted"></i>
                                        <span>Share (10)</span></a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20"><span class="f-14"><a href="#">Comments (110)</a></span><span
                                            class="f-right">see all comments</span></div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                        <div>
                            <div class="bg-white p-relative">
                                <div class="input-group wall-elips">
<span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown
                                        " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
      role="tooltip">
</span>
                                    <div class="dropdown-menu dropdown-menu-right b-none elipsis-box">
                                        <a class="dropdown-item" href="#">Remove tag</a>
                                        <a class="dropdown-item" href="#">Report Photo</a>
                                        <a class="dropdown-item" href="#">Hide From Timeline</a>
                                        <a class="dropdown-item" href="#">Blog User</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius m-r-20"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">Josephin Doe posted on your timeline</div>
                                            <div class="f-13 text-muted">50 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery11" class="wall-img-preview lightgallery-popup">
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-0 wall-item"
                                         data-responsive="{{ asset("adminity/images/timeline/img1.jpg") }} 375, img/1-480.jpg 480, img/1.jpg 800"
                                         data-src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                         data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="#">
                                            <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                                 class="img-fluid width-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block c-both">
                                    <div class="timeline-details">
                                        <div class="chat-header">Josephin Doe posted on your timeline</div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer took
                                            a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg
                                        ">
                                    <a href="#"> <i class="icofont icofont-heart-alt text-muted"></i><span class="b-r-theme
                                        ">Like (20)</span> </a>
                                    <a href="#"> <i class="icofont icofont-comment text-muted"></i> <span class="b-r-theme
                                        ">Comments (25)</span></a>
                                    <a href="#"> <i class="icofont icofont-share text-muted"></i>
                                        <span>Share (10)</span></a>


                                </div>
                                <div class="card-block user-box">
                                    <div class="p-b-20"><span class="f-14"><a href="#">Comments (110)</a></span><span
                                            class="f-right">see all comments</span></div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body b-b-theme social-client-description">
                                            <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10, 2015</span>
                                            </div>
                                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer
                                                took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img class="media-object img-radius m-r-20"
                                                 src="{{ asset("adminity/images/user.png") }}"
                                                 alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <form class="">
                                                <div class="">
                                                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                                                              required="" placeholder="Write something....."></textarea>
                                                    <div class="text-right m-t-20"><a href="#"
                                                                                      class="btn btn-primary waves-effect waves-light">Post</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="f-30 text-muted text-center p-30">2014</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="fb-timeliner">
                    <h2 class="recent-highlight bg-danger">Recent</h2>
                    <ul>
                        <li class="active"><a href="#">December</a></li>
                        <li><a href="#">November</a></li>
                        <li><a href="#">October</a></li>
                        <li><a href="#">September</a></li>
                        <li><a href="#">August</a></li>
                        <li><a href="#">July</a></li>
                        <li><a href="#">June</a></li>
                        <li><a href="#">May</a></li>
                        <li><a href="#">April</a></li>
                        <li><a href="#">March</a></li>
                        <li><a href="#">February</a></li>
                        <li><a href="#">January</a></li>
                    </ul>
                </div>
                <div class="fb-timeliner">
                    <h2>2012</h2>
                    <ul>
                        <li><a href="#">August</a></li>
                        <li><a href="#">July</a></li>
                        <li><a href="#">June</a></li>
                        <li><a href="#">May</a></li>
                        <li><a href="#">April</a></li>
                        <li><a href="#">March</a></li>
                        <li><a href="#">February</a></li>
                        <li><a href="#">January</a></li>
                    </ul>
                </div>
                <div class="fb-timeliner">
                    <h2>2011</h2>
                    <ul>
                        <li><a href="#">May</a></li>
                        <li><a href="#">April</a></li>
                        <li><a href="#">March</a></li>
                        <li><a href="#">February</a></li>
                        <li><a href="#">January</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/lightgallery/js/lightgallery.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/lightgallery/js/lg-fullscreen.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/lightgallery/js/lg-thumbnail.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/lightgallery/js/lg-video.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/lightgallery/js/lg-autoplay.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/lightgallery/js/lg-zoom.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/lightgallery/js/lg-hash.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/lightgallery/js/lg-pager.min.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/wall/wall.js") }}"></script>
@endsection

