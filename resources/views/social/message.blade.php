@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/message/message.css") }}">
@endsection

@section("content")
    <div class="page-body message">
        <div class="row">

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header bg-primary">
                        <div class="media">
                            <a class="media-left" href="#">
                                <img class="media-object img-radius msg-img-h"
                                     src="{{ asset("adminity/images/avatar-1.jpg") }}" alt="">
                            </a>
                            <div class="media-body">
                                <div class="txt-white">Sign in as</div>
                                <div class="f-13 txt-white"><a href="https://colorlib.com/cdn-cgi/l/email-protection"
                                                               class="__cf_email__"
                                                               data-cfemail="95f4f1f8fcfbf1f0f8fad5f0edf4f8e5f9f0bbf6faf8">[email&#160;protected]</a>
                                </div>
                                <div class="input-group msg-ellipsis c-pointer">
                                    <span role="tooltip" class="dropdown-toggle addon-btn ellipsis"
                                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>
                                    <div class="dropdown-menu dropdown-menu-right b-none elipsis-box msg-elipsis-box">
                                        <a class="dropdown-item" href="#">Clear chat</a>
                                        <a class="dropdown-item" href="#">Archive chat</a>
                                        <a class="dropdown-item" href="#">Create Shortcut</a>
                                        <a class="dropdown-item" href="#">Block user</a>
                                    </div>
                                </div>
                                <i class="icofont icofont-navigation-menu contact-btn msg-nav"></i>
                            </div>
                            <i class="icon-options-vertical f-24 p-absolute msg-ellipsis hidden-md-down"></i>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-9 col-md-12 messages-content ">
                                <div>
                                    <div class="media">
                                        <div class="media-left friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-1.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <p class="msg-send">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer took a galley of type
                                                and scrambled it to make a type specimen book. It has survived not only
                                                five centuries, but also the leap into electronic typesetting, remaining
                                                essentially unchanged. It was popularised in the 1960s with the release
                                                of Letraset sheets containing Lorem Ipsum passages, and more recently
                                                with desktop publishing software like Aldus PageMaker including versions
                                                of Lorem Ipsum.</p>
                                            <p><i class="icofont icofont-wall-clock f-12"></i> October 12, 2015 at 9:00
                                                pm</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-body text-right">
                                            <p class="msg-reply bg-primary">Aldus PageMaker including versions of Lorem
                                                Ipsum.</p>
                                            <p class="msg-reply bg-primary">Lorem Ipsum is simply dummy text of the
                                                printing and typesetting industry. Lorem Ipsum has been the industry's
                                                standard dummy text ever since the 1500s, when an unknown printer took a
                                                galley.</p>
                                            <p><i class="icofont icofont-wall-clock f-12"></i> October 12, 2015 at 9:01
                                                pm</p>
                                        </div>
                                        <div class="media-right friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left friend-box">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-1.jpg") }}" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <p class="msg-send">Aldus PageMaker including versions of Lorem Ipsum.</p>
                                            <p class="msg-send">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                                text ever since the 1500s, when an unknown printer took a galley.</p>
                                            <p><i class="icofont icofont-wall-clock f-12"></i> October 12, 2015 at 9:15
                                                pm</p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="messages-send">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="alighaddon2" class="form-control new-msg"
                                                   placeholder="What’s on your mind.........."
                                                   aria-describedby="basic-addon2">
                                            <span class="input-group-addon bg-white" id="basic-addon2"><i
                                                    class="icofont icofont-paper-plane f-18 text-primary"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 message-left">
                                <div class="card-block user-box contact-box assign-user">
                                    <div class="media">
                                        <div class="media-left media-middle photo-table">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                     alt="Generic placeholder image">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h6>Josephin Doe</h6>
                                            <p>Lorem ipsum dolor sit amet..</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left media-middle photo-table">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                     alt="Generic placeholder image">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h6>Josephin Doe</h6>
                                            <p>Lorem ipsum dolor sit amet..</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left media-middle photo-table">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-3.jpg") }}"
                                                     alt="Generic placeholder image">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h6>Josephin Doe</h6>
                                            <p>Lorem ipsum dolor sit amet..</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left media-middle photo-table">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                     alt="Generic placeholder image">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h6>Josephin Doe</h6>
                                            <p>Lorem ipsum dolor sit amet..</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left media-middle photo-table">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-1.jpg") }}"
                                                     alt="Generic placeholder image">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h6>Josephin Doe</h6>
                                            <p>Lorem ipsum dolor sit amet..</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left media-middle photo-table">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                     alt="Generic placeholder image">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h6>Josephin Doe</h6>
                                            <p>Lorem ipsum dolor sit amet..</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left media-middle photo-table">
                                            <a href="#">
                                                <img class="media-object img-radius"
                                                     src="{{ asset("adminity/images/avatar-3.jpg") }}"
                                                     alt="Generic placeholder image">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h6>Josephin Doe</h6>
                                            <p>Lorem ipsum dolor sit amet..</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/message/message.js") }}"></script>
@endsection

