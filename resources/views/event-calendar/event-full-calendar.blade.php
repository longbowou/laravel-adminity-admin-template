@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/fullcalendar/css/fullcalendar.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/fullcalendar/css/fullcalendar.print.css") }}" media='print'>
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Full Calender</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Widget</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="card">
            <div class="card-header">
                <h5>Full Calender</h5>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-xl-2 col-md-12">
                        <div id="external-events">
                            <h6 class="m-b-30 m-t-20">Events</h6>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 1</div>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 2</div>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 3</div>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 4</div>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 5</div>
                            <div class="checkbox-fade fade-in-primary m-t-10">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
 <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                    <span>Remove After Drop</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10 col-md-12">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/js/classie.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/js/moment.min.js") }}"></script>
    <script type="text/javascript"
            src="{{ asset("adminity/components/fullcalendar/js/fullcalendar.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/full-calender/calendar.js") }}"></script>
@endsection

