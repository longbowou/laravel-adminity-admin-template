@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/handsontable/css/handsontable.full.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Data Operations</h4>
                        <span>Sorting, Paginating &amp; Searching</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Handson Table</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Data Operations</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Sorting</h5>
                        <span>
If you want to sort data in your data source array, you can simply invoke an Array.prototype.sort() function and call the render() function to refresh the table. You don't need any plugin for this. However, this operation alters the structure of the data source, and in many cases you want to leave the data source intact, while displaying its content in a specified order.Here's where column sorting plugin comes in handy.
</span>
                        <span>
Column sorting plugin works as a proxy between the data source and the Handson table rendering module. It can map indices of displayed rows (called logical indices) to the indices of corresponding rows in data source (called physical indices) and vice versa. This way you can alter the order of rows which are being presented to user, without changing the data source internal structure.
</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="sorting" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Paginating</h5>
                        <span>Add pages to your table.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="paginating" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                        <nav aria-label="Page navigation" class="handson-pagination">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#1">1</a></li>
                                <li class="page-item"><a class="page-link" href="#2">2</a></li>
                                <li class="page-item"><a class="page-link" href="#3">3</a></li>
                                <li class="page-item"><a class="page-link" href="#4">4</a></li>
                                <li class="page-item"><a class="page-link" href="#5">5</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Searching</h5>
                        <span>The search plugin provides an easy interface to search data across Handson table.</span>
                        <span>You should first enable the plugin by setting the search option to search. When enabled, searchPlugin exposes a new method query(queryStr), where queryStr is a string to find within the table. By default, the search is case insensitive.</span>
                        <span>query(queryStr, [callback], [queryMethod]) method does 2 things. First of all, it returns an array of search results. Every element is an objects containing 3 properties:</span>
                        <ol>
                            <li>row – index of the row where the value has been found</li>
                            <li>col – index of the column where the value has been found</li>
                            <li>data – the value that has been found</li>
                        </ol>
                        <span>The second thing the query does is set the isSearchResult property for each cell. If a cell is in search results, then its isSearchResult is set to true, otherwise the property is set to false.</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <fieldset class="form-group position-relative has-icon-left col-md-3">
                                <input type="text" class="form-control" id="search_field" placeholder="Search...">
                                <div class="form-control-position">
                                    <i class="ficon icon-search7"></i>
                                </div>
                            </fieldset>
                        </div>
                        <div class="table-responsive scroll-container">
                            <div id="searching" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/handsontable/js/handsontable.full.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/handson-table/data-operation.js") }}"></script>
@endsection


