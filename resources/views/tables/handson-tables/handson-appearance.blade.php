@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/handsontable-pro/dist/handsontable.full.min.html") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>HandsonTable Appearance</h4>
                        <span>context menu, custom buttons & comments</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Handson Table</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Appearance</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Customizing Borders</h5>
                <span>To enable the custom borders feature, set the customBorders option. It could be set as true or initialized as an array with predefined setup.</span>
                <span>To initialize Handson table with predefined custom borders, provide cells coordinates and border styles in form of an array:</span>
                <span>with row/col pairs: {row: 2, col: 2, left: { /*...*/ }} or with range details: {range: {from: {row: 1, col: 1}, to:{row: 3, col: 4}}, left: { /*...*/ }}</span>
            </div>
            <div class="card-block">
                <div class="scroll-container">
                    <div id="borders"
                         class="hot handsontable htRowHeaders htColumnHeaders"></div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Highlighting Selection</h5>
                <span>Use options currentRowClassName and currentColumnClassName</span>
            </div>
            <div class="card-block">
                <div class="table-responsive scroll-container">
                    <div id="highlighting"
                         class="hot handsontable htRowHeaders htColumnHeaders"></div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Mobiles And Tablets</h5>
                <span>Currently Handson table supports only iPad 4 in a basic scope. Mobile editor and selecting modes are enabled automatically if you're viewing Handson table on a mobile device.</span>
                <span>Open this page on iPad 4 and play with the demo below:</span>
            </div>
            <div class="card-block">
                <div class="table-responsive scroll-container">
                    <div id="mobilesTablets"
                         class="hot handsontable htRowHeaders htColumnHeaders"></div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/handsontable/js/handsontable.full.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/pages/handson-table/appearance-table.js") }}"></script>
@endsection


