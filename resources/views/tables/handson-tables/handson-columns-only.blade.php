@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/handsontable/css/handsontable.full.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Columns only</h4>
                        <span>Columns Stretching &amp; Freezing</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Handson Table</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Colums Only</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Stretch Last Column</h5>
                        <span>The following example creates a table with vertical scrollbar by specifying only the container height and overflow: hidden in CSS. The last column is stretched using stretchH: 'last' option.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="stretch" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Freezing</h5>
                        <span>In order to manually freeze a column (in another words - make it fixed), you need to set the manualColumnFreeze config item to true in Handson table initialization. When the Manual Column Freeze plugin is enabled, you can freeze any non-fixed column and unfreeze any fixed column in your Handson table instance using the Context Menu.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="freezing" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/handsontable/js/handsontable.full.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/handson-table/columns-only.js") }}"></script>
@endsection


