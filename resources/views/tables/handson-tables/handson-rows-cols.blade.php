@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/handsontable/css/handsontable.full.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Rows and Columns</h4>
                        <span>Scrolling, fixing, resizing &amp; moving</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Handson Table</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Rows Columns</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Scrolling</h5>
                        <span>To make the grid scrollable, set constant width and height to the container holding Handson table and set the overflow property to hidden in the container's stylesheet. Then, if the table contains enough rows or columns, you can scroll through it.</span>
                        <span>Note, that Handson table renders only the visible part of the table plus a fixed amount of rows and columns. You can experiment with the viewportColumnRenderingOffset and viewportRowRenderingOffset config options, which define this behavior, to improve the performance of your app.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="scrolling" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Fixing</h5>
                        <span>Specify two fixed rows with fixedRowsTop: 2 and two fixed columns with fixedColumnsLeft: 2 option.</span>
                        <span>Note: You'll need horizontal scrollbars, so just set a container width and overflow: hidden in CSS.</span>
                        <span>If you're looking for an option to manually fix columns, see the Freezing section of this documentation.
</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="fixing" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Resizing</h5>
                        <span>To enable these features, use settings manualColumnResize: true and manualRowResize: true</span>
                        <span>The draggable resize handle appears:</span>
                        <ol>
                            <li>In the right part of the column header,</li>
                            <li>In the bottom part of the row header.</li>
                        </ol>
                        <span>Double click on the resize handle automatically adjusts size of the row or column. </span>
                        <span>For the selected rows or columns works simultaneously resize.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="resizing" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Moving</h5>
                        <span>To enable these features, use settings manualColumnMove: true and manualRowMove: true</span>
                        <span>The draggable move handle appears:</span>
                        <ol>
                            <li>In the right part of the column header,</li>
                            <li>In the top part of the row header.</li>
                        </ol>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="moving" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/handsontable/js/handsontable.full.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/handson-table/rows-cols-table.js") }}"></script>
@endsection


