@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/handsontable/css/handsontable.full.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>HandsonTable Utilities</h4>
                        <span>context menu, custom buttons & comments</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Handson Table</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Utilities</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Context Menu With Default Options</h5>
                        <span class="m-b-10">To run the basic configuration of the Context Menu, just set the contextMenu option to true.</span>
                        <span>From version 0.11, context menu also works for row and column headers. When the context menu for the row header is opened, the column options are disabled. Likewise, when the context menu for the column header is opened, the row options are disabled</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="context" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Context Menu With Fully Custom Configuration</h5>
                        <span class="m-b-10">For greatest configurability, you use contextMenu option as a configuration object as described in jQuery contextMenu documentation.</span>
                        <span>This example shows how to set custom text, how to disable "Remove row" and "Insert row above" for the first row and how to add your own option.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="configuration" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Copy-Paste Configuration</h5>
                        <span class="m-b-10">Copy and Paste feature are available in Handson table by default, but only as key shortcuts (Ctrl (Cmd) + C and Ctrl (Cmd) + V respectively).</span>
                        <span>This plugin makes them available also as a clickable options in the context menu. Unfortunately, due to the browser's security restrictions, Handson table is forced to use a third-party flash-based library. We chose to use ZeroClipboard - it adds a small invisible flash clip to your website, allowing the user to click on it, and thus save data to the clipboard.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="copyPaste" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Custom Buttons</h5>
                        <span class="m-b-10">The alter method can be used if you want to insert or remove rows and columns using external buttons. The below example uses the a custom made handson table.removeRow.js plugin for that matter. Move your mouse over a row to see it.</span>
                        <span>You can programmatically select a cell using the selectCell method. The below button implements it.</span>
                    </div>
                    <div class="card-block">
                        <button class="intext-btn btn btn-primary mr-1 m-b-20" id="selectFirst">Select first cell</button>
                        <div class="checklist">
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" id="rowHeaders" checked="checked">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                    <span>Show row headers</span>
                                </label>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" id="colHeaders" checked="checked">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                    <span>Show col headers</span>
                                </label>
                            </div>
                        </div>
                        <div class="table-responsive scroll-container">
                            <div id="buttons" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Comments</h5>
                        <span>With option comments: true, you can add and remove cell comments through the context menu. To initialize Handson table with predefined comments, provide comment cell property: {row: 1, col: 1, comment: "Test comment"}</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="comments" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/handsontable/js/handsontable.full.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/handson-table/utilities.js") }}"></script>
@endsection


