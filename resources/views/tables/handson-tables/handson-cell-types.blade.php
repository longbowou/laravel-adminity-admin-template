@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/handsontable/css/handsontable.full.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>HandsonTable Cell Types</h4>
                        <span>Support almost all kind of cell types</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Handson Table</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">HandsonTable Cell Types</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Rendering Custom HTML In Cells</h5>
                        <span>This example shows how to use custom cell renderers to display HTML content in a cell. This is a very powerful feature. Just remember to escape any HTML code that could be used for XSS attacks. In the below configuration.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="rendering" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Numeric</h5>
                        <span class="m-b-10">By default, Handson table treats all cell values as string type. This is because "textarea" returns a string as its value. In many cases you will prefer cell values to be treated as number type. This allows to format numbers nicely and sort them correctly.</span>
                        <span class="m-b-10">To trigger the Numeric cell type, use the option type: 'numeric' in columns array or cells function. Make sure your cell values are numbers and not strings as Handson table will not parse strings to numbers. Numeric cell type uses Numbro.js as the formatting library. Head over to their website to learn about the formatting syntax.</span>
                        <span>To use number formatting style valid for your language (i18n), load language definition to Numbro.js. See "Languages" section in Numbro.js docs for more info.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="numericData" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Date</h5>
                        <span>All data entered to the data-typed cells are validated against the default date format (DD/MM/YYYY), unless another format is provided. If you enable the correctFormat config item, the dates will be automatically formatted to match the desired format.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="dateDate" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>





                <div class="card">
                    <div class="card-header">
                        <h5>Checkbox True/False Values</h5>
                        <span>This is default usage scenario when columns data have true or false value and we want to display ony checkboxes.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="checkbox" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Select</h5>
                        <span>Select editor should be considered an example how to write editors rather than used as a fully featured editor. It is a much simpler form of the Dropdown editor. It is suggested to use the latter in your projects.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="select" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Dropdown</h5>
                        <span>This example shows the usage of the Dropdown feature. Dropdown is based on Autocomplete cell type. All options used by autocomplete cell type apply to dropdown as well.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="dropdown" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Autocomplete Lazy Mode</h5>
                        <span>This example shows the usage of the Autocomplete feature in the default lazy mode. In this mode, user can choose one of the suggested options while typing or enter a custom value that is not included in the suggestions. In this mode, the mouse and keyboard bindings are identical as in Handson table cell type. The options are rendered from the source property which can be an array, or a function that returns an array.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="autocomplete" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Password Cell Type</h5>
                        <span>This kind of cell behaves like a text cell with a difference that it masks its value using asterisk in cell renderer. For the cell editor, a ' input type="password" ' field is used. Data is stored in the data source as plain text.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="password" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/handsontable/js/handsontable.full.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/handson-table/cell-types.js") }}"></script>
@endsection


