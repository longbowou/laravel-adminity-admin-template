@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/handsontable/css/handsontable.full.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>HandsonTable Cell Features</h4>
                        <span>Data validation, Drag down, Merged cells</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Handson Table</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cell Features</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Data Validation</h5>
                        <span class="m-b-10">
If you want to sort data in your data source array, you can simply invoke an Array.prototype.sort() function and call the render() function to refresh the table. You don't need any plugin for this. However, this operation alters the structure of the data source, and in many cases you want to leave the data source intact, while displaying its content in a specified order.Here's where column sorting plugin comes in handy.
</span>
                        <span>
Column sorting plugin works as a proxy between the data source and the Handson table rendering module. It can map indices of displayed rows (called logical indices) to the indices of corresponding rows in data source (called physical indices) and vice versa. This way you can alter the order of rows which are being presented to user, without changing the data source internal structure.
</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="validation" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Drag Down In All Directions</h5>
                        <span>
Notice the little square (fill handle) in the corner of the selected cell. You can drag it (drag-down) to repeat the values from the cell. Double click the fill handle in cell B4 (value "30") to fill the selection down to the last value in neighbouring column, just like it would in LibreOffice or google Docs.
</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="drag" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Merged Cells</h5>
                        <span>
To enable the merge cells feature, set the mergeCells option to be true or an array. To initialize Handson table with predefined merged cells, provide merged cells details in form of an array: mergeCells: [{row: 1, col: 1, rowspan: 2, colspan: 2}]
</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="merged" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Alignment</h5>
                        <span>
To initialize Handson table with predefined horizontal and vertical alignment, provide className cells details in form of a grid, columns or cell setting (see code sample below).
</span>
                        <span>Available classNames:</span>
                        <ol>
                            <li>Horizontal: htLeft, htCenter, htRight, htJustify,</li>
                            <li>Vertical: htTop, htMiddle, htBottom.</li>
                            <li>Alignment changes can be tracked using afterSetCellMeta hook callback.</li>
                        </ol>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="alignment" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Read-Only Columns</h5>
                        <span class="m-b-10">
In many usage cases, you will need to configure a certain column to be read only. This column will be available for keyboard navigation and CTRL+C. Only editing and pasting data will be disabled.
</span>
                        <span>To make a column read-only, declare it in the columns setting. You can also define a special renderer function that will dim the read-only values.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="readOnly" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Non-Editable Columns</h5>
                        <span class="m-b-10">
In many cases you will need to configure a certain column to be non-editable. Doing it does not change it's basic behavior (apart from editing), which means you are still available to use keyboard navigation, CTRL+C and CTRL+V functionalities, drag-to-fill etc.
</span>
                        <span>To make a column non-editable, declare it in the columns setting. You can also define a special renderer function that will dim the editor value.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive scroll-container">
                            <div id="nonEditable" class="hot handsontable htRowHeaders htColumnHeaders"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/handsontable/js/handsontable.full.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/handson-table/cell-features.js") }}"></script>
@endsection


