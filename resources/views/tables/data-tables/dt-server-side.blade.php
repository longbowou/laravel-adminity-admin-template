@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-bs4/css/dataTables.bootstrap4.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/data-table/css/buttons.dataTables.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Server Side Data-table</h4>
                        <span>Real power of DataTables</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Data Table</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Server Table</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Server Side Processing</h5>
                <span>The example below shows DataTables loading data for a table from arrays as the data source, where the structure of the row's data source in this example is:</span>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="dt-server-processing" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Custom HTTP Variables</h5>
                <span>The example below shows server-side processing being used with an extra parameter being sent to the server by using the ajax.data option as a function</span>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="dt-http" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>POST Data</h5>
                <span>The example below shows server-side processing being used with an extra parameter being sent to the server by using the ajax.data option as a function</span>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="dt-post" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/datatables.net/js/jquery.dataTables.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/dataTables.buttons.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/jszip.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/pdfmake.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/vfs_fonts.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.print.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.html5.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-bs4/js/dataTables.bootstrap4.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive/js/dataTables.responsive.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/data-table-custom.js") }}" type="text/javascript"></script>
@endsection


