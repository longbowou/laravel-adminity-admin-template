@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-bs4/css/dataTables.bootstrap4.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/data-table/css/buttons.dataTables.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Ajax DataTable</h4>
                        <span>Real power of DataTables</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Data Table</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ajax</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Ajax Data Source (Arrays)</h5>
                        <span>The example below shows DataTables loading data for a table from arrays as the data source, where the structure of the row's data source in this example is:</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive dt-responsive">
                            <table id="dt-ajax-array" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Ajax Data Source (Objects)</h5>
                        <span>The example below shows DataTables loading data for a table from arrays as the data source, where the structure of the row's data source in this example is:</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive dt-responsive">
                            <table id="dt-ajax-object" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Nested Object Data (objects)</h5>
                        <span>The example below shows DataTables loading data for a table from arrays as the data source, where the structure of the row's data source in this example is:</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive dt-responsive">
                            <table id="dt-nested-object" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Orthogonal Data</h5>
                        <span>In this example the Ajax source returns an array of objects, which DataTables uses to display the table. The structure of the row's data source in this example is:</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive dt-responsive">
                            <table id="dt-orthogonal" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Generated Content for a Column</h5>
                        <span>In some tables you might wish to have some content generated automatically.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive dt-responsive">
                            <table id="dt-generate-content" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Deferred Rendering for Speed</h5>
                        <span>The example below shows DataTables with deferred rendering enabled. For this small example you'll likely notice no difference, but larger tables can benefit significantly from simply enabling this parameter.</span>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive dt-responsive">
                            <table id="dt-render" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/datatables.net/js/jquery.dataTables.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/dataTables.buttons.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/jszip.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/pdfmake.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/vfs_fonts.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.print.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.html5.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-bs4/js/dataTables.bootstrap4.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive/js/dataTables.responsive.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/data-table-custom.js") }}" type="text/javascript"></script>
@endsection


