@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-bs4/css/dataTables.bootstrap4.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/data-table/css/buttons.dataTables.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/data-table/extensions/responsive/css/responsive.dataTables.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Scroller</h4>
                        <span>Scroll in tables</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Data Table Extensions</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Scroller</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Basic Table Scroll</h5>
                <span>Scroller is a plug-in for DataTables which enhances built-in scrolling features to allow large amounts of data to be rendered on page very quickly. This is done by Scroller through the use of a virtual rendering technique that will render only the part of the table that is actually required for the current view. Scroller assumes that all rows are of the same height (in order to preform the required calculations). Demo data in this example has 2,500 rows.</span>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="basic-scroller" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>ZIP / Post code</th>
                            <th>Country</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>State Saving</h5>
                <span>Scroller will automatically integrate with DataTables in order to save the scrolling position of the table, if state saving is enabled in the DataTable (stateSave). This example shows that in practice - to demonstrate, scroll the table and then reload the page.</span>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="state-scroller" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>ZIP / Post code</th>
                            <th>Country</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>API</h5>
                <span>Scroller will automatically integrate with DataTables in order to save the scrolling position of the table, if state saving is enabled in the DataTable (stateSave). This example shows that in practice - to demonstrate, scroll the table and then reload the page.</span>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="api-scroller" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>ZIP / Post code</th>
                            <th>Country</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/datatables.net/js/jquery.dataTables.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/dataTables.buttons.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/jszip.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/pdfmake.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/vfs_fonts.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.print.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.html5.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-bs4/js/dataTables.bootstrap4.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive/js/dataTables.responsive.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") }}"
            type="text/javascript"></script>

    <script src="{{ asset("adminity/pages/data-table/extensions/scroller/js/dataTables.scroller.min.js") }}"
            type="text/javascript"></script>

    <script src="{{ asset("adminity/pages/data-table/extensions/scroller/js/scroller-custom.js") }}"
            type="text/javascript"></script>
@endsection
