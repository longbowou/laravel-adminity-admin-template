@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-bs4/css/dataTables.bootstrap4.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/data-table/css/buttons.dataTables.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/data-table/extensions/responsive/css/responsive.dataTables.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Responsive Datatable</h4>
                        <span>Optimising the table's layout for different screen</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Data Table Extensions</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Responsive</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Configuration Option</h5>
                <span>The Responsive extension for DataTables can be applied to a DataTable in one of two ways; with a specific class name on the table, or using the DataTables initialisation options. This method shows the latter, with the responsive option being set to the boolean value true.</span>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div class="table-responsive">
                    <div class="dt-responsive table-responsive">
                        <table id="res-config" class="table table-striped table-bordered nowrap">
                            <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                                <th>Extn.</th>
                                <th>E-mail</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tiger</td>
                                <td>Nixon</td>
                                <td>System Architect</td>
                                <td>Edinburgh</td>
                                <td>61</td>
                                <td>2011/04/25</td>
                                <td>$320,800</td>
                                <td>5421</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="e397cd8d8a9b8c8da3878297829782818f8690cd8d8697">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Garrett</td>
                                <td>Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td>63</td>
                                <td>2011/07/25</td>
                                <td>$170,750</td>
                                <td>8422</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="caade4bda3a4beafb8b98aaeabbeabbeaba8a6afb9e4a4afbe">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Ashton</td>
                                <td>Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                                <td>1562</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="bbda95d8d4c3fbdfdacfdacfdad9d7dec895d5decf">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Cedric</td>
                                <td>Kelly</td>
                                <td>Senior Javascript Developer</td>
                                <td>Edinburgh</td>
                                <td>22</td>
                                <td>2012/03/29</td>
                                <td>$433,060</td>
                                <td>6224</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="6b0845000e0707122b0f0a1f0a1f0a09070e1845050e1f">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Airi</td>
                                <td>Satou</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td>33</td>
                                <td>2008/11/28</td>
                                <td>$162,700</td>
                                <td>5407</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="c1a0efb2a0b5aeb481a5a0b5a0b5a0a3ada4b2efafa4b5">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Brielle</td>
                                <td>Williamson</td>
                                <td>Integration Specialist</td>
                                <td>New York</td>
                                <td>61</td>
                                <td>2012/12/02</td>
                                <td>$372,000</td>
                                <td>4804</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="11733f66787d7d78707c627e7f51757065706570737d74623f7f7465">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Herrod</td>
                                <td>Chandler</td>
                                <td>Sales Assistant</td>
                                <td>San Francisco</td>
                                <td>59</td>
                                <td>2012/08/06</td>
                                <td>$137,500</td>
                                <td>9608</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="ed85c38e858c838981889fad898c998c998c8f81889ec3838899">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Rhona</td>
                                <td>Davidson</td>
                                <td>Integration Specialist</td>
                                <td>Tokyo</td>
                                <td>55</td>
                                <td>2010/10/14</td>
                                <td>$327,900</td>
                                <td>6200</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="24560a4045524d40574b4a64404550455045464841570a4a4150">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Colleen</td>
                                <td>Hurst</td>
                                <td>Javascript Developer</td>
                                <td>San Francisco</td>
                                <td>39</td>
                                <td>2009/09/15</td>
                                <td>$205,500</td>
                                <td>2360</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="c4a7eaacb1b6b7b084a0a5b0a5b0a5a6a8a1b7eaaaa1b0">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Sonya</td>
                                <td>Frost</td>
                                <td>Software Engineer</td>
                                <td>Edinburgh</td>
                                <td>23</td>
                                <td>2008/12/13</td>
                                <td>$103,600</td>
                                <td>1667</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="d9aaf7bfabb6aaad99bdb8adb8adb8bbb5bcaaf7b7bcad">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jena</td>
                                <td>Gaines</td>
                                <td>Office Manager</td>
                                <td>London</td>
                                <td>30</td>
                                <td>2008/12/19</td>
                                <td>$90,560</td>
                                <td>3814</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="99f3b7fef8f0f7fcead9fdf8edf8edf8fbf5fceab7f7fced">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Quinn</td>
                                <td>Flynn</td>
                                <td>Support Lead</td>
                                <td>Edinburgh</td>
                                <td>22</td>
                                <td>2013/03/03</td>
                                <td>$342,000</td>
                                <td>9497</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="7f0e5119130611113f1b1e0b1e0b1e1d131a0c51111a0b">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Charde</td>
                                <td>Marshall</td>
                                <td>Regional Director</td>
                                <td>San Francisco</td>
                                <td>36</td>
                                <td>2008/10/16</td>
                                <td>$470,600</td>
                                <td>6741</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="88eba6e5e9fafbe0e9e4e4c8ece9fce9fce9eae4edfba6e6edfc">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Haley</td>
                                <td>Kennedy</td>
                                <td>Senior Marketing Designer</td>
                                <td>London</td>
                                <td>43</td>
                                <td>2012/12/18</td>
                                <td>$313,500</td>
                                <td>3597</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="670f490c02090902031e27030613061306050b021449090213">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Tatyana</td>
                                <td>Fitzpatrick</td>
                                <td>Regional Director</td>
                                <td>London</td>
                                <td>19</td>
                                <td>2010/03/17</td>
                                <td>$385,750</td>
                                <td>1965</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="bbcf95ddd2cfc1cbdacfc9d2d8d0fbdfdacfdacfdad9d7dec895d5decf">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Michael</td>
                                <td>Silva</td>
                                <td>Marketing Designer</td>
                                <td>London</td>
                                <td>66</td>
                                <td>2012/11/27</td>
                                <td>$198,500</td>
                                <td>1581</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="eb86c59882879d8aab8f8a9f8a9f8a89878e98c5858e9f">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Paul</td>
                                <td>Byrd</td>
                                <td>Chief Financial Officer (CFO)</td>
                                <td>New York</td>
                                <td>64</td>
                                <td>2010/06/09</td>
                                <td>$725,000</td>
                                <td>3059</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="e898c68a919a8ca88c899c899c898a848d9bc6868d9c">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Gloria</td>
                                <td>Little</td>
                                <td>Systems Administrator</td>
                                <td>New York</td>
                                <td>59</td>
                                <td>2009/04/10</td>
                                <td>$237,500</td>
                                <td>1721</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="6c0b420005181800092c080d180d180d0e00091f42020918">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Bradley</td>
                                <td>Greer</td>
                                <td>Software Engineer</td>
                                <td>London</td>
                                <td>41</td>
                                <td>2012/10/13</td>
                                <td>$132,000</td>
                                <td>2558</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="197b377e6b7c7c6b597d786d786d787b757c6a37777c6d">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Dai</td>
                                <td>Rios</td>
                                <td>Personnel Lead</td>
                                <td>Edinburgh</td>
                                <td>35</td>
                                <td>2012/09/26</td>
                                <td>$217,500</td>
                                <td>2290</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="2c48025e45435f6c484d584d584d4e40495f02424958">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jenette</td>
                                <td>Caldwell</td>
                                <td>Development Lead</td>
                                <td>New York</td>
                                <td>30</td>
                                <td>2011/09/03</td>
                                <td>$345,000</td>
                                <td>1937</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="3f55115c5e535b485a53537f5b5e4b5e4b5e5d535a4c11515a4b">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Yuri</td>
                                <td>Berry</td>
                                <td>Chief Marketing Officer (CMO)</td>
                                <td>New York</td>
                                <td>40</td>
                                <td>2009/06/25</td>
                                <td>$675,000</td>
                                <td>6154</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="83faade1e6f1f1fac3e7e2f7e2f7e2e1efe6f0adede6f7">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Caesar</td>
                                <td>Vance</td>
                                <td>Pre-Sales Support</td>
                                <td>New York</td>
                                <td>21</td>
                                <td>2011/12/12</td>
                                <td>$106,450</td>
                                <td>8330</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="99fab7eff8f7fafcd9fdf8edf8edf8fbf5fceab7f7fced">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Doris</td>
                                <td>Wilder</td>
                                <td>Sales Assistant</td>
                                <td>Sidney</td>
                                <td>23</td>
                                <td>2010/09/20</td>
                                <td>$85,600</td>
                                <td>3023</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="61054f16080d05041321050015001500030d04124f0f0415">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Angelica</td>
                                <td>Ramos</td>
                                <td>Chief Executive Officer (CEO)</td>
                                <td>London</td>
                                <td>47</td>
                                <td>2009/10/09</td>
                                <td>$1,200,000</td>
                                <td>5797</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="35541b4754585a4675515441544154575950461b5b5041">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Gavin</td>
                                <td>Joyce</td>
                                <td>Developer</td>
                                <td>Edinburgh</td>
                                <td>42</td>
                                <td>2010/12/22</td>
                                <td>$92,575</td>
                                <td>8822</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="4f28612520362c2a0f2b2e3b2e3b2e2d232a3c61212a3b">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jennifer</td>
                                <td>Chang</td>
                                <td>Regional Director</td>
                                <td>Singapore</td>
                                <td>28</td>
                                <td>2010/11/14</td>
                                <td>$357,650</td>
                                <td>9239</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="b1db9fd2d9d0dfd6f1d5d0c5d0c5d0d3ddd4c29fdfd4c5">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Brenden</td>
                                <td>Wagner</td>
                                <td>Software Engineer</td>
                                <td>San Francisco</td>
                                <td>28</td>
                                <td>2011/06/07</td>
                                <td>$206,850</td>
                                <td>1314</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="24460a5345434a415664404550455045464841570a4a4150">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Fiona</td>
                                <td>Green</td>
                                <td>Chief Operating Officer (COO)</td>
                                <td>San Francisco</td>
                                <td>48</td>
                                <td>2010/03/11</td>
                                <td>$850,000</td>
                                <td>2947</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="81e7afe6f3e4e4efc1e5e0f5e0f5e0e3ede4f2afefe4f5">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Shou</td>
                                <td>Itou</td>
                                <td>Regional Marketing</td>
                                <td>Tokyo</td>
                                <td>20</td>
                                <td>2011/08/14</td>
                                <td>$163,000</td>
                                <td>8899</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="22510c4b564d5762464356435643404e47510c4c4756">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Michelle</td>
                                <td>House</td>
                                <td>Integration Specialist</td>
                                <td>Sidney</td>
                                <td>37</td>
                                <td>2011/06/02</td>
                                <td>$95,400</td>
                                <td>2769</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="4d20632522383e280d292c392c392c2f21283e63232839">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Suki</td>
                                <td>Burks</td>
                                <td>Developer</td>
                                <td>London</td>
                                <td>53</td>
                                <td>2009/10/22</td>
                                <td>$114,500</td>
                                <td>6832</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="64174a0611160f1724000510051005060801174a0a0110">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Prescott</td>
                                <td>Bartlett</td>
                                <td>Technical Author</td>
                                <td>London</td>
                                <td>27</td>
                                <td>2011/05/07</td>
                                <td>$145,000</td>
                                <td>3606</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="ec9cc28e8d9e9880899898ac888d988d988d8e80899fc2828998">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Gavin</td>
                                <td>Cortez</td>
                                <td>Team Leader</td>
                                <td>San Francisco</td>
                                <td>22</td>
                                <td>2008/10/26</td>
                                <td>$235,500</td>
                                <td>2860</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="096e276a667b7d6c73496d687d687d686b656c7a27676c7d">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Martena</td>
                                <td>Mccray</td>
                                <td>Post-Sales support</td>
                                <td>Edinburgh</td>
                                <td>46</td>
                                <td>2011/03/09</td>
                                <td>$324,050</td>
                                <td>8240</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="cfa2e1a2acacbdaeb68fabaebbaebbaeada3aabce1a1aabb">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Unity</td>
                                <td>Butler</td>
                                <td>Marketing Designer</td>
                                <td>San Francisco</td>
                                <td>47</td>
                                <td>2009/12/09</td>
                                <td>$85,675</td>
                                <td>5384</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="add883cfd8d9c1c8dfedc9ccd9ccd9cccfc1c8de83c3c8d9">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Howard</td>
                                <td>Hatfield</td>
                                <td>Office Manager</td>
                                <td>San Francisco</td>
                                <td>51</td>
                                <td>2008/12/16</td>
                                <td>$164,500</td>
                                <td>7031</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="1a7234727b6e7c737f767e5a7e7b6e7b6e7b78767f6934747f6e">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Hope</td>
                                <td>Fuentes</td>
                                <td>Secretary</td>
                                <td>San Francisco</td>
                                <td>41</td>
                                <td>2010/02/12</td>
                                <td>$109,850</td>
                                <td>6318</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="bcd492dac9d9d2c8d9cffcd8ddc8ddc8ddded0d9cf92d2d9c8">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Vivian</td>
                                <td>Harrell</td>
                                <td>Financial Controller</td>
                                <td>San Francisco</td>
                                <td>62</td>
                                <td>2009/02/14</td>
                                <td>$452,500</td>
                                <td>9422</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="d2a4fcbab3a0a0b7bebe92b6b3a6b3a6b3b0beb7a1fcbcb7a6">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Timothy</td>
                                <td>Mooney</td>
                                <td>Office Manager</td>
                                <td>London</td>
                                <td>37</td>
                                <td>2008/12/11</td>
                                <td>$136,200</td>
                                <td>7580</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="dfabf1b2b0b0b1baa69fbbbeabbeabbebdb3baacf1b1baab">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jackson</td>
                                <td>Bradshaw</td>
                                <td>Director</td>
                                <td>New York</td>
                                <td>65</td>
                                <td>2008/09/26</td>
                                <td>$645,750</td>
                                <td>1042</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="95ffbbf7e7f4f1e6fdf4e2d5f1f4e1f4e1f4f7f9f0e6bbfbf0e1">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Olivia</td>
                                <td>Liang</td>
                                <td>Support Engineer</td>
                                <td>Singapore</td>
                                <td>64</td>
                                <td>2011/02/03</td>
                                <td>$234,500</td>
                                <td>2120</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="6b044507020a050c2b0f0a1f0a1f0a09070e1845050e1f">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Bruno</td>
                                <td>Nash</td>
                                <td>Software Engineer</td>
                                <td>London</td>
                                <td>38</td>
                                <td>2011/05/03</td>
                                <td>$163,500</td>
                                <td>6222</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="5e3c70303f2d361e3a3f2a3f2a3f3c323b2d70303b2a">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Sakura</td>
                                <td>Yamamoto</td>
                                <td>Support Engineer</td>
                                <td>Tokyo</td>
                                <td>37</td>
                                <td>2009/08/19</td>
                                <td>$139,575</td>
                                <td>9383</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="1d6e33647c707c707269725d797c697c697c7f71786e33737869">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Thor</td>
                                <td>Walton</td>
                                <td>Developer</td>
                                <td>New York</td>
                                <td>61</td>
                                <td>2013/08/11</td>
                                <td>$98,540</td>
                                <td>8327</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="d2a6fca5b3bea6bdbc92b6b3a6b3a6b3b0beb7a1fcbcb7a6">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Finn</td>
                                <td>Camacho</td>
                                <td>Support Engineer</td>
                                <td>San Francisco</td>
                                <td>47</td>
                                <td>2009/07/07</td>
                                <td>$87,500</td>
                                <td>2927</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="197f377a7874787a7176597d786d786d787b757c6a37777c6d">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Serge</td>
                                <td>Baldwin</td>
                                <td>Data Coordinator</td>
                                <td>Singapore</td>
                                <td>64</td>
                                <td>2012/04/09</td>
                                <td>$138,575</td>
                                <td>8352</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="097a276b68656d7e6067496d687d687d686b656c7a27676c7d">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Zenaida</td>
                                <td>Frank</td>
                                <td>Software Engineer</td>
                                <td>New York</td>
                                <td>63</td>
                                <td>2010/01/04</td>
                                <td>$125,250</td>
                                <td>7439</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="bac094dcc8dbd4d1fadedbcedbcedbd8d6dfc994d4dfce">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Zorita</td>
                                <td>Serrano</td>
                                <td>Software Engineer</td>
                                <td>San Francisco</td>
                                <td>56</td>
                                <td>2012/06/01</td>
                                <td>$115,000</td>
                                <td>4389</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="86fca8f5e3f4f4e7e8e9c6e2e7f2e7f2e7e4eae3f5a8e8e3f2">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jennifer</td>
                                <td>Acosta</td>
                                <td>Junior Javascript Developer</td>
                                <td>Edinburgh</td>
                                <td>43</td>
                                <td>2013/02/01</td>
                                <td>$75,650</td>
                                <td>3431</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="22480c43414d51564362464356435643404e47510c4c4756">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Cara</td>
                                <td>Stevens</td>
                                <td>Sales Assistant</td>
                                <td>New York</td>
                                <td>46</td>
                                <td>2011/12/06</td>
                                <td>$145,600</td>
                                <td>3990</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="e685c895928390838895a6828792879287848a8395c8888392">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Hermione</td>
                                <td>Butler</td>
                                <td>Regional Director</td>
                                <td>London</td>
                                <td>47</td>
                                <td>2011/03/21</td>
                                <td>$356,250</td>
                                <td>1016</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="c5adeba7b0b1a9a0b785a1a4b1a4b1a4a7a9a0b6ebaba0b1">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Lael</td>
                                <td>Greer</td>
                                <td>Systems Administrator</td>
                                <td>London</td>
                                <td>21</td>
                                <td>2009/02/27</td>
                                <td>$103,500</td>
                                <td>6733</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="a5c98bc2d7c0c0d7e5c1c4d1c4d1c4c7c9c0d68bcbc0d1">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jonas</td>
                                <td>Alexander</td>
                                <td>Developer</td>
                                <td>San Francisco</td>
                                <td>30</td>
                                <td>2010/07/14</td>
                                <td>$86,500</td>
                                <td>8196</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="80eaaee1ece5f8e1eee4e5f2c0e4e1f4e1f4e1e2ece5f3aeeee5f4">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Shad</td>
                                <td>Decker</td>
                                <td>Regional Director</td>
                                <td>Edinburgh</td>
                                <td>51</td>
                                <td>2008/11/13</td>
                                <td>$183,000</td>
                                <td>6373</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="2655084243454d435466424752475247444a435508484352">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Michael</td>
                                <td>Bruce</td>
                                <td>Javascript Developer</td>
                                <td>Singapore</td>
                                <td>29</td>
                                <td>2011/06/27</td>
                                <td>$183,000</td>
                                <td>5384</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="dab7f4b8a8afb9bf9abebbaebbaebbb8b6bfa9f4b4bfae">[email&#160;protected]</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Donna</td>
                                <td>Snider</td>
                                <td>Customer Support</td>
                                <td>New York</td>
                                <td>27</td>
                                <td>2011/01/25</td>
                                <td>$112,000</td>
                                <td>4226</td>
                                <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="cfabe1bca1a6abaabd8fabaebbaebbaeada3aabce1a1aabb">[email&#160;protected]</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>`New` Constructor</h5>
                <span>Responsive will automatically detect new DataTable instances being created on a page and initialize itself if it finds the responsive option or responsive class name on the table, as shown in the other examples.</span>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="new-cons" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Age</th>
                            <th>Start date</th>
                            <th>Salary</th>
                            <th>Extn.</th>
                            <th>E-mail</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Tiger</td>
                            <td>Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td>2011/04/25</td>
                            <td>$320,800</td>
                            <td>5421</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="d8acf6b6b1a0b7b698bcb9acb9acb9bab4bdabf6b6bdac">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Garrett</td>
                            <td>Winters</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>63</td>
                            <td>2011/07/25</td>
                            <td>$170,750</td>
                            <td>8422</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="ff98d18896918b9a8d8cbf9b9e8b9e8b9e9d939a8cd1919a8b">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Ashton</td>
                            <td>Cox</td>
                            <td>Junior Technical Author</td>
                            <td>San Francisco</td>
                            <td>66</td>
                            <td>2009/01/12</td>
                            <td>$86,000</td>
                            <td>1562</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="51307f323e2911353025302530333d34227f3f3425">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Cedric</td>
                            <td>Kelly</td>
                            <td>Senior Javascript Developer</td>
                            <td>Edinburgh</td>
                            <td>22</td>
                            <td>2012/03/29</td>
                            <td>$433,060</td>
                            <td>6224</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="fa99d4919f969683ba9e9b8e9b8e9b98969f89d4949f8e">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Airi</td>
                            <td>Satou</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>33</td>
                            <td>2008/11/28</td>
                            <td>$162,700</td>
                            <td>5407</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="4c2d623f2d3823390c282d382d382d2e20293f62222938">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Brielle</td>
                            <td>Williamson</td>
                            <td>Integration Specialist</td>
                            <td>New York</td>
                            <td>61</td>
                            <td>2012/12/02</td>
                            <td>$372,000</td>
                            <td>4804</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="b5d79bc2dcd9d9dcd4d8c6dadbf5d1d4c1d4c1d4d7d9d0c69bdbd0c1">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Herrod</td>
                            <td>Chandler</td>
                            <td>Sales Assistant</td>
                            <td>San Francisco</td>
                            <td>59</td>
                            <td>2012/08/06</td>
                            <td>$137,500</td>
                            <td>9608</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="e78fc9848f8689838b8295a7838693869386858b8294c9898293">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Rhona</td>
                            <td>Davidson</td>
                            <td>Integration Specialist</td>
                            <td>Tokyo</td>
                            <td>55</td>
                            <td>2010/10/14</td>
                            <td>$327,900</td>
                            <td>6200</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="55277b3134233c31263a3b15313421342134373930267b3b3021">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Colleen</td>
                            <td>Hurst</td>
                            <td>Javascript Developer</td>
                            <td>San Francisco</td>
                            <td>39</td>
                            <td>2009/09/15</td>
                            <td>$205,500</td>
                            <td>2360</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="85e6abedf0f7f6f1c5e1e4f1e4f1e4e7e9e0f6abebe0f1">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Sonya</td>
                            <td>Frost</td>
                            <td>Software Engineer</td>
                            <td>Edinburgh</td>
                            <td>23</td>
                            <td>2008/12/13</td>
                            <td>$103,600</td>
                            <td>1667</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="94e7baf2e6fbe7e0d4f0f5e0f5e0f5f6f8f1e7bafaf1e0">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jena</td>
                            <td>Gaines</td>
                            <td>Office Manager</td>
                            <td>London</td>
                            <td>30</td>
                            <td>2008/12/19</td>
                            <td>$90,560</td>
                            <td>3814</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="701a5e1711191e150330141104110411121c15035e1e1504">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Quinn</td>
                            <td>Flynn</td>
                            <td>Support Lead</td>
                            <td>Edinburgh</td>
                            <td>22</td>
                            <td>2013/03/03</td>
                            <td>$342,000</td>
                            <td>9497</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="41306f272d382f2f01252035203520232d24326f2f2435">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Charde</td>
                            <td>Marshall</td>
                            <td>Regional Director</td>
                            <td>San Francisco</td>
                            <td>36</td>
                            <td>2008/10/16</td>
                            <td>$470,600</td>
                            <td>6741</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="6605480b0714150e070a0a26020712071207040a031548080312">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Haley</td>
                            <td>Kennedy</td>
                            <td>Senior Marketing Designer</td>
                            <td>London</td>
                            <td>43</td>
                            <td>2012/12/18</td>
                            <td>$313,500</td>
                            <td>3597</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="4f2761242a21212a2b360f2b2e3b2e3b2e2d232a3c61212a3b">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Tatyana</td>
                            <td>Fitzpatrick</td>
                            <td>Regional Director</td>
                            <td>London</td>
                            <td>19</td>
                            <td>2010/03/17</td>
                            <td>$385,750</td>
                            <td>1965</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="d7a3f9b1bea3ada7b6a3a5beb4bc97b3b6a3b6a3b6b5bbb2a4f9b9b2a3">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Michael</td>
                            <td>Silva</td>
                            <td>Marketing Designer</td>
                            <td>London</td>
                            <td>66</td>
                            <td>2012/11/27</td>
                            <td>$198,500</td>
                            <td>1581</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="d2bffca1bbbea4b392b6b3a6b3a6b3b0beb7a1fcbcb7a6">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Paul</td>
                            <td>Byrd</td>
                            <td>Chief Financial Officer (CFO)</td>
                            <td>New York</td>
                            <td>64</td>
                            <td>2010/06/09</td>
                            <td>$725,000</td>
                            <td>3059</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="addd83cfd4dfc9edc9ccd9ccd9cccfc1c8de83c3c8d9">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Gloria</td>
                            <td>Little</td>
                            <td>Systems Administrator</td>
                            <td>New York</td>
                            <td>59</td>
                            <td>2009/04/10</td>
                            <td>$237,500</td>
                            <td>1721</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="f493da989d80809891b490958095809596989187da9a9180">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Bradley</td>
                            <td>Greer</td>
                            <td>Software Engineer</td>
                            <td>London</td>
                            <td>41</td>
                            <td>2012/10/13</td>
                            <td>$132,000</td>
                            <td>2558</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="a4c68ac3d6c1c1d6e4c0c5d0c5d0c5c6c8c1d78acac1d0">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Dai</td>
                            <td>Rios</td>
                            <td>Personnel Lead</td>
                            <td>Edinburgh</td>
                            <td>35</td>
                            <td>2012/09/26</td>
                            <td>$217,500</td>
                            <td>2290</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="c7a3e9b5aea8b487a3a6b3a6b3a6a5aba2b4e9a9a2b3">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jenette</td>
                            <td>Caldwell</td>
                            <td>Development Lead</td>
                            <td>New York</td>
                            <td>30</td>
                            <td>2011/09/03</td>
                            <td>$345,000</td>
                            <td>1937</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="fd97d39e9c91998a989191bd999c899c899c9f91988ed3939889">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Yuri</td>
                            <td>Berry</td>
                            <td>Chief Marketing Officer (CMO)</td>
                            <td>New York</td>
                            <td>40</td>
                            <td>2009/06/25</td>
                            <td>$675,000</td>
                            <td>6154</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="5f26713d3a2d2d261f3b3e2b3e2b3e3d333a2c71313a2b">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Caesar</td>
                            <td>Vance</td>
                            <td>Pre-Sales Support</td>
                            <td>New York</td>
                            <td>21</td>
                            <td>2011/12/12</td>
                            <td>$106,450</td>
                            <td>8330</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="4d2e633b2c232e280d292c392c392c2f21283e63232839">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Doris</td>
                            <td>Wilder</td>
                            <td>Sales Assistant</td>
                            <td>Sidney</td>
                            <td>23</td>
                            <td>2010/09/20</td>
                            <td>$85,600</td>
                            <td>3023</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="1f7b316876737b7a6d5f7b7e6b7e6b7e7d737a6c31717a6b">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Angelica</td>
                            <td>Ramos</td>
                            <td>Chief Executive Officer (CEO)</td>
                            <td>London</td>
                            <td>47</td>
                            <td>2009/10/09</td>
                            <td>$1,200,000</td>
                            <td>5797</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="45246b3724282a3605212431243124272920366b2b2031">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Gavin</td>
                            <td>Joyce</td>
                            <td>Developer</td>
                            <td>Edinburgh</td>
                            <td>42</td>
                            <td>2010/12/22</td>
                            <td>$92,575</td>
                            <td>8822</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="12753c787d6b717752767366736673707e77613c7c7766">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jennifer</td>
                            <td>Chang</td>
                            <td>Regional Director</td>
                            <td>Singapore</td>
                            <td>28</td>
                            <td>2010/11/14</td>
                            <td>$357,650</td>
                            <td>9239</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="c4aeeaa7aca5aaa384a0a5b0a5b0a5a6a8a1b7eaaaa1b0">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Brenden</td>
                            <td>Wagner</td>
                            <td>Software Engineer</td>
                            <td>San Francisco</td>
                            <td>28</td>
                            <td>2011/06/07</td>
                            <td>$206,850</td>
                            <td>1314</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="c5a7ebb2a4a2aba0b785a1a4b1a4b1a4a7a9a0b6ebaba0b1">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Fiona</td>
                            <td>Green</td>
                            <td>Chief Operating Officer (COO)</td>
                            <td>San Francisco</td>
                            <td>48</td>
                            <td>2010/03/11</td>
                            <td>$850,000</td>
                            <td>2947</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="385e165f4a5d5d56785c594c594c595a545d4b16565d4c">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Shou</td>
                            <td>Itou</td>
                            <td>Regional Marketing</td>
                            <td>Tokyo</td>
                            <td>20</td>
                            <td>2011/08/14</td>
                            <td>$163,000</td>
                            <td>8899</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="9fecb1f6ebf0eadffbfeebfeebfefdf3faecb1f1faeb">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Michelle</td>
                            <td>House</td>
                            <td>Integration Specialist</td>
                            <td>Sidney</td>
                            <td>37</td>
                            <td>2011/06/02</td>
                            <td>$95,400</td>
                            <td>2769</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="95f8bbfdfae0e6f0d5f1f4e1f4e1f4f7f9f0e6bbfbf0e1">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Suki</td>
                            <td>Burks</td>
                            <td>Developer</td>
                            <td>London</td>
                            <td>53</td>
                            <td>2009/10/22</td>
                            <td>$114,500</td>
                            <td>6832</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="10633e7265627b6350747164716471727c75633e7e7564">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Prescott</td>
                            <td>Bartlett</td>
                            <td>Technical Author</td>
                            <td>London</td>
                            <td>27</td>
                            <td>2011/05/07</td>
                            <td>$145,000</td>
                            <td>3606</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="9eeeb0fcffeceaf2fbeaeadefaffeaffeafffcf2fbedb0f0fbea">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Gavin</td>
                            <td>Cortez</td>
                            <td>Team Leader</td>
                            <td>San Francisco</td>
                            <td>22</td>
                            <td>2008/10/26</td>
                            <td>$235,500</td>
                            <td>2860</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="e681c885899492839ca6828792879287848a8395c8888392">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Martena</td>
                            <td>Mccray</td>
                            <td>Post-Sales support</td>
                            <td>Edinburgh</td>
                            <td>46</td>
                            <td>2011/03/09</td>
                            <td>$324,050</td>
                            <td>8240</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="0b6625666868796a724b6f6a7f6a7f6a69676e7825656e7f">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Unity</td>
                            <td>Butler</td>
                            <td>Marketing Designer</td>
                            <td>San Francisco</td>
                            <td>47</td>
                            <td>2009/12/09</td>
                            <td>$85,675</td>
                            <td>5384</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="ea9fc4889f9e868f98aa8e8b9e8b9e8b88868f99c4848f9e">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Howard</td>
                            <td>Hatfield</td>
                            <td>Office Manager</td>
                            <td>San Francisco</td>
                            <td>51</td>
                            <td>2008/12/16</td>
                            <td>$164,500</td>
                            <td>7031</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="9bf3b5f3faeffdf2fef7ffdbfffaeffaeffaf9f7fee8b5f5feef">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Hope</td>
                            <td>Fuentes</td>
                            <td>Secretary</td>
                            <td>San Francisco</td>
                            <td>41</td>
                            <td>2010/02/12</td>
                            <td>$109,850</td>
                            <td>6318</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="f29adc9487979c869781b2969386938693909e9781dc9c9786">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Vivian</td>
                            <td>Harrell</td>
                            <td>Financial Controller</td>
                            <td>San Francisco</td>
                            <td>62</td>
                            <td>2009/02/14</td>
                            <td>$452,500</td>
                            <td>9422</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="40366e28213232252c2c00242134213421222c25336e2e2534">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Timothy</td>
                            <td>Mooney</td>
                            <td>Office Manager</td>
                            <td>London</td>
                            <td>37</td>
                            <td>2008/12/11</td>
                            <td>$136,200</td>
                            <td>7580</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="20540e4d4f4f4e455960444154415441424c45530e4e4554">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jackson</td>
                            <td>Bradshaw</td>
                            <td>Director</td>
                            <td>New York</td>
                            <td>65</td>
                            <td>2008/09/26</td>
                            <td>$645,750</td>
                            <td>1042</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="86eca8e4f4e7e2f5eee7f1c6e2e7f2e7f2e7e4eae3f5a8e8e3f2">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Olivia</td>
                            <td>Liang</td>
                            <td>Support Engineer</td>
                            <td>Singapore</td>
                            <td>64</td>
                            <td>2011/02/03</td>
                            <td>$234,500</td>
                            <td>2120</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="b0df9edcd9d1ded7f0d4d1c4d1c4d1d2dcd5c39eded5c4">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Bruno</td>
                            <td>Nash</td>
                            <td>Software Engineer</td>
                            <td>London</td>
                            <td>38</td>
                            <td>2011/05/03</td>
                            <td>$163,500</td>
                            <td>6222</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="62004c0c03110a22060316031603000e07114c0c0716">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Sakura</td>
                            <td>Yamamoto</td>
                            <td>Support Engineer</td>
                            <td>Tokyo</td>
                            <td>37</td>
                            <td>2009/08/19</td>
                            <td>$139,575</td>
                            <td>9383</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="ed9ec3948c808c80829982ad898c998c998c8f81889ec3838899">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Thor</td>
                            <td>Walton</td>
                            <td>Developer</td>
                            <td>New York</td>
                            <td>61</td>
                            <td>2013/08/11</td>
                            <td>$98,540</td>
                            <td>8327</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="ff8bd1889e938b9091bf9b9e8b9e8b9e9d939a8cd1919a8b">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Finn</td>
                            <td>Camacho</td>
                            <td>Support Engineer</td>
                            <td>San Francisco</td>
                            <td>47</td>
                            <td>2009/07/07</td>
                            <td>$87,500</td>
                            <td>2927</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="395f175a5854585a5156795d584d584d585b555c4a17575c4d">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Serge</td>
                            <td>Baldwin</td>
                            <td>Data Coordinator</td>
                            <td>Singapore</td>
                            <td>64</td>
                            <td>2012/04/09</td>
                            <td>$138,575</td>
                            <td>8352</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="bfcc91ddded3dbc8d6d1ffdbdecbdecbdeddd3dacc91d1dacb">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Zenaida</td>
                            <td>Frank</td>
                            <td>Software Engineer</td>
                            <td>New York</td>
                            <td>63</td>
                            <td>2010/01/04</td>
                            <td>$125,250</td>
                            <td>7439</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="acd682cadecdc2c7ecc8cdd8cdd8cdcec0c9df82c2c9d8">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Zorita</td>
                            <td>Serrano</td>
                            <td>Software Engineer</td>
                            <td>San Francisco</td>
                            <td>56</td>
                            <td>2012/06/01</td>
                            <td>$115,000</td>
                            <td>4389</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="2d57035e485f5f4c43426d494c594c594c4f41485e03434859">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jennifer</td>
                            <td>Acosta</td>
                            <td>Junior Javascript Developer</td>
                            <td>Edinburgh</td>
                            <td>43</td>
                            <td>2013/02/01</td>
                            <td>$75,650</td>
                            <td>3431</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="63094d02000c10170223070217021702010f06104d0d0617">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Cara</td>
                            <td>Stevens</td>
                            <td>Sales Assistant</td>
                            <td>New York</td>
                            <td>46</td>
                            <td>2011/12/06</td>
                            <td>$145,600</td>
                            <td>3990</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="73105d00071605161d0033171207120712111f16005d1d1607">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Hermione</td>
                            <td>Butler</td>
                            <td>Regional Director</td>
                            <td>London</td>
                            <td>47</td>
                            <td>2011/03/21</td>
                            <td>$356,250</td>
                            <td>1016</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="2b4305495e5f474e596b4f4a5f4a5f4a49474e5805454e5f">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Lael</td>
                            <td>Greer</td>
                            <td>Systems Administrator</td>
                            <td>London</td>
                            <td>21</td>
                            <td>2009/02/27</td>
                            <td>$103,500</td>
                            <td>6733</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="c2aeeca5b0a7a7b082a6a3b6a3b6a3a0aea7b1ecaca7b6">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jonas</td>
                            <td>Alexander</td>
                            <td>Developer</td>
                            <td>San Francisco</td>
                            <td>30</td>
                            <td>2010/07/14</td>
                            <td>$86,500</td>
                            <td>8196</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="c0aaeea1aca5b8a1aea4a5b280a4a1b4a1b4a1a2aca5b3eeaea5b4">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Shad</td>
                            <td>Decker</td>
                            <td>Regional Director</td>
                            <td>Edinburgh</td>
                            <td>51</td>
                            <td>2008/11/13</td>
                            <td>$183,000</td>
                            <td>6373</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="4a39642e2f29212f380a2e2b3e2b3e2b28262f3964242f3e">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Michael</td>
                            <td>Bruce</td>
                            <td>Javascript Developer</td>
                            <td>Singapore</td>
                            <td>29</td>
                            <td>2011/06/27</td>
                            <td>$183,000</td>
                            <td>5384</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="0865266a7a7d6b6d486c697c697c696a646d7b26666d7c">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Donna</td>
                            <td>Snider</td>
                            <td>Customer Support</td>
                            <td>New York</td>
                            <td>27</td>
                            <td>2011/01/25</td>
                            <td>$112,000</td>
                            <td>4226</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="d2b6fca1bcbbb6b7a092b6b3a6b3a6b3b0beb7a1fcbcb7a6">[email&#160;protected]</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h5>Immediately Show Hidden Details</h5>
                <span>Responsive has the ability to display the details that it has hidden in a variety of different ways. Its default is to allow the end user to toggle the the display by clicking on a row and showing the information in a DataTables child row. At times it can be useful not to require end user interaction to display the hidden data in a responsive manner, which can be done with the childRowImmediate display type.</span>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="feather icon-maximize full-card"></i></li>
                        <li><i class="feather icon-minus minimize-card"></i></li>
                        <li><i class="feather icon-trash-2 close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="show-hide-res" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Age</th>
                            <th>Start date</th>
                            <th>Salary</th>
                            <th>Extn.</th>
                            <th>E-mail</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Tiger</td>
                            <td>Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td>2011/04/25</td>
                            <td>$320,800</td>
                            <td>5421</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="0f7b2161667760614f6b6e7b6e7b6e6d636a7c21616a7b">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Garrett</td>
                            <td>Winters</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>63</td>
                            <td>2011/07/25</td>
                            <td>$170,750</td>
                            <td>8422</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="cdaae3baa4a3b9a8bfbe8da9acb9acb9acafa1a8bee3a3a8b9">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Ashton</td>
                            <td>Cox</td>
                            <td>Junior Technical Author</td>
                            <td>San Francisco</td>
                            <td>66</td>
                            <td>2009/01/12</td>
                            <td>$86,000</td>
                            <td>1562</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="0b6a256864734b6f6a7f6a7f6a69676e7825656e7f">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Cedric</td>
                            <td>Kelly</td>
                            <td>Senior Javascript Developer</td>
                            <td>Edinburgh</td>
                            <td>22</td>
                            <td>2012/03/29</td>
                            <td>$433,060</td>
                            <td>6224</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="aecd80c5cbc2c2d7eecacfdacfdacfccc2cbdd80c0cbda">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Airi</td>
                            <td>Satou</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>33</td>
                            <td>2008/11/28</td>
                            <td>$162,700</td>
                            <td>5407</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="62034c1103160d1722060316031603000e07114c0c0716">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Brielle</td>
                            <td>Williamson</td>
                            <td>Integration Specialist</td>
                            <td>New York</td>
                            <td>61</td>
                            <td>2012/12/02</td>
                            <td>$372,000</td>
                            <td>4804</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="1b79356c727777727a766874755b7f7a6f7a6f7a79777e6835757e6f">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Herrod</td>
                            <td>Chandler</td>
                            <td>Sales Assistant</td>
                            <td>San Francisco</td>
                            <td>59</td>
                            <td>2012/08/06</td>
                            <td>$137,500</td>
                            <td>9608</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="4c24622f242d222820293e0c282d382d382d2e20293f62222938">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Rhona</td>
                            <td>Davidson</td>
                            <td>Integration Specialist</td>
                            <td>Tokyo</td>
                            <td>55</td>
                            <td>2010/10/14</td>
                            <td>$327,900</td>
                            <td>6200</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="483a662c293e212c3b2726082c293c293c292a242d3b66262d3c">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Colleen</td>
                            <td>Hurst</td>
                            <td>Javascript Developer</td>
                            <td>San Francisco</td>
                            <td>39</td>
                            <td>2009/09/15</td>
                            <td>$205,500</td>
                            <td>2360</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="2645084e5354555266424752475247444a435508484352">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Sonya</td>
                            <td>Frost</td>
                            <td>Software Engineer</td>
                            <td>Edinburgh</td>
                            <td>23</td>
                            <td>2008/12/13</td>
                            <td>$103,600</td>
                            <td>1667</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="364518504459454276525742574257545a534518585342">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jena</td>
                            <td>Gaines</td>
                            <td>Office Manager</td>
                            <td>London</td>
                            <td>30</td>
                            <td>2008/12/19</td>
                            <td>$90,560</td>
                            <td>3814</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="e48eca83858d8a8197a480859085908586888197ca8a8190">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Quinn</td>
                            <td>Flynn</td>
                            <td>Support Lead</td>
                            <td>Edinburgh</td>
                            <td>22</td>
                            <td>2013/03/03</td>
                            <td>$342,000</td>
                            <td>9497</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="1869367e74617676587c796c796c797a747d6b36767d6c">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Charde</td>
                            <td>Marshall</td>
                            <td>Regional Director</td>
                            <td>San Francisco</td>
                            <td>36</td>
                            <td>2008/10/16</td>
                            <td>$470,600</td>
                            <td>6741</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="7615581b1704051e171a1a36121702170217141a130558181302">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Haley</td>
                            <td>Kennedy</td>
                            <td>Senior Marketing Designer</td>
                            <td>London</td>
                            <td>43</td>
                            <td>2012/12/18</td>
                            <td>$313,500</td>
                            <td>3597</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="046c2a6f616a6a61607d44606570657065666861772a6a6170">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Tatyana</td>
                            <td>Fitzpatrick</td>
                            <td>Regional Director</td>
                            <td>London</td>
                            <td>19</td>
                            <td>2010/03/17</td>
                            <td>$385,750</td>
                            <td>1965</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="9febb1f9f6ebe5effeebedf6fcf4dffbfeebfeebfefdf3faecb1f1faeb">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Michael</td>
                            <td>Silva</td>
                            <td>Marketing Designer</td>
                            <td>London</td>
                            <td>66</td>
                            <td>2012/11/27</td>
                            <td>$198,500</td>
                            <td>1581</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="264b08554f4a504766424752475247444a435508484352">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Paul</td>
                            <td>Byrd</td>
                            <td>Chief Financial Officer (CFO)</td>
                            <td>New York</td>
                            <td>64</td>
                            <td>2010/06/09</td>
                            <td>$725,000</td>
                            <td>3059</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="661648041f140226020712071207040a031548080312">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Gloria</td>
                            <td>Little</td>
                            <td>Systems Administrator</td>
                            <td>New York</td>
                            <td>59</td>
                            <td>2009/04/10</td>
                            <td>$237,500</td>
                            <td>1721</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="8ceba2e0e5f8f8e0e9cce8edf8edf8edeee0e9ffa2e2e9f8">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Bradley</td>
                            <td>Greer</td>
                            <td>Software Engineer</td>
                            <td>London</td>
                            <td>41</td>
                            <td>2012/10/13</td>
                            <td>$132,000</td>
                            <td>2558</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="25470b425740405765414451445144474940560b4b4051">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Dai</td>
                            <td>Rios</td>
                            <td>Personnel Lead</td>
                            <td>Edinburgh</td>
                            <td>35</td>
                            <td>2012/09/26</td>
                            <td>$217,500</td>
                            <td>2290</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="9ffbb1edf6f0ecdffbfeebfeebfefdf3faecb1f1faeb">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jenette</td>
                            <td>Caldwell</td>
                            <td>Development Lead</td>
                            <td>New York</td>
                            <td>30</td>
                            <td>2011/09/03</td>
                            <td>$345,000</td>
                            <td>1937</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="4b2165282a272f3c2e27270b2f2a3f2a3f2a29272e3865252e3f">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Yuri</td>
                            <td>Berry</td>
                            <td>Chief Marketing Officer (CMO)</td>
                            <td>New York</td>
                            <td>40</td>
                            <td>2009/06/25</td>
                            <td>$675,000</td>
                            <td>6154</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="d2abfcb0b7a0a0ab92b6b3a6b3a6b3b0beb7a1fcbcb7a6">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Caesar</td>
                            <td>Vance</td>
                            <td>Pre-Sales Support</td>
                            <td>New York</td>
                            <td>21</td>
                            <td>2011/12/12</td>
                            <td>$106,450</td>
                            <td>8330</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="cdaee3bbaca3aea88da9acb9acb9acafa1a8bee3a3a8b9">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Doris</td>
                            <td>Wilder</td>
                            <td>Sales Assistant</td>
                            <td>Sidney</td>
                            <td>23</td>
                            <td>2010/09/20</td>
                            <td>$85,600</td>
                            <td>3023</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="aeca80d9c7c2cacbdceecacfdacfdacfccc2cbdd80c0cbda">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Angelica</td>
                            <td>Ramos</td>
                            <td>Chief Executive Officer (CEO)</td>
                            <td>London</td>
                            <td>47</td>
                            <td>2009/10/09</td>
                            <td>$1,200,000</td>
                            <td>5797</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="45246b3724282a3605212431243124272920366b2b2031">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Gavin</td>
                            <td>Joyce</td>
                            <td>Developer</td>
                            <td>Edinburgh</td>
                            <td>42</td>
                            <td>2010/12/22</td>
                            <td>$92,575</td>
                            <td>8822</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="f89fd69297819b9db89c998c998c999a949d8bd6969d8c">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jennifer</td>
                            <td>Chang</td>
                            <td>Regional Director</td>
                            <td>Singapore</td>
                            <td>28</td>
                            <td>2010/11/14</td>
                            <td>$357,650</td>
                            <td>9239</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="711b5f1219101f1631151005100510131d14025f1f1405">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Brenden</td>
                            <td>Wagner</td>
                            <td>Software Engineer</td>
                            <td>San Francisco</td>
                            <td>28</td>
                            <td>2011/06/07</td>
                            <td>$206,850</td>
                            <td>1314</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="06642871676168637446626772677267646a637528686372">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Fiona</td>
                            <td>Green</td>
                            <td>Chief Operating Officer (COO)</td>
                            <td>San Francisco</td>
                            <td>48</td>
                            <td>2010/03/11</td>
                            <td>$850,000</td>
                            <td>2947</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="42246c253027272c02262336233623202e27316c2c2736">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Shou</td>
                            <td>Itou</td>
                            <td>Regional Marketing</td>
                            <td>Tokyo</td>
                            <td>20</td>
                            <td>2011/08/14</td>
                            <td>$163,000</td>
                            <td>8899</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="790a57100d160c391d180d180d181b151c0a57171c0d">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Michelle</td>
                            <td>House</td>
                            <td>Integration Specialist</td>
                            <td>Sidney</td>
                            <td>37</td>
                            <td>2011/06/02</td>
                            <td>$95,400</td>
                            <td>2769</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="d8b5f6b0b7adabbd98bcb9acb9acb9bab4bdabf6b6bdac">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Suki</td>
                            <td>Burks</td>
                            <td>Developer</td>
                            <td>London</td>
                            <td>53</td>
                            <td>2009/10/22</td>
                            <td>$114,500</td>
                            <td>6832</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="51227f3324233a2211353025302530333d34227f3f3425">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Prescott</td>
                            <td>Bartlett</td>
                            <td>Technical Author</td>
                            <td>London</td>
                            <td>27</td>
                            <td>2011/05/07</td>
                            <td>$145,000</td>
                            <td>3606</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="463668242734322a23323206222732273227242a233568282332">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Gavin</td>
                            <td>Cortez</td>
                            <td>Team Leader</td>
                            <td>San Francisco</td>
                            <td>22</td>
                            <td>2008/10/26</td>
                            <td>$235,500</td>
                            <td>2860</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="e384cd808c91978699a3878297829782818f8690cd8d8697">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Martena</td>
                            <td>Mccray</td>
                            <td>Post-Sales support</td>
                            <td>Edinburgh</td>
                            <td>46</td>
                            <td>2011/03/09</td>
                            <td>$324,050</td>
                            <td>8240</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="ff92d1929c9c8d9e86bf9b9e8b9e8b9e9d939a8cd1919a8b">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Unity</td>
                            <td>Butler</td>
                            <td>Marketing Designer</td>
                            <td>San Francisco</td>
                            <td>47</td>
                            <td>2009/12/09</td>
                            <td>$85,675</td>
                            <td>5384</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="a9dc87cbdcddc5ccdbe9cdc8ddc8ddc8cbc5ccda87c7ccdd">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Howard</td>
                            <td>Hatfield</td>
                            <td>Office Manager</td>
                            <td>San Francisco</td>
                            <td>51</td>
                            <td>2008/12/16</td>
                            <td>$164,500</td>
                            <td>7031</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="ff97d1979e8b99969a939bbf9b9e8b9e8b9e9d939a8cd1919a8b">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Hope</td>
                            <td>Fuentes</td>
                            <td>Secretary</td>
                            <td>San Francisco</td>
                            <td>41</td>
                            <td>2010/02/12</td>
                            <td>$109,850</td>
                            <td>6318</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="4b23652d3e2e253f2e380b2f2a3f2a3f2a29272e3865252e3f">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Vivian</td>
                            <td>Harrell</td>
                            <td>Financial Controller</td>
                            <td>San Francisco</td>
                            <td>62</td>
                            <td>2009/02/14</td>
                            <td>$452,500</td>
                            <td>9422</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="f187df99908383949d9db1959085908590939d9482df9f9485">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Timothy</td>
                            <td>Mooney</td>
                            <td>Office Manager</td>
                            <td>London</td>
                            <td>37</td>
                            <td>2008/12/11</td>
                            <td>$136,200</td>
                            <td>7580</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="25510b484a4a4b405c65414451445144474940560b4b4051">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jackson</td>
                            <td>Bradshaw</td>
                            <td>Director</td>
                            <td>New York</td>
                            <td>65</td>
                            <td>2008/09/26</td>
                            <td>$645,750</td>
                            <td>1042</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="82e8ace0f0e3e6f1eae3f5c2e6e3f6e3f6e3e0eee7f1acece7f6">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Olivia</td>
                            <td>Liang</td>
                            <td>Support Engineer</td>
                            <td>Singapore</td>
                            <td>64</td>
                            <td>2011/02/03</td>
                            <td>$234,500</td>
                            <td>2120</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="d5bafbb9bcb4bbb295b1b4a1b4a1b4b7b9b0a6fbbbb0a1">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Bruno</td>
                            <td>Nash</td>
                            <td>Software Engineer</td>
                            <td>London</td>
                            <td>38</td>
                            <td>2011/05/03</td>
                            <td>$163,500</td>
                            <td>6222</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="3e5c10505f4d567e5a5f4a5f4a5f5c525b4d10505b4a">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Sakura</td>
                            <td>Yamamoto</td>
                            <td>Support Engineer</td>
                            <td>Tokyo</td>
                            <td>37</td>
                            <td>2009/08/19</td>
                            <td>$139,575</td>
                            <td>9383</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="f88bd68199959995978c97b89c998c998c999a949d8bd6969d8c">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Thor</td>
                            <td>Walton</td>
                            <td>Developer</td>
                            <td>New York</td>
                            <td>61</td>
                            <td>2013/08/11</td>
                            <td>$98,540</td>
                            <td>8327</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="daaef4adbbb6aeb5b49abebbaebbaebbb8b6bfa9f4b4bfae">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Finn</td>
                            <td>Camacho</td>
                            <td>Support Engineer</td>
                            <td>San Francisco</td>
                            <td>47</td>
                            <td>2009/07/07</td>
                            <td>$87,500</td>
                            <td>2927</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="ddbbf3bebcb0bcbeb5b29db9bca9bca9bcbfb1b8aef3b3b8a9">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Serge</td>
                            <td>Baldwin</td>
                            <td>Data Coordinator</td>
                            <td>Singapore</td>
                            <td>64</td>
                            <td>2012/04/09</td>
                            <td>$138,575</td>
                            <td>8352</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="1f6c317d7e737b6876715f7b7e6b7e6b7e7d737a6c31717a6b">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Zenaida</td>
                            <td>Frank</td>
                            <td>Software Engineer</td>
                            <td>New York</td>
                            <td>63</td>
                            <td>2010/01/04</td>
                            <td>$125,250</td>
                            <td>7439</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="403a6e2632212e2b00242134213421222c25336e2e2534">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Zorita</td>
                            <td>Serrano</td>
                            <td>Software Engineer</td>
                            <td>San Francisco</td>
                            <td>56</td>
                            <td>2012/06/01</td>
                            <td>$115,000</td>
                            <td>4389</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="6913471a0c1b1b080706290d081d081d080b050c1a47070c1d">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jennifer</td>
                            <td>Acosta</td>
                            <td>Junior Javascript Developer</td>
                            <td>Edinburgh</td>
                            <td>43</td>
                            <td>2013/02/01</td>
                            <td>$75,650</td>
                            <td>3431</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="442e6a25272b37302504202530253025262821376a2a2130">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Cara</td>
                            <td>Stevens</td>
                            <td>Sales Assistant</td>
                            <td>New York</td>
                            <td>46</td>
                            <td>2011/12/06</td>
                            <td>$145,600</td>
                            <td>3990</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="e083ce93948596858e93a0">[email&#160;protected]</a>datatables.net
                            </td>
                        </tr>
                        <tr>
                            <td>Hermione</td>
                            <td>Butler</td>
                            <td>Regional Director</td>
                            <td>London</td>
                            <td>47</td>
                            <td>2011/03/21</td>
                            <td>$356,250</td>
                            <td>1016</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="b4dc9ad6c1c0d8d1c6f4d0d5c0d5c0d5d6d8d1c79adad1c0">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Lael</td>
                            <td>Greer</td>
                            <td>Systems Administrator</td>
                            <td>London</td>
                            <td>21</td>
                            <td>2009/02/27</td>
                            <td>$103,500</td>
                            <td>6733</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="167a38716473736456727762776277747a736538787362">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Jonas</td>
                            <td>Alexander</td>
                            <td>Developer</td>
                            <td>San Francisco</td>
                            <td>30</td>
                            <td>2010/07/14</td>
                            <td>$86,500</td>
                            <td>8196</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="adc783ccc1c8d5ccc3c9c8dfedc9ccd9ccd9cccfc1c8de83c3c8d9">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Shad</td>
                            <td>Decker</td>
                            <td>Regional Director</td>
                            <td>Edinburgh</td>
                            <td>51</td>
                            <td>2008/11/13</td>
                            <td>$183,000</td>
                            <td>6373</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="62114c06070109071022060316031603000e07114c0c0716">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Michael</td>
                            <td>Bruce</td>
                            <td>Javascript Developer</td>
                            <td>Singapore</td>
                            <td>29</td>
                            <td>2011/06/27</td>
                            <td>$183,000</td>
                            <td>5384</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="5c31723e2e293f391c383d283d283d3e30392f72323928">[email&#160;protected]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Donna</td>
                            <td>Snider</td>
                            <td>Customer Support</td>
                            <td>New York</td>
                            <td>27</td>
                            <td>2011/01/25</td>
                            <td>$112,000</td>
                            <td>4226</td>
                            <td><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                   data-cfemail="a5c18bd6cbccc1c0d7e5c1c4d1c4d1c4c7c9c0d68bcbc0d1">[email&#160;protected]</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/datatables.net/js/jquery.dataTables.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/dataTables.buttons.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/jszip.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/pdfmake.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/vfs_fonts.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.print.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.html5.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-bs4/js/dataTables.bootstrap4.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive/js/dataTables.responsive.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") }}"
            type="text/javascript"></script>

    <script src="{{ asset("adminity/pages/data-table/extensions/responsive/js/dataTables.responsive.min.js") }}"
            type="text/javascript"></script>

    <script src="{{ asset("adminity/pages/data-table/extensions/responsive/js/responsive-custom.js") }}"
            type="text/javascript"></script>
@endsection
