@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>To-Do</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">To-Do</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">To Do</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>To Do Card List</h5>
                    </div>
                    <div class="card-block">
                        <div class="input-group input-group-button">
                            <input type="text" class="form-control" placeholder="Create your task list"
                                   name="task-insert">
                            <button id="create-task" class="btn btn-primary input-group-addon">Add Task</button>
                        </div>
                        <section id="task-container">
                            <ul id="task-list">
                                <li>
                                    <p>Lorem Ipsum Dolor Sit Amet</p>
                                </li>
                                <li>
                                    <p>Lorem Ipsum Dolor Sit Amet</p>
                                </li>
                                <li>
                                    <p>Lorem Ipsum Dolor Sit Amet</p>
                                </li>
                                <li>
                                    <p>Lorem Ipsum Dolor Sit Amet</p>
                                </li>
                                <li>
                                    <p>Lorem Ipsum Dolor Sit Amet</p>
                                </li>
                                <li>
                                    <p>Lorem Ipsum Dolor Sit Amet</p>
                                </li>
                            </ul>
                            <div class="text-center">
                                <button id="clear-all-tasks" class="btn btn-danger m-b-0" type="button">Clear All
                                    Tasks
                                </button>
                            </div>
                        </section>
                    </div>
                </div>

            </div>
            <div class="col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>To Do List
                        </h5>
                        <label class="label label-success">Today</label>
                    </div>
                    <div class="card-block">
                        <div class="input-group input-group-button">
                            <input type="text" class="form-control add_task_todo" placeholder="Create your task list"
                                   name="task-insert">
                            <button id="add-btn" class="btn btn-primary input-group-addon">Add Task</button>
                        </div>
                        <div class="new-task">
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label class="check-task">
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
 </span>
                                        <span>Lorem Ipsum Dolor Sit Amet</span>
                                    </label>
                                </div>
                                <div class="f-right">
                                    <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                </div>
                            </div>
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label class="check-task">
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                        <span>Lorem Ipsum Dolor Sit Amet</span>
                                    </label>
                                </div>
                                <div class="f-right">
                                    <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                </div>
                            </div>
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label class="check-task">
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                        <span>Lorem Ipsum Dolor Sit Amet</span>
                                    </label>
                                </div>
                                <div class="f-right">
                                    <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                </div>
                            </div>
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label class="check-task">
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                        <span>Lorem Ipsum Dolor Sit Amet</span>
                                    </label>
                                </div>
                                <div class="f-right">
                                    <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                </div>
                            </div>
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label class="check-task">
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                        <span>Lorem Ipsum Dolor Sit Amet</span>
                                    </label>
                                </div>
                                <div class="f-right">
                                    <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                </div>
                            </div>
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label class="check-task">
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                        <span>Lorem Ipsum Dolor Sit Amet</span>
                                    </label>
                                </div>
                                <div class="f-right">
                                    <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>To Do List in Modal</h5>
                    </div>
                    <div class="card-block">
                        <section class="task-panel tasks-widget">
                            <div class="panel-body">
                                <div class="task-content">
                                    <div class="to-do-label">
                                        <div class="checkbox-fade fade-in-primary">
                                            <label class="check-task">
                                                <input type="checkbox" value="">
                                                <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                <span class="task-title-sp">Lorem Ipsum Dolor Sit Amet</span>
                                                <span class="f-right hidden-phone">
<i class="icofont icofont-ui-delete delete_todo"></i>
</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="to-do-label">
                                        <div class="checkbox-fade fade-in-primary">
                                            <label class="check-task">
                                                <input type="checkbox" value="">
                                                <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                <span class="task-title-sp">Lorem Ipsum Dolor Sit Amet</span>
                                                <span class="f-right hidden-phone">
<i class="icofont icofont-ui-delete delete_todo"></i>
</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="to-do-label">
                                        <div class="checkbox-fade fade-in-primary">
                                            <label class="check-task">
                                                <input type="checkbox" value="">
                                                <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                <span class="task-title-sp">Lorem Ipsum Dolor Sit Amet</span>
                                                <span class="f-right hidden-phone">
<i class="icofont icofont-ui-delete delete_todo"></i>
</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="to-do-label">
                                        <div class="checkbox-fade fade-in-primary">
                                            <label class="check-task">
                                                <input type="checkbox" value="">
                                                <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                <span class="task-title-sp">Lorem Ipsum Dolor Sit Amet</span>
                                                <span class="f-right hidden-phone">
<i class="icofont icofont-ui-delete delete_todo"></i>
</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <a class="btn btn-primary btn-add-task waves-effect waves-light m-t-10" href="#"
                                       data-toggle="modal" data-target="#flipFlop"><i class="icofont icofont-plus"></i>
                                        Add New Tasks</a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/todo/todo.js") }}"></script>
@endsection

