@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/vector-maps/css/jquery-jvectormap-2.0.2.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Vector Map</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Maps</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Vector Maps</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Basic Map With Markers</h5>
                    </div>
                    <div class="card-block">
                        <div id="world-map-markers" class="set-map"></div>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>India Map</h5>
                    </div>
                    <div class="card-block">
                        <div id="india" class="set-map"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Asia Map</h5>
                    </div>
                    <div class="card-block">
                        <div id="asia" class="set-map"></div>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Canada Map</h5>
                    </div>
                    <div class="card-block">
                        <div id="canada" class="set-map"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>USA Map</h5>
                    </div>
                    <div class="card-block">
                        <div id="usa" class="set-map"></div>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <h5>UK Map</h5>
                    </div>
                    <div class="card-block">
                        <div id="uk" class="set-map"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/js/markerclusterer.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-2.0.2.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-world-mill-en.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/gdp-data.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-us-aea-en.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-uk-mill-en.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-au-mill.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-us-il-chicago-mill-en.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-ca-lcc.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-de-mill.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-in-mill.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/jquery-jvectormap-asia-mill.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/vector-maps/js/map-vector.js") }}" type="text/javascript"></script>
@endsection
