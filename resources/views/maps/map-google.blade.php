@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Google Map</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Maps</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Google Maps</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Basic</h5>
                        <span>Map shows places around the world</span>
                    </div>
                    <div class="card-block">
                        <div id="basic-map" class="set-map"></div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Markers</h5>
                        <span>Maps shows <code>location</code> of the place</span>
                    </div>
                    <div class="card-block">
                        <div id="markers-map" class="set-map"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Overlay</h5>
                        <span>Map shows places around the world</span>
                    </div>
                    <div class="card-block">
                        <div id="mapOverlay" class="set-map"></div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Geo-Coding</h5>
                        <span>Search your location</span>
                    </div>
                    <div class="card-block">
                        <form method="post" id="geocoding_form">
                            <div class="input-group input-group-button">
                                <input type="text" id="address" class="form-control" placeholder="Write your place">
                                <span class="input-group-addon" id="basic-addon1">
<button class="btn btn-primary">Search Location</button>
</span>
                            </div>
                        </form>
                        <div id="mapGeo" class="set-map"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Street View</h5>
                        <span>Map shows view of street</span>
                    </div>
                    <div class="card-block">
                        <div id="mapStreet" class="set-map"></div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Map Types</h5>
                        <span>Select your <code>map-types</code> to see differant views</span>
                    </div>
                    <div class="card-block">
                        <div id="mapTypes" class="set-map"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>GeoRSS Layers</h5>
                        <span>Shows <code>RSS</code> location</span>
                    </div>
                    <div class="card-block">
                        <div id="georssmap" class="set-map"></div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Marker Clustering</h5>
                        <span>Multiple markers show differant location</span>
                    </div>
                    <div class="card-block">
                        <div id="map" class="set-map"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/js/markerclusterer.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/google-maps/gmaps.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/google-maps/google-maps.js") }}"></script>
@endsection
