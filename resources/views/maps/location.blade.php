@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/leaflet/v1.0.2/leaflet.css") }}"/>
    <script src="{{ asset("adminity/components/leaflet/v1.0.2/leaflet.js") }}" type="text/javascript"></script>

    <link href='{{ asset("adminity/components/mapbox-gl-js/v0.28.0/mapbox-gl.css") }}' rel='stylesheet'/>
    <script src='{{ asset("adminity/pages/location/leaflet-mapbox-gl.js") }}' type="text/javascript"></script>

    <script src='{{ asset("adminity/components/leaflet.markercluster/1.0.0/leaflet.markercluster.js") }}'
            type="text/javascript"></script>
    <link href='{{ asset("adminity/components/leaflet.markercluster/1.0.0/MarkerCluster.css") }}' rel='stylesheet'/>
    <link href='{{ asset("adminity/components/leaflet.markercluster/1.0.0/MarkerCluster.Default.css") }}'
          rel='stylesheet'/>

    <script src="{{ asset("adminity/components/www.mapbox.com/files/assets/data/realworld.388.html") }}"
            type="text/javascript"></script>
    <style>
        #map {
            width: 100%;
            height: 600px;
        }
    </style>
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Location</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Maps</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Location</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="card">
                    <div class="card-block">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/location/leaflet-mapbox-gl.js") }}"></script>
    <script type="text/javascript">
        var map = L.map('map', {
            maxZoom: 17
        }).setView([-37.821, 175.219], 16);
        L.tileLayer('../../../../%7bs%7d.tile.osm.org/%7bz%7d/%7bx%7d/%7by%7d.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        var greenIcon = L.icon({
            iconUrl: '{{ asset("adminity/images/social/img1.jpg") }}',
            shadowUrl: '{{ asset("adminity/images/social/profile.jpg") }}',

            iconSize: [38, 95], // size of the icon
            shadowSize: [50, 64], // size of the shadow
            iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
            shadowAnchor: [4, 62], // the same for the shadow
            popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        });

        var gl = L.mapboxGL({
            accessToken: 'no-token',
            style: 'https://raw.githubusercontent.com/osm2vectortiles/mapbox-gl-styles/master/styles/bright-v9-cdn.json'
        }).addTo(map);

        var LeafIcon = L.Icon.extend({
            options: {
                shadowUrl: '{{ asset("adminity/images/social/img1.jpg") }}',
                iconSize: [38, 95],
                shadowSize: [50, 64],
                iconAnchor: [22, 94],
                shadowAnchor: [4, 62],
                popupAnchor: [-3, -76]
            }
        });

        var markers = L.markerClusterGroup();

        for (var i = 0; i < addressPoints.length; i++) {
            var a = addressPoints[i];
            var title = a[2];
            var marker = L.marker(new L.LatLng(a[0], a[1]), {
                title: title
            });
            marker.bindPopup(title);
            markers.addLayer(marker);
        }

        map.addLayer(markers);
    </script>
@endsection
