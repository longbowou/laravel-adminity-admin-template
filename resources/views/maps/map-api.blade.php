@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/vector-maps/css/jquery-jvectormap-2.0.2.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Google map search API</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Maps</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Vector Maps</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Google map search</h5>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="">
                                    <form method="post" id="address-search">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Enter address">
                                            <span class="input-group-btn">
<button type="submit" class="btn btn-primary">Search</button>
</span>
                                        </div>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                                <br>
                                <div class="gmap1 full-page-google-map">
                                    <div id="map-1" style='max-height:600px;height:1067px;'></div>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                                <div class="map-toolbar">
                                    <div class="row">
                                        <div class="col-xl-12 text-center location-mob-btn">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-danger" id="map-unzoom">-</button>
                                                <button type="button" class="btn btn-danger" id="map-resetzoom">Reset</button>
                                                <button type="button" class="btn btn-danger" id="map-zoom">+</button>
                                            </div>
                                            &nbsp;
                                            <a href="#" class="btn btn-primary m-r-10" id="go-sthlm">Go to Stockholm</a>
                                            <a href="#" class="btn btn-warning" id="go-bln">Go to Berlin</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/js/markerclusterer.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/google-maps/gmaps.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/map-api/map-api.js") }}"></script>
@endsection
