@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Search Result 1</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Search</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Search Result 1</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-6 offset-lg-3">
                                <p class="txt-highlight text-center m-t-20">lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                    nisi ut aliquip ex ea commodo consequat.
                                </p>
                            </div>
                        </div>
                        <div class="row seacrh-header">
                            <div class="col-lg-4 offset-lg-4 offset-sm-3 col-sm-6 offset-sm-1 col-xs-12">
                                <div class="input-group input-group-button input-group-primary">
                                    <input type="text" class="form-control" placeholder="Search here...">
                                    <button class="btn btn-primary input-group-addon" id="basic-addon1">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="m-b-20"><b>20</b> Search Results Found</h4>

                <div class="row search-result">
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/01.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i class="icofont icofont-question"></i></a>
                                </h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/02.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i class="icofont icofont-question"></i></a>
                                </h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 30 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/03.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i class="icofont icofont-question"></i></a>
                                </h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 2 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/04.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i class="icofont icofont-question"></i></a>
                                </h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 20 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/05.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i class="icofont icofont-question"></i></a>
                                </h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 12 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/06.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i
                                            class="icofont icofont-question f-right"></i></a></h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 12 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/06.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i
                                            class="icofont icofont-question f-right"></i></a></h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 12 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/03.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i
                                            class="icofont icofont-question f-right"></i></a></h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 12 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/02.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i class="icofont icofont-question"></i></a>
                                </h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 30 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/03.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i class="icofont icofont-question"></i></a>
                                </h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 2 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/04.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i class="icofont icofont-question"></i></a>
                                </h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 20 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 ">
                        <div class="card">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset("adminity/images/search-images/05.jpg") }}"
                                 alt="Card image cap">
                            <div class="card-block">
                                <h5 class="card-title">Web Apps <a href="#"><i class="icofont icofont-question"></i></a>
                                </h5>
                                <p class="card-text text-muted">This is a longer card with supporting text below as a
                                    natural </p>
                                <p class="card-text"><small class="text-muted">Last updated 12 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

