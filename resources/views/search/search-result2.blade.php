@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/advance-elements/css/bootstrap-datetimepicker.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/bootstrap-daterangepicker/css/daterangepicker.css") }}"/>

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datedropper/css/datedropper.min.css") }}"/>
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Search Result 2</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Search</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Search Result 2</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-6 offset-lg-3">
                                <p class="txt-highlight text-center m-t-20">lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                    nisi ut aliquip ex ea commodo consequat.
                                </p>
                            </div>
                        </div>
                        <div class="row seacrh-header">
                            <div class="col-lg-4 offset-lg-4 offset-sm-3 col-sm-6 offset-sm-1 col-xs-12">
                                <div class="input-group input-group-button input-group-primary">
                                    <input type="text" class="form-control" placeholder="Search here...">
                                    <button class="btn btn-primary input-group-addon">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="m-b-20"><b>20</b> Search Results Found</h4>
                <div class="row">

                    <div class="col-lg-12 col-xl-7 search2 search-result">
                        <div class="card card-main">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="search-content">
                                            <img class="card-img-top" src="{{ asset("adminity/images/search-images/01.jpg") }}"
                                                 alt="Card image cap">
                                            <div class="card-block">
                                                <h5 class="card-title">Web Apps
                                                    <a href="#"></a>
                                                </h5>
                                                <p class="card-text text-muted">This is a longer card with supporting
                                                    text below as a natural quis nostrud exercitation ullamco laboris
                                                    nisi ut aliquip ex ea commodo consequat </p>
                                                <p class="card-text"><small class="text-muted">Last updated 12 mins
                                                        ago</small></p>
                                            </div>
                                        </div>
                                        <div class="search-content">
                                            <img class="card-img-top" src="{{ asset("adminity/images/search-images/02.jpg") }}"
                                                 alt="Card image cap">
                                            <div class="card-block">
                                                <h5 class="card-title">Web Apps
                                                    <a href="#"></a>
                                                </h5>
                                                <p class="card-text text-muted">Quis nostrud exercitation ullamco
                                                    laboris nisi ut aliquip ex ea commodo consequat, This is a longer
                                                    card with supporting text below as a natural </p>
                                                <p class="card-text"><small class="text-muted">Last updated 2 weeks mins
                                                        ago</small></p>
                                            </div>
                                        </div>
                                        <div class="search-content">
                                            <img class="card-img-top" src="{{ asset("adminity/images/search-images/03.jpg") }}"
                                                 alt="Card image cap">
                                            <div class="card-block">
                                                <h5 class="card-title">Web Apps
                                                    <a href="#"></a>
                                                </h5>
                                                <p class="card-text text-muted">This is a longer card with supporting
                                                    text below as a natural quis nostrud exercitation ullamco laboris
                                                    nisi ut aliquip ex ea commodo consequat </p>
                                                <p class="card-text"><small class="text-muted">Last updated 59 mins
                                                        ago</small></p>
                                            </div>
                                        </div>
                                        <div class="search-content">
                                            <img class="card-img-top" src="{{ asset("adminity/images/search-images/04.jpg") }}"
                                                 alt="Card image cap">
                                            <div class="card-block">
                                                <h5 class="card-title">Web Apps
                                                    <a href="#"></a>
                                                </h5>
                                                <p class="card-text text-muted">Quis nostrud exercitation ullamco
                                                    laboris nisi ut aliquip ex ea commodo consequat, This is a longer
                                                    card with supporting text below as a natural </p>
                                                <p class="card-text"><small class="text-muted">Last updated 20 mins
                                                        ago</small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-xl-5">
                        <div class="card card-main">
                            <div class="card-header">
                                <h5 class="card-header-text">Filter</h5>
                            </div>
                            <div class="card-block">
                                <div class="input-group">
                                    <input id="dropper-default" class="form-control" type="text"
                                           placeholder="Birth Date"/>
                                </div>
                                <div class="input-group">
                                    <div class="input-group input-group-button input-group-primary">
                                        <input type="text" class="form-control" placeholder="Search here...">
                                        <button class="btn btn-primary input-group-addon">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/pages/advance-elements/moment-with-locales.min.js") }}"></script>
    <script type="text/javascript"
            src="{{ asset("adminity/components/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}"></script>
    <script type="text/javascript"
            src="{{ asset("adminity/pages/advance-elements/bootstrap-datetimepicker.min.js") }}"></script>
    <script type="text/javascript"
            src="{{ asset("adminity/components/bootstrap-daterangepicker/js/daterangepicker.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/components/datedropper/js/datedropper.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/advance-elements/custom-picker.js") }}"></script>
@endsection

