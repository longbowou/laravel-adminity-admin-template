@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/prism/prism.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Changelog</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Widget</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <div id="navigation">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card version">
                                <div class="card-header">
                                    <h5>Change Log</h5>
                                    <div class="card-header-right">
                                        <i class="icofont icofont-navigation-menu"></i>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="support-btn">
                                        <a href="#" class="btn btn-primary btn-block" target="_blank"><i
                                                class="icofont icofont-life-buoy"></i> Item support</a>
                                    </div>
                                    <ul class="nav navigation">
                                        <li class="navigation-header"><i class="icon-history pull-right"></i> <b>Version
                                                history</b></li>
                                        <li>
                                            <a href="#v_1_1">Version 1.2 <span
                                                    class="text-muted text-regular pull-right">02.12.2015</span></a>
                                        </li>
                                        <li>
                                            <a href="#v_1_0">Version 1.1 <span
                                                    class="text-muted text-regular pull-right">21.10.2015</span></a>
                                        </li>
                                        <li>
                                            <a href="#release">Initial Release <span
                                                    class="text-muted text-regular pull-right">01.10.2015</span></a>
                                        </li>
                                        <li class="navigation-divider"></li>
                                        <li class="navigation-header"><i class="icon-gear pull-right"></i> <b>Extras</b>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i
                                                    class="icofont icofont-speech-comments m-r-5"></i> Contact me</a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i class="icofont icofont-life-buoy m-r-5"></i>
                                                Website</a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i
                                                    class="icofont icofont-rocket-alt-2 m-r-5"></i> Other templates</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card" id="v_1_1">
                            <div class="card-header">
                                <h5>Version 1.2</h5>
                                <span>Version 1.2 is a quick update</span>
                                <div class="">
                                    <span class="text-muted heading-text m-r-5">December 1, 2015</span>
                                    <label class="label label-info">V.1.2</label>
                                </div>
                            </div>
                            <div class="card-block">
                                <p class="content-group">Version 1.2 is a quick update that contains some reported bugs
                                    fixes and plugins update. Nothing new is added since this is an addition to global
                                    1.2 version. Important milestone for 1.3
                                    version is footer and container height enhancements - jquery solution for
                                    calculating minimum container height will be replaced with pure CSS.</p>
                                <pre class="language-javascript"><code>// # List of updated plugins
                            // ------------------------------

                            [updated]  Bootstrap file input - to the latest version
                            [updated]  Select2 - from RC1 to stable 4.0.1 version


                            // # List of fixed bugs
                            // ------------------------------

                            // Core fixes
                            [fixed]  Documentation - correct release date on main page, fixed path to globalize/ library, gulp plugins to install
                            [fixed]  Navbar - added sticky sidebar top spacing if used with fixed top single navbar. To be enhanced in 1.3
                            [fixed]  Fixed sidebar and navbar - removed unnecessary affix code from the page

                            // Components fixes
                            [fixed]  Centered timeline - extra dots on desktop
                            [fixed]  Datatables Select extension - checkboxes are not selectable
                            [fixed]  Datatables Autofill and Select - wrong columns sorting in examples with checkboxes
                            [fixed]  Select2 selects - selected text overlaps arrow in single select
                            [fixed]  Select2 selects validation - wrong error/success label placement
                            </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card" id="v_1_0">
                            <div class="card-header">
                                <h5>Version 1.1</h5>
                                <span>First update is the most simplified and includes urgent bug fixes of core components</span>
                                <div class="">
                                    <span class="text-muted heading-text m-r-5">October 21, 2015</span>
                                    <label class="label label-info">V.1.1</label>
                                </div>
                            </div>
                            <div class="card-block">
                                <p class="content-group">First update is the most simplified and includes urgent bug
                                    fixes of core components, plugins and libraries. Also version 1.1 includes updates
                                    of some components to the latest stable versions.
                                    The only new thing here is RTL version of all 4 layouts, that support almost all
                                    available components and layout features. Below you can find general list of all
                                    changes and details about
                                    upgrading.</p>
                                <pre class="language-javascript"><code>// # List of new components
                            // ------------------------------

                            [new]  RTL layout for all 4 main layout variations
                            [new]  bootbox.less - new LESS file for extended Bootstrap modal dialogs


                            // # List of updated plugins
                            // ------------------------------

                            [updated]  CKEditor - latest version
                            [updated]  Select2 - latest 3.5.x version, 4.0 is coming
                            [updated]  Bootstrap Multiselect - latest version
                            [updated]  Datatables - latest version


                            // # List of fixed bugs
                            // ------------------------------

                            // Core fixes
                            [fixed]  Sidebar - side border overlaped content in light sidebar (layout 1 and 2)
                            [fixed]  Breadcrumbs - in colored version links had wrong background color on hover/active
                            [fixed]  Breadcrumbs - dropdown menu didn't have borders in breadcrumb line component
                            [fixed]  Labels - striped labels didn't have right border variation as supposed to
                            [fixed]  Navbars - unnecessary dropdown menu re-position in navbar component
                            [fixed]  Button groups - extra space between buttons in toolbar
                            [fixed]  Tables - extra border in framed table in responsive table container

                            // Components fixes
                            [fixed]  Bootstrap Select - wrong rounded corners inside input group
                            [fixed]  Bootstrap Select - no styling of dropdown menu
                            [fixed]  SelectBox - wrong rounded corners inside input group
                            [fixed]  Tags Input - input field didn't have bottom spacing
                            [fixed]  Typeahead - small menu width if text options are too short
                            [fixed]  Sweet alerts - title was too big for motification size
                            [fixed]  Anytime picker - wrong title margin and unnecessary close button
                            [fixed]  jQuery UI Datepicker - extra RTL-related code in less file
                            [fixed]  Fullcalendar - extra RTL-related code in less file
                            [fixed]  Chats - wrong variables in LESS file
                            [fixed]  Dropzone Uploader - success/error markers moved down in thumbnails is name is visible
                            [fixed]  Colors - default BS styles overrided text hover state
                            [fixed]  SelectBox page - extra panel control buttons
                            </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card" id="release">
                            <div class="card-header">
                                <h5>Initial Release</h5>
                                <span>flatable is in active development.</span>
                                <div class="">
                                    <span class="text-muted heading-text m-r-5">October 1, 2015</span>
                                    <label class="label label-info">V.1.0</label>
                                </div>
                            </div>
                            <div class="card-block">
                                <p class="content-group">flatable is in active development. All updates will be properly
                                    documented and explained, to make your upgrade process as easy as possible. In all
                                    new updates will be included: bug fixing,
                                    new functionality, plugins version control and code improvement. Feel free to
                                    contact me if you have any suggestions or requests!</p>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-xs">
                                        <thead>
                                        <tr>
                                            <th class="col-xs-3">What</th>
                                            <th>Quantity</th>
                                            <th>Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th colspan="3" class="active">Core files</th>
                                        </tr>
                                        <tr>
                                            <td>Folders</td>
                                            <td>268</td>
                                            <td>Folders with files, excluding CKEditor and Starter kit folders</td>
                                        </tr>
                                        <tr>
                                            <td>HTML files</td>
                                            <td>249</td>
                                            <td>Depending on layout, around 249 main HTML files in each layout</td>
                                        </tr>
                                        <tr>
                                            <td>CSS files</td>
                                            <td>7</td>
                                            <td>4 main CSS files, 2 CSS for icon fonts and 1
                                                <code>animate.min.css</code> animation library
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>LESS files</td>
                                            <td>203</td>
                                            <td>All LESS files, including Bootstrap core</td>
                                        </tr>
                                        <tr>
                                            <td>JS files</td>
                                            <td>896</td>
                                            <td>All JS files, excluding starter kit and CKEditor folders</td>
                                        </tr>
                                        <tr>
                                            <td>Image files</td>
                                            <td>256</td>
                                            <td>Logos, flag icons and notification icons</td>
                                        </tr>
                                        <tr>
                                            <th colspan="3" class="active">Other files</th>
                                        </tr>
                                        <tr>
                                            <td>JSON files</td>
                                            <td>23</td>
                                            <td>Different demo data sources. For demo purposes</td>
                                        </tr>
                                        <tr>
                                            <td>CSV files</td>
                                            <td>11</td>
                                            <td>Mainly for charts based on <code>D3.js</code> library. For demo purposes
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>TSV files</td>
                                            <td>13</td>
                                            <td>Mainly for charts based on <code>D3.js</code> library. For demo purposes
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>SWF files</td>
                                            <td>3</td>
                                            <td>Additional files for datatables TableTools extension and Plupload file
                                                uploader
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/prism/custom-prism.js") }}"></script>
@endsection

