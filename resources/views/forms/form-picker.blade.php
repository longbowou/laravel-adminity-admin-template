@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/advance-elements/css/bootstrap-datetimepicker.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/bootstrap-daterangepicker/css/daterangepicker.css") }}"/>

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datedropper/css/datedropper.min.css") }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/spectrum/css/spectrum.css") }}"/>

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-minicolors/css/jquery.minicolors.css") }}"/>
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Form Picker</h4>
                        <span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Form Picker</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Default Date-Picker</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date</h4>
                                <p>Add type<code>&lt;input type="date"&gt;</code></p>
                                <input class="form-control" type="date"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Month</h4>
                                <p>Add type<code>&lt;input type="month"&gt;</code></p>
                                <input class="form-control" type="month"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Week</h4>
                                <p>Add type<code>&lt;input type="week"&gt;</code></p>
                                <input class="form-control" type="week"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date-time-local</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <input class="form-control" type="datetime-local"/>
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <h4 class="sub-title">Time</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <input class="form-control" type="time"/>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Date-Dropper</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Default</h4>
                                <p>Add <code>id="#dropper-default"</code></p>
                                <input id="dropper-default" class="form-control" type="text"
                                       placeholder="Select your date"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">With Animation</h4>
                                <p>Add <code>id="#dropper-animation"</code></p>
                                <input id="dropper-animation" class="form-control" type="text"
                                       placeholder="Select your animation"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date format</h4>
                                <p>Add <code>id="#dropper-format"</code></p>
                                <input id="dropper-format" class="form-control" type="text"
                                       placeholder="Select your format"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Language Support</h4>
                                <p>Add <code>id="#dropper-lang"</code></p>
                                <input id="dropper-lang" class="form-control" type="text"
                                       placeholder="Language Support"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Lock Support</h4>
                                <p>Add <code>id="#dropper-lock"</code></p>
                                <input id="dropper-lock" class="form-control" type="text"
                                       placeholder="Select your date"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Max Year</h4>
                                <p>Add <code>id="#dropper-max-year"</code></p>
                                <input id="dropper-max-year" class="form-control" type="text"
                                       placeholder="Max Year 2020"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Min Year</h4>
                                <p>Add <code>id="#dropper-min-year"</code></p>
                                <input id="dropper-min-year" class="form-control" type="text"
                                       placeholder="Min Year 1990"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Year-range</h4>
                                <p>Add <code>id="#year-range"</code></p>
                                <input id="year-range" class="form-control" type="text" placeholder="Select your date"/>
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <h4 class="sub-title">Custom Width</h4>
                                <p>Add <code>id="#dropper-width"</code></p>
                                <input id="dropper-width" class="form-control" type="text"
                                       placeholder="Select your date"/>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Color &amp; Style Dropper</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Primary Color</h4>
                                <p>Add <code>id="#dropper-dangercolor"</code></p>
                                <input id="dropper-dangercolor" class="form-control" type="text"
                                       placeholder="Select your time"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Background Color</h4>
                                <p>Add <code>id="#dropper-backcolor"</code></p>
                                <input id="dropper-backcolor" class="form-control" type="text"
                                       placeholder="Select your time"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Text Color</h4>
                                <p>Add <code>id="#dropper-txtcolor"</code></p>
                                <input id="dropper-txtcolor" class="form-control" type="text"
                                       placeholder="Select your time"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Border-radius</h4>
                                <p>Add <code>id="#dropper-radius"</code></p>
                                <input id="dropper-radius" class="form-control" type="text"
                                       placeholder="Select your time"/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Border Style</h4>
                                <p>Add <code>id="#dropper-border"</code></p>
                                <input id="dropper-border" class="form-control" type="text"
                                       placeholder="Select your time"/>
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <h4 class="sub-title">Shadow Color</h4>
                                <p>Add <code>id="#dropper-shadow"</code></p>
                                <input id="dropper-shadow" class="form-control" type="text"
                                       placeholder="Select your time"/>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Bootstrap Date-Picker</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date Time Picker</h4>
                                <p>Add type<code>&lt;input type="date"&gt;</code></p>
                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' class="form-control"/>
                                        <span class="input-group-addon ">
<span class="icofont icofont-ui-calendar"></span>
</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Time Picker</h4>
                                <p>Add type<code>&lt;input type="week"&gt;</code></p>
                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker3'>
                                        <input type='text' class="form-control"/>
                                        <span class="input-group-addon ">
<span class="icofont icofont-ui-calendar"></span>
 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">No Icon (Input Field Only)</h4>
                                <p>Add type<code>&lt;input type="week"&gt;</code></p>
                                <div class="form-group">
                                    <input type='text' class="form-control" id='datetimepicker4'/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Enabled/Disabled Dates</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker5'>
                                        <input type='text' class="form-control"/>
                                        <span class="input-group-addon ">
<span class="icofont icofont-ui-calendar"></span>
</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Custom Icons</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class='input-group date' id='datetimepicker8'>
                                    <input type='text' class="form-control"/>
                                    <span class="input-group-addon ">
<span class="icofont icofont-ui-calendar"></span>
</span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Birthday Calender</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <input type="text" name="birthdate" class="form-control" value="10/24/1984"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-8 m-b-30">
                                <h4 class="sub-title">Linked Pickers</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class='input-group date' id='datetimepicker6'>
                                                <input type='text' class="form-control"/>
                                                <span class="input-group-addon ">
<span class="icofont icofont-ui-calendar"></span>
</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class='input-group date' id='datetimepicker7'>
                                                <input type='text' class="form-control"/>
                                                <span class="input-group-addon ">
<span class="icofont icofont-ui-calendar"></span>
</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date Range Picker</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <input type="text" name="daterange" class="form-control"
                                       value="01/01/2015 - 01/31/2015"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Min View Mode</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <div class='input-group date' id='datetimepicker10'>
                                    <input type='text' class="form-control"/>
                                    <span class="input-group-addon ">
<span class="icofont icofont-ui-calendar"></span>
</span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Disabled Days Of The Week</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class='input-group date' id='datetimepicker11'>
                                    <input type='text' class="form-control"/>
                                    <span class="input-group-addon ">
<span class="icofont icofont-ui-calendar">
</span>
</span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">View Mode</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class='input-group date' id='datetimepicker9'>
                                    <input type='text' class="form-control"/>
                                    <span class="input-group-addon ">
<span class="icofont icofont-ui-calendar">
</span>
</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date Range Picker Blank Input</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <input type="text" name="datefilter" class="form-control" value=""/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Multiple Option</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div id="reportrange" class="f-right"
                                     style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i>
                                    <span></span> <b class="caret"></b>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date Picker With Today</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class="input-group date input-group-date-custom">
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon ">
<i class="icofont icofont-clock-time"></i>
</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-8">
                                <h4 class="sub-title">Date Time Picker</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-sm form-control" name="start"/>
                                    <span class="input-group-addon">to</span>
                                    <input type="text" class="input-sm form-control" name="end"/>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <h4 class="sub-title">Multi Select Dates</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class="input-group date multiple-select">
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon ">
<i class="icofont icofont-clock-time"></i>
</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Color Picker</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4 class="sub-title">Flat Mode</h4>
                                <div class="form-group">
                                    <input type="text" id="flat"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <h4 class="sub-title">Flat Mode With Clear</h4>
                                <div class="form-group">
                                    <input type='text' id="flatClearable"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <h4 class="sub-title"> No Icon (Input Field Only)</h4>
                                <div class="form-group">
                                    <input type='color' name='color'/>
                                    <input type='color' name='color2' value='#3355cc'/>
                                    <hr/>
                                    <input type="color"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Mini Color</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">Control-Types</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">Hue (default)</h4>
                                            <input type="text" id="hue-demo" class="form-control demo"
                                                   data-control="hue" value="#ff6161">
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">Saturation</h4>
                                            <input type="text" id="saturation-demo" class="form-control demo"
                                                   data-control="saturation" value="#0088cc">
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">Brightness</h4>
                                            <input type="text" id="brightness-demo" class="form-control demo"
                                                   data-control="brightness" value="#00ffff">
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">Wheel</h4>
                                            <input type="text" id="wheel-demo" class="form-control demo"
                                                   data-control="wheel" value="#ff99ee">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">Input Modes</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-12 m-b-30">
                                                    <h4 class="sub-title">Text field </h4>
                                                    <input type="text" id="text-field" class="form-control demo"
                                                           value="#70c24a">
                                                </div>
                                                <div class="col-sm-12">
                                                    <h4 class="sub-title">Hidden Input</h4>
                                                    <input type="hidden" id="hidden-input" class="demo" value="#db913d">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Brightness</h4>
                                            <input type="text" id="inline" class="form-control demo" data-inline="true"
                                                   value="#4fc8db">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">Positions</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">bottom left (default)</h4>
                                            <input type="text" id="position-bottom-left" class="form-control demo"
                                                   data-position="bottom left" value="#0088cc">
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">top left</h4>
                                            <input type="text" id="position-top-left" class="form-control demo"
                                                   data-position="top left" value="#0088cc">
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">bottom right</h4>
                                            <input type="text" id="position-bottom-right" class="form-control demo"
                                                   data-position="bottom right" value="#0088cc">
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">top right</h4>
                                            <input type="text" id="position-top-right" class="form-control demo"
                                                   data-position="top right" value="#0088cc">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">RGB(A)</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">RGB</h4>
                                            <input type="text" id="rgb" class="form-control demo" data-format="rgb"
                                                   value="rgb(33, 147, 58)">
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">RGBA</h4>
                                            <input type="text" id="rgba" class="form-control demo" data-format="rgb"
                                                   data-opacity=".5" value="rgba(52, 64, 158, 0.5)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">More</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row m-b-30">
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Opacity</h4>
                                            <input type="text" id="opacity" class="form-control demo" data-opacity=".5"
                                                   value="#766fa8">
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Keywords</h4>
                                            <input type="text" id="keywords" class="form-control demo"
                                                   data-keywords="transparent, initial, inherit" value="transparent">
                                        </div>
                                    </div>
                                    <div class="row m-b-30">
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Default Value</h4>
                                            <input type="text" id="default-value" class="form-control demo"
                                                   data-defaultValue="#ff6600">
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Letter Case</h4>
                                            <input type="text" id="letter-case" class="form-control demo"
                                                   data-letterCase="uppercase" value="#abcdef">
                                        </div>
                                    </div>
                                    <div class="row m-b-30">
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Swatches</h4>
                                            <input type="text" id="swatches" class="form-control demo"
                                                   data-swatches="#fff|#000|#f00|#0f0|#00f|#ff0|#0ff" value="#abcdef">
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Swatches and opacity</h4>
                                            <input type="text" id="swatches-2" class="form-control demo"
                                                   data-format="rgb" data-opacity="1"
                                                   data-swatches="#fff|#000|#f00|#0f0|#00f|#ff0|rgba(0,0,255,0.5)"
                                                   value="rgba(14, 206, 235, .5)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/pages/advance-elements/moment-with-locales.min.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/components/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/pages/advance-elements/bootstrap-datetimepicker.min.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/components/bootstrap-daterangepicker/js/daterangepicker.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/components/datedropper/js/datedropper.min.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/components/spectrum/js/spectrum.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/components/jscolor/js/jscolor.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/components/jquery-minicolors/js/jquery.minicolors.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/advance-elements/custom-picker.js") }}"></script>
@endsection


