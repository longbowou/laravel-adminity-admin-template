@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/jquery.steps/css/jquery.steps.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Forms Masking</h4>
                        <span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Form Masking</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Date</h5>
                        <span>Add class of <code>.date</code> with <code>data-mask</code> attribute</span>
                    </div>
                    <div class="card-block">
                        <form>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Insert Date 1</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control date" data-mask="99/99/9999">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Insert Date 2</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control date2" data-mask="99-99-9999">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Time</h5>
                        <span>Add class of <code>.hour</code> with <code>data-mask</code> attribute</span>
                    </div>
                    <div class="card-block">
                        <form>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Hour</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control hour" data-mask="99:99:99">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Date & Hour</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control dateHour" data-mask="99/99/9999 99:99:99">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Phone no.</h5>
                        <span>Add class of <code>.mob_no</code> with <code>data-mask</code> attribute</span>
                    </div>
                    <div class="card-block">
                        <form>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Mobile No.</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control mob_no" data-mask="9999-999-999">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Telephone</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control phone" data-mask="9999-9999">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Tel. with Code Area</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control telphone_with_code"
                                           data-mask="(99) 9999-9999">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">US Telephone</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control us_telephone" data-mask="(999) 999-9999">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Network</h5>
                        <span>Add class of <code>.ip</code> with <code>data-mask</code> attribute</span>
                    </div>
                    <div class="card-block">
                        <form>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">IP Address</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control ip" data-mask="999.999.999.999">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">IPV4</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control ipv4" data-mask="99/99/9999 99:99:99">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">IPV6</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control ipv6"
                                           data-mask="9999:9999:9999:9:999:9999:9999:9999">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Numbers</h5>
                        <span>Add class of <code>.autonumber</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <form>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Default</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-a-sep="." data-a-dec=",">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Auto Numeric (%)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-a-sign="%" data-p-sign="s">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Maximum Value (0 - 9999)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-v-max="9999" data-v-min="0">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Range Value (-99.99 - 1000.00)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-v-min="-99.99"
                                           data-v-max="1000.00">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Bracket Value</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-a-sep="." data-a-dec=","
                                           data-v-min="-9999.99" data-v-max="0.00" data-n-bracket="(,)">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-6">

                <div class="card">
                    <div class="card-header">
                        <h5>Currency</h5>
                        <span>Add class of <code>.autonumber</code> with <code>data-a-*</code> attribute</span>
                    </div>
                    <div class="card-block">
                        <form>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Dollar ($)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-a-sign="$">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Euro (€)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-a-sign="€">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Rupee (Rs.)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-a-sign="Rs. ">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Renminbi (¥)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-d-group="4"
                                           data-a-sign="¥ ">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Pound (£)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control autonumber" data-a-sign="£ ">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/form-masking/inputmask.js") }}" type="text/javascript"></script>

    <script src="{{ asset("adminity/components/form-masking/jquery.inputmask.js") }}" type="text/javascript"></script>

    <script src="{{ asset("adminity/components/form-masking/autoNumeric.js") }}" type="text/javascript"></script>

    <script src="{{ asset("adminity/pages/form-masking/form-mask.js") }}" type="text/javascript"></script>
@endsection


