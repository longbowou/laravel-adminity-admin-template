@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/switchery/css/switchery.min.css") }}">

    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/bootstrap-tagsinput/css/bootstrap-tagsinput.css") }}"/>
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Basic Form Inputs</h4>
                        <span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Form-Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Form-Elements-Advance</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Switches</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Single Switche</h4>
                                <input type="checkbox" class="js-single" checked/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Multiple Switches</h4>
                                <input type="checkbox" class="js-switch" checked/>
                                <input type="checkbox" class="js-switch" checked/>
                                <input type="checkbox" class="js-switch" checked/>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Enable Disable Switches</h4>
                                <input type="checkbox" class="js-dynamic-state" checked/>
                                <button class="btn btn-primary js-dynamic-enable">Enable
                                </button>
                                <button class="btn btn-inverse js-dynamic-disable m-t-10">
                                    Disable
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <h4 class="sub-title">Color Switches</h4>
                                <input type="checkbox" class="js-default" checked/>
                                <input type="checkbox" class="js-primary" checked/>
                                <input type="checkbox" class="js-success" checked/>
                                <input type="checkbox" class="js-info" checked/>
                                <input type="checkbox" class="js-warning" checked/>
                                <input type="checkbox" class="js-danger" checked/>
                                <input type="checkbox" class="js-inverse" checked/>
                            </div>
                            <div class="col-sm-4">
                                <h4 class="sub-title">Switch Sizes</h4>
                                <input type="checkbox" class="js-large" checked/>
                                <input type="checkbox" class="js-medium" checked/>
                                <input type="checkbox" class="js-small" checked/>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Radio</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-xl-4 m-b-30">
                                <h4 class="sub-title">Radio Fill Button</h4>
                                <div class="form-radio">
                                    <form>
                                        <div class="radio radio-inline">
                                            <label>
                                                <input type="radio" name="radio"
                                                       checked="checked">
                                                <i class="helper"></i>Radio 1
                                            </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <label>
                                                <input type="radio" name="radio">
                                                <i class="helper"></i>Radio 2
                                            </label>
                                        </div>
                                        <div class="radio radio-inline radio-disable">
                                            <label>
                                                <input type="radio" disabled=""
                                                       name="radio">
                                                <i class="helper"></i>Radio Disable
                                            </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-4 m-b-30">
                                <h4 class="sub-title">Radio outline Button</h4>
                                <div class="form-radio">
                                    <form>
                                        <div class="radio radio-outline radio-inline">
                                            <label>
                                                <input type="radio" name="radio"
                                                       checked="checked">
                                                <i class="helper"></i>Radio 1
                                            </label>
                                        </div>
                                        <div class="radio radio-outline radio-inline">
                                            <label>
                                                <input type="radio" name="radio">
                                                <i class="helper"></i>Radio 2
                                            </label>
                                        </div>
                                        <div class="radio radio-inline radio-disable">
                                            <label>
                                                <input type="radio" disabled=""
                                                       name="radio">
                                                <i class="helper"></i>Radio Disable
                                            </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-4 m-b-30">
                                <h4 class="sub-title">Radio Button</h4>
                                <div class="form-radio">
                                    <form>
                                        <div class="radio radiofill radio-inline">
                                            <label>
                                                <input type="radio" name="radio"
                                                       checked="checked">
                                                <i class="helper"></i>Radio-fill 1
                                            </label>
                                        </div>
                                        <div class="radio radiofill radio-inline">
                                            <label>
                                                <input type="radio" name="radio">
                                                <i class="helper"></i>Radio-fill 2
                                            </label>
                                        </div>
                                        <div class="radio radiofill radio-inline radio-disable">
                                            <label>
                                                <input type="radio" disabled=""
                                                       name="radio">
                                                <i class="helper"></i>Radio-fill Disable
                                            </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <h4 class="sub-title">Color Radio Button</h4>
                        <div class="form-radio m-b-30">
                            <form>
                                <div class="radio radiofill radio-default radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Default Color
                                    </label>
                                </div>
                                <div class="radio radiofill radio-primary radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Primary Color
                                    </label>
                                </div>
                                <div class="radio radiofill radio-success radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Success Color
                                    </label>
                                </div>
                                <div class="radio radiofill radio-info radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Info Color
                                    </label>
                                </div>
                                <div class="radio radiofill radio-warning radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Warning Color
                                    </label>
                                </div>
                                <div class="radio radiofill radio-danger radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Danger Color
                                    </label>
                                </div>
                                <div class="radio radiofill radio-inverse radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Inverse Color
                                    </label>
                                </div>
                            </form>
                        </div>
                        <h4 class="sub-title">Color Radio material Button</h4>
                        <div class="form-radio m-b-30">
                            <form>
                                <div class="radio radio-matrial radio-default radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Default Color
                                    </label>
                                </div>
                                <div class="radio radio-matrial radio-primary radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Primary Color
                                    </label>
                                </div>
                                <div class="radio radio-matrial radio-success radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Success Color
                                    </label>
                                </div>
                                <div class="radio radio-matrial radio-info radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Info Color
                                    </label>
                                </div>
                                <div class="radio radio-matrial radio-warning radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Warning Color
                                    </label>
                                </div>
                                <div class="radio radio-matrial radio-danger radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Danger Color
                                    </label>
                                </div>
                                <div class="radio radio-matrial radio-inverse radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i>Inverse Color
                                    </label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Checkbox</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-6 m-b-30">
                                <h4 class="sub-title">Border Checkbox</h4>
                                <div class="border-checkbox-section">
                                    <div class="border-checkbox-group border-checkbox-group-default">
                                        <input class="border-checkbox" type="checkbox"
                                               id="checkbox0">
                                        <label class="border-checkbox-label"
                                               for="checkbox0">Do you like it?</label>
                                    </div>
                                    <div class="border-checkbox-group border-checkbox-group-primary">
                                        <input class="border-checkbox" type="checkbox"
                                               id="checkbox1">
                                        <label class="border-checkbox-label"
                                               for="checkbox1">Primary</label>
                                    </div>
                                    <div class="border-checkbox-group border-checkbox-group-success">
                                        <input class="border-checkbox" type="checkbox"
                                               id="checkbox2">
                                        <label class="border-checkbox-label"
                                               for="checkbox2">Success</label>
                                    </div>
                                    <div class="border-checkbox-group border-checkbox-group-info">
                                        <input class="border-checkbox" type="checkbox"
                                               id="checkbox3">
                                        <label class="border-checkbox-label"
                                               for="checkbox3">Info</label>
                                    </div>
                                    <div class="border-checkbox-group border-checkbox-group-warning">
                                        <input class="border-checkbox" type="checkbox"
                                               id="checkbox4">
                                        <label class="border-checkbox-label"
                                               for="checkbox4">Warning</label>
                                    </div>
                                    <div class="border-checkbox-group border-checkbox-group-danger">
                                        <input class="border-checkbox" type="checkbox"
                                               id="checkbox5">
                                        <label class="border-checkbox-label"
                                               for="checkbox5">Danger</label>
                                    </div>
                                    <div class="border-checkbox-group">
                                        <input class="border-checkbox" type="checkbox"
                                               id="checkbox6" disabled>
                                        <label class="border-checkbox-label"
                                               for="checkbox6">Disabled</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-6 m-b-30">
                                <h4 class="sub-title">Fade-in Checkbox</h4>
                                <div class="checkbox-fade fade-in-default">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-default"></i>
</span>
                                        <span>Default</span>
                                    </label>
                                </div>
                                <div class="checkbox-fade fade-in-primary">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                        <span>Primary</span>
                                    </label>
                                </div>
                                <div class="checkbox-fade fade-in-warning">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-warning"></i>
</span>
                                        <span> Warning</span>
                                    </label>
                                </div>
                                <div class="checkbox-fade fade-in-success">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-success"></i>
</span>
                                        <span>Success</span>
                                    </label>
                                </div>
                                <div class="checkbox-fade fade-in-info">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-info"></i>
</span>
                                        <span> Info</span>
                                    </label>
                                </div>
                                <div class="checkbox-fade fade-in-danger">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-danger"></i>
</span>
                                        <span> Danger</span>
                                    </label>
                                </div>
                                <div class="checkbox-fade fade-in-disable">
                                    <label>
                                        <input type="checkbox" value="" disabled>
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check text-default"></i>
</span>
                                        <span>Disabled</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-6 m-b-30">
                                <h4 class="sub-title">Color Checkbox</h4>
                                <div class="checkbox-color checkbox-default">
                                    <input id="checkbox12" type="checkbox" checked="">
                                    <label for="checkbox12">
                                        Default
                                    </label>
                                </div>
                                <div class="checkbox-color checkbox-primary">
                                    <input id="checkbox18" type="checkbox" checked="">
                                    <label for="checkbox18">
                                        Primary
                                    </label>
                                </div>
                                <div class="checkbox-color checkbox-success">
                                    <input id="checkbox13" type="checkbox" checked="">
                                    <label for="checkbox13">
                                        Success
                                    </label>
                                </div>
                                <div class="checkbox-color checkbox-info">
                                    <input id="checkbox14" type="checkbox" checked="">
                                    <label for="checkbox14">
                                        Info
                                    </label>
                                </div>
                                <div class="checkbox-color checkbox-warning">
                                    <input id="checkbox15" type="checkbox" checked="">
                                    <label for="checkbox15">
                                        Warning
                                    </label>
                                </div>
                                <div class="checkbox-color checkbox-danger">
                                    <input id="checkbox16" type="checkbox" checked="">
                                    <label for="checkbox16">
                                        Danger
                                    </label>
                                </div>
                                <div class="checkbox-color checkbox-default">
                                    <input id="checkbox17" type="checkbox" disabled="">
                                    <label for="checkbox17">
                                        Disabled
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-6 m-b-30">
                                <h4 class="sub-title">zoom Checkbox</h4>
                                <div class="checkbox-zoom zoom-default">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-default"></i>
</span>
                                        <span>Default</span>
                                    </label>
                                </div>
                                <div class="checkbox-zoom zoom-primary">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                        <span>Primary</span>
                                    </label>
                                </div>
                                <div class="checkbox-zoom zoom-warning">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-warning"></i>
</span>
                                        <span> Warning</span>
                                    </label>
                                </div>
                                <div class="checkbox-zoom zoom-success">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-success"></i>
</span>
                                        <span>Success</span>
                                    </label>
                                </div>
                                <div class="checkbox-zoom zoom-info">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-info"></i>
</span>
                                        <span> Info</span>
                                    </label>
                                </div>
                                <div class="checkbox-zoom zoom-danger">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-danger"></i>
</span>
                                        <span> Danger</span>
                                    </label>
                                </div>
                                <div class="checkbox-zoom zoom-disable">
                                    <label>
                                        <input type="checkbox" value="" disabled>
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check text-default"></i>
</span>
                                        <span>Disabled</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Tags Input</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Markup</h4>
                                <p>Just add <code>data-role="tagsinput"</code> to your input
                                    field to automatically change it to a tags input field.
                                </p>
                                <div class="tags_add">
                                    <input type="text" class="form-control"
                                           value="Amsterdam,Washington,Sydney"
                                           data-role="tagsinput">
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Colors</h4>
                                <p>You can set a fixed css class for your tags, or determine
                                    dynamically by providing a custom function.</p>
                                <div class="color-tags">
                                    <input class="" type="text"/>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Max-Tags</h4>
                                <p>Just add <code>"tags_max"</code> to your input field to
                                    automatically change it to a tags input field.</p>
                                <div class="tags_max">
                                    <input class="" type="text"
                                           value="Amsterdam,Washington,Sydney"
                                           data-role="tagsinput">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Max-Characters</h4>
                                <p>Just add <code>"tags_max_char"</code> to your input field
                                    to automatically change it to a tags input field.</p>
                                <div class="tags_max_char">
                                    <input class="" type="text"
                                           value="Amsterdam,Washington,Sydney"
                                           data-role="tagsinput">
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <h4 class="sub-title">Multiple</h4>
                                <p>Just add <code>"tags_add_multiple"</code> to your input
                                    field to automatically change it to a tags input field.
                                </p>
                                <div class="tags_add_multiple">
                                    <input class="" type="text"
                                           value="Amsterdam,Washington,Sydney"
                                           data-role="tagsinput">
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Max Length</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Default</h4>
                                <p>The badge will show up by default when the remaining
                                    chars are <code>10</code> or <code>less</code>:</p>
                                <input type="text" class="form-control"
                                       placeholder="Type your keywords..." maxlength="10">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Threshold</h4>
                                <p>Do you want the badge to show up when there are <code>20
                                        chars</code> or less? Use the <code>threshold</code>
                                    option:</p>
                                <input type="text" class="form-control thresold-i"
                                       placeholder="Type your keywords..." maxlength="20">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Color-lables</h4>
                                <p>Just add <code>color-class</code>with <code>input</code>
                                </p>
                                <input type="text" class="form-control color-class"
                                       placeholder="Type your keywords..." maxlength="25">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Play With Positions</h4>
                                <p>All you need to do is specify the <code>placement
                                        option</code>, with one of those strings. If none is
                                    specified, the positioning will be defauted to <code>'top-left'</code>.
                                </p>
                                <input type="text" class="form-control position-class"
                                       placeholder="Type your keywords..." maxlength="25">
                            </div>
                            <div class="col-sm-12 col-xl-8">
                                <h4 class="sub-title">Also Working With Textarea</h4>
                                <p>Bootstrap maxlength supports textarea as well as inputs.
                                    Even on old IE.</p>
                                <textarea class="form-control max-textarea" maxlength="255"
                                          rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/switchery/js/switchery.min.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/components/bootstrap-tagsinput/js/bootstrap-tagsinput.js") }}"></script>

    <script src="{{ asset("adminity/js/typeahead.bundle.min.js") }}"
            type="text/javascript"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/js/bootstrap-maxlength.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/pages/advance-elements/swithces.js") }}"></script>
@endsection


