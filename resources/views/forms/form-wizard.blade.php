@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/jquery.steps/css/jquery.steps.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Forms Wizard</h4>
                        <span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Form Wizard</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Form Wizard With Validation</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="wizard">
                                    <section>
                                        <form class="wizard-form" id="example-advanced-form" action="#">
                                            <h3> Registration </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="userName-2" class="block">User name *</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="userName-2" name="userName" type="text"
                                                               class="required form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="email-2" class="block">Email *</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="email-2" name="email" type="email"
                                                               class="required form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="password-2" class="block">Password *</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="password-2" name="password" type="password"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="confirm-2" class="block">Confirm Password *</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="confirm-2" name="confirm" type="password"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3> General information </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="name-2" class="block">First name *</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="name-2" name="name" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="surname-2" class="block">Last name *</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="surname-2" name="surname" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="phone-2" class="block">Phone #</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="phone-2" name="phone" type="number"
                                                               class="form-control required phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="date" class="block">Date Of Birth</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="date" name="Date Of Birth" type="text"
                                                               class="form-control required date-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">Select Country</div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <select class="form-control required">
                                                            <option>Select State</option>
                                                            <option>Gujarat</option>
                                                            <option>Kerala</option>
                                                            <option>Manipur</option>
                                                            <option>Tripura</option>
                                                            <option>Sikkim</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3> Education </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="University-2" class="block">University</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="University-2" name="University" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="Country-2" class="block">Country</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="Country-2" name="Country" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="Degreelevel-2" class="block">Degree level #</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="Degreelevel-2" name="Degree level" type="text"
                                                               class="form-control required phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="datejoin" class="block">Date Join</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="datejoin" name="Date Of Birth" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3> Work experience </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="Company-2" class="block">Company:</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="Company-2" name="Company:" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="CountryW-2" class="block">Country</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="CountryW-2" name="Country" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-lg-2">
                                                        <label for="Position-2" class="block">Position</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-10">
                                                        <input id="Position-2" name="Position" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Form Basic Wizard</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="wizard1">
                                    <section>
                                        <form class="wizard-form" id="basic-forms" action="#">
                                            <h3> Registration </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="userName-2" class="block">User name *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="userName-21" name="userName" type="text"
                                                               class=" form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="email-2-1" class="block">Email *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="email-2-1" name="email" type="email"
                                                               class=" form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="password-2" class="block">Password *</label>
                                                    </div>
                                                    <div class="col-sm-8 col-lg-10">
                                                        <input id="password-21" name="password" type="password"
                                                               class="form-control ">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="confirm-2" class="block">Confirm Password *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="confirm-21" name="confirm" type="password"
                                                               class="form-control ">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3> General information </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="name-2" class="block">First name *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="name-21" name="name" type="text"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="surname-2" class="block">Last name *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="surname-21" name="surname" type="text"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="phone-2" class="block">Phone #</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="phone-21" name="phone" type="number"
                                                               class="form-control phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="date" class="block">Date Of Birth</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="date1" name="Date Of Birth" type="text"
                                                               class="form-control date-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        Select Country
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select class="form-control required">
                                                            <option>Select State</option>
                                                            <option>Gujarat</option>
                                                            <option>Kerala</option>
                                                            <option>Manipur</option>
                                                            <option>Tripura</option>
                                                            <option>Sikkim</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3> Education </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="University-2" class="block">University</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="University-21" name="University" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Country-2" class="block">Country</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Country-21" name="Country" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Degreelevel-2" class="block">Degree level #</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Degreelevel-21" name="Degree level" type="text"
                                                               class="form-control required phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="datejoin" class="block">Date Join</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="datejoin1" name="Date Of Birth" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3> Work experience </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Company-2" class="block">Company:</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Company-21" name="Company:" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="CountryW-2" class="block">Country</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="CountryW-21" name="Country" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Position-2" class="block">Position</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Position-21" name="Position" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Verticle Wizard</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="wizard2">
                                    <section>
                                        <form class="wizard-form" id="verticle-wizard" action="#">
                                            <h3> Registration </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="userName-2" class="block">User name *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="userName-22" name="userName" type="text"
                                                               class=" form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="email-2" class="block">Email *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="email-22" name="email" type="email"
                                                               class=" form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="password-2" class="block">Password *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="password-22" name="password" type="password"
                                                               class="form-control ">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="confirm-2" class="block">Confirm Password *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="confirm-22" name="confirm" type="password"
                                                               class="form-control ">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3> General information </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="name-2" class="block">First name *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="name-22" name="name" type="text"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="surname-2" class="block">Last name *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="surname-22" name="surname" type="text"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="phone-2" class="block">Phone #</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="phone-22" name="phone" type="number"
                                                               class="form-control phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="date" class="block">Date Of Birth</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="date22" name="Date Of Birth" type="text"
                                                               class="form-control date-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">Select Country</div>
                                                    <div class="col-sm-12">
                                                        <select class="form-control required">
                                                            <option>Select State</option>
                                                            <option>Gujarat</option>
                                                            <option>Kerala</option>
                                                            <option>Manipur</option>
                                                            <option>Tripura</option>
                                                            <option>Sikkim</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3> Education </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="University-2" class="block">University</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="University-22" name="University" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Country-2" class="block">Country</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Country-22" name="Country" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Degreelevel-2" class="block">Degree level #</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Degreelevel-22" name="Degree level" type="text"
                                                               class="form-control required phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="datejoin" class="block">Date Join</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="datejoin2" name="Date Of Birth" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3> Work experience </h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Company-2" class="block">Company:</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Company-22" name="Company:" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="CountryW-2" class="block">Country</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="CountryW-22" name="Country" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Position-2" class="block">Position</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Position-22" name="Position" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Design Wizard</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="wizard3">
                                    <section>
                                        <form class="wizard-form" id="design-wizard" action="#">
                                            <h3></h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="userName-2" class="block">User name *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="userName-23" name="userName" type="text"
                                                               class=" form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="email-2" class="block">Email *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="email-23" name="email" type="email"
                                                               class=" form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="password-2" class="block">Password *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="password-23" name="password" type="password"
                                                               class="form-control ">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="confirm-2" class="block">Confirm Password *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="confirm-23" name="confirm" type="password"
                                                               class="form-control ">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3></h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="name-2" class="block">First name *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="name-23" name="name" type="text"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="surname-2" class="block">Last name *</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="surname-23" name="surname" type="text"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="phone-2" class="block">Phone #</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="phone-23" name="phone" type="number"
                                                               class="form-control phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="date" class="block">Date Of Birth</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="date3" name="Date Of Birth" type="text"
                                                               class="form-control date-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">Select Country</div>
                                                    <div class="col-sm-12">
                                                        <select class="form-control required">
                                                            <option>Select State</option>
                                                            <option>Gujarat</option>
                                                            <option>Kerala</option>
                                                            <option>Manipur</option>
                                                            <option>Tripura</option>
                                                            <option>Sikkim</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3></h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="University-2" class="block">University</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="University-23" name="University" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Country-2" class="block">Country</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Country-23" name="Country" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Degreelevel-2" class="block">Degree level #</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Degreelevel-23" name="Degree level" type="text"
                                                               class="form-control required phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="datejoin" class="block">Date Join</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="datejoin3" name="Date Of Birth" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3></h3>
                                            <fieldset>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Company-2" class="block">Company:</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Company-23" name="Company:" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="CountryW-2" class="block">Country</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="CountryW-23" name="Country" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label for="Position-2" class="block">Position</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input id="Position-23" name="Position" type="text"
                                                               class="form-control required">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/jquery.cookie/js/jquery.cookie.js") }}"
            type="text/javascript"></script>

    <script src="{{ asset("adminity/components/jquery.steps/js/jquery.steps.js") }}"
            type="text/javascript"></script>

    <script src="{{ asset("adminity/components/jquery-validation/js/jquery.validate.js") }}"
            type="text/javascript"></script>

    <script src="{{ asset("adminity/js/underscore-min.js") }}"
            type="text/javascript"></script>

    <script src="{{ asset("adminity/js/moment.min.js") }}"
            type="text/javascript"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/pages/form-validation/validate.js") }}"></script>

    <script src="{{ asset("adminity/pages/forms-wizard-validation/form-wizard.js") }}"
            type="text/javascript"></script>
@endsection


