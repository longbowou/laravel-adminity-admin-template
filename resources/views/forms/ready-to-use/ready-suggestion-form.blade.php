@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-pro-modern.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Suggestion form</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Suggestion form</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Give Your Suggestion Here</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="j-wrapper j-wrapper-640">
                            <form action="https://colorlib.com//polygon/adminty/default/j-pro/php/action.php"
                                  method="post" class="j-pro" id="j-pro" enctype="multipart/form-data" novalidate>

                                <div class="j-content">

                                    <div class="j-row">
                                        <div class="j-span6 j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-left" for="first_name">
                                                    <i class="icofont icofont-ui-user"></i>
                                                </label>
                                                <input type="text" id="first_name" name="first_name"
                                                       placeholder="First name" class="name-group">
                                            </div>
                                        </div>
                                        <div class="j-span6 j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-left" for="last_name">
                                                    <i class="icofont icofont-ui-user"></i>
                                                </label>
                                                <input type="text" id="last_name" name="last_name"
                                                       placeholder="Last name" class="name-group">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <div class="j-input">
                                            <label class="j-icon-left" for="email">
                                                <i class="icofont icofont-envelope"></i>
                                            </label>
                                            <input type="email" placeholder="Email" id="email" name="email">
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <label class="j-input j-select">
                                            <select name="department">
                                                <option value="" selected>Choose your department</option>
                                                <option value="general">General</option>
                                                <option value="sales and marketing">Sales and Marketing</option>
                                                <option value="deposit">Deposit</option>
                                                <option value="IT and development">IT and Development</option>
                                                <option value="design and branding">Design and Branding</option>
                                            </select>
                                            <i></i>
                                        </label>
                                    </div>

                                    <div class="j-divider j-gap-bottom-25"></div>

                                    <div class="j-unit">
                                        <div class="j-input">
                                            <label class="j-icon-left" for="subject">
                                                <i class="icofont icofont-ui-check"></i>
                                            </label>
                                            <input type="text" id="subject" name="subject" placeholder="Subject">
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <div class="j-input">
                                            <label class="j-icon-left" for="message">
                                                <i class="icofont icofont-file-text"></i>
                                            </label>
                                            <textarea placeholder="Additional info" spellcheck="false" id="message"
                                                      name="message"></textarea>
                                            <span class="j-tooltip j-tooltip-right-top">Describe your proposal as detailed as possible</span>
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <div class="j-input j-append-big-btn">
                                            <label class="j-icon-left" for="file_input">
                                                <i class="icofont icofont-download"></i>
                                            </label>
                                            <div class="j-file-button">
                                                Browse
                                                <input type="file" name="file_name"
                                                       onchange="if (!window.__cfRLUnblockHandlers) return false; document.getElementById('file_input').value = this.value;"
                                                       data-cf-modified-c700fcaae9433afdd1c95438-="">
                                            </div>
                                            <input type="text" id="file_input" readonly=""
                                                   placeholder="no file selected">
                                            <span class="j-hint">Only: jpg / png / doc, less 1Mb</span>
                                        </div>
                                    </div>


                                    <div class="j-response"></div>

                                </div>

                                <div class="j-footer">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                    <button type="reset" class="btn btn-default m-r-20">Reset</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.maskedinput.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.j-pro.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/suggestion-form.js") }}"></script>
@endsection


