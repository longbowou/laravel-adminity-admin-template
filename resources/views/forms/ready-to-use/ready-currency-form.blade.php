@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-forms.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Currency Format</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route('dashboard') }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Currency format</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Currency Format</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="wrapper wrapper-640">
                            <form action="#" method="post" class="j-forms" id="j-forms" novalidate>
                                <div class="content">
                                    <div class="divider-text gap-top-20 gap-bottom-45">
                                        <span>Numeric separator</span>
                                    </div>

                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <label class="label">Coma</label>
                                            <div class="input">
                                                <label class="icon-right" for="coma">
                                                    <i class="fa fa-usd"></i>
                                                </label>
                                                <input type="text" id="coma" class="currency">
                                            </div>
                                        </div>
                                        <div class="span6 unit">
                                            <label class="label">Apostrophe</label>
                                            <div class="input">
                                                <label class="icon-right" for="apostrophe">
                                                    <i class="fa fa-euro"></i>
                                                </label>
                                                <input type="text" id="apostrophe" class="currency" data-a-dec="."
                                                       data-a-sep="'">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <label class="label">Period</label>
                                            <div class="input">
                                                <label class="icon-right" for="period">
                                                    <i class="fa fa-gbp"></i>
                                                </label>
                                                <input type="text" id="period" class="currency" data-a-dec=","
                                                       data-a-sep=".">
                                            </div>
                                        </div>
                                        <div class="span6 unit">
                                            <label class="label">Space</label>
                                            <div class="input">
                                                <label class="icon-right" for="space">
                                                    <i class="fa fa-jpy"></i>
                                                </label>
                                                <input type="text" id="space" class="currency" data-a-dec="."
                                                       data-a-sep=" ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="divider-text gap-top-20 gap-bottom-45">
                                        <span>Currency signs</span>
                                    </div>

                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <label class="label">US Dollar</label>
                                            <div class="input">
                                                <input type="text" class="currency" data-a-sign="$ ">
                                            </div>
                                        </div>
                                        <div class="span6 unit">
                                            <label class="label">Yen</label>
                                            <div class="input">
                                                <input type="text" class="currency" data-a-sign="¥ ">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <label class="label">Euro</label>
                                            <div class="input">
                                                <input type="text" class="currency" data-a-sign="€ ">
                                            </div>
                                        </div>
                                        <div class="span6 unit">
                                            <label class="label">Pound</label>
                                            <div class="input">
                                                <input type="text" class="currency" data-a-sign="£ ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="divider-text gap-top-20 gap-bottom-45">
                                        <span>Range values</span>
                                    </div>

                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <div class="input">
                                                <input type="text" class="currency" placeholder="0 - 5000.00"
                                                       data-v-max="5000.00" data-v-min="0">
                                            </div>
                                        </div>
                                        <div class="span6 unit">
                                            <div class="input">
                                                <input type="text" class="currency" placeholder="-1000.00 - 1000.00"
                                                       data-v-min="-1000.00" data-v-max="1000.00">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="divider-text gap-top-20 gap-bottom-45">
                                        <span>Numeric Stepper</span>
                                    </div>
                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <label class="label">Default</label>
                                            <label class="input">
                                                <input type="text" id="stepper1">
                                            </label>
                                        </div>
                                        <div class="span6 unit">
                                            <label class="label">Disable mouse wheel</label>
                                            <label class="input">
                                                <input type="text" value="3" id="stepper2">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <label class="label">Value range (min: -10; max: 10)</label>
                                            <label class="input">
                                                <input type="text" id="stepper3">
                                            </label>
                                        </div>
                                        <div class="span6 unit">
                                            <label class="label">Limit (min: 5)</label>
                                            <label class="input">
                                                <input type="text" id="stepper4">
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="footer">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.maskedinput.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery-cloneya.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/cloned-form.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/autoNumeric.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.stepper.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/currency-form.js") }}"></script>
@endsection


