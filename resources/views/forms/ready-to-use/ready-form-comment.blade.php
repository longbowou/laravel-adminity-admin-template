@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-pro-modern.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/switchery/css/switchery.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Comment form</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Comment form</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Post Your Comment Here</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="j-wrapper j-wrapper-640">
                            <form action="https://colorlib.com//polygon/adminty/default/j-pro/php/action.php"
                                  method="post" class="j-pro" id="j-pro" novalidate>
                                <div class="j-content">

                                    <div class="j-row">
                                        <div class="j-span6 j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-left" for="first_name">
                                                    <i class="icofont icofont-ui-user"></i>
                                                </label>
                                                <input type="text" id="first_name" name="first_name"
                                                       placeholder="First name" class="name-group">
                                            </div>
                                        </div>
                                        <div class="j-span6 j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-left" for="last_name">
                                                    <i class="icofont icofont-ui-user"></i>
                                                </label>
                                                <input type="text" id="last_name" name="last_name"
                                                       placeholder="Last name" class="name-group">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="j-row">
                                        <div class="j-span6 j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-left" for="email">
                                                    <i class="icofont icofont-envelope"></i>
                                                </label>
                                                <input type="email" placeholder="Email" id="email" name="email">
                                            </div>
                                        </div>
                                        <div class="j-span6 j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-left" for="url">
                                                    <i class="icofont icofont-globe"></i>
                                                </label>
                                                <input type="url" placeholder="Website" id="url" name="url">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <div class="j-input">
                                            <label class="j-icon-left" for="message">
                                                <i class="icofont icofont-file-text"></i>
                                            </label>
                                            <textarea placeholder="Comments" spellcheck="false" id="message"
                                                      name="message"></textarea>
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <label class="j-checkbox-toggle">
                                            <input type="checkbox" class="js-single" checked/> Notify me about new
                                            comments
                                        </label>
                                    </div>


                                    <div class="j-response"></div>

                                </div>

                                <div class="j-footer">
                                    <button type="submit" class="btn btn-primary">Post comment</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.maskedinput.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.j-pro.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/components/switchery/js/switchery.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/booking-comment.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/form-comment.js") }}"></script>
@endsection


