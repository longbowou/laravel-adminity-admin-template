@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-pro-modern.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Subscribe-Form</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Subscribe-Form</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Subscribe From Here</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="j-wrapper j-wrapper-640">
                            <form action="https://colorlib.com//polygon/adminty/default/j-pro/php/action.php"
                                  method="post" class="j-pro" id="j-pro" novalidate>
                                <div class="j-content">
                                    <div class="j-divider-text j-gap-top-20 j-gap-bottom-45">
                                        <span>Subscribe to our newsletter</span>
                                    </div>

                                    <div class="j-row">
                                        <div class="j-span6 j-unit j-input">
                                            <label class="j-label">Receive newsletter:</label>
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                    <span>Every day</span>
                                                </label>
                                            </div>
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                    <span>Weekly newsletter</span>
                                                </label>
                                            </div>
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                    <span>Mothly newsletter</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="j-span6 j-unit j-input">
                                            <label class="j-label">News and Articles:</label>
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                    <span>Only The Latest</span>
                                                </label>
                                            </div>
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                    <span>Most Viewd</span>
                                                </label>
                                            </div>
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                    <span>Most Rated</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="j-divider j-gap-bottom-25"></div>

                                    <div class="j-unit">
                                        <div class="j-widget j-right-130">
                                            <div class="j-input">
                                                <input type="email" placeholder="enter your email" name="email">
                                            </div>
                                            <button type="submit" class="j-addon-btn j-adn-130 j-adn-right">
                                                Subscribe
                                            </button>
                                        </div>
                                    </div>


                                    <div class="j-response"></div>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.maskedinput.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.j-pro.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/subscrbe-form.js") }}"></script>
@endsection


