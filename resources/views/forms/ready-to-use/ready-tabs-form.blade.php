@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-forms.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Tab form</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Tab form</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Tab Form</h5>
                <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
            </div>
            <div class="card-block">
                <div class="wrapper wrapper-640">
                    <div class="j-tabs-container">

                        <input id="tab1" type="radio" name="tabs" checked>
                        <label class="j-tabs-label" for="tab1" title="Login">
                            <i class="icofont icofont-login "></i>
                            <span>Login</span>
                        </label>


                        <input id="tab2" type="radio" name="tabs">
                        <label class="j-tabs-label" for="tab2" title="Registration">
                            <i class="icofont icofont-ui-user"></i>
                            <span>Registration</span>
                        </label>


                        <input id="tab3" type="radio" name="tabs">
                        <label class="j-tabs-label" for="tab3" title="Reset password">
                            <i class="icofont icofont-unlock"></i>
                            <span>Reset password</span>
                        </label>


                        <div id="tabs-section-1" class="j-tabs-section">
                            <form action="#" method="post" class="j-forms" novalidate>
                                <div class="content">
                                    <div class="j-row">
                                        <div class="span6">
                                            <div class="divider-text gap-top-20 gap-bottom-45">
                                                <span>Sign in with</span>
                                            </div>

                                            <div class="unit">
                                                <div class="social-center">
                                                    <div class="social-icon twitter">
                                                        <i class="fa fa-twitter"></i>
                                                        <button type="button"></button>
                                                    </div>
                                                    <div class="social-icon google-plus">
                                                        <i class="fa fa-google-plus"></i>
                                                        <button type="button"></button>
                                                    </div>
                                                    <div class="social-icon facebook">
                                                        <i class="fa fa-facebook"></i>
                                                        <button type="button"></button>
                                                    </div>
                                                    <div class="social-icon linkedin">
                                                        <i class="fa fa-linkedin"></i>
                                                        <button type="button"></button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="span6">
                                            <div class="divider-text gap-top-20 gap-bottom-45">
                                                <span>Or login</span>
                                            </div>

                                            <div class="unit">
                                                <div class="input">
                                                    <label class="icon-right" for="login">
                                                        <i class="icofont icofont-ui-user"></i>
                                                    </label>
                                                    <input type="text" id="login" name="login"
                                                           placeholder="your login...">
                                                </div>
                                            </div>


                                            <div class="unit">
                                                <div class="input">
                                                    <label class="icon-right" for="password">
                                                        <i class="icofont icofont-lock"></i>
                                                    </label>
                                                    <input type="password" id="password" name="password"
                                                           placeholder="your password...">
                                                </div>
                                            </div>


                                            <div class="unit">
                                                <div class="checkbox-fade fade-in-primary">
                                                    <label>
                                                        <input type="checkbox" value="true" checked="">
                                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                        <span>Keep me logged in</span>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <button type="submit" class="btn btn-primary m-b-20 f-right">Sign in</button>
                                </div>

                            </form>
                        </div>


                        <div id="tabs-section-2" class="j-tabs-section">
                            <form action="#" method="post" class="j-forms" novalidate>
                                <div class="content">
                                    <div class="divider-text gap-top-20 gap-bottom-45">
                                        <span>Registration</span>
                                    </div>

                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <div class="input">
                                                <label class="icon-right" for="username">
                                                    <i class="icofont icofont-ui-user"></i>
                                                </label>
                                                <input type="text" id="username" name="username" placeholder="username">
                                            </div>
                                        </div>
                                        <div class="span6 unit">
                                            <div class="input">
                                                <label class="icon-right" for="email">
                                                    <i class="icofont icofont-envelope"></i>
                                                </label>
                                                <input type="email" id="email" name="email" placeholder="email">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <div class="input">
                                                <label class="icon-right" for="reg_password">
                                                    <i class="icofont icofont-lock"></i>
                                                </label>
                                                <input type="password" id="reg_password" name="password"
                                                       placeholder="password">
                                            </div>
                                        </div>
                                        <div class="span6 unit">
                                            <div class="input">
                                                <label class="icon-right" for="confirm_password">
                                                    <i class="icofont icofont-unlock"></i>
                                                </label>
                                                <input type="password" id="confirm_password" name="confirm_password"
                                                       placeholder="confirm password">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="divider gap-bottom-25"></div>

                                    <div class="j-row">
                                        <div class="span6 unit">
                                            <div class="input">
                                                <input type="text" placeholder="first name" name="first_name">
                                            </div>
                                        </div>
                                        <div class="span6 unit">
                                            <div class="input">
                                                <input type="text" placeholder="last name" name="last_name">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="unit">
                                        <label class="input select">
                                            <select name="gender">
                                                <option value="none" selected disabled="">select gender...</option>
                                                <option value="male">male</option>
                                                <option value="female">female</option>
                                                <option value="other">other</option>
                                            </select>
                                            <i></i>
                                        </label>
                                    </div>

                                    <button type="submit" class="btn btn-primary m-b-20 f-right">Register</button>
                                </div>

                            </form>
                        </div>


                        <div id="tabs-section-3" class="j-tabs-section">
                            <form action="#" method="post" class="j-forms" novalidate>
                                <div class="content">
                                    <div class="divider-text gap-top-20 gap-bottom-45">
                                        <span>Password recovery</span>
                                    </div>

                                    <div class="unit">
                                        <div class="widget right-130">
                                            <div class="input">
                                                <input type="email" placeholder="enter your email" name="email">
                                            </div>
                                            <button type="submit" class="addon-btn adn-130 adn-right">
                                                Recovery
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/subscrbe-form.js") }}"></script>
@endsection


