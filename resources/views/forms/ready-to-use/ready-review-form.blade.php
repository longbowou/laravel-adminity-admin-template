@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-pro-modern.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Review</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Review Form</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Review Our Product</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="j-wrapper j-wrapper-640">
                            <form action="https://colorlib.com//polygon/adminty/default/j-pro/php/action.php"
                                  method="post" class="j-pro" id="j-pro" novalidate>
                                <div class="j-content">

                                    <div class="j-unit">
                                        <label class="j-label">Name</label>
                                        <div class="j-input">
                                            <label class="j-icon-right" for="name">
                                                <i class="icofont icofont-user"></i>
                                            </label>
                                            <input type="text" id="name" name="name" placeholder="e.g. John Doe">
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <label class="j-label">Email</label>
                                        <div class="j-input">
                                            <label class="j-icon-right" for="email">
                                                <i class="icofont icofont-envelope"></i>
                                            </label>
                                            <input type="email" placeholder="email@domain.com" id="email" name="email">
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <label class="j-label">Review message</label>
                                        <div class="j-input">
                                            <textarea placeholder="message..." spellcheck="false"
                                                      name="message"></textarea>
                                        </div>
                                    </div>

                                    <div class="j-divider j-gap-bottom-25"></div>

                                    <div class="j-unit">
                                        <div class="j-rating-group">
                                            <label class="j-label">Product quality</label>
                                            <div class="j-ratings">
                                                <input id="5q" type="radio" name="product_rating" value="5">
                                                <label for="5q">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="4q" type="radio" name="product_rating" value="4">
                                                <label for="4q">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="3q" type="radio" name="product_rating" value="3">
                                                <label for="3q">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="2q" type="radio" name="product_rating" value="2">
                                                <label for="2q">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="1q" type="radio" name="product_rating" value="1" checked="">
                                                <label for="1q">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="j-rating-group">
                                            <label class="j-label">Service quality</label>
                                            <div class="j-ratings">
                                                <input id="5s" type="radio" name="service_rating" value="5">
                                                <label for="5s">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="4s" type="radio" name="service_rating" value="4">
                                                <label for="4s">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="3s" type="radio" name="service_rating" value="3">
                                                <label for="3s">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="2s" type="radio" name="service_rating" value="2">
                                                <label for="2s">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="1s" type="radio" name="service_rating" value="1" checked="">
                                                <label for="1s">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="j-rating-group">
                                            <label class="j-label">Support quality</label>
                                            <div class="j-ratings">
                                                <input id="5supp" type="radio" name="support_rating" value="5">
                                                <label for="5supp">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="4supp" type="radio" name="support_rating" value="4">
                                                <label for="4supp">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="3supp" type="radio" name="support_rating" value="3">
                                                <label for="3supp">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="2supp" type="radio" name="support_rating" value="2">
                                                <label for="2supp">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                                <input id="1supp" type="radio" name="support_rating" value="1"
                                                       checked="">
                                                <label for="1supp">
                                                    <i class="icofont icofont-star"></i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="j-response"></div>

                                </div>

                                <div class="j-footer">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.maskedinput.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.j-pro.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/review-form.js") }}"></script>
@endsection


