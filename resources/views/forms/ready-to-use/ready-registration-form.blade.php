@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-pro-modern.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Registration</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Registration Form</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Register Here</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="j-wrapper j-wrapper-640">
                            <form action="https://colorlib.com//polygon/adminty/default/j-pro/php/action.php"
                                  method="post" class="j-pro" id="j-pro" novalidate>
                                <div class="j-content">

                                    <div>
                                        <label class="j-label">Name</label>
                                        <div class="j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-right" for="name">
                                                    <i class="icofont icofont-ui-user"></i>
                                                </label>
                                                <input type="text" id="name" name="name">
                                            </div>
                                        </div>
                                    </div>


                                    <div>
                                        <div>
                                            <label class="j-label">Email</label>
                                        </div>
                                        <div class="j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-right" for="email">
                                                    <i class="icofont icofont-envelope"></i>
                                                </label>
                                                <input type="email" id="email" name="email">
                                            </div>
                                        </div>
                                    </div>


                                    <div>
                                        <div>
                                            <label class="j-label ">Login</label>
                                        </div>
                                        <div class="j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-right" for="login">
                                                    <i class="icofont icofont-ui-check"></i>
                                                </label>
                                                <input type="text" id="login" name="login">
                                            </div>
                                        </div>
                                    </div>


                                    <div>
                                        <div>
                                            <label class="j-label ">Password</label>
                                        </div>
                                        <div class="j-unit">
                                            <div class="j-input">
                                                <label class="j-icon-right" for="password">
                                                    <i class="icofont icofont-lock"></i>
                                                </label>
                                                <input type="password" id="password" name="password">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="j-response"></div>

                                </div>

                                <div class="j-footer">
                                    <button type="submit" class="btn btn-primary">Register</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.maskedinput.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.j-pro.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/reg-form.js") }}"></script>
@endsection


