@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-pro-modern.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Contact Form</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Contact Form</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Give Us Your Contact Details</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="j-wrapper j-wrapper-640">
                            <form action="https://colorlib.com//polygon/adminty/default/j-pro/php/action.php"
                                  method="post" class="j-pro" id="j-pro" novalidate>

                                <div class="j-content">

                                    <div class="j-unit">
                                        <label class="j-label m-b-10">Google Map</label>
                                        <div class="unit" id="google-map" style="width:100%;height:380px;">
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <label class="j-label">Name</label>
                                        <div class="j-input">
                                            <label class="j-icon-right" for="name">
                                                <i class="icofont icofont-ui-user"></i>
                                            </label>
                                            <input type="text" placeholder="e.g. John Doe" id="name" name="name">
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <label class="j-label">Email</label>
                                        <div class="j-input">
                                            <label class="j-icon-right" for="email">
                                                <i class="icofont icofont-envelope"></i>
                                            </label>
                                            <input type="email" placeholder="email@example.com" id="email" name="email">
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <label class="j-label">Phone</label>
                                        <div class="j-input">
                                            <label class="j-icon-right" for="phone">
                                                <i class="icofont icofont-phone"></i>
                                            </label>
                                            <input type="text" placeholder="telephone or mobile" id="phone"
                                                   name="phone">
                                        </div>
                                    </div>


                                    <div class="j-unit">
                                        <label class="j-label">Message</label>
                                        <div class="j-input">
                                            <textarea placeholder="your message..." spellcheck="false"
                                                      name="message"></textarea>
                                        </div>
                                    </div>


                                    <div class="j-response"></div>

                                </div>

                                <div class="j-footer">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.maskedinput.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.j-pro.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/contact-form.js") }}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY0mgQROcL8aXp-abr432Xx8DMeItHkzM"
            type="text/javascript"></script>
@endsection


