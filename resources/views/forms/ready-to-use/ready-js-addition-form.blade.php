@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-forms.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/switchery/css/switchery.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>JavaScript additions</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">JavaScript additions</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>JavaScript Additions</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="wrapper wrapper-640">
                            <form action="#" method="post" class="j-forms" novalidate>

                                <div class="content">
                                    <div class="divider-text gap-top-20 gap-bottom-45">
                                        <span>Show/hide password</span>
                                    </div>

                                    <div class="unit">
                                        <label class="label">Password</label>
                                        <div class="input">
                                            <label class="icon-right" for="password">
                                                <i class="fa fa-lock"></i>
                                            </label>
                                            <input type="password" id="password">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" id="show-pass">
                                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span> <span>Show password</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="divider-text gap-top-45 gap-bottom-45">
                                        <span>Enable/disable elements</span>
                                    </div>

                                    <div class="j-row">
                                        <div class="span4">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" id="check-enable-input">
                                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span> <span>Enable input</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="span8 unit">
                                            <div class="input disabled-view">
                                                <label class="icon-right" for="enable-input">
                                                    <i class="fa fa-edit"></i>
                                                </label>
                                                <input type="text" id="enable-input" placeholder="enable/disable input"
                                                       disabled="" class="m-t-10">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="j-row">
                                        <div class="span4">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" id="check-enable-button">
                                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span> <span>Enable button</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="span8">
                                            <button type="submit" class="btn btn-primary m-t-10 m-b-20 disabled-view"
                                                    id="enable-button" disabled="">Button
                                            </button>
                                        </div>
                                    </div>

                                    <div class="divider-text gap-top-20 gap-bottom-45">
                                        <span>Show/hide elements</span>
                                    </div>

                                    <div class="unit">
                                        <div class="checkbox-fade fade-in-primary">
                                            <label>
                                                <input type="checkbox" id="show-elements-checkbox">
                                                <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span> <span>Show additional elements</span>
                                            </label>
                                        </div>
                                    </div>


                                    <div class="j-row hidden-elements hidden">
                                        <div class="span9 unit">
                                            <div class="input">
                                                <label class="icon-right" for="input">
                                                    <i class="fa fa-edit"></i>
                                                </label>
                                                <input type="text" id="input" placeholder="placeholder text">
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <button type="submit" class="btn btn-primary">Button</button>
                                        </div>
                                    </div>

                                    <div class="divider gap-bottom-25"></div>

                                    <div class="unit">
                                        <label class="label">Show additional elements</label>
                                        <label class="input select" id="show-elements-select">
                                            <select>
                                                <option value="none">select action...</option>
                                                <option value="field-1">show field № 1</option>
                                                <option value="field-2">show field № 2</option>
                                                <option value="field-1-2">show field № 1 and field № 2</option>
                                            </select>
                                            <i></i>
                                        </label>
                                    </div>


                                    <div class="unit hidden" id="field-1">
                                        <label class="label">Field № 1</label>
                                        <div class="input">
                                            <input type="text" placeholder="field № 1">
                                        </div>
                                    </div>


                                    <div class="unit hidden" id="field-2">
                                        <label class="label">Field № 2</label>
                                        <div class="input">
                                            <input type="text" placeholder="field № 2">
                                        </div>
                                    </div>

                                    <div class="divider-text gap-top-45 gap-bottom-45">
                                        <span>Select condition</span>
                                    </div>

                                    <div class="j-row">
                                        <div class="span4 unit">
                                            <label class="label">Car</label>
                                            <select id="car">
                                                <option value="none">select a car...</option>
                                                <option value="VO">Volvo</option>
                                                <option value="VW">Volkswagen</option>
                                                <option value="BMW">BMW</option>
                                            </select>
                                        </div>
                                        <div class="span4 unit">
                                            <label class="label">Model</label>
                                            <select id="car-model">
                                                <option value="">select a model...</option>
                                            </select>
                                        </div>
                                        <div class="span4 unit">
                                            <label class="label">Color</label>
                                            <select id="car-model-color">
                                                <option value="">select a color...</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="divider-text gap-top-20 gap-bottom-45">
                                        <span>Checkbox conditions</span>
                                    </div>

                                    <div class="unit">
                                        <div class="j-row">
                                            <label class="label">Do you want to subscribe to our newsletter?</label>
                                            <div class="span5">
                                                <label class="checkbox-toggle">
                                                    <input type="checkbox" class="js-single" id="subscribe"/>
                                                </label>
                                            </div>
                                            <div class="span7">
                                                <div class="checkbox-fade fade-in-primary m-b-5">
                                                    <label class="disabled-view subscribe">
                                                        <input type="checkbox" id="show-elements-checkbox2" disabled="">
                                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                        <span>News and Articles (only the latest)</span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-fade fade-in-primary">
                                                    <label class="disabled-view subscribe">
                                                        <input type="checkbox" id="show-elements-checkbox3" disabled="">
                                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                        <span>Mothly newsletter</span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-fade fade-in-primary">
                                                    <label class="disabled-view subscribe">
                                                        <input type="checkbox" id="show-elements-checkbox4" disabled="">
                                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                                        <span>Weekly newsletter</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.maskedinput.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery-cloneya.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/j-forms-additions.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/advance-elements/swithces.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/components/switchery/js/switchery.min.js") }}"></script>
@endsection


