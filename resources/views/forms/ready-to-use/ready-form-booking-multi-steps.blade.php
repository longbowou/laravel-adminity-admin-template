@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/demo.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/j-pro/css/j-pro-modern.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Booking Multi Steps</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Ready To Use</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Booking Multi Steps</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Book Now</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="j-wrapper j-wrapper-640">
                            <form action="https://colorlib.com//polygon/adminty/default/j-pro/php/action.php"
                                  method="post" class="j-pro j-multistep" id="j-pro" novalidate>

                                <div class="j-content">
                                    <fieldset>
                                        <div class="j-divider-text j-gap-top-20 j-gap-bottom-45">
                                            <span>Step 1/3 - Personal info</span>
                                        </div>

                                        <div class="j-unit">
                                            <label class="j-label">Your name</label>
                                            <div class="j-input">
                                                <label class="j-icon-right" for="name">
                                                    <i class="icofont icofont-ui-user"></i>
                                                </label>
                                                <input type="text" id="name" name="name">
                                            </div>
                                        </div>


                                        <div class="j-row">
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Your email</label>
                                                <div class="j-input">
                                                    <label class="j-icon-right" for="email">
                                                        <i class="icofont icofont-envelope"></i>
                                                    </label>
                                                    <input type="email" id="email" name="email">
                                                </div>
                                            </div>
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Phone/Mobile</label>
                                                <div class="j-input">
                                                    <label class="j-icon-right" for="phone">
                                                        <i class="icofont icofont-phone"></i>
                                                    </label>
                                                    <input type="text" id="phone" name="phone">
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                    <fieldset>
                                        <div class="j-divider-text j-gap-top-20 j-gap-bottom-45">
                                            <span>Step 2/3 - Booking details</span>
                                        </div>

                                        <div class="j-row">
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Adult guests</label>
                                                <div class="j-input">
                                                    <label class="j-icon-right" for="adults">
                                                        <i class="icofont icofont-waiter"></i>
                                                    </label>
                                                    <input type="text" id="adults" name="adults">
                                                    <span
                                                        class="j-tooltip j-tooltip-right-top">Number of adult guests</span>
                                                </div>
                                            </div>
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Children guests</label>
                                                <div class="j-input">
                                                    <label class="j-icon-right" for="children">
                                                        <i class="icofont icofont-woman-in-glasses"></i>
                                                    </label>
                                                    <input type="text" id="children" name="children">
                                                    <span
                                                        class="j-tooltip j-tooltip-right-top">Number of children</span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="j-row">
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Check-in date</label>
                                                <div class="j-input">
                                                    <label class="j-icon-right" for="date_from">
                                                        <i class="icofont icofont-ui-calendar"></i>
                                                    </label>
                                                    <input type="text" id="date_from" name="date_from" readonly="">
                                                </div>
                                            </div>
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Check-out date</label>
                                                <div class="j-input">
                                                    <label class="j-icon-right" for="date_to">
                                                        <i class="icofont icofont-ui-calendar"></i>
                                                    </label>
                                                    <input type="text" id="date_to" name="date_to" readonly="">
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                    <fieldset>
                                        <div class="j-divider-text j-gap-top-20 j-gap-bottom-45">
                                            <span>Step 3/3 - Comments</span>
                                        </div>

                                        <div class="j-unit">
                                            <label class="j-label">Comments/Message</label>
                                            <div class="j-input">
                                                <textarea spellcheck="false" name="message"></textarea>
                                            </div>
                                        </div>

                                    </fieldset>

                                    <div class="j-response"></div>

                                </div>

                                <div class="j-footer">
                                    <button type="submit" class="btn btn-primary j-multi-submit-btn">Booking</button>
                                    <button type="button" class="btn btn-primary j-multi-next-btn">Next</button>
                                    <button type="button" class="btn btn-default m-r-20 j-multi-prev-btn">Back</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.maskedinput.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/jquery.j-pro.js") }}"></script>

    <script type="text/javascript" src="{{ asset("adminity/pages/j-pro/js/custom/booking-multistep.js") }}"></script>
@endsection


