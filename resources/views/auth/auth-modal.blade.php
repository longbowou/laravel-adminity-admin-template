@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Authentication Modal</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Authentication</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Modal</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Normal Sign in</h5>
                    </div>
                    <div class="card-block">
                        <p>
                            Click on Below Button For Normal Login Modal.
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sign-in">
                                Normal Sign in
                            </button>
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Social Sign in</h5>
                    </div>
                    <div class="card-block">
                        <p>
                            Click on Below Button For Social Login Modal.
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#sign-in-social">Social Sign in
                            </button>
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Register</h5>
                    </div>
                    <div class="card-block">
                        <p>
                            Click on Below Button For Register Modal.
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register">
                                Register
                            </button>
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Lock Screen Modal</h5>
                    </div>
                    <div class="card-block">
                        <p>
                            Click on Below Button For Lock screen Modal.
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#lock-screen">Lock Screen
                            </button>
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Confirm Mail Modal</h5>
                    </div>
                    <div class="card-block">
                        <p>
                            Click on Below Button For Confirmation Mail Modal.
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#confirm-mail">Confirm Mail
                            </button>
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Forgot Password</h5>
                    </div>
                    <div class="card-block">
                        <p>
                            Click on Below Button For Forgot Password Modal.
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#reset-password">Forgot Password
                            </button>
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

                <div class="card">
                    <div class="card-header">
                        <h5>Tabbed modal</h5>
                    </div>
                    <div class="card-block">
                        <p>
                            Click on Below Button For Tabbed Modal.
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#tabbed-form">Tabbed Form
                            </button>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

