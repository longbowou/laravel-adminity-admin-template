<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from colorlib.com//polygon/adminty/default/auth-sign-up.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Nov 2019 19:00:49 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <title>Adminty - Premium Admin Template by Colorlib </title>


    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="#">
    <meta name="keywords"
          content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="{{ asset("adminity/images/favicon.ico") }}" type="image/x-icon">

    <link href="{{ asset("adminity/css/css3b0a.css") }}?family=Open+Sans:400,600,800" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/css/bootstrap.min.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/icon/themify-icons/themify-icons.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/icon/icofont/css/icofont.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/css/style.css") }}">
</head>
<body class="fix-menu">

@include("partials.theme-loader")

<section class="login-block">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <form class="md-float-material form-material">
                    <div class="text-center">
                        <img src="{{ asset("adminity/images/logo.png") }}" alt="logo.png">
                    </div>
                    <div class="auth-box card">
                        <div class="card-block">
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <h3 class="text-center txt-primary">Sign up</h3>
                                </div>
                            </div>
                            <div class="form-group form-primary">
                                <input type="text" name="user-name" class="form-control" required=""
                                       placeholder="Choose Username">
                                <span class="form-bar"></span>
                            </div>
                            <div class="form-group form-primary">
                                <input type="text" name="email" class="form-control" required=""
                                       placeholder="Your Email Address">
                                <span class="form-bar"></span>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-primary">
                                        <input type="password" name="password" class="form-control" required=""
                                               placeholder="Password">
                                        <span class="form-bar"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-primary">
                                        <input type="password" name="confirm-password" class="form-control" required=""
                                               placeholder="Confirm Password">
                                        <span class="form-bar"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-25 text-left">
                                <div class="col-md-12">
                                    <div class="checkbox-fade fade-in-primary">
                                        <label>
                                            <input type="checkbox" value="">
                                            <span class="cr"><i
                                                    class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                            <span class="text-inverse">I read and accept <a href="#">Terms &amp; Conditions.</a></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="checkbox-fade fade-in-primary">
                                        <label>
                                            <input type="checkbox" value="">
                                            <span class="cr"><i
                                                    class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                            <span class="text-inverse">Send me the <a
                                                    href="#!">Newsletter</a> weekly.</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12">
                                    <button type="button"
                                            class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                        Sign up now
                                    </button>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-10">
                                    <p class="text-inverse text-left m-b-0">Thank you.</p>
                                    <p class="text-inverse text-left"><a href="{{ route("welcome") }}"><b
                                                class="f-w-600">Back to
                                                website</b></a></p>
                                </div>
                                <div class="col-md-2">
                                    <img src="{{ asset("adminity/images/auth/Logo-small-bottom.png") }}"
                                         alt="small-logo.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>

</section>

@include("partials.ie-warning")

<script type="text/javascript"
        src="{{ asset("adminity/js/jquery.min.js") }}"></script>
<script type="text/javascript"
        src="{{ asset("adminity/js/jquery-ui.min.js") }}"></script>
<script type="text/javascript"
        src="{{ asset("adminity/js/popper.min.js") }}"></script>
<script type="text/javascript"
        src="{{ asset("welcome/js/bootstrap.min.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("adminity/js/jquery.slimscroll.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("adminity/js/modernizr.js") }}"></script>
<script type="text/javascript"
        src="{{ asset("adminity/js/css-scrollbars.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("adminity/js/i18next.min.js") }}"></script>
<script type="text/javascript"
        src="{{ asset("adminity/js/i18nextXHRBackend.min.js") }}"></script>
<script type="text/javascript"
        src="{{ asset("adminity/js/i18nextBrowserLanguageDetector.min.js") }}"></script>
<script type="text/javascript"
        src="{{ asset("adminity/js/jquery-i18next.min.js") }}"></script>

<script type="text/javascript" src="{{ asset("adminity/js/common-pages.js") }}"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"
        type="text/javascript"></script>
<script type="text/javascript">
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>
</body>

<!-- Mirrored from colorlib.com//polygon/adminty/default/auth-sign-up.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Nov 2019 19:00:49 GMT -->
</html>
