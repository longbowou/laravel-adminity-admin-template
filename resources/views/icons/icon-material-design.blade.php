@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Material-Design-Icons</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Icons</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Material-Design-Icons</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5> New Icons</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-group"></i> <span>group</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-rss"></i> <span>rss</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-shape"></i> <span>shape</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-spinner"></i> <span>spinner</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-ungroup"></i> <span>ungroup</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-500px"></i> <span>500px</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-8tracks"></i> <span>8tracks</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-amazon"></i> <span>amazon</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-blogger"></i> <span>blogger</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-delicious"></i> <span>delicious</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-disqus"></i> <span>disqus</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flattr"></i> <span>flattr</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flickr"></i> <span>flickr</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-github-alt"></i> <span>github-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-google-old"></i> <span>google-old</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-linkedin"></i> <span>linkedin</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-odnoklassniki"></i> <span>odnoklassniki</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-outlook"></i> <span>outlook</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-paypal-alt"></i> <span>paypal-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pinterest"></i> <span>pinterest</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-playstation"></i> <span>playstation</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-reddit"></i> <span>reddit</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-skype"></i> <span>skype</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-slideshare"></i> <span>slideshare</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-soundcloud"></i> <span>soundcloud</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tumblr"></i> <span>tumblr</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-twitch"></i> <span>twitch</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-vimeo"></i> <span>vimeo</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-whatsapp"></i> <span>whatsapp</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-xbox"></i> <span>xbox</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-yahoo"></i> <span>yahoo</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-youtube-play"></i> <span>youtube-play</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-youtube"></i> <span>youtube</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5> Web Application</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-3d-rotation"></i> <span>3d-rotation</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-airplane-off"></i> <span>airplane-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-airplane"></i> <span>airplane</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-album"></i> <span>album</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-archive"></i> <span>archive</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-assignment-account"></i> <span>assignment-account</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-assignment-alert"></i> <span>assignment-alert</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-assignment-check"></i> <span>assignment-check</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-assignment-o"></i> <span>assignment-o</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-assignment-return"></i> <span>assignment-return</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-assignment-returned"></i>
                                    <span>assignment-returned</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-assignment"></i> <span>assignment</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-attachment-alt"></i> <span>attachment-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-attachment"></i> <span>attachment</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-audio"></i> <span>audio</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-badge-check"></i> <span>badge-check</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-balance-wallet"></i> <span>balance-wallet</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-balance"></i> <span>balance</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-battery-alert"></i> <span>battery-alert</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-battery-flash"></i> <span>battery-flash</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-battery-unknown"></i> <span>battery-unknown</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-battery"></i> <span>battery</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bike"></i> <span>bike</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-block-alt"></i> <span>block-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-block"></i> <span>block</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-boat"></i> <span>boat</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-book-image"></i> <span>book-image</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-book"></i> <span>book</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bookmark-outline"></i> <span>bookmark-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bookmark"></i> <span>bookmark</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-brush"></i> <span>brush</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bug"></i> <span>bug</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bus"></i> <span>bus</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cake"></i> <span>cake</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-car-taxi"></i> <span>car-taxi</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-car-wash"></i> <span>car-wash</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-car"></i> <span>car</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-card-giftcard"></i> <span>card-giftcard</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-card-membership"></i> <span>card-membership</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-card-travel"></i> <span>card-travel</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-card"></i> <span>card</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-case-check"></i> <span>case-check</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-case-download"></i> <span>case-download</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-case-play"></i> <span>case-play</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-case"></i> <span>case</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cast-connected"></i> <span>cast-connected</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cast"></i> <span>cast</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-chart-donut"></i> <span>chart-donut</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-chart"></i> <span>chart</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-city-alt"></i> <span>city-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-city"></i> <span>city</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-close-circle-o"></i> <span>close-circle-o</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-close-circle"></i> <span>close-circle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-close"></i> <span>close</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cocktail"></i> <span>cocktail</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-code-setting"></i> <span>code-setting</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-code-smartphone"></i> <span>code-smartphone</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-code"></i> <span>code</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-coffee"></i> <span>coffee</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-bookmark"></i>
                                    <span>collection-bookmark</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-case-play"></i>
                                    <span>collection-case-play</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-folder-image"></i>
                                    <span>collection-folder-image</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-image-o"></i> <span>collection-image-o</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-image"></i> <span>collection-image</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-1"></i> <span>collection-item-1</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-2"></i> <span>collection-item-2</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-3"></i> <span>collection-item-3</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-4"></i> <span>collection-item-4</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-5"></i> <span>collection-item-5</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-6"></i> <span>collection-item-6</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-7"></i> <span>collection-item-7</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-8"></i> <span>collection-item-8</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-9-plus"></i>
                                    <span>collection-item-9-plus</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item-9"></i> <span>collection-item-9</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-item"></i> <span>collection-item</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-music"></i> <span>collection-music</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-pdf"></i> <span>collection-pdf</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-plus"></i> <span>collection-plus</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-speaker"></i> <span>collection-speaker</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-text"></i> <span>collection-text</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-collection-video"></i> <span>collection-video</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-compass"></i> <span>compass</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cutlery"></i> <span>cutlery</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-delete"></i> <span>delete</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-dialpad"></i> <span>dialpad</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-dns"></i> <span>dns</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-drink"></i> <span>drink</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-edit"></i> <span>edit</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-email-open"></i> <span>email-open</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-email"></i> <span>email</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-eye-off"></i> <span>eye-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-eye"></i> <span>eye</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-eyedropper"></i> <span>eyedropper</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-favorite-outline"></i> <span>favorite-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-favorite"></i> <span>favorite</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-filter-list"></i> <span>filter-list</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-fire"></i> <span>fire</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flag"></i> <span>flag</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flare"></i> <span>flare</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flash-auto"></i> <span>flash-auto</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flash-off"></i> <span>flash-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flash"></i> <span>flash</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flip"></i> <span>flip</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flower-alt"></i> <span>flower-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-flower"></i> <span>flower</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-font"></i> <span>font</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-fullscreen-alt"></i> <span>fullscreen-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-fullscreen-exit"></i> <span>fullscreen-exit</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-fullscreen"></i> <span>fullscreen</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-functions"></i> <span>functions</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-gas-station"></i> <span>gas-station</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-gesture"></i> <span>gesture</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-globe-alt"></i> <span>globe-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-globe-lock"></i> <span>globe-lock</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-globe"></i> <span>globe</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-graduation-cap"></i> <span>graduation-cap</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-group"></i> <span>group</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-home"></i> <span>home</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-hospital-alt"></i> <span>hospital-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-hospital"></i> <span>hospital</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-hotel"></i> <span>hotel</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-hourglass-alt"></i> <span>hourglass-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-hourglass-outline"></i> <span>hourglass-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-hourglass"></i> <span>hourglass</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-http"></i> <span>http</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-image-alt"></i> <span>image-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-image-o"></i> <span>image-o</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-image"></i> <span>image</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-inbox"></i> <span>inbox</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-invert-colors-off"></i> <span>invert-colors-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-invert-colors"></i> <span>invert-colors</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-key"></i> <span>key</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-label-alt-outline"></i> <span>label-alt-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-label-alt"></i> <span>label-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-label-heart"></i> <span>label-heart</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-label"></i> <span>label</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-labels"></i> <span>labels</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-lamp"></i> <span>lamp</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-landscape"></i> <span>landscape</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-layers-off"></i> <span>layers-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-layers"></i> <span>layers</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-library"></i> <span>library</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-link"></i> <span>link</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-lock-open"></i> <span>lock-open</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-lock-outline"></i> <span>lock-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-lock"></i> <span>lock</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mail-reply-all"></i> <span>mail-reply-all</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mail-reply"></i> <span>mail-reply</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mail-send"></i> <span>mail-send</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mall"></i> <span>mall</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-map"></i> <span>map</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-menu"></i> <span>menu</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-money-box"></i> <span>money-box</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-money-off"></i> <span>money-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-money"></i> <span>money</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-more-vert"></i> <span>more-vert</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-more"></i> <span>more</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-movie-alt"></i> <span>movie-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-movie"></i> <span>movie</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-nature-people"></i> <span>nature-people</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-nature"></i> <span>nature</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-navigation"></i> <span>navigation</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-open-in-browser"></i> <span>open-in-browser</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-open-in-new"></i> <span>open-in-new</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-palette"></i> <span>palette</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-parking"></i> <span>parking</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-account"></i> <span>pin-account</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-assistant"></i> <span>pin-assistant</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-drop"></i> <span>pin-drop</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-help"></i> <span>pin-help</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-off"></i> <span>pin-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin"></i> <span>pin</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pizza"></i> <span>pizza</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-plaster"></i> <span>plaster</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-power-setting"></i> <span>power-setting</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-power"></i> <span>power</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-print"></i> <span>print</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-puzzle-piece"></i> <span>puzzle-piece</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-quote"></i> <span>quote</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-railway"></i> <span>railway</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-receipt"></i> <span>receipt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-refresh-alt"></i> <span>refresh-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-refresh-sync-alert"></i> <span>refresh-sync-alert</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-refresh-sync-off"></i> <span>refresh-sync-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-refresh-sync"></i> <span>refresh-sync</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-refresh"></i> <span>refresh</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-roller"></i> <span>roller</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-ruler"></i> <span>ruler</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-scissors"></i> <span>scissors</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-screen-rotation-lock"></i>
                                    <span>screen-rotation-lock</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-screen-rotation"></i> <span>screen-rotation</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-search-for"></i> <span>search-for</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-search-in-file"></i> <span>search-in-file</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-search-in-page"></i> <span>search-in-page</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-search-replace"></i> <span>search-replace</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-search"></i> <span>search</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-seat"></i> <span>seat</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-settings-square"></i> <span>settings-square</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-settings"></i> <span>settings</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-shape"></i> <span>shape</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-shield-check"></i> <span>shield-check</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-shield-security"></i> <span>shield-security</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-shopping-basket"></i> <span>shopping-basket</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-shopping-cart-plus"></i> <span>shopping-cart-plus</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-shopping-cart"></i> <span>shopping-cart</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-sign-in"></i> <span>sign-in</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-sort-amount-asc"></i> <span>sort-amount-asc</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-sort-amount-desc"></i> <span>sort-amount-desc</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-sort-asc"></i> <span>sort-asc</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-sort-desc"></i> <span>sort-desc</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-spellcheck"></i> <span>spellcheck</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-spinner"></i> <span>spinner</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-storage"></i> <span>storage</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-store-24"></i> <span>store-24</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-store"></i> <span>store</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-subway"></i> <span>subway</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-sun"></i> <span>sun</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tab-unselected"></i> <span>tab-unselected</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tab"></i> <span>tab</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tag-close"></i> <span>tag-close</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tag-more"></i> <span>tag-more</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tag"></i> <span>tag</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-thumb-down"></i> <span>thumb-down</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-thumb-up-down"></i> <span>thumb-up-down</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-thumb-up"></i> <span>thumb-up</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-ticket-star"></i> <span>ticket-star</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-toll"></i> <span>toll</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-toys"></i> <span>toys</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-traffic"></i> <span>traffic</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-translate"></i> <span>translate</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-triangle-down"></i> <span>triangle-down</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-triangle-up"></i> <span>triangle-up</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-truck"></i> <span>truck</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-turning-sign"></i> <span>turning-sign</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-ungroup"></i> <span>ungroup</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wallpaper"></i> <span>wallpaper</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-washing-machine"></i> <span>washing-machine</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-window-maximize"></i> <span>window-maximize</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-window-minimize"></i> <span>window-minimize</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-window-restore"></i> <span>window-restore</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wrench"></i> <span>wrench</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-zoom-in"></i> <span>zoom-in</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-zoom-out"></i> <span>zoom-out</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5> Notifications</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-alert-circle-o"></i> <span>alert-circle-o</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-alert-circle"></i> <span>alert-circle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-alert-octagon"></i> <span>alert-octagon</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-alert-polygon"></i> <span>alert-polygon</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-alert-triangle"></i> <span>alert-triangle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-help-outline"></i> <span>help-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-help"></i> <span>help</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-info-outline"></i> <span>info-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-info"></i> <span>info</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-notifications-active"></i>
                                    <span>notifications-active</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-notifications-add"></i> <span>notifications-add</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-notifications-none"></i> <span>notifications-none</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-notifications-off"></i> <span>notifications-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-notifications-paused"></i>
                                    <span>notifications-paused</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-notifications"></i> <span>notifications</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5> Person</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-account-add"></i> <span>account-add</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-account-box-mail"></i> <span>account-box-mail</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-account-box-o"></i> <span>account-box-o</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-account-box-phone"></i> <span>account-box-phone</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-account-box"></i> <span>account-box</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-account-calendar"></i> <span>account-calendar</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-account-circle"></i> <span>account-circle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-account-o"></i> <span>account-o</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-account"></i> <span>account</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-accounts-add"></i> <span>accounts-add</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-accounts-alt"></i> <span>accounts-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-accounts-list-alt"></i> <span>accounts-list-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-accounts-list"></i> <span>accounts-list</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-accounts-outline"></i> <span>accounts-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-accounts"></i> <span>accounts</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-face"></i> <span>face</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-female"></i> <span>female</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-male-alt"></i> <span>male-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-male-female"></i> <span>male-female</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-male"></i> <span>male</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mood-bad"></i> <span>mood-bad</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mood"></i> <span>mood</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-run"></i> <span>run</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-walk"></i> <span>walk</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>File</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cloud-box"></i> <span>cloud-box</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cloud-circle"></i> <span>cloud-circle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cloud-done"></i> <span>cloud-done</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cloud-download"></i> <span>cloud-download</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cloud-off"></i> <span>cloud-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cloud-outline-alt"></i> <span>cloud-outline-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cloud-outline"></i> <span>cloud-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cloud-upload"></i> <span>cloud-upload</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-cloud"></i> <span>cloud</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-download"></i> <span>download</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-file-plus"></i> <span>file-plus</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-file-text"></i> <span>file-text</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-file"></i> <span>file</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-folder-outline"></i> <span>folder-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-folder-person"></i> <span>folder-person</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-folder-star-alt"></i> <span>folder-star-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-folder-star"></i> <span>folder-star</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-folder"></i> <span>folder</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-gif"></i> <span>gif</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-upload"></i> <span>upload</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5> Editor</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-all"></i> <span>border-all</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-bottom"></i> <span>border-bottom</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-clear"></i> <span>border-clear</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-color"></i> <span>border-color</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-horizontal"></i> <span>border-horizontal</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-inner"></i> <span>border-inner</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-left"></i> <span>border-left</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-outer"></i> <span>border-outer</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-right"></i> <span>border-right</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-style"></i> <span>border-style</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-top"></i> <span>border-top</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-border-vertical"></i> <span>border-vertical</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-copy"></i> <span>copy</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-crop"></i> <span>crop</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-align-center"></i>
                                    <span>format-align-center</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-align-justify"></i>
                                    <span>format-align-justify</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-align-left"></i> <span>format-align-left</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-align-right"></i> <span>format-align-right</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-bold"></i> <span>format-bold</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-clear-all"></i> <span>format-clear-all</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-clear"></i> <span>format-clear</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-color-fill"></i> <span>format-color-fill</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-color-reset"></i> <span>format-color-reset</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-color-text"></i> <span>format-color-text</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-indent-decrease"></i>
                                    <span>format-indent-decrease</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-indent-increase"></i>
                                    <span>format-indent-increase</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-italic"></i> <span>format-italic</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-line-spacing"></i>
                                    <span>format-line-spacing</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-list-bulleted"></i>
                                    <span>format-list-bulleted</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-list-numbered"></i>
                                    <span>format-list-numbered</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-ltr"></i> <span>format-ltr</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-rtl"></i> <span>format-rtl</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-size"></i> <span>format-size</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-strikethrough-s"></i>
                                    <span>format-strikethrough-s</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-strikethrough"></i>
                                    <span>format-strikethrough</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-subject"></i> <span>format-subject</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-underlined"></i> <span>format-underlined</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-valign-bottom"></i>
                                    <span>format-valign-bottom</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-valign-center"></i>
                                    <span>format-valign-center</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-format-valign-top"></i> <span>format-valign-top</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-redo"></i> <span>redo</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-select-all"></i> <span>select-all</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-space-bar"></i> <span>space-bar</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-text-format"></i> <span>text-format</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-transform"></i> <span>transform</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-undo"></i> <span>undo</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wrap-text"></i> <span>wrap-text</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5> Comment</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-alert"></i> <span>comment-alert</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-alt-text"></i> <span>comment-alt-text</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-alt"></i> <span>comment-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-edit"></i> <span>comment-edit</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-image"></i> <span>comment-image</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-list"></i> <span>comment-list</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-more"></i> <span>comment-more</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-outline"></i> <span>comment-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-text-alt"></i> <span>comment-text-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-text"></i> <span>comment-text</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment-video"></i> <span>comment-video</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comment"></i> <span>comment</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-comments"></i> <span>comments</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5> Hardware</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bluetooth-connected"></i>
                                    <span>bluetooth-connected</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bluetooth-off"></i> <span>bluetooth-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bluetooth-search"></i> <span>bluetooth-search</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bluetooth-setting"></i> <span>bluetooth-setting</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-bluetooth"></i> <span>bluetooth</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera-add"></i> <span>camera-add</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera-alt"></i> <span>camera-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera-bw"></i> <span>camera-bw</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera-front"></i> <span>camera-front</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera-mic"></i> <span>camera-mic</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera-party-mode"></i> <span>camera-party-mode</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera-rear"></i> <span>camera-rear</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera-roll"></i> <span>camera-roll</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera-switch"></i> <span>camera-switch</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-camera"></i> <span>camera</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-card-alert"></i> <span>card-alert</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-card-off"></i> <span>card-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-card-sd"></i> <span>card-sd</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-card-sim"></i> <span>card-sim</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-desktop-mac"></i> <span>desktop-mac</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-desktop-windows"></i> <span>desktop-windows</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-device-hub"></i> <span>device-hub</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-devices-off"></i> <span>devices-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-devices"></i> <span>devices</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-dock"></i> <span>dock</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-floppy"></i> <span>floppy</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-gamepad"></i> <span>gamepad</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-gps-dot"></i> <span>gps-dot</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-gps-off"></i> <span>gps-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-gps"></i> <span>gps</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-headset-mic"></i> <span>headset-mic</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-headset"></i> <span>headset</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-input-antenna"></i> <span>input-antenna</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-input-composite"></i> <span>input-composite</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-input-hdmi"></i> <span>input-hdmi</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-input-power"></i> <span>input-power</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-input-svideo"></i> <span>input-svideo</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-keyboard-hide"></i> <span>keyboard-hide</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-keyboard"></i> <span>keyboard</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-laptop-chromebook"></i> <span>laptop-chromebook</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-laptop-mac"></i> <span>laptop-mac</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-laptop"></i> <span>laptop</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mic-off"></i> <span>mic-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mic-outline"></i> <span>mic-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mic-setting"></i> <span>mic-setting</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mic"></i> <span>mic</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-mouse"></i> <span>mouse</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-network-alert"></i> <span>network-alert</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-network-locked"></i> <span>network-locked</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-network-off"></i> <span>network-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-network-outline"></i> <span>network-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-network-setting"></i> <span>network-setting</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-network"></i> <span>network</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-bluetooth"></i> <span>phone-bluetooth</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-end"></i> <span>phone-end</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-forwarded"></i> <span>phone-forwarded</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-in-talk"></i> <span>phone-in-talk</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-locked"></i> <span>phone-locked</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-missed"></i> <span>phone-missed</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-msg"></i> <span>phone-msg</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-paused"></i> <span>phone-paused</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-ring"></i> <span>phone-ring</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-setting"></i> <span>phone-setting</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone-sip"></i> <span>phone-sip</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-phone"></i> <span>phone</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-portable-wifi-changes"></i>
                                    <span>portable-wifi-changes</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-portable-wifi-off"></i> <span>portable-wifi-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-portable-wifi"></i> <span>portable-wifi</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-radio"></i> <span>radio</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-reader"></i> <span>reader</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-remote-control-alt"></i> <span>remote-control-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-remote-control"></i> <span>remote-control</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-router"></i> <span>router</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-scanner"></i> <span>scanner</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-android"></i> <span>smartphone-android</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-download"></i>
                                    <span>smartphone-download</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-erase"></i> <span>smartphone-erase</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-info"></i> <span>smartphone-info</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-iphone"></i> <span>smartphone-iphone</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-landscape-lock"></i>
                                    <span>smartphone-landscape-lock</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-landscape"></i>
                                    <span>smartphone-landscape</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-lock"></i> <span>smartphone-lock</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-portrait-lock"></i>
                                    <span>smartphone-portrait-lock</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-ring"></i> <span>smartphone-ring</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-setting"></i> <span>smartphone-setting</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone-setup"></i> <span>smartphone-setup</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-smartphone"></i> <span>smartphone</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-speaker"></i> <span>speaker</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tablet-android"></i> <span>tablet-android</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tablet-mac"></i> <span>tablet-mac</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tablet"></i> <span>tablet</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tv-alt-play"></i> <span>tv-alt-play</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tv-list"></i> <span>tv-list</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tv-play"></i> <span>tv-play</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-tv"></i> <span>tv</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-usb"></i> <span>usb</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-videocam-off"></i> <span>videocam-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-videocam-switch"></i> <span>videocam-switch</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-videocam"></i> <span>videocam</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-watch"></i> <span>watch</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wifi-alt-2"></i> <span>wifi-alt-2</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wifi-alt"></i> <span>wifi-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wifi-info"></i> <span>wifi-info</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wifi-lock"></i> <span>wifi-lock</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wifi-off"></i> <span>wifi-off</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wifi-outline"></i> <span>wifi-outline</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-wifi"></i> <span>wifi</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5> Directional</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-arrow-left-bottom"></i> <span>arrow-left-bottom</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-arrow-left"></i> <span>arrow-left</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-arrow-merge"></i> <span>arrow-merge</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-arrow-missed"></i> <span>arrow-missed</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-arrow-right-top"></i> <span>arrow-right-top</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-arrow-right"></i> <span>arrow-right</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-arrow-split"></i> <span>arrow-split</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-arrows"></i> <span>arrows</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-caret-down-circle"></i> <span>caret-down-circle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-caret-down"></i> <span>caret-down</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-caret-left-circle"></i> <span>caret-left-circle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-caret-left"></i> <span>caret-left</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-caret-right-circle"></i> <span>caret-right-circle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-caret-right"></i> <span>caret-right</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-caret-up-circle"></i> <span>caret-up-circle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-caret-up"></i> <span>caret-up</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-chevron-down"></i> <span>chevron-down</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-chevron-left"></i> <span>chevron-left</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-chevron-right"></i> <span>chevron-right</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-chevron-up"></i> <span>chevron-up</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-forward"></i> <span>forward</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-long-arrow-down"></i> <span>long-arrow-down</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-long-arrow-left"></i> <span>long-arrow-left</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-long-arrow-return"></i> <span>long-arrow-return</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-long-arrow-right"></i> <span>long-arrow-right</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-long-arrow-tab"></i> <span>long-arrow-tab</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-long-arrow-up"></i> <span>long-arrow-up</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-rotate-ccw"></i> <span>rotate-ccw</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-rotate-cw"></i> <span>rotate-cw</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-rotate-left"></i> <span>rotate-left</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-rotate-right"></i> <span>rotate-right</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-square-down"></i> <span>square-down</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-square-right"></i> <span>square-right</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-swap-alt"></i> <span>swap-alt</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-swap-vertical-circle"></i>
                                    <span>swap-vertical-circle</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-swap-vertical"></i> <span>swap-vertical</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-swap"></i> <span>swap</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-trending-down"></i> <span>trending-down</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-trending-flat"></i> <span>trending-flat</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-trending-up"></i> <span>trending-up</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-unfold-less"></i> <span>unfold-less</span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-unfold-more"></i> <span>unfold-more</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5> Map (aliases) </h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="data-table-main icon-list-demo">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-directions-bike"></i> zmdi-directions-bike
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-directions-boat"></i> zmdi-directions-boat
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-directions-bus"></i> zmdi-directions-bus
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-directions-car"></i> zmdi-directions-car
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-directions-railway"></i> zmdi-directions-railway
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-directions-run"></i> zmdi-directions-run
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-directions-subway"></i> zmdi-directions-subway
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-directions-walk"></i> zmdi-directions-walk
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-directions"></i> zmdi-directions
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-layers-off"></i> zmdi-layers-off
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-layers"></i> zmdi-layers
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-activity"></i> zmdi-local-activity
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-airport"></i> zmdi-local-airport
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-atm"></i> zmdi-local-atm
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-bar"></i> zmdi-local-bar
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-cafe"></i> zmdi-local-cafe
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-car-wash"></i> zmdi-local-car-wash
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-convenience-store"></i> zmdi-local-convenience-store
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-dining"></i> zmdi-local-dining
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-drink"></i> zmdi-local-drink
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-florist"></i> zmdi-local-florist
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-gas-station"></i> zmdi-local-gas-station
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-grocery-store"></i> zmdi-local-grocery-store
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-hospital"></i> zmdi-local-hospital
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-hotel"></i> zmdi-local-hotel
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-laundry-service"></i> zmdi-local-laundry-service
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-library"></i> zmdi-local-library
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-mall"></i> zmdi-local-mall
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-movies"></i> zmdi-local-movies
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-offer"></i> zmdi-local-offer
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-parking"></i> zmdi-local-parking
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-pharmacy"></i> zmdi-local-pharmacy
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-phone"></i> zmdi-local-phone
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-pizza"></i> zmdi-local-pizza
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-activity"></i> zmdi-local-activity
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-post-office"></i> zmdi-local-post-office
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-printshop"></i> zmdi-local-printshop
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-see"></i> zmdi-local-see
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-shipping"></i> zmdi-local-shipping
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-store"></i> zmdi-local-store
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-taxi"></i> zmdi-local-taxi
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-local-wc"></i> zmdi-local-wc
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-map"></i> zmdi-map
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-my-location"></i> zmdi-my-location
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-nature-people"></i> zmdi-nature-people
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-nature"></i> zmdi-nature
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-navigation"></i> zmdi-navigation
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-account"></i> zmdi-pin-account
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-assistant"></i> zmdi-pin-assistant
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-drop"></i> zmdi-pin-drop
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-help"></i> zmdi-pin-help
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin-off"></i> zmdi-pin-off
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-pin"></i> zmdi-pin
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xs-12 m-b-10 outer-ellipsis">
                                    <i class="zmdi zmdi-traffic"></i> zmdi-traffic
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection


