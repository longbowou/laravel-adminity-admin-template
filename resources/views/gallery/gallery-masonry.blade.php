@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/select2/css/select2.min.css") }}"/>
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Gallery Masonry</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Pages</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Sample page</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body masonry-page">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Gallery Masonry</h5>
                    </div>
                    <div class="card-block masonry-image">
                        <div class="default-grid ">
                            <div class="row lightboxgallery-popup">
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-1.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-2.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-3.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-4.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-5.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-6.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-7.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-8.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-3.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 default-grid-item">
                                    <div class="masonry-media">
                                        <a class="media-middle" href="#!">
                                            <img class="img-fluid"
                                                 src="{{ asset("adminity/images/gallery-grid/masonry-2.jpg") }}"
                                                 alt="masonary">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <h5 class="m-b-20">Gallery with description</h5>
        <div class="default-grid row">
            <div class="row lightboxgallery-popup">
                <div class="col-lg-3 col-md-6 default-grid-item">
                    <div class="card gallery-desc">
                        <div class="masonry-media">
                            <a class="media-middle" href="#!">
                                <img class="img-fluid" src="{{ asset("adminity/images/gallery-grid/masonry-1.jpg") }}"
                                     alt="masonary">
                            </a>
                        </div>
                        <div class="card-block">
                            <h6 class="job-card-desc">Job Description</h6>
                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                            <div class="job-meta-data"><i class="icofont icofont-safety"></i>washington</div>
                            <div class="job-meta-data"><i class="icofont icofont-university"></i>10 Years</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 default-grid-item">
                    <div class="card gallery-desc">
                        <div class="masonry-media">
                            <a class="media-middle" href="#!">
                                <img class="img-fluid" src="{{ asset("adminity/images/gallery-grid/masonry-2.jpg") }}"
                                     alt="masonary">
                            </a>
                        </div>
                        <div class="card-block">
                            <h6 class="job-card-desc">Job Description</h6>
                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                            <div class="job-meta-data"><i class="icofont icofont-safety"></i>washington</div>
                            <div class="job-meta-data"><i class="icofont icofont-university"></i>10 Years</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 default-grid-item">
                    <div class="card gallery-desc">
                        <div class="masonry-media">
                            <a class="media-middle" href="#!">
                                <img class="img-fluid" src="{{ asset("adminity/images/gallery-grid/masonry-3.jpg") }}"
                                     alt="masonary">
                            </a>
                        </div>
                        <div class="card-block">
                            <h6 class="job-card-desc">Job Description</h6>
                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                            <div class="job-meta-data"><i class="icofont icofont-safety"></i>washington</div>
                            <div class="job-meta-data"><i class="icofont icofont-university"></i>10 Years</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 default-grid-item">
                    <div class="card gallery-desc">
                        <div class="masonry-media">
                            <a class="media-middle" href="#!">
                                <img class="img-fluid" src="{{ asset("adminity/images/gallery-grid/masonry-4.jpg") }}"
                                     alt="masonary">
                            </a>
                        </div>
                        <div class="card-block">
                            <h6 class="job-card-desc">Job Description</h6>
                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                            <div class="job-meta-data"><i class="icofont icofont-safety"></i>washington</div>
                            <div class="job-meta-data"><i class="icofont icofont-university"></i>10 Years</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 default-grid-item">
                    <div class="card gallery-desc">
                        <div class="masonry-media">
                            <a class="media-middle" href="#!">
                                <img class="img-fluid" src="{{ asset("adminity/images/gallery-grid/masonry-5.jpg") }}"
                                     alt="masonary">
                            </a>
                        </div>
                        <div class="card-block">
                            <h6 class="job-card-desc">Job Description</h6>
                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                            <div class="job-meta-data"><i class="icofont icofont-safety"></i>washington</div>
                            <div class="job-meta-data"><i class="icofont icofont-university"></i>10 Years</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 default-grid-item">
                    <div class="card gallery-desc">
                        <div class="masonry-media">
                            <a class="media-middle" href="#!">
                                <img class="img-fluid" src="{{ asset("adminity/images/gallery-grid/masonry-6.jpg") }}"
                                     alt="masonary">
                            </a>
                        </div>
                        <div class="card-block">
                            <h6 class="job-card-desc">Job Description</h6>
                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                            <div class="job-meta-data"><i class="icofont icofont-safety"></i>washington</div>
                            <div class="job-meta-data"><i class="icofont icofont-university"></i>10 Years</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 default-grid-item">
                    <div class="card gallery-desc">
                        <div class="masonry-media">
                            <a class="media-middle" href="#!">
                                <img class="img-fluid" src="{{ asset("adminity/images/gallery-grid/masonry-7.jpg") }}"
                                     alt="masonary">
                            </a>
                        </div>
                        <div class="card-block">
                            <h6 class="job-card-desc">Job Description</h6>
                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                            <div class="job-meta-data"><i class="icofont icofont-safety"></i>washington</div>
                            <div class="job-meta-data"><i class="icofont icofont-university"></i>10 Years</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 default-grid-item">
                    <div class="card gallery-desc">
                        <div class="masonry-media">
                            <a class="media-middle" href="#!">
                                <img class="img-fluid" src="{{ asset("adminity/images/gallery-grid/masonry-8.jpg") }}"
                                     alt="masonary">
                            </a>
                        </div>
                        <div class="card-block">
                            <h6 class="job-card-desc">Job Description</h6>
                            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                            <div class="job-meta-data"><i class="icofont icofont-safety"></i>washington</div>
                            <div class="job-meta-data"><i class="icofont icofont-university"></i>10 Years</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/pages/isotope/jquery.isotope.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/isotope/isotope.pkgd.min.js") }}"
            type="text/javascript"></script>
    <script type="text/javascript">
        $(window).on('load', function () {
            var $container = $('.filter-container');
            $container.isotope({
                filter: '*',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            var $grid = $('.default-grid').isotope({
                itemSelector: '.default-grid-item',
                masonry: {}
            });
        });
    </script>
@endsection

