@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/hover-effect/normalize.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/hover-effect/set2.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Gallery advance</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Gallery</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Gallery</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body gallery-page">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Gallery Advance</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-julia">
                                        <img src="{{ asset("adminity/images/light-box/l1.jpg") }}" alt="img21"
                                             class="img-fluid"/>
                                        <figcaption>
                                            <h2>Passionate <span>Julia</span></h2>
                                            <div>
                                                <p>Julia dances in the deep dark</p>
                                                <p>She loves the smell of the ocean</p>
                                                <p>And dives into the morning light</p>
                                            </div>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-julia">
                                        <img src="{{ asset("adminity/images/light-box/l2.jpg") }}" alt="img21"
                                             class="img-fluid"/>
                                        <figcaption>
                                            <h2>Passionate <span>Julia</span></h2>
                                            <div>
                                                <p>Julia dances in the deep dark</p>
                                                <p>She loves the smell of the ocean</p>
                                                <p>And dives into the morning light</p>
                                            </div>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-goliath">
                                        <img src="{{ asset("adminity/images/light-box/l3.jpg") }}" alt="img23"/>
                                        <figcaption>
                                            <h2>Thoughtful <span>Goliath</span></h2>
                                            <p>When Goliath comes out, you should run.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-goliath">
                                        <img src="{{ asset("adminity/images/light-box/l4.jpg") }}" alt="img24"/>
                                        <figcaption>
                                            <h2>Thoughtful <span>Goliath</span></h2>
                                            <p>When Goliath comes out, you should run.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-hera">
                                        <img src="{{ asset("adminity/images/light-box/l5.jpg") }}" alt="img17"/>
                                        <figcaption>
                                            <h2>Tender <span>Hera</span></h2>
                                            <p>
                                                <a href="#"><i class="fa fa-fw fa-file-pdf-o"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-file-image-o"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-file-archive-o"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-hera">
                                        <img src="{{ asset("adminity/images/light-box/l6.jpg") }}" alt="img25"/>
                                        <figcaption>
                                            <h2>Tender <span>Hera</span></h2>
                                            <p>
                                                <a href="#"><i class="icofont icofont-file-pdf"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-file-image-o"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-file-archive-o"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-winston">
                                        <img src="{{ asset("adminity/images/light-box/l1.jpg") }}" alt="img30"/>
                                        <figcaption>
                                            <h2>Jolly <span>Winston</span></h2>
                                            <p>
                                                <a href="#"><i class="fa fa-fw fa-star-o"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-comments-o"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-envelope-o"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-winston">
                                        <img src="{{ asset("adminity/images/light-box/l2.jpg") }}" alt="img01"/>
                                        <figcaption>
                                            <h2>Jolly <span>Winston</span></h2>
                                            <p>
                                                <a href="#"><i class="fa fa-fw fa-star-o"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-comments-o"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-envelope-o"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-selena">
                                        <img src="{{ asset("adminity/images/light-box/l3.jpg") }}" alt="img10"/>
                                        <figcaption>
                                            <h2>Happy <span>Selena</span></h2>
                                            <p>Selena is a tiny-winged bird.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-selena">
                                        <img src="{{ asset("adminity/images/light-box/l4.jpg") }}" alt="img31"/>
                                        <figcaption>
                                            <h2>Happy <span>Selena</span></h2>
                                            <p>Selena is a tiny-winged bird.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-terry">
                                        <img src="{{ asset("adminity/images/light-box/l5.jpg") }}" alt="img16"/>
                                        <figcaption>
                                            <h2>Noisy <span>Terry</span></h2>
                                            <p>
                                                <a href="#"><i class="fa fa-fw fa-download"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-heart"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-share"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-tags"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-terry">
                                        <img src="{{ asset("adminity/images/light-box/l6.jpg") }}" alt="img26"/>
                                        <figcaption>
                                            <h2>Noisy <span>Terry</span></h2>
                                            <p>
                                                <a href="#"><i class="fa fa-fw fa-download"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-heart"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-share"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-tags"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-phoebe">
                                        <img src="{{ asset("adminity/images/light-box/l1.jpg") }}" alt="img03"/>
                                        <figcaption>
                                            <h2>Plain <span>Phoebe</span></h2>
                                            <p>
                                                <a href="#"><i class="fa fa-fw fa-user"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-heart"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-cog"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-phoebe">
                                        <img src="{{ asset("adminity/images/light-box/l2.jpg") }}" alt="img07"/>
                                        <figcaption>
                                            <h2>Plain <span>Phoebe</span></h2>
                                            <p>
                                                <a href="#"><i class="fa fa-fw fa-user"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-heart"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-cog"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-apollo">
                                        <img src="{{ asset("adminity/images/light-box/l3.jpg") }}" alt="img18"/>
                                        <figcaption>
                                            <h2>Strong <span>Apollo</span></h2>
                                            <p>Apollo's last game of pool was so strange.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-apollo">
                                        <img src="{{ asset("adminity/images/light-box/l4.jpg") }}" alt="img22"/>
                                        <figcaption>
                                            <h2>Strong <span>Apollo</span></h2>
                                            <p>Apollo's last game of pool was so strange.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-kira">
                                        <img src="{{ asset("adminity/images/light-box/l5.jpg") }}" alt="img17"/>
                                        <figcaption>
                                            <h2>Dark <span>Kira</span></h2>
                                            <p>
                                                <a href="#"><i class="fa fa-fw fa-home"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-download"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-heart"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-share"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-kira">
                                        <img src="{{ asset("adminity/images/light-box/l6.jpg") }}" alt="img05"/>
                                        <figcaption>
                                            <h2>Dark <span>Kira</span></h2>
                                            <p>
                                                <a href="#"><i class="fa fa-fw fa-home"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-download"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-heart"></i></a>
                                                <a href="#"><i class="fa fa-fw fa-share"></i></a>
                                            </p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-steve">
                                        <img src="{{ asset("adminity/images/light-box/l1.jpg") }}" alt="img29"/>
                                        <figcaption>
                                            <h2>Lonely <span>Steve</span></h2>
                                            <p>Steve was afraid of the Boogieman.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-steve">
                                        <img src="{{ asset("adminity/images/light-box/l2.jpg") }}" alt="img33"/>
                                        <figcaption>
                                            <h2>Lonely <span>Steve</span></h2>
                                            <p>Steve was afraid of the Boogieman.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-moses">
                                        <img src="{{ asset("adminity/images/light-box/l3.jpg") }}" alt="img24"/>
                                        <figcaption>
                                            <h2>Cute <span>Moses</span></h2>
                                            <p>Moses loves to run after butterflies.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-moses">
                                        <img src="{{ asset("adminity/images/light-box/l4.jpg") }}" alt="img20"/>
                                        <figcaption>
                                            <h2>Cute <span>Moses</span></h2>
                                            <p>Moses loves to run after butterflies.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-jazz">
                                        <img src="{{ asset("adminity/images/light-box/l5.jpg") }}" alt="img25"/>
                                        <figcaption>
                                            <h2>Dynamic <span>Jazz</span></h2>
                                            <p>When Jazz starts to chase cars, the whole world stands still.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-jazz">
                                        <img src="{{ asset("adminity/images/light-box/l6jpg") }}" alt="img06"/>
                                        <figcaption>
                                            <h2>Dynamic <span>Jazz</span></h2>
                                            <p>When Jazz starts to chase cars, the whole world stands still.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-ming">
                                        <img src="{{ asset("adminity/images/light-box/l1.jpg") }}" alt="img09"/>
                                        <figcaption>
                                            <h2>Funny <span>Ming</span></h2>
                                            <p>Ming sits in the corner the whole day. She's into crochet.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-ming">
                                        <img src="{{ asset("adminity/images/light-box/l2.jpg") }}" alt="img08"/>
                                        <figcaption>
                                            <h2>Funny <span>Ming</span></h2>
                                            <p>Ming sits in the corner the whole day. She's into crochet.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-lexi">
                                        <img src="{{ asset("adminity/images/light-box/l3.jpg") }}" alt="img12"/>
                                        <figcaption>
                                            <h2>Altruistic <span>Lexi</span></h2>
                                            <p>Each and every friend is special. Lexi won't hide a single cookie.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-lexi">
                                        <img src="{{ asset("adminity/images/light-box/l4.jpg") }}" alt="img03"/>
                                        <figcaption>
                                            <h2>Altruistic <span>Lexi</span></h2>
                                            <p>Each and every friend is special. Lexi won't hide a single cookie.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="grid">
                                    <figure class="effect-duke">
                                        <img src="{{ asset("adminity/images/light-box/l5.jpg") }}" alt="img27"/>
                                        <figcaption>
                                            <h2>Messy <span>Duke</span></h2>
                                            <p>Duke is very bored. When he looks at the sky, he feels to run.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-duke">
                                        <img src="{{ asset("adminity/images/light-box/l6.jpg") }}" alt="img17"/>
                                        <figcaption>
                                            <h2>Messy <span>Duke</span></h2>
                                            <p>Duke is very bored. When he looks at the sky, he feels to run.</p>
                                            <a href="#">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

