<div class="pcoded-navigatio-lavel">Navigation</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu {{ !Route::is("dashboard*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
            <span class="pcoded-mtext">Dashboard</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("dashboard") ?: "active" }}">
                <a href="{{ route("dashboard") }}">
                    <span class="pcoded-mtext">Default</span>
                </a>
            </li>
            <li class="{{ !Route::is("dashboard.crm") ?: "active" }}">
                <a href="{{ route("dashboard.crm") }}">
                    <span class="pcoded-mtext">CRM</span>
                </a>
            </li>
            <li class="{{ !Route::is("dashboard.analytics") ?: "active" }}">
                <a href="{{ route("dashboard.analytics") }}">
                    <span class="pcoded-mtext">Analytics</span>
                    <span class="pcoded-badge label label-info ">NEW</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("layouts.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-sidebar"></i></span>
            <span class="pcoded-mtext">Page layouts</span>
            <span class="pcoded-badge label label-warning">NEW</span>
        </a>
        <ul class="pcoded-submenu">
            <li class=" pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-mtext">Vertical</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="{{ !Route::is("layouts.vertical.static") ?: "active" }}">
                        <a href="{{ route("layouts.vertical.static") }}">
                            <span class="pcoded-mtext">Static Layout</span>
                        </a>
                    </li>
                    <li class="{{ !Route::is("layouts.vertical.header.fixed") ?: "active" }}">
                        <a href="{{ route("layouts.vertical.header.fixed") }}">
                            <span class="pcoded-mtext">Header Fixed</span>
                        </a>
                    </li>
                    <li class="{{ !Route::is("layouts.vertical.compact") ?: "active" }}">
                        <a href="{{ route("layouts.vertical.compact") }}">
                            <span class="pcoded-mtext">Compact</span>
                        </a>
                    </li>
                    <li class="{{ !Route::is("layouts.vertical.sidebar.fixed") ?: "active" }}">
                        <a href="{{ route("layouts.vertical.sidebar.fixed") }}">
                            <span class="pcoded-mtext">Sidebar Fixed</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class=" pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-mtext">Horizontal</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="{{ !Route::is("layouts.horizontal.static") ?: "active" }}">
                        <a href="{{ route("layouts.horizontal.static") }}">
                            <span class="pcoded-mtext">Static Layout</span>
                        </a>
                    </li>
                    <li class="{{ !Route::is("layouts.horizontal.fixed") ?: "active" }}">
                        <a href="{{ route("layouts.horizontal.fixed") }}">
                            <span class="pcoded-mtext">Fixed layout</span>
                        </a>
                    </li>
                    <li class="{{ !Route::is("layouts.horizontal.icon") ?: "active" }}">
                        <a href="{{ route("layouts.horizontal.icon") }}">
                            <span class="pcoded-mtext">Static With Icon</span>
                        </a>
                    </li>
                    <li class="{{ !Route::is("layouts.horizontal.icon.fixed") ?: "active" }}">
                        <a href="{{ route("layouts.horizontal.icon.fixed") }}">
                            <span class="pcoded-mtext">Fixed With Icon</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ !Route::is("layouts.bottom") ?: "active" }}">
                <a href="{{ route("layouts.bottom") }}">
                    <span class="pcoded-mtext">Bottom Menu</span>
                </a>
            </li>
            <li class="{{ !Route::is("layouts.box") ?: "active" }}">
                <a href="{{ route("layouts.box") }}">
                    <span class="pcoded-mtext">Box Layout</span>
                </a>
            </li>
            <li class="{{ !Route::is("layouts.rtl") ?: "active" }}">
                <a href="{{ route("layouts.rtl") }}">
                    <span class="pcoded-mtext">RTL</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ !Route::is("nav.light") ?: "active" }}">
        <a href="{{ route("nav.light") }}">
            <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
            <span class="pcoded-mtext">Navigation</span>
        </a>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("widgets.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-layers"></i></span>
            <span class="pcoded-mtext">Widget</span>
            <span class="pcoded-badge label label-danger">100+</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("widgets.statistic") ?: "active" }}">
                <a href="{{ route("widgets.statistic") }}">
                    <span class="pcoded-mtext">Statistic</span>
                </a>
            </li>
            <li class="{{ !Route::is("widgets.data") ?: "active" }}">
                <a href="{{ route("widgets.data") }}">
                    <span class="pcoded-mtext">Data</span>
                </a>
            </li>
            <li class="{{ !Route::is("widgets.chart") ?: "active" }}">
                <a href="{{ route("widgets.chart") }}">
                    <span class="pcoded-mtext">Chart Widget</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
<div class="pcoded-navigatio-lavel">UI Element</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu {{ !Route::is("ui-elements.basic-components.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-box"></i></span>
            <span class="pcoded-mtext">Basic Components</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("ui-elements.basic-components.alert") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.alert") }}">
                    <span class="pcoded-mtext">Alert</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.breadcrumb") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.breadcrumb") }}">
                    <span class="pcoded-mtext">Breadcrumbs</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.button") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.button") }}">
                    <span class="pcoded-mtext">Button</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.box-shadow") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.box-shadow") }}">
                    <span class="pcoded-mtext">Box-Shadow</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.accordion") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.accordion") }}">
                    <span class="pcoded-mtext">Accordion</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.generic-class") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.generic-class") }}">
                    <span class="pcoded-mtext">Generic Class</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.tabs") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.tabs") }}">
                    <span class="pcoded-mtext">Tabs</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.color") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.color") }}">
                    <span class="pcoded-mtext">Color</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.label-badge") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.label-badge") }}">
                    <span class="pcoded-mtext">Label Badge</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.progress-bar") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.progress-bar") }}">
                    <span class="pcoded-mtext">Progress Bar</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.preloader") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.preloader") }}">
                    <span class="pcoded-mtext">Pre-Loader</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.list") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.list") }}">
                    <span class="pcoded-mtext">List</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.tooltip") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.tooltip") }}">
                    <span class="pcoded-mtext">Tooltip And Popover</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.typography") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.typography") }}">
                    <span class="pcoded-mtext">Typography</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.basic-components.other") ?: "active" }}">
                <a href="{{ route("ui-elements.basic-components.other") }}">
                    <span class="pcoded-mtext">Other</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("ui-elements.advanced-components.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
            <span class="pcoded-mtext">Advance Components</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("ui-elements.advanced-components.draggable") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.draggable") }}">
                    <span class="pcoded-mtext">Draggable</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.bs-grid") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.bs-grid") }}">
                    <span class="pcoded-mtext">Grid Stack</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.light-box") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.light-box") }}">
                    <span class="pcoded-mtext">Light Box</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.modal") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.modal") }}">
                    <span class="pcoded-mtext">Modal</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.notification") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.notification") }}">
                    <span class="pcoded-mtext">Notifications</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.notify") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.notify") }}">
                    <span class="pcoded-mtext">PNOTIFY</span>
                    <span class="pcoded-badge label label-info">NEW</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.rating") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.rating") }}">
                    <span class="pcoded-mtext">Rating</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.range-slider") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.range-slider") }}">
                    <span class="pcoded-mtext">Range Slider</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.slider") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.slider") }}">
                    <span class="pcoded-mtext">Slider</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.syntax-highlighter") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.syntax-highlighter") }}">
                    <span class="pcoded-mtext">Syntax Highlighter</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.tour") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.tour") }}">
                    <span class="pcoded-mtext">Tour</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.treeview") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.treeview") }}">
                    <span class="pcoded-mtext">Tree View</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.nestable") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.nestable") }}">
                    <span class="pcoded-mtext">Nestable</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.toolbar") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.toolbar") }}">
                    <span class="pcoded-mtext">Toolbar</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.advanced-components.x-editable") ?: "active" }}">
                <a href="{{ route("ui-elements.advanced-components.x-editable") }}">
                    <span class="pcoded-mtext">X-Editable</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("ui-elements.extra-components.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-package"></i></span>
            <span class="pcoded-mtext">Extra Components</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("ui-elements.extra-components.session-timeout") ?: "active" }}">
                <a href="{{ route("ui-elements.extra-components.session-timeout") }}">
                    <span class="pcoded-mtext">Session Timeout</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.extra-components.session-idle-timeout") ?: "active" }}">
                <a href="{{ route("ui-elements.extra-components.session-idle-timeout") }}">
                    <span class="pcoded-mtext">Session Idle Timeout</span>
                </a>
            </li>
            <li class="{{ !Route::is("ui-elements.extra-components.offline") ?: "active" }}">
                <a href="{{ route("ui-elements.extra-components.offline") }}">
                    <span class="pcoded-mtext">Offline</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ !Route::is("animation") ?: "active" }}">
        <a href="{{ route("animation") }}">
                                    <span class="pcoded-micon"><i
                                            class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
            <span class="pcoded-mtext">Animations</span>
        </a>
    </li>
    <li class="{{ !Route::is("sticky") ?: "active" }}">
        <a href="{{ route("sticky") }}">
            <span class="pcoded-micon"><i class="feather icon-cpu"></i></span>
            <span class="pcoded-mtext">Sticky Notes</span>
            <span class="pcoded-badge label label-danger">HOT</span>
        </a>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("icons.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-command"></i></span>
            <span class="pcoded-mtext">Icons</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("icons.icon-font-awesome") ?: "active" }}">
                <a href="{{ route("icons.icon-font-awesome") }}">
                    <span class="pcoded-mtext">Font Awesome</span>
                </a>
            </li>
            <li class="{{ !Route::is("icons.icon-themify") ?: "active" }}">
                <a href="{{ route("icons.icon-themify") }}">
                    <span class="pcoded-mtext">Themify</span>
                </a>
            </li>
            <li class="{{ !Route::is("icons.icon-simple-line") ?: "active" }}">
                <a href="{{ route("icons.icon-simple-line") }}">
                    <span class="pcoded-mtext">Simple Line Icon</span>
                </a>
            </li>
            <li class="{{ !Route::is("icons.icon-ion") ?: "active" }}">
                <a href="{{ route("icons.icon-ion") }}">
                    <span class="pcoded-mtext">Ion Icon</span>
                </a>
            </li>
            <li class="{{ !Route::is("icons.icon-material-design") ?: "active" }}">
                <a href="{{ route("icons.icon-material-design") }}">
                    <span class="pcoded-mtext">Material Design</span>
                </a>
            </li>
            <li class="{{ !Route::is("icons.icon-icofonts") ?: "active" }}">
                <a href="{{ route("icons.icon-icofonts") }}">
                    <span class="pcoded-mtext">Ico Fonts</span>
                </a>
            </li>
            <li class="{{ !Route::is("icons.icon-weather") ?: "active" }}">
                <a href="{{ route("icons.icon-weather") }}">
                    <span class="pcoded-mtext">Weather Icon</span>
                </a>
            </li>
            <li class="{{ !Route::is("icons.icon-typicons") ?: "active" }}">
                <a href="{{ route("icons.icon-typicons") }}">
                    <span class="pcoded-mtext">Typicons</span>
                </a>
            </li>
            <li class="{{ !Route::is("icons.icon-flags") ?: "active" }}">
                <a href="{{ route("icons.icon-flags") }}">
                    <span class="pcoded-mtext">Flags</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
<div class="pcoded-navigatio-lavel">Forms</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu {{ !Route::is("forms.form-components.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
            <span class="pcoded-mtext">Form Components</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("forms.form-components.form-elements-component") ?: "active" }}">
                <a href="{{ route("forms.form-components.form-elements-component") }}">
                    <span class="pcoded-mtext">Form Components</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.form-components.form-elements-add-on") ?: "active" }}">
                <a href="{{ route("forms.form-components.form-elements-add-on") }}">
                    <span class="pcoded-mtext">Form-Elements-Add-On</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.form-components.form-elements-advance") ?: "active" }}">
                <a href="{{ route("forms.form-components.form-elements-advance") }}">
                    <span class="pcoded-mtext">Form-Elements-Advance</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.form-components.form-validation") ?: "active" }}">
                <a href="{{ route("forms.form-components.form-validation") }}">
                    <span class="pcoded-mtext">Form Validation</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ !Route::is("forms.form-picker") ?: "active" }}">
        <a href="{{ route("forms.form-picker") }}">
            <span class="pcoded-micon"><i class="feather icon-edit-1"></i></span>
            <span class="pcoded-mtext">Form Picker</span>
            <span class="pcoded-badge label label-warning">NEW</span>
        </a>
    </li>
    <li class="{{ !Route::is("forms.form-select") ?: "active" }}">
        <a href="{{ route("forms.form-select") }}">
            <span class="pcoded-micon"><i class="feather icon-feather"></i></span>
            <span class="pcoded-mtext">Form Select</span>
        </a>
    </li>
    <li class="{{ !Route::is("forms.form-masking") ?: "active" }}">
        <a href="{{ route("forms.form-masking") }}">
            <span class="pcoded-micon"><i class="feather icon-shield"></i></span>
            <span class="pcoded-mtext">Form Masking</span>
        </a>
    </li>
    <li class="{{ !Route::is("forms.form-wizard") ?: "active" }}">
        <a href="{{ route("forms.form-wizard") }}">
            <span class="pcoded-micon"><i class="feather icon-tv"></i></span>
            <span class="pcoded-mtext">Form Wizard</span>
        </a>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("forms.ready-to-use.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-book"></i></span>
            <span class="pcoded-mtext">Ready To Use</span>
            <span class="pcoded-badge label label-danger">HOT</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("forms.ready-to-use.ready-cloned-elements-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-cloned-elements-form") }}">
                    <span class="pcoded-mtext">Cloned Elements Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-currency-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-currency-form") }}">
                    <span class="pcoded-mtext">Currency Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-form-booking") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-form-booking") }}">
                    <span class="pcoded-mtext">Booking Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-form-booking-multi-steps") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-form-booking-multi-steps") }}">
                    <span class="pcoded-mtext">Booking Multi Steps Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-form-comment") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-form-comment") }}">
                    <span class="pcoded-mtext">Comment Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-form-contact") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-form-contact") }}">
                    <span class="pcoded-mtext">Contact Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-job-application-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-job-application-form") }}">
                    <span class="pcoded-mtext">Job Application Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-js-addition-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-js-addition-form") }}">
                    <span class="pcoded-mtext">JS Addition Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-login-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-login-form") }}">
                    <span class="pcoded-mtext">Login Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-popup-modal-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-popup-modal-form") }}">
                    <span class="pcoded-mtext">Popup Modal Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-registration-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-registration-form") }}">
                    <span class="pcoded-mtext">Registration Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-review-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-review-form") }}">
                    <span class="pcoded-mtext">Review Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-subscribe-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-subscribe-form") }}">
                    <span class="pcoded-mtext">Subscribe Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-suggestion-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-suggestion-form") }}">
                    <span class="pcoded-mtext">Suggestion Form</span>
                </a>
            </li>
            <li class="{{ !Route::is("forms.ready-to-use.ready-tabs-form") ?: "active" }}">
                <a href="{{ route("forms.ready-to-use.ready-tabs-form") }}">
                    <span class="pcoded-mtext">Tabs Form</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
<div class="pcoded-navigatio-lavel">Tables</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu {{ !Route::is("tables.bootstrap-tables.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-credit-card"></i></span>
            <span class="pcoded-mtext">Bootstrap Table</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("tables.bootstrap-tables.bs-basic") ?: "active" }}">
                <a href="{{ route("tables.bootstrap-tables.bs-basic") }}">
                    <span class="pcoded-mtext">Basic Table</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.bootstrap-tables.bs-table-sizing") ?: "active" }}">
                <a href="{{ route("tables.bootstrap-tables.bs-table-sizing") }}">
                    <span class="pcoded-mtext">Sizing Table</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.bootstrap-tables.bs-table-border") ?: "active" }}">
                <a href="{{ route("tables.bootstrap-tables.bs-table-border") }}">
                    <span class="pcoded-mtext">Border Table</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.bootstrap-tables.bs-table-styling") ?: "active" }}">
                <a href="{{ route("tables.bootstrap-tables.bs-table-styling") }}">
                    <span class="pcoded-mtext">Styling Table</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("tables.data-tables.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-inbox"></i></span>
            <span class="pcoded-mtext">Data Table</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("tables.data-tables.bs-basic") ?: "active" }}">
                <a href="{{ route("tables.data-tables.bs-basic") }}">
                    <span class="pcoded-mtext">Basic Initialization</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-tables.dt-advance") ?: "active" }}">
                <a href="{{ route("tables.data-tables.dt-advance") }}">
                    <span class="pcoded-mtext">Advance Initialization</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-tables.dt-styling") ?: "active" }}">
                <a href="{{ route("tables.data-tables.dt-styling") }}">
                    <span class="pcoded-mtext">Styling</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-tables.dt-api") ?: "active" }}">
                <a href="{{ route("tables.data-tables.dt-api") }}">
                    <span class="pcoded-mtext">API</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-tables.dt-ajax") ?: "active" }}">
                <a href="{{ route("tables.data-tables.dt-ajax") }}">
                    <span class="pcoded-mtext">Ajax</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-tables.dt-server-side") ?: "active" }}">
                <a href="{{ route("tables.data-tables.dt-server-side") }}">
                    <span class="pcoded-mtext">Server Side</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-tables.dt-plugin") ?: "active" }}">
                <a href="{{ route("tables.data-tables.dt-plugin") }}">
                    <span class="pcoded-mtext">Plug-In</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-tables.dt-data-sources") ?: "active" }}">
                <a href="{{ route("tables.data-tables.dt-data-sources") }}">
                    <span class="pcoded-mtext">Data Sources</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("tables.data-table-extensions.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-server"></i></span>
            <span class="pcoded-mtext">Data Table Extensions</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-autofill") ?: "active" }}">
                <a href="{{ route("tables.data-table-extensions.dt-ext-autofill") }}">
                    <span class="pcoded-mtext">AutoFill</span>
                </a>
            </li>
            <li class="pcoded-hasmenu {{ !Route::is("tables.data-table-extensions.dt-ext-basic-buttons") || !Route::is("tables.data-table-extensions.dt-ext-buttons-html-5-data-export") ?: "active pcoded-trigger" }}">
                <a href="javascript:void(0)">
                    <span class="pcoded-mtext">Button</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-basic-buttons") ?: "active" }}">
                        <a href="{{ route("tables.data-table-extensions.dt-ext-basic-buttons") }}">
                            <span class="pcoded-mtext">Basic Button</span>
                        </a>
                    </li>
                    <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-buttons-html-5-data-export") ?: "active" }}">
                        <a href="{{ route("tables.data-table-extensions.dt-ext-buttons-html-5-data-export") }}">
                            <span class="pcoded-mtext">Html-5 Data Export</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-col-reorder") ?: "active" }}">
                <a href="{{ route("tables.data-table-extensions.dt-ext-col-reorder") }}">
                    <span class="pcoded-mtext">Col Reorder</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-fixed-columns") ?: "active" }}">
                <a href="{{ route("tables.data-table-extensions.dt-ext-fixed-columns") }}">
                    <span class="pcoded-mtext">Fixed Columns</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-fixed-header") ?: "active" }}">
                <a href="{{ route("tables.data-table-extensions.dt-ext-fixed-header") }}">
                    <span class="pcoded-mtext">Fixed Header</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-key-table") ?: "active" }}">
                <a href="{{ route("tables.data-table-extensions.dt-ext-key-table") }}">
                    <span class="pcoded-mtext">Key Table</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-responsive") ?: "active" }}">
                <a href="{{ route("tables.data-table-extensions.dt-ext-responsive") }}">
                    <span class="pcoded-mtext">Responsive</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-row-reorder") ?: "active" }}">
                <a href="{{ route("tables.data-table-extensions.dt-ext-row-reorder") }}">
                    <span class="pcoded-mtext">Row Reorder</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-scroller") ?: "active" }}">
                <a href="{{ route("tables.data-table-extensions.dt-ext-scroller") }}">
                    <span class="pcoded-mtext">Scroller</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.data-table-extensions.dt-ext-select") ?: "active" }}">
                <a href="{{ route("tables.data-table-extensions.dt-ext-select") }}">
                    <span class="pcoded-mtext">Select Table</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ !Route::is("tables.foo-table") ?: "active" }}">
        <a href="{{ route("tables.foo-table") }}">
            <span class="pcoded-micon"><i class="feather icon-hash"></i></span>
            <span class="pcoded-mtext">FooTable</span>
        </a>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("tables.handson-tables.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-airplay"></i></span>
            <span class="pcoded-mtext">Handson Table</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("tables.handson-tables.handson-appearance") ?: "active" }}">
                <a href="{{ route("tables.handson-tables.handson-appearance") }}">
                    <span class="pcoded-mtext">Appearance</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.handson-tables.handson-data-operation") ?: "active" }}">
                <a href="{{ route("tables.handson-tables.handson-data-operation") }}">
                    <span class="pcoded-mtext">Data Operation</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.handson-tables.handson-rows-cols") ?: "active" }}">
                <a href="{{ route("tables.handson-tables.handson-rows-cols") }}">
                    <span class="pcoded-mtext">Rows Columns</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.handson-tables.handson-columns-only") ?: "active" }}">
                <a href="{{ route("tables.handson-tables.handson-columns-only") }}">
                    <span class="pcoded-mtext">Columns Only</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.handson-tables.handson-cell-features") ?: "active" }}">
                <a href="{{ route("tables.handson-tables.handson-cell-features") }}">
                    <span class="pcoded-mtext">Cell Features</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.handson-tables.handson-cell-types") ?: "active" }}">
                <a href="{{ route("tables.handson-tables.handson-cell-types") }}">
                    <span class="pcoded-mtext">Cell Types</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.handson-tables.handson-integrations") ?: "active" }}">
                <a href="{{ route("tables.handson-tables.handson-integrations") }}">
                    <span class="pcoded-mtext">Integrations</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.handson-tables.handson-rows-only") ?: "active" }}">
                <a href="{{ route("tables.handson-tables.handson-rows-only") }}">
                    <span class="pcoded-mtext">Rows Only</span>
                </a>
            </li>
            <li class="{{ !Route::is("tables.handson-tables.handson-utilities") ?: "active" }}">
                <a href="{{ route("tables.handson-tables.handson-utilities") }}">
                    <span class="pcoded-mtext">Utilities</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ !Route::is("tables.editable-table") ?: "active" }}">
        <a href="{{ route("tables.editable-table") }}">
            <span class="pcoded-micon"><i class="feather icon-edit"></i></span>
            <span class="pcoded-mtext">Editable Table</span>
        </a>
    </li>
</ul>
<div class="pcoded-navigatio-lavel">Chart And Maps</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu {{ !Route::is("charts.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-pie-chart"></i></span>
            <span class="pcoded-mtext">Charts</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("charts.chart-google") ?: "active" }}">
                <a href="{{ route("charts.chart-google") }}">
                    <span class="pcoded-mtext">Google Chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-echart") ?: "active" }}">
                <a href="{{ route("charts.chart-echart") }}">
                    <span class="pcoded-mtext">Echarts</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-chartjs") ?: "active" }}">
                <a href="{{ route("charts.chart-chartjs") }}">
                    <span class="pcoded-mtext">ChartJs</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-list") ?: "active" }}">
                <a href="{{ route("charts.chart-list") }}">
                    <span class="pcoded-mtext">List Chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-float") ?: "active" }}">
                <a href="{{ route("charts.chart-float") }}">
                    <span class="pcoded-mtext">Float Chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-knob") ?: "active" }}">
                <a href="{{ route("charts.chart-knob") }}">
                    <span class="pcoded-mtext">Knob chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-morris") ?: "active" }}">
                <a href="{{ route("charts.chart-morris") }}">
                    <span class="pcoded-mtext">Morris Chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-nvd3") ?: "active" }}">
                <a href="{{ route("charts.chart-nvd3") }}">
                    <span class="pcoded-mtext">Nvd3 Chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-peity") ?: "active" }}">
                <a href="{{ route("charts.chart-peity") }}">
                    <span class="pcoded-mtext">Peity Chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-radial") ?: "active" }}">
                <a href="{{ route("charts.chart-radial") }}">
                    <span class="pcoded-mtext">Radial Chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-rickshaw") ?: "active" }}">
                <a href="{{ route("charts.chart-rickshaw") }}">
                    <span class="pcoded-mtext">Rickshaw Chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-sparkline") ?: "active" }}">
                <a href="{{ route("charts.chart-sparkline") }}">
                    <span class="pcoded-mtext">Sparkline Chart</span>
                </a>
            </li>
            <li class="{{ !Route::is("charts.chart-c3") ?: "active" }}">
                <a href="{{ route("charts.chart-c3") }}">
                    <span class="pcoded-mtext">C3 Chart</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("maps.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-map"></i></span>
            <span class="pcoded-mtext">Maps</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("maps.map-google") ?: "active" }}">
                <a href="{{ route("maps.map-google") }}">
                    <span class="pcoded-mtext">Google Maps</span>
                </a>
            </li>
            <li class="{{ !Route::is("maps.map-vector") ?: "active" }}">
                <a href="{{ route("maps.map-vector") }}">
                    <span class="pcoded-mtext">Vector Maps</span>
                </a>
            </li>
            <li class="{{ !Route::is("maps.map-api") ?: "active" }}">
                <a href="{{ route("maps.map-api") }}">
                    <span class="pcoded-mtext">Google Map Search API</span>
                </a>
            </li>
            <li class="{{ !Route::is("maps.location") ?: "active" }}">
                <a href="{{ route("maps.location") }}">
                    <span class="pcoded-mtext">Location</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="">
        <a href="{{ route("welcome") }}"
           target="_blank">
            <span class="pcoded-micon"><i class="feather icon-navigation-2"></i></span>
            <span class="pcoded-mtext">Landing Page</span>
        </a>
    </li>
</ul>
<div class="pcoded-navigatio-lavel">Pages</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu ">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-unlock"></i></span>
            <span class="pcoded-mtext">Authentication</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="">
                <a href="auth-normal-sign-in.html" target="_blank">
                    <span class="pcoded-mtext">Login With BG Image</span>
                </a>
            </li>
            <li class="">
                <a href="auth-sign-in-social.html" target="_blank">
                    <span class="pcoded-mtext">Login With Social Icon</span>
                </a>
            </li>
            <li class="">
                <a href="auth-sign-in-social-header-footer.html" target="_blank">
                    <span class="pcoded-mtext">Login Social With Header And Footer</span>
                </a>
            </li>
            <li class="">
                <a href="auth-normal-sign-in-header-footer.html" target="_blank">
                    <span class="pcoded-mtext">Login With Header And Footer</span>
                </a>
            </li>
            <li class="">
                <a href="auth-sign-up.html" target="_blank">
                    <span class="pcoded-mtext">Registration BG Image</span>
                </a>
            </li>
            <li class="">
                <a href="auth-sign-up-social.html" target="_blank">
                    <span class="pcoded-mtext">Registration Social Icon</span>
                </a>
            </li>
            <li class="">
                <a href="auth-sign-up-social-header-footer.html" target="_blank">
                    <span class="pcoded-mtext">Registration Social With Header And Footer</span>
                </a>
            </li>
            <li class="">
                <a href="auth-sign-up-header-footer.html" target="_blank">
                    <span class="pcoded-mtext">Registration With Header And Footer</span>
                </a>
            </li>
            <li class="">
                <a href="auth-multi-step-sign-up.html" target="_blank">
                    <span class="pcoded-mtext">Multi Step Registration</span>
                </a>
            </li>
            <li class="">
                <a href="auth-reset-password.html" target="_blank">
                    <span class="pcoded-mtext">Forgot Password</span>
                </a>
            </li>
            <li class="">
                <a href="auth-lock-screen.html" target="_blank">
                    <span class="pcoded-mtext">Lock Screen</span>
                </a>
            </li>
            <li class="">
                <a href="auth-modal.html" target="_blank">
                    <span class="pcoded-mtext">Modal</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("maintenance.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-sliders"></i></span>
            <span class="pcoded-mtext">Maintenance</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("maintenance.error") ?: "active" }}">
                <a href="{{ route("maintenance.error") }}">
                    <span class="pcoded-mtext">Error</span>
                </a>
            </li>
            <li class="{{ !Route::is("maintenance.comming-soon") ?: "active" }}">
                <a href="{{ route("maintenance.comming-soon") }}">
                    <span class="pcoded-mtext">Comming Soon</span>
                </a>
            </li>
            <li class="{{ !Route::is("maintenance.offline-ui") ?: "active" }}">
                <a href="{{ route("maintenance.offline-ui") }}">
                    <span class="pcoded-mtext">Offline UI</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("user-profiles.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-users"></i></span>
            <span class="pcoded-mtext">User Profile</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("user-profiles.timeline") ?: "active" }}">
                <a href="{{ route("user-profiles.timeline") }}">
                    <span class="pcoded-mtext">Timeline</span>
                </a>
            </li>
            <li class="{{ !Route::is("user-profiles.timeline-social") ?: "active" }}">
                <a href="{{ route("user-profiles.timeline-social") }}">
                    <span class="pcoded-mtext">Timeline Social</span>
                </a>
            </li>
            <li class="{{ !Route::is("user-profiles.user-profile") ?: "active" }}">
                <a href="{{ route("user-profiles.user-profile") }}">
                    <span class="pcoded-mtext">User Profile</span>
                </a>
            </li>
            <li class="{{ !Route::is("user-profiles.user-card") ?: "active" }}">
                <a href="{{ route("user-profiles.user-card") }}">
                    <span class="pcoded-mtext">User Card</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("e-commerce.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
            <span class="pcoded-mtext">E-Commerce</span>
            <span class="pcoded-badge label label-danger">NEW</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("e-commerce.product") ?: "active" }}">
                <a href="{{ route("e-commerce.product") }}">
                    <span class="pcoded-mtext">Product</span>
                </a>
            </li>
            <li class="{{ !Route::is("e-commerce.product-list") ?: "active" }}">
                <a href="{{ route("e-commerce.product-list") }}">
                    <span class="pcoded-mtext">Product List</span>
                </a>
            </li>
            <li class="{{ !Route::is("e-commerce.product-edit") ?: "active" }}">
                <a href="{{ route("e-commerce.product-edit") }}">
                    <span class="pcoded-mtext">Product Edit</span>
                </a>
            </li>
            <li class="{{ !Route::is("e-commerce.product-detail") ?: "active" }}">
                <a href="{{ route("e-commerce.product-detail") }}">
                    <span class="pcoded-mtext">Product Detail</span>
                </a>
            </li>
            <li class="{{ !Route::is("e-commerce.product-cart") ?: "active" }}">
                <a href="{{ route("e-commerce.product-cart") }}">
                    <span class="pcoded-mtext">Product Card</span>
                </a>
            </li>
            <li class="{{ !Route::is("e-commerce.product-payment") ?: "active" }}">
                <a href="{{ route("e-commerce.product-payment") }}">
                    <span class="pcoded-mtext">Credit Card Form</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("email.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-mail"></i></span>
            <span class="pcoded-mtext">Email</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("email.email-compose") ?: "active" }}">
                <a href="{{ route("email.email-compose") }}">
                    <span class="pcoded-mtext">Compose Email</span>
                </a>
            </li>
            <li class="{{ !Route::is("email.email-inbox") ?: "active" }}">
                <a href="{{ route("email.email-inbox") }}">
                    <span class="pcoded-mtext">Inbox</span>
                </a>
            </li>
            <li class="{{ !Route::is("email.email-read") ?: "active" }}">
                <a href="{{ route("email.email-read") }}">
                    <span class="pcoded-mtext">Read Mail</span>
                </a>
            </li>
            <li class="pcoded-hasmenu ">
                <a href="javascript:void(0)">
                    <span class="pcoded-mtext">Email Template</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/email-templates/email-welcome.html">
                            <span class="pcoded-mtext">Welcome Email</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/email-templates/email-password.html">
                            <span class="pcoded-mtext">Reset Password</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/email-templates/email-newsletter.html">
                            <span class="pcoded-mtext">Newsletter Email</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/email-templates/email-launch.html">
                            <span class="pcoded-mtext">App Launch</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/email-templates/email-activation.html">
                            <span class="pcoded-mtext">Activation Code</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>
<div class="pcoded-navigatio-lavel">App</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="{{ !Route::is("chat") ?: "active" }}">
        <a href="{{ route("chat") }}">
            <span class="pcoded-micon"><i class="feather icon-message-square"></i></span>
            <span class="pcoded-mtext">Chat</span>
        </a>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("social.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-globe"></i></span>
            <span class="pcoded-mtext">Social</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("social.fb-wall") ?: "active" }}">
                <a href="{{ route("social.fb-wall") }}">
                    <span class="pcoded-mtext">Wall</span>
                </a>
            </li>
            <li class="{{ !Route::is("social.message") ?: "active" }}">
                <a href="{{ route("social.message") }}">
                    <span class="pcoded-mtext">Messages</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("tasks.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-check-circle"></i></span>
            <span class="pcoded-mtext">Task</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("tasks.task-list") ?: "active" }}">
                <a href="{{ route("tasks.task-list") }}">
                    <span class="pcoded-mtext">Task List</span>
                </a>
            </li>
            <li class="{{ !Route::is("tasks.task-board") ?: "active" }}">
                <a href="{{ route("tasks.task-board") }}">
                    <span class="pcoded-mtext">Task Board</span>
                </a>
            </li>
            <li class="{{ !Route::is("tasks.task-detail") ?: "active" }}">
                <a href="{{ route("tasks.task-detail") }}">
                    <span class="pcoded-mtext">Task Detail</span>
                </a>
            </li>
            <li class="{{ !Route::is("tasks.issue-list") ?: "active" }}">
                <a href="{{ route("tasks.issue-list") }}">
                    <span class="pcoded-mtext">Issue List</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("to-do.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
            <span class="pcoded-mtext">To-Do</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("to-do.todo") ?: "active" }}">
                <a href="{{ route("to-do.todo") }}">
                    <span class="pcoded-mtext">To-Do</span>
                </a>
            </li>
            <li class="{{ !Route::is("to-do.notes") ?: "active" }}">
                <a href="{{ route("to-do.notes") }}">
                    <span class="pcoded-mtext">Notes</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("gallery.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-image"></i></span>
            <span class="pcoded-mtext">Gallery</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("gallery.gallery-grid") ?: "active" }}">
                <a href="{{ route("gallery.gallery-grid") }}">
                    <span class="pcoded-mtext">Gallery-Grid</span>
                </a>
            </li>
            <li class="{{ !Route::is("gallery.gallery-masonry") ?: "active" }}">
                <a href="{{ route("gallery.gallery-masonry") }}">
                    <span class="pcoded-mtext">Masonry Gallery</span>
                </a>
            </li>
            <li class="{{ !Route::is("gallery.gallery-advance") ?: "active" }}">
                <a href="{{ route("gallery.gallery-advance") }}">
                    <span class="pcoded-mtext">Advance Gallery</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("search.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-search"></i><b>S</b></span>
            <span class="pcoded-mtext">Search</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("search.search-result") ?: "active" }}">
                <a href="{{ route("search.search-result") }}">
                    <span class="pcoded-mtext">Simple Search</span>
                </a>
            </li>
            <li class="{{ !Route::is("search.search-result2") ?: "active" }}">
                <a href="{{ route("search.search-result2") }}">
                    <span class="pcoded-mtext">Grouping Search</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("job-search.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-award"></i></span>
            <span class="pcoded-mtext">Job Search</span>
            <span class="pcoded-badge label label-danger">NEW</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("job-search.job-card-view") ?: "active" }}">
                <a href="{{ route("job-search.job-card-view") }}">
                    <span class="pcoded-mtext">Card View</span>
                </a>
            </li>
            <li class="{{ !Route::is("job-search.job-details") ?: "active" }}">
                <a href="{{ route("job-search.job-details") }}">
                    <span class="pcoded-mtext">Job Detailed</span>
                </a>
            </li>
            <li class="{{ !Route::is("job-search.job-find") ?: "active" }}">
                <a href="{{ route("job-search.job-find") }}">
                    <span class="pcoded-mtext">Job Find</span>
                </a>
            </li>
            <li class="{{ !Route::is("job-search.job-panel-view") ?: "active" }}">
                <a href="{{ route("job-search.job-panel-view") }}">
                    <span class="pcoded-mtext">Job Panel View</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
<div class="pcoded-navigatio-lavel">Extension</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu {{ !Route::is("editor.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-file-plus"></i></span>
            <span class="pcoded-mtext">Editor</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("editor.ck-editor") ?: "active" }}">
                <a href="{{ route("editor.ck-editor") }}">
                    <span class="pcoded-mtext">CK-Editor</span>
                </a>
            </li>
            <li class="{{ !Route::is("editor.wysiwyg-editor") ?: "active" }}">
                <a href="{{ route("editor.wysiwyg-editor") }}">
                    <span class="pcoded-mtext">WYSIWYG Editor</span>
                </a>
            </li>
            <li class="{{ !Route::is("editor.ace-editor") ?: "active" }}">
                <a href="{{ route("editor.ace-editor") }}">
                    <span class="pcoded-mtext">Ace Editor</span>
                </a>
            </li>
            <li class="{{ !Route::is("editor.long-press-editor") ?: "active" }}">
                <a href="{{ route("editor.long-press-editor") }}">
                    <span class="pcoded-mtext">Long Press Editor</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu {{ !Route::is("invoice.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-file-minus"></i></span>
            <span class="pcoded-mtext">Invoice</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("invoice.invoice") ?: "active" }}">
                <a href="{{ route("invoice.invoice") }}">
                    <span class="pcoded-mtext">Invoice</span>
                </a>
            </li>
            <li class="{{ !Route::is("invoice.invoice-summary") ?: "active" }}">
                <a href="{{ route("invoice.invoice-summary") }}">
                    <span class="pcoded-mtext">Invoice Summary</span>
                </a>
            </li>
            <li class="{{ !Route::is("invoice.invoice-list") ?: "active" }}">
                <a href="{{ route("invoice.invoice-list") }}">
                    <span class="pcoded-mtext">Invoice List</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu {{ !Route::is("event-calendar.*") ?: "active pcoded-trigger" }}">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-calendar"></i></span>
            <span class="pcoded-mtext">Event Calendar</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ !Route::is("event-calendar.event-full-calendar") ?: "active" }}">
                <a href="{{ route("event-calendar.event-full-calendar") }}">
                    <span class="pcoded-mtext">Full Calendar</span>
                </a>
            </li>
            <li class="{{ !Route::is("event-calendar.event-clndr") ?: "active" }}">
                <a href="{{ route("event-calendar.event-clndr") }}">
                    <span class="pcoded-mtext">CLNDER</span>
                    <span class="pcoded-badge label label-info">NEW</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ !Route::is("image-crop") ?: "active" }}">
        <a href="{{ route("image-crop") }}">
            <span class="pcoded-micon"><i class="feather icon-scissors"></i></span>
            <span class="pcoded-mtext">Image Cropper</span>
        </a>
    </li>
    <li class="{{ !Route::is("file-upload") ?: "active" }}">
        <a href="{{ route("file-upload") }}">
            <span class="pcoded-micon"><i class="feather icon-upload-cloud"></i></span>
            <span class="pcoded-mtext">File Upload</span>
        </a>
    </li>
    <li class="{{ !Route::is("change-loges") ?: "active" }}">
        <a href="{{ route("change-loges") }}">
            <span class="pcoded-micon"><i class="feather icon-briefcase"></i><b>CL</b></span>
            <span class="pcoded-mtext">Change Loges</span>
        </a>
    </li>
</ul>
<div class="pcoded-navigatio-lavel">Other</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu ">
        <a href="javascript:void(0)">
            <span class="pcoded-micon"><i class="feather icon-list"></i></span>
            <span class="pcoded-mtext">Menu Levels</span>
        </a>
        <ul class="pcoded-submenu">
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-mtext">Menu Level 2.1</span>
                </a>
            </li>
            <li class="pcoded-hasmenu ">
                <a href="javascript:void(0)">
                    <span class="pcoded-mtext">Menu Level 2.2</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="javascript:void(0)">
                            <span class="pcoded-mtext">Menu Level 3.1</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-mtext">Menu Level 2.3</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="">
        <a href="javascript:void(0)" class="disabled">
            <span class="pcoded-micon"><i class="feather icon-power"></i></span>
            <span class="pcoded-mtext">Disabled Menu</span>
        </a>
    </li>
    <li class="{{ !Route::is("sample-page") ?: "active" }}">
        <a href="{{ route("sample-page") }}">
            <span class="pcoded-micon"><i class="feather icon-watch"></i></span>
            <span class="pcoded-mtext">Sample Page</span>
        </a>
    </li>
</ul>
<div class="pcoded-navigatio-lavel">Support</div>
<ul class="pcoded-item pcoded-left-item">
    <li class="">
        <a href="#" target="_blank">
            <span class="pcoded-micon"><i class="feather icon-monitor"></i></span>
            <span class="pcoded-mtext">Documentation</span>
        </a>
    </li>
    <li class="">
        <a href="#" target="_blank">
            <span class="pcoded-micon"><i class="feather icon-help-circle"></i></span>
            <span class="pcoded-mtext">Submit Issue</span>
        </a>
    </li>
</ul>
