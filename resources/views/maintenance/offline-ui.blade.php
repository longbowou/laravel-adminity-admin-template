<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from colorlib.com//polygon/adminty/default/offline-ui.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Nov 2019 19:01:04 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <title>Adminty - Premium Admin Template by Colorlib </title>

    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="#">
    <meta name="keywords"
          content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="{{ asset("adminity/images/favicon.ico") }}" type="image/x-icon">

    <link href="{{ asset("adminity/css/css0e2b.css") }}?family=Open+Sans:400,600" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/css/bootstrap.min.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/icon/feather/css/feather.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/icon/themify-icons/themify-icons.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/menu-search/css/component.css") }}">

    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/css/style.css") }}">
</head>
<body>

@include("partials.theme-loader")

<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <nav class="navbar header-navbar pcoded-header" header-theme="theme1">
            <div class="navbar-wrapper">
                <div class="navbar-logo" logo-theme="theme1">
                    <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="feather icon-menu"></i>
                    </a>
                    <a href="{{ route("dashboard") }}">
                        <img class="img-fluid" src="{{ asset("adminity/images/logo.png") }}" alt="Theme-Logo"/>
                    </a>
                    <a class="mobile-options">
                        <i class="feather icon-more-horizontal"></i>
                    </a>
                </div>
                <div class="navbar-container container-fluid">
                    <ul class="nav-left">
                        <li class="header-search">
                            <div class="main-search morphsearch-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon search-btn"><i
                                            class="feather icon-search"></i></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#!"
                               onclick="if (!window.__cfRLUnblockHandlers) return false; javascript:toggleFullScreen()"
                               data-cf-modified-="">
                                <i class="feather icon-maximize full-screen"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">
                        <li class="header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-bell"></i>
                                    <span class="badge bg-c-pink">5</span>
                                </div>
                                <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn"
                                    data-dropdown-out="fadeOut">
                                    <li>
                                        <h6>Notifications</h6>
                                        <label class="label label-danger">New</label>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius"
                                                 src="{{ asset("adminity/images/avatar-4.jpg") }}"
                                                 alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">John Doe</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer
                                                    elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius"
                                                 src="{{ asset("adminity/images/avatar-3.jpg") }}"
                                                 alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">Joseph William</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer
                                                    elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius"
                                                 src="{{ asset("adminity/images/avatar-4.jpg") }}"
                                                 alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">Sara Soudein</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer
                                                    elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-message-square"></i>
                                    <span class="badge bg-c-green">3</span>
                                </div>
                            </div>
                        </li>
                        <li class="user-profile header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{ asset("adminity/images/avatar-4.jpg") }}" class="img-radius"
                                         alt="User-Profile-Image">
                                    <span>John Doe</span>
                                    <i class="feather icon-chevron-down"></i>
                                </div>
                                <ul class="show-notification profile-notification dropdown-menu"
                                    data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                    <li>
                                        <a href="#!">
                                            <i class="feather icon-settings"></i> Settings
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="feather icon-user"></i> Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="feather icon-mail"></i> My Messages
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="feather icon-lock"></i> Lock Screen
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="feather icon-log-out"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>

<section class="login offline-404 p-fixed d-flex text-center">

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="auth-body">
                    <form>
                        <h1>Offline</h1>
                        <h2>Oops! Website Is Temporarily Offline</h2>
                        <div class="left-icon-control">
                            <input type="text" class="form-control form-control-lg" placeholder="Try a new Search">
                            <div class="form-icon">
                                <i class="icofont icofont-search"></i>
                            </div>
                        </div>
                        <a href="{{ route("dashboard") }}" class="btn btn-primary btn-lg m-t-30">Back to Home</a>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>

@include("partials.ie-warning")

<script type="text/javascript"
        src="{{ asset("adminity/js/jquery.min.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("adminity/js/jquery-ui.min.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("adminity/js/popper.min.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("welcome/js/bootstrap.min.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("adminity/js/jquery.slimscroll.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("adminity/js/modernizr.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("adminity/js/css-scrollbars.js") }}"></script>

<script src="{{ asset("adminity/js/jquery.mCustomScrollbar.concat.min.js") }}"
        type="text/javascript"></script>

<script type="text/javascript" src="{{ asset("adminity/js/i18next.min.js") }}"></script>

<script type="text/javascript" src="{{ asset("adminity/js/i18nextXHRBackend.min.js") }}"></script>

<script type="text/javascript"
        src="{{ asset("adminity/js/i18nextBrowserLanguageDetector.min.js") }}"></script>

<script type="text/javascript" src="{{ asset("adminity/js/jquery-i18next.min.js") }}"></script>

<script type="text/javascript" src="{{ asset("adminity/js/script.js") }}"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="text/javascript"></script>
<script type="text/javascript">
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>
</body>
<!-- Mirrored from colorlib.com//polygon/adminty/default/offline-ui.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Nov 2019 19:01:04 GMT -->
</html>
