@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Comming Soooonnnn .....</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Maintenance</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Comming Soon</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Select Your Layout</h5>
                <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-3 m-b-30">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/comming-soon/index1.html"
                           target="_blank"><img src="{{ asset("adminity/images/commingsoon/cs-1.jpg") }}" alt="Layout-1"
                                                class="img-fluid img-thumbnail"></a>
                    </div>
                    <div class="col-sm-3 m-b-30">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/comming-soon/index2.html"
                           target="_blank"><img src="{{ asset("adminity/images/commingsoon/cs-2.jpg") }}" alt="Layout-1"
                                                class="img-fluid img-thumbnail"></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
