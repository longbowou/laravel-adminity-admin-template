@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Error Page</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item"><a href="#!">Maintenance</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Error</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="card">
            <div class="card-header">
                <h5>Error Found</h5>
                <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-3 m-b-30">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/404/2/index.html"
                           target="_blank"><img src="{{ asset("adminity/images/error/error-1.jpg") }}" alt="Layout-1"
                                                class="img-fluid img-thumbnail"></a>
                    </div>
                    <div class="col-sm-3 m-b-30">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/404/1/index-bubble.html"
                           target="_blank"><img src="{{ asset("adminity/images/error/error-2.jpg") }}" alt="Layout-2"
                                                class="img-fluid img-thumbnail"></a>
                    </div>
                    <div class="col-sm-3 m-b-30">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/404/1/index-flat.html"
                           target="_blank"><img src="{{ asset("adminity/images/error/error-3.jpg") }}" alt="Layout-3"
                                                class="img-fluid img-thumbnail"></a>
                    </div>
                    <div class="col-sm-3 m-b-30">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/404/1/index-image.html"
                           target="_blank"><img src="{{ asset("adminity/images/error/error-4.jpg") }}" alt="Layout-4"
                                                class="img-fluid img-thumbnail"></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 m-b-30">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/404/1/index-mozaic.html"
                           target="_blank"><img src="{{ asset("adminity/images/error/error-5.jpg") }}" alt="Layout-5"
                                                class="img-fluid img-thumbnail"></a>
                    </div>
                    <div class="col-sm-3 m-b-30">
                        <a href="https://colorlib.com//polygon/adminty/files/extra-pages/404/1/index-youtube.html"
                           target="_blank"><img src="{{ asset("adminity/images/error/error-6.jpg") }}" alt="Layout-5"
                                                class="img-fluid img-thumbnail"></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
