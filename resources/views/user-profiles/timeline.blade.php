@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>timeline</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Timeline</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Timeline</h5>
                    </div>
                    <div class="card-block">
                        <div class="main-timeline">
                            <div class="cd-timeline cd-container">
                                <div class="cd-timeline-block">
                                    <div class="cd-timeline-icon bg-primary">
                                        <i class="icofont icofont-ui-file"></i>
                                    </div>

                                    <div class="cd-timeline-content card_main">
                                        <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                             class="img-fluid width-100" alt=""/>
                                        <div class="p-20">
                                            <h6>We’d like to introduce our new website</h6>
                                            <div class="timeline-details">
                                                <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>24 October 2014</span>
                                                </a>
                                                <a href="#">
                                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>
                                                </a>
                                                <p class="m-t-20">lorem ipsum dolor sit amet, consectetur adipisicing
                                                    elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat
                                                    iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus
                                                    minus veritatis qui ut.</p>
                                            </div>
                                        </div>
                                        <span class="cd-date">October 23, 2014, 12:56 AM</span>
                                        <span class="cd-details">You posed an artical with public</span>
                                    </div>

                                </div>

                                <div class="cd-timeline-block">
                                    <div class="cd-timeline-icon bg-success">
                                        <i class="icofont icofont-ui-user"></i>
                                    </div>

                                    <div class="cd-timeline-content card_main">
                                        <div class="media bg-white d-flex p-10 d-block-phone">
                                            <div class="media-left media-middle col-xs-12">
                                                <a href="#">
                                                    <img class="media-object img-fluid"
                                                         src="{{ asset("adminity/images/timeline/img2.png") }}" alt="">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <div class="f-15 f-bold m-b-5">Josephin Doe</div>
                                                <div class="f-13 text-muted">Software Engineer</div>
                                            </div>
                                        </div>
                                        <span class="cd-date">October 25, 2014, 01:15 PM</span>
                                        <span class="cd-details">You posed an artical with public</span>
                                    </div>

                                </div>

                                <div class="cd-timeline-block">
                                    <div class="cd-timeline-icon bg-warning">
                                        <i class="icofont icofont-ui-file"></i>
                                    </div>

                                    <div class="cd-timeline-content card_main">
                                        <div class="p-20">
                                            <h6>We’d like to introduce our new website</h6>
                                            <div class="timeline-details">
                                                <a href="#">
                                                    <i class="icofont icofont-ui-calendar"></i><span>10 November 2014</span>
                                                </a>
                                                <a href="#">
                                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>
                                                </a>
                                            </div>
                                        </div>
                                        <img src="{{ asset("adminity/images/timeline/img1.jpg") }}"
                                             class="img-fluid width-100"
                                             alt=""/>
                                        <div class="p-20">
                                            <p>lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio,
                                                dolorum provident rerum aut hic quasi placeat iure tempora laudantium
                                                ipsa ad debitis unde? Iste voluptatibus
                                                minus veritatis qui ut.</p>
                                        </div>
                                        <span class="cd-date">November 12, 2014, 11:27 AM</span>
                                        <span class="cd-details">You posed an artical with public</span>
                                    </div>

                                </div>

                                <div class="cd-timeline-block">
                                    <div class="cd-timeline-icon bg-danger">
                                        <i class="icofont icofont-ui-music"></i>
                                    </div>

                                    <div class="cd-timeline-content card_main">
                                        <div class="embed-responsive embed-responsive-4by3">
                                            <iframe class="embed-responsive-item"
                                                    src="https://www.youtube.com/embed/XGSy3_Czz8k"></iframe>
                                        </div>
                                        <div class="p-20">
                                            <p>lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio,
                                                dolorum provident rerum aut hic quasi placeat iure tempora laudantium
                                                ipsa ad debitis unde? Iste voluptatibus
                                                minus veritatis qui ut.</p>
                                        </div>
                                        <span class="cd-date">December 01, 2014, 04:56 PM</span>
                                        <span class="cd-details">You posed an artical with public</span>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

