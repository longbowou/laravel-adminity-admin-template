@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-bs4/css/dataTables.bootstrap4.min.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/pages/data-table/css/buttons.dataTables.min.css") }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Task List</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Task</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Task list</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Task List</h5>
                    </div>
                    <div class="card-block task-list">
                        <div class="table-responsive">
                            <table id="simpletable"
                                   class="table dt-responsive task-list-table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Task list</th>
                                    <th>Last Commit</th>
                                    <th>Status</th>
                                    <th>Assigned User</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody class="task-page">
                                <tr>
                                    <td>#12</td>
                                    <td>Add Proper Cursor In Sortable Page</td>
                                    <td>
                                        <input type="date" class="form-control"/>
                                    </td>
                                    <td>
                                        <select name="select" class="form-control form-control-sm">
                                            <option value="opt1">Open</option>
                                            <option value="opt2">Resolved</option>
                                            <option value="opt3">Invalid</option>
                                            <option value="opt4">On hold</option>
                                            <option value="opt5">Close</option>
                                        </select>
                                    </td>
                                    <td>
                                        <a href="#"><img class="img-fluid img-radius"
                                                         src="{{ asset("adminity/images/avatar-1.jpg") }}" alt=""></a>
                                        <a href="#"><img class="img-fluid img-radius"
                                                         src="{{ asset("adminity/images/avatar-2.jpg") }}" alt=""></a>
                                        <a href="#"><i class="icofont icofont-plus f-w-600"></i></a>
                                    </td>
                                    <td class="text-center">
                                        <a class="dropdown-toggle addon-btn" data-toggle="dropdown"
                                           aria-expanded="true">
                                            <i class="icofont icofont-ui-settings"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-attachment"></i>Attach
                                                File</a>
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-ui-edit"></i>Edit
                                                Task</a>
                                            <div role="separator" class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-refresh"></i>Reassign
                                                Task</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#56</td>
                                    <td>Edit the draft for the icons</td>
                                    <td>
                                        <input type="date" class="form-control"/>
                                    </td>
                                    <td>
                                        <select name="select" class="form-control form-control-sm">
                                            <option value="opt1">Open</option>
                                            <option value="opt2">Resolved</option>
                                            <option value="opt3">Invalid</option>
                                            <option value="opt4">On hold</option>
                                            <option value="opt5">Close</option>
                                        </select>
                                    </td>
                                    <td>
                                        <a href="#"><img class="img-fluid img-radius"
                                                         src="{{ asset("adminity/images/avatar-1.jpg") }}" alt=""></a>
                                        <a href="#"><img class="img-fluid img-radius"
                                                         src="{{ asset("adminity/images/avatar-2.jpg") }}" alt=""></a>
                                        <a href="#"><i class="icofont icofont-plus f-w-600"></i></a>
                                    </td>
                                    <td class="text-center">
                                        <a class="dropdown-toggle addon-btn" data-toggle="dropdown"
                                           aria-expanded="true">
                                            <i class="icofont icofont-ui-settings"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-attachment"></i>Attach
                                                File</a>
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-ui-edit"></i>Edit
                                                Task</a>
                                            <div role="separator" class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-refresh"></i>Reassign
                                                Task</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#78</td>
                                    <td>Create UI design model</td>
                                    <td>
                                        <input type="date" class="form-control"/>
                                    </td>
                                    <td>
                                        <select name="select" class="form-control form-control-sm">
                                            <option value="opt1">Open</option>
                                            <option value="opt2">Resolved</option>
                                            <option value="opt3">Invalid</option>
                                            <option value="opt4">On hold</option>
                                            <option value="opt5">Close</option>
                                        </select>
                                    </td>
                                    <td>
                                        <a href="#"><img class="img-fluid img-radius"
                                                         src="{{ asset("adminity/images/avatar-1.jpg") }}" alt=""></a>
                                        <a href="#"><img class="img-fluid img-radius"
                                                         src="{{ asset("adminity/images/avatar-2.jpg") }}" alt=""></a>
                                        <a href="#"><img class="img-fluid img-radius"
                                                         src="{{ asset("adminity/images/avatar-3.jpg") }}" alt=""></a>
                                        <a href="#"><i class="icofont icofont-plus f-w-600"></i></a>
                                    </td>
                                    <td class="text-center">
                                        <a class="dropdown-toggle addon-btn" data-toggle="dropdown"
                                           aria-expanded="true">
                                            <i class="icofont icofont-ui-settings"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-attachment"></i>Attach
                                                File</a>
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-ui-edit"></i>Edit
                                                Task</a>
                                            <div role="separator" class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-refresh"></i>Reassign
                                                Task</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#35</td>
                                    <td>Checkbox Design issue</td>
                                    <td>
                                        <input type="date" class="form-control"/>
                                    </td>
                                    <td>
                                        <select name="select" class="form-control form-control-sm">
                                            <option value="opt1">Open</option>
                                            <option value="opt2">Resolved</option>
                                            <option value="opt3">Invalid</option>
                                            <option value="opt4">On hold</option>
                                            <option value="opt5">Close</option>
                                        </select>
                                    </td>
                                    <td>
                                        <a href="#"><img class="img-fluid img-radius"
                                                         src="{{ asset("adminity/images/avatar-1.jpg") }}" alt=""></a>
                                        <a href="#"><i class="icofont icofont-plus f-w-600"></i></a>
                                    </td>
                                    <td class="text-center">
                                        <a class="dropdown-toggle addon-btn" data-toggle="dropdown"
                                           aria-expanded="true">
                                            <i class="icofont icofont-ui-settings"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-attachment"></i>Attach
                                                File</a>
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-ui-edit"></i>Edit
                                                Task</a>
                                            <div role="separator" class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#"><i class="icofont icofont-refresh"></i>Reassign
                                                Task</a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>To List</h5>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12 btn-add-task">
                                <div class="input-group input-group-button">
                                    <input type="text" class="form-control" placeholder="Add Task">
                                    <span class="input-group-addon btn btn-primary" id="basic-addon1">
<i class="icofont icofont-plus f-w-600"></i>Add task
</span>
                                </div>
                            </div>
                        </div>
                        <div class="new-task">
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span> <span>Primary</span>
                                    </label>
                                </div>
                            </div>
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span> <span>Primary</span>
                                    </label>
                                </div>
                            </div>
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span> <span>Primary</span>
                                    </label>
                                </div>
                            </div>
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span> <span>Primary</span>
                                    </label>
                                </div>
                            </div>
                            <div class="to-do-list">
                                <div class="checkbox-fade fade-in-primary">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span> <span>Primary</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/datatables.net/js/jquery.dataTables.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/dataTables.buttons.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/jszip.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/pdfmake.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/vfs_fonts.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.print.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-buttons/js/buttons.html5.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-bs4/js/dataTables.bootstrap4.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive/js/dataTables.responsive.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/data-table/js/data-table-custom.js") }}" type="text/javascript"></script>
@endsection
