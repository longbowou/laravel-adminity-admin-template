@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/pages/sticky/css/jquery.postitall.css") }}" type="text/css"
          media="all">
    <link rel="stylesheet" href="{{ asset("adminity/pages/sticky/css/trumbowyg.css") }}" type="text/css" media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Sticky</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Sticky</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Sticky Notes</h5>
                        <span>Click <code>Add Note</code> button to add new sticky notes</span>
                    </div>
                    <div class="card-block sticky-card">
                        <button type="button" id="idRunTheCode" class="btn btn-primary waves-effect waves-light"
                                data-toggle="tooltip" data-placement="top" title="Add note">
                            <i class="icofont icofont-ui-add"></i><span class="m-l-10">Add note</span>
                        </button>
                        <div class="row">
                            <div class="col-sm-2">
                                <p id="notes" class="notes"></p>
                            </div>
                            <div class="col-sm-2">
                                <p id="notes1" class="notes1"></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/sticky/js/trumbowyg.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/sticky/js/jquery.minicolors.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/sticky/js/jquery.postitall.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/sticky/js/sticky.js") }}"></script>
@endsection

