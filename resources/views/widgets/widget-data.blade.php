@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Widget</h4>
                        <span>More than 100+ widget</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Widget</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">

            <div class="col-xl-6 col-md-12">
                <div class="card table-card">
                    <div class="card-header">
                        <h5>Recent Tickets</h5>
                        <div class="card-header-right">
                            <ul class="list-unstyled card-option">
                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                <li><i class="fa fa-minus minimize-card"></i></li>
                                <li><i class="fa fa-refresh reload-card"></i></li>
                                <li><i class="fa fa-trash close-card"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover table-borderless">
                                <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Subject</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><label class="label label-success">open</label></td>
                                    <td>Website down for one week</td>
                                    <td>Support</td>
                                    <td>Today 2:00</td>
                                </tr>
                                <tr>
                                    <td><label class="label label-primary">progress</label></td>
                                    <td>Loosing control on server</td>
                                    <td>Support</td>
                                    <td>Yesterday</td>
                                </tr>
                                <tr>
                                    <td><label class="label label-danger">closed</label></td>
                                    <td>Authorizations keys</td>
                                    <td>Support</td>
                                    <td>27, Aug</td>
                                </tr>
                                <tr>
                                    <td><label class="label label-success">open</label></td>
                                    <td>Restoring default settings</td>
                                    <td>Support</td>
                                    <td>Today 9:00</td>
                                </tr>
                                <tr>
                                    <td><label class="label label-primary">progress</label></td>
                                    <td>Loosing control on server</td>
                                    <td>Support</td>
                                    <td>Yesterday</td>
                                </tr>
                                <tr>
                                    <td><label class="label label-success">open</label></td>
                                    <td>Restoring default settings</td>
                                    <td>Support</td>
                                    <td>Today 9:00</td>
                                </tr>
                                <tr>
                                    <td><label class="label label-danger">closed</label></td>
                                    <td>Authorizations keys</td>
                                    <td>Support</td>
                                    <td>27, Aug</td>
                                </tr>
                                <tr>
                                    <td><label class="label label-success">open</label></td>
                                    <td>Restoring default settings</td>
                                    <td>Support</td>
                                    <td>Today 9:00</td>
                                </tr>
                                <tr>
                                    <td><label class="label label-primary">progress</label></td>
                                    <td>Loosing control on server</td>
                                    <td>Support</td>
                                    <td>Yesterday</td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="text-right m-r-20">
                                <a href="#!" class=" b-b-primary text-primary">View all Projects</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12">
                <div class="card latest-update-card">
                    <div class="card-header">
                        <h5>Latest Updates</h5>
                        <div class="card-header-right">
                            <ul class="list-unstyled card-option">
                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                <li><i class="fa fa-minus minimize-card"></i></li>
                                <li><i class="fa fa-refresh reload-card"></i></li>
                                <li><i class="fa fa-trash close-card"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="latest-update-box">
                            <div class="row p-t-20 p-b-30">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline">2 hrs ago</p>
                                    <i class="feather icon-twitter bg-info update-icon"></i>
                                </div>
                                <div class="col">
                                    <h6>+ 1652 Followers</h6>
                                    <p class="text-muted m-b-0">You’re getting more and more followers, keep it up!</p>
                                </div>
                            </div>
                            <div class="row p-b-30">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline">4 hrs ago</p>
                                    <i class="feather icon-briefcase bg-simple-c-pink update-icon"></i>
                                </div>
                                <div class="col">
                                    <h6>+ 5 New Products were added!</h6>
                                    <p class="text-muted m-b-0">Congratulations!</p>
                                </div>
                            </div>
                            <div class="row p-b-30">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline">1 day ago</p>
                                    <i class="feather icon-check bg-simple-c-yellow  update-icon"></i>
                                </div>
                                <div class="col">
                                    <h6>Database backup completed!</h6>
                                    <p class="text-muted m-b-0">Download the <span
                                            class="text-c-blue">latest backup</span>.</p>
                                </div>
                            </div>
                            <div class="row p-b-0">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline">2 day ago</p>
                                    <i class="feather icon-facebook bg-simple-c-green update-icon"></i>
                                </div>
                                <div class="col">
                                    <h6>+2 Friend Requests</h6>
                                    <p class="text-muted m-b-10">This is great, keep it up!</p>
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <tr>
                                                <td class="b-none">
                                                    <a href="#!" class="align-middle">
                                                        <img src="{{ asset("adminity/images/avatar-2.jpg") }}"
                                                             alt="user image"
                                                             class="img-radius img-40 align-top m-r-15">
                                                        <div class="d-inline-block">
                                                            <h6>Jeny William</h6>
                                                            <p class="text-muted m-b-0">Graphic Designer</p>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="b-none">
                                                    <a href="#!" class="align-middle">
                                                        <img src="{{ asset("adminity/images/avatar-1.jpg") }}" alt="user image"
                                                             class="img-radius img-40 align-top m-r-15">
                                                        <div class="d-inline-block">
                                                            <h6>John Deo</h6>
                                                            <p class="text-muted m-b-0">Web Designer</p>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#!" class="b-b-primary text-primary">View all Projects</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card table-card">
                    <div class="card-header">
                        <h5>Application Sales</h5>
                        <div class="card-header-right">
                            <ul class="list-unstyled card-option">
                                <li><i class="feather icon-maximize full-card"></i></li>
                                <li><i class="feather icon-minus minimize-card"></i></li>
                                <li><i class="feather icon-trash-2 close-card"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover  table-borderless">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="chk-option">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label class="check-task">
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon feather icon-check txt-default"></i>
</span>
                                                </label>
                                            </div>
                                        </div>
                                        Application
                                    </th>
                                    <th>Sales</th>
                                    <th>Change</th>
                                    <th>Avg Price</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="chk-option">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label class="check-task">
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon feather icon-check txt-default"></i>
</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="d-inline-block align-middle">
                                            <h6>Able Pro</h6>
                                            <p class="text-muted m-b-0">Powerful Admin Theme</p>
                                        </div>
                                    </td>
                                    <td>16,300</td>
                                    <td>
                                        <canvas id="app-sale1" height="50" width="100"></canvas>
                                    </td>
                                    <td>$53</td>
                                    <td class="text-c-blue">$15,652</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="chk-option">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label class="check-task">
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon feather icon-check txt-default"></i>
</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="d-inline-block align-middle">
                                            <h6>Photoshop</h6>
                                            <p class="text-muted m-b-0">Design Software</p>
                                        </div>
                                    </td>
                                    <td>26,421</td>
                                    <td>
                                        <canvas id="app-sale2" height="50" width="100"></canvas>
                                    </td>
                                    <td>$35</td>
                                    <td class="text-c-blue">$18,785</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="chk-option">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label class="check-task">
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
 <i class="cr-icon feather icon-check txt-default"></i>
</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="d-inline-block align-middle">
                                            <h6>Guruable</h6>
                                            <p class="text-muted m-b-0">Best Admin Template</p>
                                        </div>
                                    </td>
                                    <td>8,265</td>
                                    <td>
                                        <canvas id="app-sale3" height="50" width="100"></canvas>
                                    </td>
                                    <td>$98</td>
                                    <td class="text-c-blue">$9,652</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="chk-option">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label class="check-task">
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
<i class="cr-icon feather icon-check txt-default"></i>
</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="d-inline-block align-middle">
                                            <h6>Flatable</h6>
                                            <p class="text-muted m-b-0">Admin App</p>
                                        </div>
                                    </td>
                                    <td>10,652</td>
                                    <td>
                                        <canvas id="app-sale4" height="50" width="100"></canvas>
                                    </td>
                                    <td>$20</td>
                                    <td class="text-c-blue">$7,856</td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="text-center">
                                <a href="#!" class=" b-b-primary text-primary">View all Projects</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-8 col-md-12">
                <div class="card latest-activity-card">
                    <div class="card-header">
                        <h5>Latest Activity</h5>
                    </div>
                    <div class="card-block">
                        <div class="latest-update-box">
                            <div class="row p-t-20 p-b-30">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline">just now</p>
                                    <i class="feather icon-sunrise bg-simple-c-blue update-icon"></i>
                                </div>
                                <div class="col">
                                    <h6>John Deo</h6>
                                    <p class="text-muted m-b-15">The trip was an amazing and a life changing
                                        experience!!</p>
                                    <img src="{{ asset("adminity/images/mega-menu/01.jpg") }}" alt=""
                                         class="img-fluid img-100 m-r-15 m-b-10">
                                    <img src="{{ asset("adminity/images/mega-menu/03.jpg") }}" alt=""
                                         class="img-fluid img-100 m-r-15 m-b-10">
                                </div>
                            </div>
                            <div class="row p-b-30">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline">5 min ago</p>
                                    <i class="feather icon-file-text bg-simple-c-blue update-icon"></i>
                                </div>
                                <div class="col">
                                    <h6>Administrator</h6>
                                    <p class="text-muted m-b-0">Free courses for all our customers at A1 Conference Room
                                        - 9:00 am tomorrow!</p>
                                </div>
                            </div>
                            <div class="row p-b-30">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline">3 hours ago</p>
                                    <i class="feather icon-map-pin bg-simple-c-blue update-icon"></i>
                                </div>
                                <div class="col">
                                    <h6>Jeny William</h6>
                                    <p class="text-muted m-b-15">Happy Hour! Free drinks at <span class="text-c-blue">Cafe-Bar all </span>day
                                        long!</p>
                                    <div id="markers-map" style="height:200px;width:100%"></div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <a href="#!" class=" b-b-primary text-primary">View all Activity</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-12">
                <div class="card feed-card">
                    <div class="card-header">
                        <h5>Upcoming Deadlines</h5>
                        <div class="card-header-right">
                            <ul class="list-unstyled card-option">
                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                <li><i class="fa fa-minus minimize-card"></i></li>
                                <li><i class="fa fa-refresh reload-card"></i></li>
                                <li><i class="fa fa-trash close-card"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row m-b-25">
                            <div class="col-auto p-r-0">
                                <img src="{{ asset("adminity/images/mega-menu/01.jpg") }}" alt="" class="img-fluid img-50">
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">New able - Redesign</h6>
                                <p class="text-c-pink m-b-0">2 Days Remaining</p>
                            </div>
                        </div>
                        <div class="row m-b-25">
                            <div class="col-auto p-r-0">
                                <img src="{{ asset("adminity/images/mega-menu/02.jpg") }}" alt="" class="img-fluid img-50">
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">New Admin Dashboard</h6>
                                <p class="text-c-pink m-b-0">Proposal in 6 Days</p>
                            </div>
                        </div>
                        <div class="row m-b-25">
                            <div class="col-auto p-r-0">
                                <img src="{{ asset("adminity/images/mega-menu/03.jpg") }}" alt="" class="img-fluid img-50">
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">Logo Design</h6>
                                <p class="text-c-green m-b-0">10 Days Remaining</p>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#!" class="b-b-primary text-primary">View all Feeds</a>
                        </div>
                    </div>
                </div>
                <div class="card feed-card">
                    <div class="card-header">
                        <h5>Feeds</h5>
                    </div>
                    <div class="card-block">
                        <div class="row m-b-30">
                            <div class="col-auto p-r-0">
                                <i class="feather icon-bell bg-simple-c-blue feed-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">You have 3 pending tasks. <span class="text-muted f-right f-13">Just Now</span>
                                </h6>
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-auto p-r-0">
                                <i class="feather icon-shopping-cart bg-simple-c-pink feed-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">New order received <span
                                        class="text-muted f-right f-13">Just Now</span></h6>
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-auto p-r-0">
                                <i class="feather icon-file-text bg-simple-c-green feed-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">You have 3 pending tasks. <span class="text-muted f-right f-13">Just Now</span>
                                </h6>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-auto p-r-0">
                                <i class="feather icon-shopping-cart bg-simple-c-pink feed-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">New order received <span
                                        class="text-muted f-right f-13">Just Now</span></h6>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#!" class="b-b-primary text-primary">View all Feeds</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-md-12">
                <div class="card user-activity-card">
                    <div class="card-header">
                        <h5>User Activity</h5>
                    </div>
                    <div class="card-block">
                        <div class="row m-b-25">
                            <div class="col-auto p-r-0">
                                <div class="u-img">
                                    <img src="{{ asset("adminity/images/breadcrumb-bg.jpg") }}" alt="user image"
                                         class="img-radius cover-img">
                                    <img src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="user image"
                                         class="img-radius profile-img">
                                </div>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">John Deo</h6>
                                <p class="text-muted m-b-0">Lorem Ipsum is simply dummy text.</p>
                                <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>2 min ago</p>
                            </div>
                        </div>
                        <div class="row m-b-25">
                            <div class="col-auto p-r-0">
                                <div class="u-img">
                                    <img src="{{ asset("adminity/images/breadcrumb-bg.jpg") }}" alt="user image"
                                         class="img-radius cover-img">
                                    <img src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="user image"
                                         class="img-radius profile-img">
                                </div>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">John Deo</h6>
                                <p class="text-muted m-b-0">Lorem Ipsum is simply dummy text.</p>
                                <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>2 min ago</p>
                            </div>
                        </div>
                        <div class="row m-b-25">
                            <div class="col-auto p-r-0">
                                <div class="u-img">
                                    <img src="{{ asset("adminity/images/breadcrumb-bg.jpg") }}" alt="user image"
                                         class="img-radius cover-img">
                                    <img src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="user image"
                                         class="img-radius profile-img">
                                </div>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">John Deo</h6>
                                <p class="text-muted m-b-0">Lorem Ipsum is simply dummy text.</p>
                                <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>2 min ago</p>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#!" class="b-b-primary text-primary">View all Projects</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-md-12">
                <div class="card table-card">
                    <div class="card-header">
                        <h5>Global Sales by Top Locations</h5>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover table-borderless">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Country</th>
                                    <th>Sales</th>
                                    <th class="text-right">Average</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><img src="{{ asset("adminity/images/widget/GERMANY.jpg") }}" alt=""
                                             class="img-fluid img-30"></td>
                                    <td>Germany</td>
                                    <td>3,562</td>
                                    <td class="text-right">56.23%</td>
                                </tr>
                                <tr>
                                    <td><img src="{{ asset("adminity/images/widget/USA.jpg") }}" alt=""
                                             class="img-fluid img-30"></td>
                                    <td>USA</td>
                                    <td>2,650</td>
                                    <td class="text-right">25.23%</td>
                                </tr>
                                <tr>
                                    <td><img src="{{ asset("adminity/images/widget/AUSTRALIA.jpg") }}" alt=""
                                             class="img-fluid img-30"></td>
                                    <td>Australia</td>
                                    <td>956</td>
                                    <td class="text-right">12.45%</td>
                                </tr>
                                <tr>
                                    <td><img src="{{ asset("adminity/images/widget/UK.jpg") }}" alt="" class="img-fluid img-30">
                                    </td>
                                    <td>United Kingdom</td>
                                    <td>689</td>
                                    <td class="text-right">8.65%</td>
                                </tr>
                                <tr>
                                    <td><img src="{{ asset("adminity/images/widget/BRAZIL.jpg") }}" alt=""
                                             class="img-fluid img-30"></td>
                                    <td>Brazil</td>
                                    <td>560</td>
                                    <td class="text-right">3.56%</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right  m-r-20">
                            <a href="#!" class="b-b-primary text-primary">View all Sales Locations </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="{{ asset("adminity/pages/widget/excanvas.js") }}"></script>

    <script type="text/javascript"
            src="{{ asset("adminity/js/Chart.js") }}"></script>

    <script
        src="{{ asset("adminity/js/markerclusterer.js") }}"
        type="text/javascript"></script>
    <script type="text/javascript"
            src="https://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/google-maps/gmaps.js") }}"></script>

    <script src="{{ asset("adminity/js/jquery.mousewheel.min.js") }}" type="text/javascript"></script>
    <script type="text/javascript">
        /*markers map*/
        var map;
        map = new GMaps({
            el: '#markers-map',
            lat: 21.2334329,
            lng: 72.866472,
            scrollwheel: false
        });

        map.addMarker({
            lat: 21.2334329,
            lng: 72.866472,
            title: 'Marker with InfoWindow',
            infoWindow: {
                content: '<p><Phoenicoded></Phoenicoded> <br/> Buy Now at <a href="">Themeforest</a></p>'
            }
        });
        var ctx = document.getElementById('app-sale1').getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: amuntchart('#11c15b', [1, 15, 30, 15, 25, 35, 45, 20, 25, 30], 'transparent'),
            options: buildchartoption(),
        });
        var ctx = document.getElementById('app-sale2').getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: amuntchart('#448aff', [45, 30, 25, 35, 20, 35, 45, 20, 25, 1], 'transparent'),
            options: buildchartoption(),
        });
        var ctx = document.getElementById('app-sale3').getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: amuntchart('#ff5252', [1, 45, 24, 40, 20, 35, 10, 20, 45, 30], 'transparent'),
            options: buildchartoption(),
        });
        var ctx = document.getElementById('app-sale4').getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: amuntchart('#536dfe', [1, 15, 45, 15, 25, 35, 45, 20, 25, 30], 'transparent'),
            options: buildchartoption(),
        });

        function amuntchart(a, b, f) {
            if (f == null) {
                f = "rgba(0,0,0,0)";
            }
            return {
                labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October"],
                datasets: [{
                    label: "",
                    borderColor: a,
                    borderWidth: 2,
                    hitRadius: 30,
                    pointHoverRadius: 4,
                    pointBorderWidth: 50,
                    pointHoverBorderWidth: 12,
                    pointBackgroundColor: Chart.helpers.color("#000000").alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color("#000000").alpha(0).rgbString(),
                    pointHoverBackgroundColor: a,
                    pointHoverBorderColor: Chart.helpers.color("#000000").alpha(.1).rgbString(),
                    fill: true,
                    backgroundColor: f,
                    data: b,
                }]
            };
        }

        function buildchartoption() {
            return {
                maintainAspectRatio: false,
                title: {
                    display: !1
                },
                tooltips: {
                    position: 'nearest',
                    mode: 'index',
                    intersect: false,
                    yPadding: 10,
                    xPadding: 10,
                },
                legend: {
                    display: !1,
                    labels: {
                        usePointStyle: !1
                    }
                },
                responsive: !0,
                maintainAspectRatio: !0,
                hover: {
                    mode: "index"
                },
                scales: {
                    xAxes: [{
                        display: !1,
                        gridLines: !1,
                        scaleLabel: {
                            display: !0,
                            labelString: "Month"
                        }
                    }],
                    yAxes: [{
                        display: !1,
                        gridLines: !1,
                        scaleLabel: {
                            display: !0,
                            labelString: "Value"
                        },
                        ticks: {
                            beginAtZero: !0
                        }
                    }]
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 12
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 0
                    }
                }
            };
        }
    </script>
@endsection

