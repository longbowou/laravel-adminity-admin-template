@extends("layouts.app")

@section("content")
    <div class="page-body">
        <div class="card">

            <div class="card-block email-card">
                <div class="row">
                    <div class="col-lg-12 col-xl-3">
                        <div class="user-head row">
                            <div class="user-face">
                                <img class="img-fluid" src="{{ asset("adminity/images/logo.png") }}" alt="Theme-Logo"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-9">
                        <div class="mail-box-head row">
                            <div class="col-md-12">
                                <form class="f-right">
                                    <div class="right-icon-control">
                                        <input type="text" class="form-control  search-text" placeholder="Search Friend"
                                               id="search-friends-2">
                                        <div class="form-icon">
                                            <i class="icofont icofont-search"></i>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-12 col-xl-3">
                        <div class="user-body">
                            <div class="p-20 text-center">
                                <a href="#" class="btn btn-danger">Compose</a>
                            </div>
                            <ul class="page-list nav nav-tabs flex-column">
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-inbox"></i> Inbox
                                        <span class="label label-primary f-right">6</span>
                                    </a>
                                </li>
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-star"></i> Starred
                                    </a>
                                </li>
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-file-text"></i> Drafts
                                    </a>
                                </li>
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-paper-plane"></i> Sent Mail
                                    </a>
                                </li>
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-ui-delete"></i> Trash
                                        <span class="label label-info f-right">30</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="p-20 label-list">
                                <li>
                                    <h5>Labels</h5>
                                </li>
                                <li>
                                    <a class="mail-work" href="#">Work</a>
                                </li>
                                <li>
                                    <a class="mail-design" href="#">Design</a>
                                </li>
                                <li>
                                    <a class="mail-family" href="#">Family</a>
                                </li>
                                <li>
                                    <a class="mail-friends" href="#">Friends</a>
                                </li>
                                <li>
                                    <a class="mail-office" href="#">Office</a>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="col-lg-12 col-xl-9">
                        <div class="mail-body">
                            <div class="mail-body-content email-read">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Here You Have New Opportunity...</h5>
                                        <h6 class="f-right">08:23 AM</h6>
                                    </div>
                                    <div class="card-block">
                                        <div class="media m-b-20">
                                            <div class="media-left photo-table">
                                                <a href="#">
                                                    <img class="media-object img-radius"
                                                         src="{{ asset("adminity/images/avatar-3.jpg") }}"
                                                         alt="E-mail User">
                                                </a>
                                            </div>
                                            <div class="media-body photo-contant">
                                                <a href="#">
                                                    <h6 class="user-name txt-primary">John Doe</h6>
                                                </a>
                                                <a class="user-mail txt-muted" href="#">
                                                    <h6>From:<span class="__cf_email__"
                                                                   data-cfemail="e9838681878d868cd8dcdddaa98e84888085c78a8684">[email&#160;protected]</span>
                                                    </h6>
                                                </a>
                                                <div>
                                                    <h6 class="email-welcome-txt">Hello Dear...</h6>
                                                    <p class="email-content">
                                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
                                                        commodo ligula eget dolor. Aenean massa. Cum sociis natoque
                                                        penatibus et magnis dis parturient montes, nascetur ridiculus
                                                        mus. Donec quam felis, ultricies nec, pellentesque eu, pretium
                                                        quis, sem. Nulla consequat massa quis enim. Donec pede justo,
                                                        fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo,
                                                        rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
                                                        felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.
                                                        Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.
                                                        Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac,
                                                        enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,
                                                        tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque
                                                        rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue.
                                                        Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam
                                                        rhoncus.
                                                    </p>
                                                </div>
                                                <div class="m-t-15">
                                                    <i class="icofont icofont-clip f-20 m-r-10"></i>Attachments
                                                    <b>(3)</b>
                                                    <div class="row mail-img">
                                                        <div class="col-sm-4 col-md-2 col-xs-12">
                                                            <a href="#"><img
                                                                    class="card-img-top img-fluid img-thumbnail"
                                                                    src="{{ asset("adminity/images/card-block/card1.jpg") }}"
                                                                    alt="Card image cap"></a>
                                                        </div>
                                                        <div class="col-sm-4 col-md-2 col-xs-12">
                                                            <a href="#"><img
                                                                    class="card-img-top img-fluid img-thumbnail"
                                                                    src="{{ asset("adminity/images/card-block/card2.jpg") }}"
                                                                    alt="Card image cap"></a>
                                                        </div>
                                                        <div class="col-sm-4 col-md-2 col-xs-12">
                                                            <a href="#"><img
                                                                    class="card-img-top img-fluid img-thumbnail"
                                                                    src="{{ asset("adminity/images/card-block/card13.jpg") }}"
                                                                    alt="Card image cap"></a>
                                                        </div>
                                                    </div>
                                                    <textarea class="form-control m-t-30 col-xs-12 email-textarea"
                                                              id="exampleTextarea-1" placeholder="Reply Your Thoughts"
                                                              rows="4"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

