@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/fontawesome-stars.css") }}">
@endsection

@section("content")
    <div class="page-body">
        <div class="card">

            <div class="card-block email-card">
                <div class="row">
                    <div class="col-lg-12 col-xl-3">
                        <div class="user-head row">
                            <div class="user-face">
                                <img class="img-fluid" src="{{ asset("adminity/images/logo.png") }}" alt="Theme-Logo"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-9">
                        <div class="mail-box-head row">
                            <div class="col-md-12">
                                <form class="f-right">
                                    <div class="right-icon-control">
                                        <input type="text" class="form-control  search-text" placeholder="Search Friend"
                                               id="search-friends-2">
                                        <div class="form-icon">
                                            <i class="icofont icofont-search"></i>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-12 col-xl-3">
                        <div class="user-body">
                            <div class="p-20 text-center">
                            </div>
                            <ul class="page-list nav nav-tabs flex-column">
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-inbox"></i> Inbox
                                        <span class="label label-primary f-right">6</span>
                                    </a>
                                </li>
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-star"></i> Starred
                                    </a>
                                </li>
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-file-text"></i> Drafts
                                    </a>
                                </li>
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-paper-plane"></i> Sent Mail
                                    </a>
                                </li>
                                <li class="nav-item mail-section">
                                    <a class="nav-link" href="#">
                                        <i class="icofont icofont-ui-delete"></i> Trash
                                        <span class="label label-info f-right">30</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="p-20 label-list">
                                <li>
                                    <h5>Labels</h5>
                                </li>
                                <li>
                                    <a class="mail-work" href="#">Work</a>
                                </li>
                                <li>
                                    <a class="mail-design" href="#">Design</a>
                                </li>
                                <li>
                                    <a class="mail-family" href="#">Family</a>
                                </li>
                                <li>
                                    <a class="mail-friends" href="#">Friends</a>
                                </li>
                                <li>
                                    <a class="mail-office" href="#">Office</a>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="col-lg-12 col-xl-9">
                        <div class="mail-body">
                            <div class="mail-body">
                                <div class="mail-body-content">
                                    <form>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="To">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="email" class="form-control" placeholder="Cc">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="email" class="form-control" placeholder="Bcc">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Subject">
                                        </div>
                                        <textarea name="name">Put your things hear</textarea>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/pages/wysiwyg-editor/js/tinymce.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/wysiwyg-editor/wysiwyg-editor.js") }}" type="text/javascript"></script>
@endsection

