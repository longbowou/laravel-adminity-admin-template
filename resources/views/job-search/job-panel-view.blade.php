@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/select2/css/select2.min.css") }}"/>
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Job Search - Panel List</h4>
                        <span>Here you got your job details</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Job Search</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Job Panel View</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12 col-xl-9">

                <div class="card">
                    <div class="card-header">
                        <h5>Open Position</h5>
                    </div>
                    <div class="card-block">
                        <div class="job-cards">
                            <div class="media">
                                <a class="media-left media-middle" href="#">
                                    <img class="media-object m-r-10 m-l-10"
                                         src="{{ asset("adminity/images/browser/chrome.png") }}"
                                         alt="Generic placeholder image">
                                </a>
                                <div class="media-body">
                                    <div class="company-name m-b-10">
                                        <p>Chrome</p>
                                        <i class="text-muted f-14">December 16, 2017</i></div>
                                    <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the industry's standard dummy text
                                        ever since the 1500s, when an unknown printer took a galley of type and
                                        scrambled it to make a type specimen book.</p>
                                </div>
                                <div class="media-right">
                                    <div class="label-main">
                                        <label class="label bg-primary">New</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="job-cards">
                            <div class="media">
                                <a class="media-left media-middle">
                                    <img class="media-object m-r-10 m-l-10"
                                         src="{{ asset("adminity/images/browser/firefox.png") }}"
                                         alt="Generic placeholder image">
                                </a>
                                <div class="media-body">
                                    <div class="company-name m-b-10">
                                        <p>Firefox</p>
                                        <i class="text-muted f-14">May 1, 2012</i></div>
                                    <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the industry's standard dummy text
                                        ever since the 1500s, when an unknown printer took a galley of type and
                                        scrambled it to make a type specimen book.</p>
                                </div>
                                <div class="media-right">
                                    <div class="label-main">
                                        <label class="label bg-primary">New</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="job-cards">
                            <div class="media">
                                <a class="media-left media-middle" href="#">
                                    <img class="media-object m-r-10 m-l-10" src="{{ asset("adminity/images/browser/ie.png") }}"
                                         alt="Generic placeholder image">
                                </a>
                                <div class="media-body">
                                    <div class="company-name m-b-10">
                                        <p>Internet Explorer</p>
                                        <i class="text-muted f-14">January 10, 2016</i></div>
                                    <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the industry's standard dummy text
                                        ever since the 1500s, when an unknown printer took a galley of type and
                                        scrambled it to make a type specimen book.</p>
                                </div>
                                <div class="media-right">
                                    <div class="label-main">
                                        <label class="label bg-primary">New</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="job-cards">
                            <div class="media">
                                <a class="media-left media-middle">
                                    <img class="media-object m-r-10 m-l-10"
                                         src="{{ asset("adminity/images/browser/opera.png") }}" alt="Generic placeholder image">
                                </a>
                                <div class="media-body">
                                    <div class="company-name m-b-10">
                                        <p>Opera</p>
                                        <i class="text-muted f-14">July 16, 2014</i></div>
                                    <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the industry's standard dummy text
                                        ever since the 1500s, when an unknown printer took a galley of type and
                                        scrambled it to make a type specimen book.</p>
                                </div>
                                <div class="media-right">
                                    <div class="label-main">
                                        <label class="label bg-primary">New</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="job-cards">
                            <div class="media">
                                <a class="media-left media-middle">
                                    <img class="media-object m-r-10 m-l-10"
                                         src="{{ asset("adminity/images/browser/safari.png") }}"
                                         alt="Generic placeholder image">
                                </a>
                                <div class="media-body">
                                    <div class="company-name m-b-10">
                                        <p>Safari</p>
                                        <i class="text-muted f-14">July 16, 2014</i></div>
                                    <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the industry's standard dummy text
                                        ever since the 1500s, when an unknown printer took a galley of type and
                                        scrambled it to make a type specimen book.</p>
                                </div>
                                <div class="media-right">
                                    <div class="label-main">
                                        <label class="label bg-primary">New</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <nav aria-label="...">
                    <ul class="pagination justify-content-center">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item active">
                            <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>

            </div>
            <div class="col-lg-12 col-xl-3">

                <div class="card">
                    <div class="card-header">
                        <h5><i class="icofont icofont-filter m-r-5"></i>Filter</h5>
                    </div>
                    <div class="card-block">
                        <form action="#">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Job-title">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Location">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <select class="form-control">
                                        <option>Select Job Type</option>
                                        <option>Full Time</option>
                                        <option>Part Time</option>
                                        <option>Remote</option>
                                    </select>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icofont icofont-job-search m-r-5"></i> Job Find
                                </button>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="card job-right-header">
                    <div class="card-header">
                        <h5>Location</h5>
                        <div class="card-header-right">
                            <label class="label label-danger">Add</label>
                        </div>
                    </div>
                    <div class="card-block">
                        <form action="#">
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>Amsterdam, North Holland Province, Netherlands</div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>Koog aan de Zaan, North Holland Province, Netherlands</div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>Amsterdam Binnenstad en Oostelijk Havengebied, North Holland Province,
                                    Netherlands
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>Hoofddorp, North Holland Province, Netherlands</div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>Alkmaar, North Holland Province, Netherlands</div>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="card job-right-header">
                    <div class="card-header">
                        <h5>Job Title</h5>
                        <div class="card-header-right">
                            <label class="label label-danger">Add</label>
                        </div>
                    </div>
                    <div class="card-block">
                        <form action="#">
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Developer
                                    <span class="text-muted">(30)</span>
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Front end designer
                                    <span class="text-muted">(48)</span>
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    UX designer
                                    <span class="text-muted">(37)</span>
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Software engineer
                                    <span class="text-muted">(57)</span>
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    PHP developer
                                    <span class="text-muted">(60)</span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="card job-right-header">
                    <div class="card-header">
                        <h5>Specific Skills</h5>
                        <div class="card-header-right">
                            <label class="label label-danger">Add</label>
                        </div>
                    </div>
                    <div class="card-block">
                        <form action="#">
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    HTML / CSS / SCSS
                                    <span class="text-muted">(30)</span>
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Javascript
                                    <span class="text-muted">(48)</span>
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Jquery
                                    <span class="text-muted">(37)</span>
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
 </span>
                                </label>
                                <div>
                                    Angular JS
                                    <span class="text-muted">(57)</span>
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Node js
                                    <span class="text-muted">(60)</span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="card job-right-header">
                    <div class="card-header">
                        <h5>Date Posted</h5>
                        <div class="card-header-right">
                            <label class="label label-danger">Add</label>
                        </div>
                    </div>
                    <div class="card-block">
                        <form action="#">
                            <div class="form-radio">
                                <div class="radio radiofill radio-inline">
                                    <label>
                                        <input type="radio" name="radio" checked="checked">
                                        <i class="helper"></i> Today
                                        <span class="text-muted">(30)</span>
                                    </label>
                                </div>
                                <div class="radio radiofill radio-inline">
                                    <label>
                                        <input type="radio" name="radio">
                                        <i class="helper"></i> Yesterday
                                        <span class="text-muted">(85)</span>
                                    </label>
                                </div>
                                <div class="radio radiofill radio-inline">
                                    <label>
                                        <input type="radio" name="radio">
                                        <i class="helper"></i> Last-week
                                        <span class="text-muted">(184)</span>
                                    </label>
                                </div>
                                <div class="radio radiofill radio-inline">
                                    <label>
                                        <input type="radio" name="radio">
                                        <i class="helper"></i> Last month
                                        <span class="text-muted">(195)</span>
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="card job-right-header">
                    <div class="card-header">
                        <h5>Company</h5>
                        <div class="card-header-right">
                            <label class="label label-danger">Add</label>
                        </div>
                    </div>
                    <div class="card-block">
                        <form action="#">
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Phoenixcoded
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Amazon
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Flipkart
                                </div>
                            </div>
                            <div class="checkbox-fade fade-in-primary">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
</span>
                                </label>
                                <div>
                                    Snapdeal
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="card job-right-header">
                    <div class="card-header">
                        <h5>Recent Searches</h5>
                        <div class="card-header-right">
                            <label class="label label-danger">Add</label>
                        </div>
                    </div>
                    <div class="card-block">
                        <form action="#">
                            <div>
                                Senior Web designer
                                <p>Amsterdam</p>
                            </div>
                            <div>
                                PHP Devloper
                                <p>Amsterdam</p>
                            </div>
                            <div>
                                Fresher UI designer
                                <p>Amsterdam</p>
                            </div>
                            <div>
                                Wordpress devloper
                                <p>Amsterdam</p>
                            </div>
                            <div>
                                Opencart devloper
                                <p>Amsterdam</p>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript"
            src="{{ asset("adminity/components/select2/js/select2.full.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/js/jquery.quicksearch.js") }}"></script>
@endsection

