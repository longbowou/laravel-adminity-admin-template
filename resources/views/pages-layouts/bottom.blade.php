@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/prism/prism.css") }}">
@endsection

@section("body-class", "menu-bottom")

@section("content")
    <div class="page-header horizontal-layout-icon">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Bottom Menu</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Page Layouts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Bottom Menu</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="page-body">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-block">
<span>
Bottom Menu layout is useful for those users who wants menu in bottom of the page.
</span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>CSS Class</h5>
                    </div>
                    <div class="card-block">
                        <span>To use static layout for your project add <code><strong>.menu-bottom</strong></code> class in <code>body</code> tag.</span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>HTML Markup</h5>
                    </div>
                    <div class="card-block">
                        <p>Use the following code to use <strong>Bottom Menu</strong> in
                            vertical.</p>
                        <h6 class="m-t-20 f-w-600">Usage:</h6>
                        <pre><code class="language-markup">&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
    &lt;head&gt;&lt;/head&gt;
    &lt;body class="menu-bottom"&gt;
    &lt;div class="card"&gt;
      &lt;div class="card-header"&gt;
        &lt;h5&gt;Hello card&lt;/h5&gt;
        &lt;span&gt; lorem ipsum dolor sit amet, consectetur adipisicing elit&lt;/span&gt;
        &lt;div class="card-header-right"&gt;
            &lt;i class="icofont icofont-rounded-down"&gt;&lt;/i&gt;
            &lt;i class="icofont icofont-refresh"&gt;&lt;/i&gt;
            &lt;i class="icofont icofont-close-circled"&gt;&lt;/i&gt;
        &lt;/div&gt;
      &lt;/div&gt;

      &lt;div class="card-block"&gt;
        &lt;p&gt;
             Enjoy flat-able With Bottom-Menu Layout
        &lt;/p&gt;
      &lt;/div&gt;
    &lt;/div&gt;
&lt;/body&gt;
&lt;/html&gt;


</code> </pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>Sample Block</h5>
                    </div>
                    <div class="card-block">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                            industry. Lorem Ipsum has been the industry's standard dummy
                            text ever since the 1500s, when an unknown printer took a galley
                            of type and scrambled it to make a type specimen book. It has
                            survived not only five centuries, but also the leap into
                            electronic typesetting, remaining essentially unchanged. It was
                            popularised in the 1960s with the release of Letraset sheets
                            containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of
                            Lorem Ipsum.</p>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random
                            text. It has roots in a piece of classical Latin literature from
                            45 BC, making it over 2000 years old. Richard McClintock, a
                            Latin professor at Hampden-Sydney College in Virginia, looked up
                            one of the more obscure Latin words, consectetur, from a Lorem
                            Ipsum passage, and going through the cites of the word in
                            classical literature, discovered the undoubtable source. Lorem
                            Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus
                            Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero,
                            written in 45 BC. This book is a treatise on the theory of
                            ethics, very popular during the Renaissance. The first line of
                            Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line
                            in section 1.10.32. The standard chunk of Lorem Ipsum used since
                            the 1500s is reproduced below for those interested. Sections
                            1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by
                            Cicero are also reproduced in their exact original form,
                            accompanied by English versions from the 1914 translation by H.
                            Rackham.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/pages/prism/custom-prism.js") }}"></script>
@endsection
