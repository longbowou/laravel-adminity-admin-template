@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/pages/prism/prism.css") }}">
@endsection

@section("content")
    <div class="page-header horizontal-layout-icon">
        <div class="row align-items-end">
            <div class="col-sm-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Box-layout</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Page Layouts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Box-Layout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-block">
                        <span>Box-layout Menu layout is useful for those users who wants container or box-view of theme.</span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>JS Option</h5>
                    </div>
                    <div class="card-block">
                        <span>To use Compact Menu for your project add <code><strong>verticalMenulayout: 'box',</strong></code> class in js.</span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>HTML Markup</h5>
                    </div>
                    <div class="card-block">
                        <p>Use the following code to use <strong>Box-Layout Menu</strong> .</p>
                        <h6 class="m-t-20 f-w-600">Usage:</h6>
                        <pre>                                                        <code class="language-markup">
                                                            $( document ).ready(function() {
                                                                $( "#pcoded" ).pcodedmenu({
                                                                    verticalMenulayout: 'box',
                                                                });
                                                            });
                                                        </code>
                                                    </pre>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("adminity/js/menu/box-layout.js") }}"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/prism/custom-prism.js") }}"></script>
@endsection
