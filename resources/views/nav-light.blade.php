@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Navbar-Light</h4>
                        <span>Light varients of navbar</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="icofont icofont-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Navbar-Light</a>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body navbar-page">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Default Navbar</h5>
                    </div>
                    <div class="card-block">
                        <p class="m-b-10">Add <code>header-theme="theme1"</code> attribute in <code>.navbar-header</code> class or</p>
                        <p class="m-b-10">Add <code>logo-theme="theme1"</code> attribute in <code>.navbar-logo</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav1.jpg") }}" class="img-fluid nav-img" alt="">
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Custom Colors</h5>
                    </div>
                    <div class="card-block">
                        <h4 class="sub-title">
                            header-theme="theme1"
                        </h4>
                        <p class="m-b-10">Add <code>header-theme="theme1"</code> attribute in <code>.pcoded-header</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav1.jpg") }}" class="img-fluid nav-img" alt="">
                        <h4 class="sub-title m-t-30">
                            header-theme="theme2"
                        </h4>
                        <p class="m-b-10">Add <code>header-theme="theme2"</code> attribute in <code>.pcoded-header</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav2.jpg") }}" class="img-fluid nav-img" alt="">
                        <h4 class="sub-title m-t-30">
                            header-theme="theme3"
                        </h4>
                        <p class="m-b-10">Add <code>header-theme="theme3"</code> attribute in <code>.pcoded-header</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav3.jpg") }}" class="img-fluid nav-img" alt="">
                        <h4 class="sub-title m-t-30">
                            header-theme="theme4"
                        </h4>
                        <p class="m-b-10">Add <code>header-theme="theme4"</code> attribute in <code>.pcoded-header</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav4.jpg") }}" class="img-fluid nav-img" alt="">
                        <h4 class="sub-title m-t-30">
                            header-theme="theme5"
                        </h4>
                        <p class="m-b-10">Add <code>header-theme="theme5"</code> attribute in <code>.pcoded-header</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav5.jpg") }}" class="img-fluid nav-img" alt="">
                        <h4 class="sub-title m-t-30">
                            header-theme="theme6"
                        </h4>
                        <p class="m-b-10">Add <code>header-theme="theme6"</code> attribute in <code>.pcoded-header</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav6.jpg") }}" class="img-fluid nav-img" alt="">
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h5>Custom Brand Colors</h5>
                    </div>
                    <div class="card-block">
                        <h4 class="sub-title">
                            logo-theme="theme1"
                        </h4>
                        <p class="m-b-10">Add <code>logo-theme="theme1"</code> attribute in <code>.navbar-logo</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav1.jpg") }}" class="img-fluid nav-img" alt="">
                        <h4 class="sub-title m-t-30">
                            logo-theme="theme2"
                        </h4>
                        <p class="m-b-10">Add <code>logo-theme="theme2"</code> attribute in <code>.navbar-logo</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav7.jpg") }}" class="img-fluid nav-img" alt="">
                        <h4 class="sub-title m-t-30">
                            logo-theme="theme3"
                        </h4>
                        <p class="m-b-10">Add <code>logo-theme="theme3"</code> attribute in <code>.navbar-logo</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav8.jpg") }}" class="img-fluid nav-img" alt="">
                        <h4 class="sub-title m-t-30">
                            logo-theme="theme4"
                        </h4>
                        <p class="m-b-10">Add <code>logo-theme="theme4"</code> attribute in <code>.navbar-logo</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav9.jpg") }}" class="img-fluid nav-img" alt="">
                        <h4 class="sub-title m-t-30">
                            logo-theme="theme5"
                        </h4>
                        <p class="m-b-10">Add <code>logo-theme="theme5"</code> attribute in <code>.navbar-logo</code> class</p>
                        <img src="{{ asset("adminity/images/navbar/nav10.jpg") }}" class="img-fluid nav-img" alt="">
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

