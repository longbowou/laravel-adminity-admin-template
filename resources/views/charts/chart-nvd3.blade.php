@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/nvd3/css/nv.d3.css") }}" type="text/css"
          media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>NVD Chart</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Charts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">NVD Chart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">

            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Line chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="linechart" class="nvd-chart"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Discrete bar chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="barchart" class="nvd-chart"></div>
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Stacked/grouped multi-bar chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="stackedchart" class="nvd-chart"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Scatter chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="scatterchart" class="nvd-chart"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Regular pie chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="piechart" class="nvd-chart"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Donut chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="donutchart" class="nvd-chart w-100"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/d3/js/d3.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/nvd3/js/nv.d3.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/nv-chart/js/stream_layers.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/chart/nvd3/chart-nvd3.js") }}"></script>

@endsection
