@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/pages/chart/radial/css/radial.css") }}" type="text/css"
          media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Radial Chart</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Charts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Radial Chart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Radial variants</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div data-label="10%" class="radial-bar radial-bar-10 radial-bar-lg radial-bar-default"></div>
                        <div data-label="20%" class="radial-bar radial-bar-20 radial-bar-lg "></div>
                        <div data-label="30%" class="radial-bar radial-bar-30 radial-bar-lg radial-bar-success"></div>
                        <div data-label="40%" class="radial-bar radial-bar-40 radial-bar-lg .radial-bar-info"></div>
                        <div data-label="50%" class="radial-bar radial-bar-50 radial-bar-lg radial-bar-danger"></div>
                        <div data-label="60%" class="radial-bar radial-bar-60 radial-bar-lg radial-bar-warning"></div>
                        <div data-label="70%" class="radial-bar radial-bar-70 radial-bar-lg radial-bar-inverse"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Radial sizes</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div data-label="50%" class="radial-bar radial-bar-50 radial-bar-lg "></div>
                        <div data-label="40%" class="radial-bar radial-bar-40 radial-bar-sm"></div>
                        <div data-label="30%" class="radial-bar radial-bar-30 radial-bar-xs"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Radial with images</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div data-label="50%" class="radial-bar radial-bar-50 radial-bar-lg radial-bar-danger">
                            <img src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="User-Image">
                        </div>
                        <div data-label="40%" class="radial-bar radial-bar-40 radial-bar-sm radial-bar-warning">
                            <img src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="User-Image">
                        </div>
                        <div data-label="30%" class="radial-bar radial-bar-30 radial-bar-xs radial-bar-success">
                            <img src="{{ asset("adminity/images/avatar-2.jpg") }}" alt="User-Image">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Radial with toopltip</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div data-label="50%" data-toggle="tooltip" class="radial-bar radial-bar-50 radial-bar-lg " data-title="50%"></div>
                        <div data-label="40%" data-toggle="tooltip" class="radial-bar radial-bar-40 radial-bar-lg" data-title="40%"></div>
                        <div data-label="30%" data-toggle="tooltip" class="radial-bar radial-bar-30 radial-bar-lg" data-title="30%"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/chartist/js/chartist.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/chartlist/js/chartist-plugin-threshold.js") }}"
            type="text/javascript"></script>
@endsection
