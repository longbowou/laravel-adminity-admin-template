@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/chartist/css/chartist.css") }}" type="text/css"
          media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Peity Chart</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Charts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Peity Chart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">

            <div class="col-md-12 col-lg-4 peity-chart">
                <div class="card">
                    <div class="card-header">
                        <h5>Updating chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <span class="updating-chart">5,3,6,4,2,10,2,3,9,1,2,8</span>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4 peity-chart">
                <div class="card">
                    <div class="card-header">
                        <h5>Updating chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <span class="updating-chart1">5,3,9,6,5,9,7,3,5,2</span>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4 peity-chart">
                <div class="card">
                    <div class="card-header">
                        <h5>Line chart 2</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <span class="updating-chart2">5,3,2,-1,-3,-2,2,3,5,2</span>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4 peity-chart">
                <div class="card">
                    <div class="card-header">
                        <h5>Line chart 2</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <span class="updating-chart3">0,-3,-6,-4,-5,-4,-7,-3,-5,-2</span>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4 peity-chart">
                <div class="card">
                    <div class="card-header">
                        <h5>Line chart 2</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <span class="bar-colours-2">0,3,6,4,7,3,5,2,2</span>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4 peity-chart">
                <div class="card">
                    <div class="card-header">
                        <h5>Bar charts 2</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <span class="bar-colours-1">0,-3,-6,-4,5,-4,-5,-2,2</span>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4 peity-bar-chart">
                <div class="card">
                    <div class="card-header">
                        <h5>Custom chart 1</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <p class="data-attributes">
                            <span data-peity='{ "fill": ["rgb(96, 219, 224)", "#eeeeee"],    "innerRadius": 10, "radius": 40 }'>1/7</span>
                            <span data-peity='{ "fill": ["rgb(95, 190, 170)", "#eeeeee"], "innerRadius": 14, "radius": 36 }'>2/7</span>
                            <span data-peity='{ "fill": ["rgb(93, 156, 236)", "#eeeeee"], "innerRadius": 16, "radius": 32 }'>3/7</span>
                            <span data-peity='{ "fill": ["rgba(254, 198, 6, 0.95)", "#eeeeee"],  "innerRadius": 18, "radius": 28 }'>4/7</span>
                            <span data-peity='{ "fill": ["rgba(249, 151, 75, 0.93)", "#eeeeee"],   "innerRadius": 20, "radius": 24 }'>5/7</span>
                            <span data-peity='{ "fill": ["rgba(96, 100, 109, 0.95)", "#eeeeee"], "innerRadius": 18, "radius": 20 }'>6/7</span>
                            <span data-peity='{ "fill": ["rgba(44, 46, 49, 0.92)", "#eeeeee"], "innerRadius": 15, "radius": 16 }'>7/7</span>
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-4 peity-bar-chart">
                <div class="card">
                    <div class="card-header">
                        <h5>Custom chart 2</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <span class="pie_1">1/5</span>
                        <span class="pie_2">226/360</span>
                        <span class="pie_3">0.52/1.561</span>
                        <span class="pie_4">1,4</span>
                        <span class="pie_5">226,134</span>
                        <span class="pie_6">0.52,1.041</span>
                        <span class="pie_7">1,2,3,2,2</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/peity/js/jquery.peity.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/chart/peity/peity-custom-chart.js") }}"></script>
@endsection
