@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/chartist/css/chartist.css") }}" type="text/css"
          media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>List Chart</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Charts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">List Chart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">

            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Bi-polar line chart with area only</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="ct-chart ct-perfect-fourth"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Series overrides </h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="ct-chart-overrides ct-perfect-fourth"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Horizontal bar chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="ct-chart-horizontal ct-perfect-fourth"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Add peak circles using the draw events </h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="ct-chart-add-pack ct-perfect-fourth"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Threshold plugin for chartist</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="ct-chart1 ct-perfect-fourth"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Advanced smil animations </h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="ct-chart2 ct-perfect-fourth"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Gauge chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="ct-chart3 ct-perfect-fourth"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Animating a donut </h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div class="ct-chart-animating ct-perfect-fourth"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/chartist/js/chartist.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/chartlist/js/chartist-plugin-threshold.js") }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/chart/chartlist/chartlist-custom.js") }}"></script>
@endsection
