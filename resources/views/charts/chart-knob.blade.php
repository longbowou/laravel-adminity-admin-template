@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/chartist/css/chartist.css") }}" type="text/css"
          media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Knob Chart</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Charts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Knob Chart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">

            <div class="col-lg-6 col-xl-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Overloaded 'draw' method</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block text-center">
                        <input type="text" class="dial" value="24" data-width="200" data-height="200"
                               data-fgColor="#1abc9c" data-skin="tron" data-thickness=".1" data-angleOffset="180">
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-xl-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Angle offset and arc</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block text-center">
                        <input type="text" class="dial" value="35" data-width="200" data-height="200"
                               data-fgColor="#FF9F55" data-angleOffset="-125" data-angleArc="250"
                               data-rotation="anticlockwise">
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-xl-4">
                <div class="card">
                    <div class="card-header">
                        <h5>'Cursor' mode</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block text-center">
                        <input type="text" class="dial" value="70" data-width="200" data-height="200" data-cursor="true"
                               data-thickness=".1" data-fgColor="#9b59b6">
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-xl-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Disable display input</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block text-center">
                        <input type="text" class="dial" value="50" data-width="200" data-height="200"
                               data-linecap="round" data-displayInput="false" data-fgColor="#40c4ff">
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-xl-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Display previous value </h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block text-center">
                        <input type="text" class="dial" value="32" data-width="200" data-height="200"
                               data-linecap="round" data-displayprevious="true" data-displayInput="true"
                               data-fgColor="#34495e">
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-xl-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Read only and size </h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block text-center">
                        <input type="text" class="dial" value="48" data-width="100" data-height="200"
                               data-linecap="round" data-displayprevious="true" data-displayInput="true"
                               data-readonly="true" data-fgColor="#4ECDC4">
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/pages/chart/knob/jquery.knob.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/chart/knob/knob-custom-chart.js") }}"></script>
@endsection
