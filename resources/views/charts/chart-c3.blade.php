@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/c3/css/c3.css") }}" type="text/css" media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>C3 Chart</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route('dashboard') }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Charts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">C3 Chart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Stacked Area Chart</h5>
                    </div>
                    <div class="card-block">
                        <div id="chart"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>chart bar stacked</h5>
                    </div>
                    <div class="card-block">
                        <div id="chart1"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Gauge chart</h5>
                    </div>
                    <div class="card-block">
                        <div id="chart2"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Pie Chart</h5>
                    </div>
                    <div class="card-block">
                        <div id="chart3"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Combination Chart</h5>
                    </div>
                    <div class="card-block">
                        <div id="chart4"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Step Chart</h5>
                    </div>
                    <div class="card-block">
                        <div id="chart5"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Scatter Plot Chart</h5>
                    </div>
                    <div class="card-block">
                        <div id="chart6"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/d3/js/d3.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/c3/js/c3.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/c3/c3-custom-chart.js") }}" type="text/javascript"></script>
@endsection
