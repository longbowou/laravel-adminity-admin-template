@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/chartist/css/chartist.css") }}" type="text/css"
          media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Rickshaw Chart</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Charts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Rickshaw Chart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">

            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Stacked Bars</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="stackedchart"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Multiple Bars</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="barchart"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Area chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="areachart"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Multiple Area</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="multipleAreachart"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Interactive Hover Details</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="interactivehover"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/d3/js/d3.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/rickshaw/js/rickshaw.js") }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/chart/rickshow/rickshow-custom-.js") }}"></script>
@endsection
