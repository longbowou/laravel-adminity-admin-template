@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/chartist/css/chartist.css") }}" type="text/css"
          media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Morris Chart</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Charts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Morris Chart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">

            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Site visit chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="morris-site-visit"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Bar chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Extra area chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="morris-extra-area"></div>
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Area chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="area-example"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Line chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="line-example"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Donut chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="donut-example"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/chartist/js/chartist.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/chartlist/js/chartist-plugin-threshold.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/raphael/js/raphael.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/components/morris.js/js/morris.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/morris/morris-custom-chart.js") }}"
            type="text/javascript"></script>
@endsection
