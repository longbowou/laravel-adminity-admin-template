@extends("layouts.app")

@section("style")
    <link rel="stylesheet" href="{{ asset("adminity/components/chartist/css/chartist.css") }}" type="text/css"
          media="all">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>chart float</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Charts</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Float Chart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">

            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Categories chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="placeholder" class="demo-placeholder" style="height:300px;"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Stracking chart</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="placeholder1" class="demo-placeholder" style="height:300px;"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Pie chart ( without legend )</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="placeholder2" class="demo-placeholder" style="height: 400px;"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Image plots</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="placeholder3" class="demo-placeholder" style="height:300px;"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Series types</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="seriestypes" class="demo-placeholder" style="height:400px;"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Real-time update</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="realtimeupdate" class="demo-placeholder" style="height:400px;"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Percentiles</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-block">
                        <div id="percentiles" class="demo-placeholder" style="height:400px;"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/chartist/js/chartist.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/chartlist/js/chartist-plugin-threshold.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/float/jquery.flot.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/float/jquery.flot.categories.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/chart/float/jquery.flot.pie.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/chart/float/float-chart-custom.js") }}"></script>
@endsection
