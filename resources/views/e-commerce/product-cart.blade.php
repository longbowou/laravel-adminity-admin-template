@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("adminity/components/jquery.steps/css/jquery.steps.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Shopping Cart</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">E-Commerce</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Shopping cart</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>Shopping cart</h5>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="wizard">
                                    <section>
                                        <form class="wizard-form" id="basic-forms" action="#">

                                            <h3> Your Shopping Cart </h3>
                                            <fieldset>
                                                <table id="e-product-list"
                                                       class="table table-responsive table-striped dt-responsive nowrap dataTable no-footer dtr-inline cart-page"
                                                       role="grid" style="width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th class="sorting_disabled" rowspan="1" colspan="1"
                                                            style="width: 125px;">Image
                                                        </th>
                                                        <th class="sorting_disabled" rowspan="1" colspan="1"
                                                            style="width: 1023px;">Product Name
                                                        </th>
                                                        <th class="sorting_disabled" rowspan="1" colspan="1"
                                                            style="width: 153px;">Amount
                                                        </th>
                                                        <th class="sorting_disabled" rowspan="1" colspan="1"
                                                            style="width: 100px;">Qty
                                                        </th>
                                                        <th class="sorting_disabled" rowspan="1" colspan="1"
                                                            style="width: 134px;text-align:center">Action
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="odd">
                                                        <td class="pro-list-img" tabindex="0">
                                                            <img
                                                                src="{{ asset("adminity/images/product-list/pro-l1.png") }}"
                                                                class="img-fluid" alt="tbl">
                                                        </td>
                                                        <td class="pro-name">
                                                            <h6>Frock Designs</h6>
                                                            <span>Lorem ipsum dolor sit consec te imperdiet iaculis ipsum..</span>
                                                        </td>
                                                        <td>$456</td>
                                                        <td>
                                                            <input type="text" class="form-control" value="2">
                                                        </td>
                                                        <td class="action-icon text-center">
                                                            <a href="#!" class="text-muted" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Delete"><i
                                                                    class="icofont icofont-delete-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr class="even">
                                                        <td class="pro-list-img" tabindex="0">
                                                            <img
                                                                src="{{ asset("adminity/images/product-list/pro-l6.png") }}"
                                                                class="img-fluid" alt="tbl">
                                                        </td>
                                                        <td class="pro-name">
                                                            <h6> Style Tops </h6>
                                                            <span>Interchargebla lens Digital Camera with APS-C-X Trans CMOS Sens</span>
                                                        </td>
                                                        <td>$689</td>
                                                        <td>
                                                            <input type="text" class="form-control" value="1">
                                                        </td>
                                                        <td class="action-icon text-center">
                                                            <a href="#!" class="text-muted" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Delete"><i
                                                                    class="icofont icofont-delete-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr class="odd">
                                                        <td class="pro-list-img" tabindex="0">
                                                            <img
                                                                src="{{ asset("adminity/images/product-list/pro-l2.png") }}"
                                                                class="img-fluid" alt="tbl">
                                                        </td>
                                                        <td class="pro-name">
                                                            <h6> Kurta Women </h6>
                                                            <span>Lorem ipsum dolor sit consec te imperdiet iaculis ipsum..</span>
                                                        </td>
                                                        <td>$755</td>
                                                        <td>
                                                            <input type="text" class="form-control" value="2">
                                                        </td>
                                                        <td class="action-icon text-center">
                                                            <a href="#!" class="text-muted" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Delete"><i
                                                                    class="icofont icofont-delete-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr class="even">
                                                        <td class="pro-list-img" tabindex="0">
                                                            <img
                                                                src="{{ asset("adminity/images/product-list/pro-l3.png") }}"
                                                                class="img-fluid" alt="tbl">
                                                        </td>
                                                        <td class="pro-name">
                                                            <h6> T Shirts For Women </h6>
                                                            <span>Lorem ipsum dolor sit consec te imperdiet iaculis ipsum..</span>
                                                        </td>
                                                        <td>$989</td>
                                                        <td>
                                                            <input type="text" class="form-control" value="2">
                                                        </td>
                                                        <td class="action-icon text-center">
                                                            <a href="#!" class="text-muted" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Delete"><i
                                                                    class="icofont icofont-delete-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>


                                            <h3> Delivery Details </h3>
                                            <fieldset class="bank-detail p-t-5">
                                                <div class="row justify-content-center">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="card-number" class="form-label">First
                                                                        name *</label>
                                                                    <input id="name-2" name="name" type="text"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="card-number" class="form-label">Last
                                                                        name *</label>
                                                                    <input id="surname-2" name="surname" type="text"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="card-number" class="form-label">Select
                                                                        Country</label>
                                                                    <select class="form-control required">
                                                                        <option>Select Country</option>
                                                                        <option>Gujarat</option>
                                                                        <option>Kerala</option>
                                                                        <option>Manipur</option>
                                                                        <option>Tripura</option>
                                                                        <option>Sikkim</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="address" class="form-label">Address
                                                                        *</label>
                                                                    <input id="address" name="address" type="text"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="city" class="form-label">City *</label>
                                                                    <input id="city" name="city" type="text"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="phone-2" class="form-label">Phone
                                                                        #</label>
                                                                    <input id="phone-2" name="phone" type="number"
                                                                           class="form-control phone">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="email" class="form-label">Email
                                                                        #</label>
                                                                    <input id="email" name="email" type="email"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group row">
                                                                    <label for="date" class="form-label">Date Of
                                                                        Birth</label>
                                                                    <input id="date" name="Date Of Birth" type="text"
                                                                           class="form-control date-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>


                                            <h3> Payment Details </h3>
                                            <fieldset class="bank-detail">
                                                <div class="row justify-content-center">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <select id="hello-single" class="form-control">
                                                                        <option value="">---- Select card ----</option>
                                                                        <option value="married">Visa</option>
                                                                        <option value="unmarried">Master</option>
                                                                        <option value="married">American Express
                                                                        </option>
                                                                        <option value="unmarried">Discover</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="card-number" class="form-label">Card
                                                                        number</label>
                                                                    <input id="card-number" class="form-control"
                                                                           type="text" name="card_number">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label for="expdate-month" class="form-label">Expiration
                                                                    date</label>
                                                                <div class="row gutter-xs">
                                                                    <div class="col-md-6">
                                                                        <div class="input-group">
                                                                            <select id="expdate-month"
                                                                                    class="form-control"
                                                                                    name="expdate_month">
                                                                                <option value="" selected="selected">
                                                                                    Month
                                                                                </option>
                                                                                <option value="1">01</option>
                                                                                <option value="2">02</option>
                                                                                <option value="3">03</option>
                                                                                <option value="4">04</option>
                                                                                <option value="5">05</option>
                                                                                <option value="6">06</option>
                                                                                <option value="7">07</option>
                                                                                <option value="8">08</option>
                                                                                <option value="9">09</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <select id="expdate-year"
                                                                                    class="form-control"
                                                                                    name="expdate_year">
                                                                                <option value="" selected="selected">
                                                                                    Year
                                                                                </option>
                                                                                <option value="2016">2016</option>
                                                                                <option value="2017">2017</option>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2021">2021</option>
                                                                                <option value="2022">2022</option>
                                                                                <option value="2023">2023</option>
                                                                                <option value="2024">2024</option>
                                                                                <option value="2025">2025</option>
                                                                                <option value="2026">2026</option>
                                                                                <option value="2027">2027</option>
                                                                                <option value="2028">2028</option>
                                                                                <option value="2029">2029</option>
                                                                                <option value="2030">2030</option>
                                                                                <option value="2031">2031</option>
                                                                                <option value="2032">2032</option>
                                                                                <option value="2033">2033</option>
                                                                                <option value="2034">2034</option>
                                                                                <option value="2035">2035</option>
                                                                                <option value="2036">2036</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5 offset-md-1">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="cvv2-number" class="form-label">Card
                                                                                Security Code</label>
                                                                            <input id="cvv2-number" class="form-control"
                                                                                   type="text" name="cvv2_number">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="promotional-code" class="control-label">Promotional
                                                                        code</label>
                                                                    <input id="promotional-code" class="form-control"
                                                                           type="text" name="promotional_code">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary btn-block">Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </fieldset>


                                            <h3> Confirmation </h3>
                                            <fieldset>
                                                <div class="confirmation">
                                                    <div class="text-primary m-b-20">
                                                        <div class="icon icon-check icon-5x"></div>
                                                    </div>
                                                    <div class="confirmation-content text-center">
                                                        <h3>Congratulations! Your Order is accepted.</h3>
                                                        <div class="row">
                                                            <div class="col-md-6 offset-md-3">
                                                                <p>
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting industry. Lorem Ipsum has been the
                                                                    industry's standard dummy text ever since the 1500s,
                                                                    when an unknown printer took a galley of type and
                                                                    scrambled it to make a type specimen book.
                                                                </p>
                                                                <button class="btn btn-primary m-y">Track Order</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>

                                        </form>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/components/jquery.cookie/js/jquery.cookie.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/jquery.steps/js/jquery.steps.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/components/jquery-validation/js/jquery.validate.js") }}"
            type="text/javascript"></script>

    <script src="{{ asset("adminity/js/underscore-min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/js/moment.min.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/form-validation/validate.js") }}"></script>
    <script src="{{ asset("adminity/pages/forms-wizard-validation/form-wizard.js") }}" type="text/javascript"></script>
@endsection

