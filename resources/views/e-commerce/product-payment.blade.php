@extends("layouts.app")

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>E-payment</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">E-Commerce</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">E-payment</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-4">
                <div class="card payment-card">
                    <div>
                        <i class="icofont icofont-paypal-alt"></i>
                        <h5>**** **** **** 1234</h5>
                        <div class="row m-t-10">
                            <div class="col-sm-6">
                                <strong class="m-r-5">Expiry Date :</strong>20/09/17
                            </div>
                            <div class="col-sm-6 text-right">
                                <strong class="m-r-5">Name :</strong>Airi Sawarm
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-4">
                <div class="card payment-card">
                    <div>
                        <i class="icofont icofont-visa-alt"></i>
                        <h5>**** **** **** 1567</h5>
                        <div class="row m-t-10">
                            <div class="col-sm-6">
                                <strong class="m-r-5">Expiry Date :</strong>20/09/17
                            </div>
                            <div class="col-sm-6 text-right">
                                <strong class="m-r-5">Name :</strong>Airi Sawarm
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-4">
                <div class="card payment-card">
                    <div>
                        <i class="icofont icofont-mastercard"></i>
                        <h5>**** **** **** 1897</h5>
                        <div class="row m-t-10">
                            <div class="col-sm-6">
                                <strong class="m-r-5">Expiry Date :</strong>20/09/17
                            </div>
                            <div class="col-sm-6 text-right">
                                <strong class="m-r-5">Name :</strong>Airi Sawarm
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h5>Choose Your Payment Method</h5>
            </div>
            <div class="card-block payment-tabs">
                <ul class="nav nav-tabs md-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#credit-card" role="tab">Credit Card</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#debit-card" role="tab">Debit Card</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#emi" role="tab">EMI</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#net-banking" role="tab">Net Banking</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#cod" role="tab">COD</a>
                        <div class="slide"></div>
                    </li>
                </ul>
                <div class="tab-content m-t-15">
                    <div class="tab-pane active" id="credit-card" role="tabpanel">
                        <div class="demo-container card-block">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-wrapper"></div>
                                </div>
                                <div class="col-sm-12">
                                    <form class="payment-form">
                                        <div class="form-group">
                                            <input name="number" type="tel" class="form-control"
                                                   placeholder="Card Number">
                                        </div>
                                        <div class="form-group">
                                            <input name="name" type="text" class="form-control" placeholder="Full Name">
                                        </div>
                                        <div class="form-group">
                                            <input name="expiry" type="tel" class="form-control" placeholder="MM/YY">
                                        </div>
                                        <div class="form-group">
                                            <input name="cvc" type="number" class="form-control" placeholder="CVC">
                                        </div>
                                        <div class="text-center">
                                            <a href="#!" class="btn btn-primary waves-effect waves-light">Submit</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="debit-card" role="tabpanel">
                        <div class="debitCardForm card-block p-b-0">
                            <div class="payment">
                                <form>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control"
                                                       placeholder="Type your Full Name">
                                            </div>
                                            <div class="form-group CVV">
                                                <input type="text" class="form-control" id="cvv" placeholder="CVV">
                                            </div>
                                            <div class="form-group" id="card-number-field">
                                                <input type="text" name="name" class="form-control" id="cardNumber"
                                                       placeholder="Card Number">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group" id="expiration-date">
                                                <label>Expiration Date</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <select class="form-control m-b-10">
                                                            <option>Select Month</option>
                                                            <option value="01">January</option>
                                                            <option value="02">February</option>
                                                            <option value="03">March</option>
                                                            <option value="04">April</option>
                                                            <option value="05">May</option>
                                                            <option value="06">June</option>
                                                            <option value="07">July</option>
                                                            <option value="08">August</option>
                                                            <option value="09">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <select class="form-control m-b-10">
                                                            <option>Select Year</option>
                                                            <option value="16"> 2016</option>
                                                            <option value="17"> 2017</option>
                                                            <option value="18"> 2018</option>
                                                            <option value="19"> 2019</option>
                                                            <option value="20"> 2020</option>
                                                            <option value="21"> 2021</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="debit-cards">
                                                <img src="{{ asset("adminity/images/e-payment/card/visa.jpg") }}" id="visa"
                                                     alt="visa.jpg">
                                                <img src="{{ asset("adminity/images/e-payment/card/mastercard.jpg") }}"
                                                     id="mastercard" alt="mastercard.jpg">
                                                <img src="{{ asset("adminity/images/e-payment/card/amex.jpg") }}" id="amex"
                                                     alt="amex.jpg">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <a href="#!"
                                               class="btn btn-primary waves-effect waves-light m-t-20">Submit</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="emi" role="tabpanel">
                        <div class="card-block p-b-0">
                            <div class="row">
                                <div class="col-sm-6">
                                    <select class="form-control m-b-10">
                                        <option>Select Card</option>
                                        <option>ICICI Credit Card</option>
                                        <option>AXIS Credit Card</option>
                                        <option>HSBC Credit Card</option>
                                        <option>KOTAK Credit Card</option>
                                        <option>INDUSIND Credit Card</option>
                                        <option>HDFC Credit Card</option>
                                        <option>ICICI Debit Card</option>
                                        <option>SBI Credit Card</option>
                                        <option>CITIBANK Credit Card</option>
                                        <option>AXIS Credit Card</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <select class="form-control m-b-10">
                                        <option>Select Duration</option>
                                        <option>1 month</option>
                                        <option>2 year</option>
                                        <option>5 month</option>
                                        <option>3 week</option>
                                        <option>5 year</option>
                                        <option>7 month</option>
                                    </select>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light m-t-20">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="net-banking" role="tabpanel">
                        <div class="card-block p-b-0">
                            <div class="row">
                                <div class="col-sm-6">
                                    <select class="form-control m-b-10">
                                        <option>Select Bank</option>
                                        <option>State bank of india</option>
                                        <option>Bank of baroda</option>
                                        <option>Central bank of india</option>
                                        <option>Punjab national bank</option>
                                        <option>Yes bank</option>
                                        <option>Kotak mahindra bank</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="cod" role="tabpanel">
                        <div class="card-block p-b-0">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input name="number" type="text" class="form-control" placeholder="First Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Last Name"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Address"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" class="form-control" placeholder="Pincode"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Select country</option>
                                            <option>India</option>
                                            <option>Pakistan</option>
                                            <option>Amerika</option>
                                            <option>China</option>
                                            <option>Dubai</option>
                                            <option>Nepal</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Select state</option>
                                            <option>Gujarat</option>
                                            <option>Maharastra</option>
                                            <option>Rajastan</option>
                                            <option>Maharastra</option>
                                            <option>Rajastan</option>
                                            <option>Gujarat</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control f">
                                            <option>Select city</option>
                                            <option>Surat</option>
                                            <option>Baroda</option>
                                            <option>Navsari</option>
                                            <option>Baroda</option>
                                            <option>Surat</option>
                                        </select>
                                    </div>
                                    <input type="number" class="form-control" placeholder="Mobile no."/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light m-t-20">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("script")
    <script src="{{ asset("adminity/pages/payment-card/card.js") }}" type="text/javascript"></script>
    <script src="{{ asset("adminity/pages/payment-card/jquery.payform.min.js") }}" charset="utf-8"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("adminity/pages/payment-card/e-payment.js") }}"></script>
@endsection

