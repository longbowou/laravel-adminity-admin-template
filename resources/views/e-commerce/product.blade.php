@extends("layouts.app")

@section("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset("adminity/components/jquery-bar-rating/css/fontawesome-stars.css") }}">
@endsection

@section("content")
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>Product</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route("dashboard") }}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">E-Commerce</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Product</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">

        <div class="row">
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p1.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                            <div class="p-new"><a href="#"> New </a></div>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p2.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                            <div class="p-sale">SALE</div>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p3.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p4.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p1.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p2.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                            <div class="p-sale">SALE</div>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p3.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p4.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                            <div class="p-new"><a href="#"> New </a></div>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p1.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p2.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                            <div class="p-sale">SALE</div>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p3.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                            <div class="p-new"><a href="#"> New </a></div>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <button type="button"
                                        class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button>
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="{{ asset("adminity/images/product/p4.jpg") }}" class="img-fluid o-hidden"
                                     alt="prod1.jpg">
                            </a>
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><h4>Women black Frock</h4></a>
                            <div class="m-b-10">
                                <label class="label label-success">3.5 <i class="fa fa-star"></i></label><a
                                    class="text-muted f-w-600">14 Ratings &amp; 3 Reviews</a>
                            </div>
                            <span class="prod-price"><i class="icofont icofont-cur-dollar"></i>1250 <small
                                    class="old-price"><i class="icofont icofont-cur-dollar"></i>1850</small></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

