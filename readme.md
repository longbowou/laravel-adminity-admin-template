# Laravel Adminity Admin Template
Laravel 6.0 based project with [Adminity](https://colorlib.com//polygon/adminty/default/) Premium Boostrap 4 admin template integrated.

## Screen shots
![](screen_shots/sign-in.png) 

![](screen_shots/sign-up.png) 

![](screen_shots/dashboard.png) 
